package internshipstikombali.balitourism.business;

import java.util.ArrayList;

import internshipstikombali.balitourism.DAO.AccommodationHasFacilityDAO;

/**
 * Created by Pierre on 21/06/2016.
 */
public class Accommodation {

    // Attributes
    private int idAccommodation;
    private String name;
    private String longitude;
    private String latitude;
    private String phoneNumber;
    private String website;
    private int price;
    private String description;
    private String picture;
    private int numberOfPlaces;

    // Constructors
    public Accommodation() {}
    public Accommodation(int idAccommodation, String name) {
        this.idAccommodation = idAccommodation;
        this.name = name;
    }
    public Accommodation(int idAccommodation, String name, String longitude, String latitude, String phoneNumber, String website, int price, String description, String picture, int numberOfPlaces) {
        this.idAccommodation = idAccommodation;
        this.name = name;
        this.longitude = longitude;
        this.latitude = latitude;
        this.phoneNumber = phoneNumber;
        this.website = website;
        this.price = price;
        this.description = description;
        this.picture = picture;
        this.numberOfPlaces = numberOfPlaces;
    }

    // Getters
    public int getIdAccommodation() {
        return idAccommodation;
    }

    public String getName() {
        return name;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getWebsite() {
        return website;
    }

    public int getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getPicture() {
        return picture;
    }

    public int getNumberOfPlaces() {
        return numberOfPlaces;
    }

    // Setters
    public void setIdAccommodation(int idAccommodation) {
        this.idAccommodation = idAccommodation;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public void setNumberOfPlaces(int numberOfPlaces) {
        this.numberOfPlaces = numberOfPlaces;
    }

}
