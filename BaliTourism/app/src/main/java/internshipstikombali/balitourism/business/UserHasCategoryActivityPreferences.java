package internshipstikombali.balitourism.business;

/**
 * Created by Pierre on 22/06/2016.
 */
public class UserHasCategoryActivityPreferences {

    // Attributes
    private int idUser;
    private String nameCategoryActivity;

    // Constructors
    public UserHasCategoryActivityPreferences() {
    }

    public UserHasCategoryActivityPreferences(int idUser, String nameCategoryActivity) {
        this.idUser = idUser;
        this.nameCategoryActivity = nameCategoryActivity;
    }

    // Getters
    public int getIdUser() {
        return idUser;
    }

    public String getNameCategoryActivity() {
        return nameCategoryActivity;
    }

    // Setters
    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public void setNameCategoryActivity(String nameCategoryActivity) {
        this.nameCategoryActivity = nameCategoryActivity;
    }
}
