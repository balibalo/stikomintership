package internshipstikombali.balitourism.business;

/**
 * Created by Pierre on 22/06/2016.
 */
public class Transport {

    // Attributes
    private int idTransport;
    private String name;
    private String description;
    private String picture;
    private int numberOfPlaces;
    private int needDrivingLicense;
    private int price;

    // COnstructors
    public Transport() {
    }

    public Transport(int idTransport, String name, String description, String picture, int numberOfPlaces, int needDrivingLicense, int price) {
        this.idTransport = idTransport;
        this.name = name;
        this.description = description;
        this.picture = picture;
        this.numberOfPlaces = numberOfPlaces;
        this.needDrivingLicense = needDrivingLicense;
        this.price = price;
    }

    // Getters
    public int getIdTransport() {
        return idTransport;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getPicture() {
        return picture;
    }

    public int getNumberOfPlaces() {
        return numberOfPlaces;
    }

    public int getNeedDrivingLicense() {
        return needDrivingLicense;
    }

    public int getPrice() {
        return price;
    }

    // Setters
    public void setIdTransport(int idTransport) {
        this.idTransport = idTransport;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public void setNumberOfPlaces(int numberOfPlaces) {
        this.numberOfPlaces = numberOfPlaces;
    }

    public void setNeedDrivingLicense(int needDrivingLicense) {
        this.needDrivingLicense = needDrivingLicense;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
