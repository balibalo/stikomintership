package internshipstikombali.balitourism.business;

/**
 * Created by Pierre on 22/06/2016.
 */
public class UserHasFacilityPreferences {

    // Attributes
    private int idUser;
    private String nameFacility;

    // Constructors
    public UserHasFacilityPreferences() {
    }

    public UserHasFacilityPreferences(int idUser, String nameFacility) {
        this.idUser = idUser;
        this.nameFacility = nameFacility;
    }

    // Getters
    public int getIdUser() {
        return idUser;
    }

    public String getNameFacility() {
        return nameFacility;
    }

    // Setters
    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public void setNameFacility(String nameFacility) {
        this.nameFacility = nameFacility;
    }
}
