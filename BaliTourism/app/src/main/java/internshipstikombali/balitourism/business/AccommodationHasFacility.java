package internshipstikombali.balitourism.business;

/**
 * Created by Pierre on 22/06/2016.
 */
public class AccommodationHasFacility {

    // Attributes
    private int idAccommodation;
    private String nameFacility;

    // Constructors
    public AccommodationHasFacility() {
    }

    public AccommodationHasFacility(int idAccommodation, String nameFacility) {
        this.idAccommodation = idAccommodation;
        this.nameFacility = nameFacility;
    }

    // Getters
    public int getIdAccommodation() {
        return idAccommodation;
    }

    public String getNameFacility() {
        return nameFacility;
    }

    // Setters
    public void setIdAccommodation(int idAccommodation) {
        this.idAccommodation = idAccommodation;
    }

    public void setNameFacility(String nameFacility) {
        this.nameFacility = nameFacility;
    }
}
