package internshipstikombali.balitourism.business;

/**
 * Created by Pierre on 22/06/2016.
 */
public class UserHasCategoryAccommodationPreferences {

    // Attributes
    private int idUser;
    private String nameCategoryAccommodation;

    // Constructors
    public UserHasCategoryAccommodationPreferences() {
    }

    public UserHasCategoryAccommodationPreferences(int idUser, String nameCategoryAccommodation) {
        this.idUser = idUser;
        this.nameCategoryAccommodation = nameCategoryAccommodation;
    }

    // Getters
    public int getIdUser() {
        return idUser;
    }

    public String getNameCategoryAccommodation() {
        return nameCategoryAccommodation;
    }

    // Setters
    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public void setNameCategoryAccommodation(String nameCategoryAccommodation) {
        this.nameCategoryAccommodation = nameCategoryAccommodation;
    }
}
