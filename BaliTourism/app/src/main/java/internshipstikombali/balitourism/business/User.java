package internshipstikombali.balitourism.business;

/**
 * Created by Pierre on 21/06/2016.
 */
public class User {
    // Attributes
    private int idUser;
    private int idTrip;
    private String username;
    private String encryptedPassword;
    private String emailAddress;
    private int drivingLicense;
    private int timeSpentOutside;

    // Constructors
    public User() {}
    public User(int idUser, String username, String encryptedPassword) {
        this.idUser = idUser;
        this.username = username;
        this.encryptedPassword = encryptedPassword;
    }
    public User(int idUser, int idTrip, String username, String encryptedPassword, String emailAddress, int drivingLicense, int timeSpentOutside) {
        this.idUser = idUser;
        this.idTrip = idTrip;
        this.username = username;
        this.encryptedPassword = encryptedPassword;
        this.emailAddress = emailAddress;
        this.drivingLicense = drivingLicense;
        this.timeSpentOutside = timeSpentOutside;
    }
    public User(int idUser, String username) {
        this.idUser = idUser;
        this.username = username;
    }

    public User(int idUser, int idTrip, String username) {
        this.idUser = idUser;
        this.idTrip = idTrip;
        this.username = username;
    }

    // Getters
    public int getIdUser() {
        return this.idUser;
    }
    public String getUsername() {
        return this.username;
    }
    public int getIdTrip() {
        return this.idTrip;
    }
    public String getEncryptedPassword() {
        return this.encryptedPassword;
    }
    public String getEmailAddress() {
        return this.emailAddress;
    }
    public int getDrivingLicense() {
        return this.drivingLicense;
    }
    public int getTimeSpentOutside() { return this.timeSpentOutside; }


    // Setters
    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }
    public void setIdTrip(int idTrip) {
        this.idTrip = idTrip;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    public void setDrivingLicense(int drivingLicense) {
        this.drivingLicense = drivingLicense;
    }
    public void setTimeSpentOutside(int timeSpentOutside) {
        this.timeSpentOutside = timeSpentOutside;
    }
}
