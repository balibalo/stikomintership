package internshipstikombali.balitourism.business;

/**
 * Created by Pierre on 21/06/2016.
 */
public class CategoryAccommodation {

    // Attributes
    private String nameCategoryAccommodation;
    private String description;

    // Constructors
    public CategoryAccommodation() {
    }
    public CategoryAccommodation(String nameCategoryAccommodation, String description) {
        this.nameCategoryAccommodation = nameCategoryAccommodation;
        this.description = description;
    }

    // Getters
    public String getNameCategoryAccommodation() {
        return nameCategoryAccommodation;
    }

    public String getDescription() {
        return description;
    }

    // Setters
    public void setNameCategoryAccommodation(String nameCategoryAccommodation) {
        this.nameCategoryAccommodation = nameCategoryAccommodation;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
