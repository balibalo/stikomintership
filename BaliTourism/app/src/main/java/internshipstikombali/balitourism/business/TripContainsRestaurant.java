package internshipstikombali.balitourism.business;

/**
 * Created by Pierre on 22/06/2016.
 */
public class TripContainsRestaurant {

    // Attributes
    private int idTrip;
    private int idRestaurant;
    private String arrivalDate;
    private String arrivalTime;

    // Constructors
    public TripContainsRestaurant() {
    }

    public TripContainsRestaurant(int idTrip, int idRestaurant, String arrivalDate, String arrivalTime) {
        this.idTrip = idTrip;
        this.idRestaurant = idRestaurant;
        this.arrivalDate = arrivalDate;
        this.arrivalTime = arrivalTime;
    }

    // Getters
    public int getIdTrip() {
        return idTrip;
    }

    public int getIdRestaurant() {
        return idRestaurant;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    // Setters
    public void setIdTrip(int idTrip) {
        this.idTrip = idTrip;
    }

    public void setIdRestaurant(int idRestaurant) {
        this.idRestaurant = idRestaurant;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
}
