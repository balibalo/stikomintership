package internshipstikombali.balitourism.business;

/**
 * Created by Pierre on 22/06/2016.
 */
public class Facility {

    // Attributes
    private String nameFacility;
    private String description;

    // Constructors
    public Facility() {
    }
    public Facility(String nameFacility, String description) {
        this.nameFacility = nameFacility;
        this.description = description;
    }

    // Getters
    public String getNameFacility() {
        return nameFacility;
    }

    public String getDescription() {
        return description;
    }

    // Setters
    public void setNameFacility(String nameFacility) {
        this.nameFacility = nameFacility;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
