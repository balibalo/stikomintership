package internshipstikombali.balitourism.business;

/**
 * Created by Pierre on 21/06/2016.
 */
public class Activity {

    // Attributes
    private int idActivity;
    private String name;
    private String longitude;
    private String latitude;
    private String phoneNumber;
    private String website;
    private int price;
    private String description;
    private String picture;
    private int duration;
    private int openingHour;
    private int openingMinute;
    private int closingHour;
    private int closingMinute;

    // Constructors
    public Activity() {}

    public Activity(int idActivity, String name) {
        this.idActivity = idActivity;
        this.name = name;
    }

    public Activity(int idActivity, String name, String longitude, String latitude, String phoneNumber, String website, int price, String description, String picture, int duration, int openingHour, int openingMinute, int closingHour, int closingMinute) {
        this.idActivity = idActivity;
        this.name = name;
        this.longitude = longitude;
        this.latitude = latitude;
        this.phoneNumber = phoneNumber;
        this.website = website;
        this.price = price;
        this.description = description;
        this.picture = picture;
        this.duration = duration;
        this.openingHour = openingHour;
        this.openingMinute = openingMinute;
        this.closingHour = closingHour;
        this.closingMinute = closingMinute;
    }

    // Getters
    public int getIdActivity() {
        return idActivity;
    }

    public String getName() {
        return name;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getWebsite() {
        return website;
    }

    public int getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getPicture() {
        return picture;
    }

    public int getDuration() {
        return duration;
    }

    public int getOpeningHour() {
        return openingHour;
    }

    public int getOpeningMinute() {
        return openingMinute;
    }

    public int getClosingHour() {
        return closingHour;
    }

    public int getClosingMinute() {
        return closingMinute;
    }

    // Setters
    public void setIdActivity(int idActivity) {
        this.idActivity = idActivity;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setOpeningHour(int openingHour) {
        this.openingHour = openingHour;
    }

    public void setOpeningMinute(int openingMinute) {
        this.openingMinute = openingMinute;
    }

    public void setClosingHour(int closingHour) {
        this.closingHour = closingHour;
    }

    public void setClosingMinute(int closingMinute) {
        this.closingMinute = closingMinute;
    }

}
