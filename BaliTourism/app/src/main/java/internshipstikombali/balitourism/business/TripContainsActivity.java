package internshipstikombali.balitourism.business;

/**
 * Created by Pierre on 22/06/2016.
 */
public class TripContainsActivity {

    // Attributes
    private int idTrip;
    private int idActivity;
    private String arrivalDate;
    private String arrivalTime;

    // Constructors
    public TripContainsActivity() {
    }

    public TripContainsActivity(int idTrip, int idActivity, String arrivalDate, String arrivalTime) {
        this.idTrip = idTrip;
        this.idActivity = idActivity;
        this.arrivalDate = arrivalDate;
        this.arrivalTime = arrivalTime;
    }

    // Getters
    public int getIdTrip() {
        return idTrip;
    }

    public int getIdActivity() {
        return idActivity;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    // Setters
    public void setIdTrip(int idTrip) {
        this.idTrip = idTrip;
    }

    public void setIdActivity(int idActivity) {
        this.idActivity = idActivity;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
}
