package internshipstikombali.balitourism.business;

/**
 * Created by Pierre on 22/06/2016.
 */
public class AccommodationBelongsCategoryAccommodation {

    // Attributes
    private int idAccommodation;
    private String nameCategoryAccommodation;

    // Constructors
    public AccommodationBelongsCategoryAccommodation() {
    }

    public AccommodationBelongsCategoryAccommodation(int idAccommodation, String nameCategoryAccommodation) {
        this.idAccommodation = idAccommodation;
        this.nameCategoryAccommodation = nameCategoryAccommodation;
    }

    // Getters
    public int getIdAccommodation() {
        return idAccommodation;
    }

    public String getNameCategoryAccommodation() {
        return nameCategoryAccommodation;
    }

    // Setters
    public void setIdAccommodation(int idAccommodation) {
        this.idAccommodation = idAccommodation;
    }

    public void setNameCategoryAccommodation(String nameCategoryAccommodation) {
        this.nameCategoryAccommodation = nameCategoryAccommodation;
    }
}
