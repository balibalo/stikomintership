package internshipstikombali.balitourism.business;

/**
 * Created by Pierre on 22/06/2016.
 */
public class TripContainsTransport {

    // Attributes
    private int idTrip;
    private int idTransport;
    private String dateTransport;
    private int duration;
    private int price;

    // Constructors
    public TripContainsTransport() {
    }

    public TripContainsTransport(int idTrip, int idTransport,String dateTransport ) {
        this.idTrip = idTrip;
        this.dateTransport = dateTransport;
        this.idTransport = idTransport;
    }

    public TripContainsTransport(int idTrip, int idTransport, String dateTransport, int duration, int price) {
        this.idTrip = idTrip;
        this.idTransport = idTransport;
        this.dateTransport = dateTransport;
        this.duration = duration;
        this.price = price;
    }

    // Getters
    public int getIdTrip() {
        return idTrip;
    }

    public int getIdTransport() {
        return idTransport;
    }

    public String getDateTransport() {
        return dateTransport;
    }

    public int getDuration() {
        return duration;
    }

    public int getPrice() {
        return price;
    }

    // Setters
    public void setIdTrip(int idTrip) {
        this.idTrip = idTrip;
    }

    public void setIdTransport(int idTransport) {
        this.idTransport = idTransport;
    }

    public void setDateTransport(String date) {
        this.dateTransport = date;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
