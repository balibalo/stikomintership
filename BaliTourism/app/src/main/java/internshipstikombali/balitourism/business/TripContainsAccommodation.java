package internshipstikombali.balitourism.business;

/**
 * Created by Pierre on 22/06/2016.
 */
public class TripContainsAccommodation {

    // Attributes
    private int idTrip;
    private int idAccommodation;
    private String arrivalDate;

    // Constructors
    public TripContainsAccommodation() {
    }

    public TripContainsAccommodation(int idTrip, int idAccommodation, String arrivalDate) {
        this.idTrip = idTrip;
        this.idAccommodation = idAccommodation;
        this.arrivalDate = arrivalDate;
    }

    // Getters
    public int getIdTrip() {
        return idTrip;
    }

    public int getIdAccommodation() {
        return idAccommodation;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    // Setters
    public void setIdTrip(int idTrip) {
        this.idTrip = idTrip;
    }

    public void setIdAccommodation(int idAccommodation) {
        this.idAccommodation = idAccommodation;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

}
