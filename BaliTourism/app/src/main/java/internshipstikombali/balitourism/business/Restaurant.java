package internshipstikombali.balitourism.business;

/**
 * Created by Pierre on 22/06/2016.
 */
public class Restaurant {

    // Attributes
    private int idRestaurant;
    private String name;
    private String longitude;
    private String latitude;
    private String phoneNumber;
    private String website;
    private int price;
    private String description;
    private String picture;
    private int openingHour;
    private int openingMinute;
    private int closingHour;
    private int closingMinute;

    // Constructors
    public Restaurant() {
    }

    public Restaurant(int idRestaurant, String name) {
        this.idRestaurant = idRestaurant;
        this.name = name;
    }

    public Restaurant(int idRestaurant, String name, String longitude, String latitude, String phoneNumber, String website, int price, String description, String picture, int openingHour, int openingMinute, int closingHour, int closingMinute) {
        this.idRestaurant = idRestaurant;
        this.name = name;
        this.longitude = longitude;
        this.latitude = latitude;
        this.phoneNumber = phoneNumber;
        this.website = website;
        this.price = price;
        this.description = description;
        this.picture = picture;
        this.openingHour = openingHour;
        this.openingMinute = openingMinute;
        this.closingHour = closingHour;
        this.closingMinute = closingMinute;
    }

    // Getters
    public int getIdRestaurant() {
        return idRestaurant;
    }

    public String getName() {
        return name;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getWebsite() {
        return website;
    }

    public int getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getPicture() {
        return picture;
    }

    public int getOpeningHour() {
        return openingHour;
    }

    public int getOpeningMinute() {
        return openingMinute;
    }

    public int getClosingHour() {
        return closingHour;
    }

    public int getClosingMinute() {
        return closingMinute;
    }

    // Setters
    public void setIdRestaurant(int idRestaurant) {
        this.idRestaurant = idRestaurant;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public void setOpeningHour(int openingHour) {
        this.openingHour = openingHour;
    }

    public void setOpeningMinute(int openingMinute) {
        this.openingMinute = openingMinute;
    }

    public void setClosingHour(int closingHour) {
        this.closingHour = closingHour;
    }

    public void setClosingMinute(int closingMinute) {
        this.closingMinute = closingMinute;
    }
}
