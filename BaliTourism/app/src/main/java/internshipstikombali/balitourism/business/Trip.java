package internshipstikombali.balitourism.business;

/**
 * Created by Pierre on 22/06/2016.
 */
public class Trip {

    // Attributes
    private int idTrip;
    private int idUser;
    private String arrivalDate;
    private int duration;
    private int numberOfTravellers;
    private int budget;


    // Constructors
    public Trip() {
    }
    public Trip(int idTrip, int idUser, String arrivalDate, int duration, int numberOfTravellers, int budget) {
        this.idTrip = idTrip;
        this.idUser = idUser;
        this.arrivalDate = arrivalDate;
        this.duration = duration;
        this.numberOfTravellers = numberOfTravellers;
        this.budget = budget;
    }
    public Trip(int budget) {
        this.budget = budget;
    }

    // Getters
    public int getIdTrip() {
        return idTrip;
    }

    public int getIdUser() {
        return idUser;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public int getDuration() {
        return duration;
    }

    public int getNumberOfTravellers() {
        return numberOfTravellers;
    }

    public int getBudget() {
        return budget;
    }

    // Setters
    public void setIdTrip(int idTrip) {
        this.idTrip = idTrip;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public void setNumberOfTravellers(int numberOfTravellers) {
        this.numberOfTravellers = numberOfTravellers;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

}
