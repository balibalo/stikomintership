package internshipstikombali.balitourism.business;

import java.util.ArrayList;

import internshipstikombali.balitourism.DAO.ActivityBelongsCategoryActivityDAO;

/**
 * Created by Pierre on 22/06/2016.
 */
public class ActivityBelongsCategoryActivity {

    // Attributes
    private int idActivity;
    private String nameCategoryActivity;

    // Constructors
    public ActivityBelongsCategoryActivity() {
    }

    public ActivityBelongsCategoryActivity(int idActivity, String nameCategoryActivity) {
        this.idActivity = idActivity;
        this.nameCategoryActivity = nameCategoryActivity;
    }



    // Getters
    public int getIdActivity() {
        return idActivity;
    }

    public String getNameCategoryActivity() {
        return nameCategoryActivity;
    }

    // Setters
    public void setIdActivity(int idActivity) {
        this.idActivity = idActivity;
    }

    public void setNameCategoryActivity(String nameCategoryActivity) {
        this.nameCategoryActivity = nameCategoryActivity;
    }

}
