package internshipstikombali.balitourism.business;

/**
 * Created by Pierre on 21/06/2016.
 */
public class CategoryActivity {

    // Attributes
    private String nameCategoryActivity;
    private String description;

    // Constructors
    public CategoryActivity() {
    }
    public CategoryActivity(String nameCategoryActivity, String description) {
        this.nameCategoryActivity = nameCategoryActivity;
        this.description = description;
    }

    // Getters
    public String getNameCategoryActivity() {
        return nameCategoryActivity;
    }

    public String getDescription() {
        return description;
    }

    // Setters
    public void setNameCategoryActivity(String nameCategoryActivity) {
        this.nameCategoryActivity = nameCategoryActivity;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
