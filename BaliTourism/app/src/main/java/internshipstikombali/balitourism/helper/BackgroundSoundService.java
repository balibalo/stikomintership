package internshipstikombali.balitourism.helper;

import android.app.Application;
import android.media.MediaPlayer;

import internshipstikombali.balitourism.R;

/**
 * Created by Pierre on 27/07/2016.
 */
public class BackgroundSoundService extends Application {

        private MediaPlayer player;

        public MediaPlayer getPlayer() {
            return player;
        }

        public void setPlayer(MediaPlayer player) {
            this.player = player;
        }

        public BackgroundSoundService() {
            this.player = MediaPlayer.create(this, R.raw.song);
            this.player.setLooping(true); // Set looping
            this.player.setVolume(100,100);
        }


    @Override
    public void onCreate() {
        super.onCreate();
        this.player = MediaPlayer.create(this, R.raw.song);
        this.player.setLooping(true); // Set looping
        this.player.setVolume(100,100);

    }

    public void onStart() {
        player.start();
    }


    public void onStop() {
        player.stop();
    }
    public void onPause() {
        player.pause();
    }

}
