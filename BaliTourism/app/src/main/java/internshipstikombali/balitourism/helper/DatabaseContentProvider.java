package internshipstikombali.balitourism.helper;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Pierre on 22/06/2016.
 */
public abstract class DatabaseContentProvider  {
    public SQLiteDatabase myDatabase;

    // Constructor
    public DatabaseContentProvider(SQLiteDatabase db) {
        this.myDatabase = db;
    }

    public int delete(String tableName, String selection,
                      String[] selectionArgs) {
        return myDatabase.delete(tableName, selection, selectionArgs);
    }

    public long insert(String tableName, ContentValues values) {
        return myDatabase.insert(tableName, null, values);
    }

    protected abstract <T> T cursorToEntity(Cursor cursor);

    public Cursor query(String tableName, String[] columns,
                        String selection, String[] selectionArgs, String sortOrder) {

        final Cursor cursor = myDatabase.query(tableName, columns,
                selection, selectionArgs, null, null, sortOrder);

        return cursor;
    }

    public Cursor query(String tableName, String[] columns,
                        String selection, String[] selectionArgs, String sortOrder,
                        String limit) {

        return myDatabase.query(tableName, columns, selection,
                selectionArgs, null, null, sortOrder, limit);
    }

    public Cursor query(String tableName, String[] columns,
                        String selection, String[] selectionArgs, String groupBy,
                        String having, String orderBy, String limit) {

        return myDatabase.query(tableName, columns, selection,
                selectionArgs, groupBy, having, orderBy, limit);
    }

    public int update(String tableName, ContentValues values,
                      String selection, String[] selectionArgs) {
        return myDatabase.update(tableName, values, selection,
                selectionArgs);
    }

    public Cursor rawQuery(String sql, String[] selectionArgs) {
        return myDatabase.rawQuery(sql, selectionArgs);
    }
}
