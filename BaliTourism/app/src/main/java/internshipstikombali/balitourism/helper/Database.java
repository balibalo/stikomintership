package internshipstikombali.balitourism.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.sql.SQLException;

import internshipstikombali.balitourism.DAO.AccommodationBelongsCategoryAccommodationDAO;
import internshipstikombali.balitourism.DAO.AccommodationDAO;
import internshipstikombali.balitourism.DAO.AccommodationHasFacilityDAO;
import internshipstikombali.balitourism.DAO.ActivityBelongsCategoryActivityDAO;
import internshipstikombali.balitourism.DAO.ActivityDAO;
import internshipstikombali.balitourism.DAO.CategoryAccommodationDAO;
import internshipstikombali.balitourism.DAO.CategoryActivityDAO;
import internshipstikombali.balitourism.DAO.FacilityDAO;
import internshipstikombali.balitourism.DAO.RestaurantDAO;
import internshipstikombali.balitourism.DAO.TransportDAO;
import internshipstikombali.balitourism.DAO.TripContainsAccommodationDAO;
import internshipstikombali.balitourism.DAO.TripContainsActivityDAO;
import internshipstikombali.balitourism.DAO.TripContainsRestaurantDAO;
import internshipstikombali.balitourism.DAO.TripContainsTransportDAO;
import internshipstikombali.balitourism.DAO.TripDAO;
import internshipstikombali.balitourism.DAO.UserDAO;
import internshipstikombali.balitourism.DAO.UserHasCategoryAccommodationPreferencesDAO;
import internshipstikombali.balitourism.DAO.UserHasCategoryActivityPreferencesDAO;
import internshipstikombali.balitourism.DAO.UserHasFacilityPreferencesDAO;

/**
 * Created by Pierre on 20/06/2016.
 */
public class Database {
    private static final String TAG = "MyDatabase";
    private static final String DATABASE_NAME = "stikominternship";
    private DatabaseHelper myDatabaseHelper;
    private static final int DATABASE_VERSION = 1;
    private final Context myContext;
    public static AccommodationDAO myAccommodationDAO;
    public static AccommodationBelongsCategoryAccommodationDAO myAccommodationBelongsCategoryAccommodationDAO;
    public static AccommodationHasFacilityDAO myAccommodationHasFacilityDAO;
    public static ActivityDAO myActivityDAO;
    public static ActivityBelongsCategoryActivityDAO myActivityBelongsCategoryActivityDAO;
    public static CategoryAccommodationDAO myCategoryAccommodationDAO;
    public static CategoryActivityDAO myCategoryActivityDAO;
    public static FacilityDAO myFacilityDAO;
    public static RestaurantDAO myRestaurantDAO;
    public static TransportDAO myTransportDAO;
    public static TripDAO myTripDAO;
    public static TripContainsAccommodationDAO myTripContainsAccommodationDAO;
    public static TripContainsActivityDAO myTripContainsActivityDAO;
    public static TripContainsRestaurantDAO myTripContainsRestaurantDAO;
    public static TripContainsTransportDAO myTripContainsTransportDAO;
    public static UserDAO myUserDAO;
    public static UserHasCategoryAccommodationPreferencesDAO myUserHasCategoryAccommodationPreferencesDAO;
    public static UserHasCategoryActivityPreferencesDAO myUserHasCategoryActivityPreferencesDAO;
    public static UserHasFacilityPreferencesDAO myUserHasFacilityPreferencesDAO;

    // Constructor
    public Database(Context context) {
        this.myContext = context;
    }

    //Other functions
    public Database open() throws SQLException {
        myDatabaseHelper = new DatabaseHelper(myContext);
        SQLiteDatabase myDatabase = myDatabaseHelper.getWritableDatabase();

        myAccommodationDAO = new AccommodationDAO(myDatabase);
        myAccommodationBelongsCategoryAccommodationDAO = new AccommodationBelongsCategoryAccommodationDAO(myDatabase);
        myAccommodationHasFacilityDAO = new AccommodationHasFacilityDAO(myDatabase);
        myActivityDAO = new ActivityDAO(myDatabase);
        myActivityBelongsCategoryActivityDAO = new ActivityBelongsCategoryActivityDAO(myDatabase);
        myCategoryAccommodationDAO = new CategoryAccommodationDAO(myDatabase);
        myCategoryActivityDAO = new CategoryActivityDAO(myDatabase);
        myFacilityDAO = new FacilityDAO(myDatabase);
        myRestaurantDAO = new RestaurantDAO(myDatabase);
        myTransportDAO = new TransportDAO(myDatabase);
        myTripDAO = new TripDAO(myDatabase);
        myTripContainsAccommodationDAO = new TripContainsAccommodationDAO(myDatabase);
        myTripContainsActivityDAO = new TripContainsActivityDAO(myDatabase);
        myTripContainsRestaurantDAO = new TripContainsRestaurantDAO(myDatabase);
        myTripContainsTransportDAO = new TripContainsTransportDAO(myDatabase);
        myUserDAO = new UserDAO(myDatabase);
        myUserHasCategoryAccommodationPreferencesDAO = new UserHasCategoryAccommodationPreferencesDAO(myDatabase);
        myUserHasCategoryActivityPreferencesDAO = new UserHasCategoryActivityPreferencesDAO(myDatabase);
        myUserHasFacilityPreferencesDAO = new UserHasFacilityPreferencesDAO(myDatabase);
        return this;
    }

    public void close() {
        myDatabaseHelper.close();
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DatabaseSchema.CREATE_ACCOMMODATION_TABLE);
            db.execSQL(DatabaseSchema.CREATE_ACCOMMODATIONBELONGSCATEGORYACCOMMODATION_TABLE);
            db.execSQL(DatabaseSchema.CREATE_ACCOMMODATIONHASFACILITY_TABLE);
            db.execSQL(DatabaseSchema.CREATE_ACTIVITY_TABLE);
            db.execSQL(DatabaseSchema.CREATE_ACTIVITYBELONGSCATEGORYACTIVITY_TABLE);
            db.execSQL(DatabaseSchema.CREATE_CATEGORYACCOMMODATION_TABLE);
            db.execSQL(DatabaseSchema.CREATE_CATEGORYACTIVITY_TABLE);
            db.execSQL(DatabaseSchema.CREATE_FACILITY_TABLE);
            db.execSQL(DatabaseSchema.CREATE_RESTAURANT_TABLE);
            db.execSQL(DatabaseSchema.CREATE_TRANSPORT_TABLE);
            db.execSQL(DatabaseSchema.CREATE_TRIP_TABLE);
            db.execSQL(DatabaseSchema.CREATE_TRIPCONTAINSACCOMMODATION_TABLE);
            db.execSQL(DatabaseSchema.CREATE_TRIPCONTAINSACTIVITY_TABLE);
            db.execSQL(DatabaseSchema.CREATE_TRIPCONTAINSRESTAURANT_TABLE);
            db.execSQL(DatabaseSchema.CREATE_TRIPCONTAINSTRANSPORT_TABLE);
            db.execSQL(DatabaseSchema.CREATE_USER_TABLE);
            db.execSQL(DatabaseSchema.CREATE_USERHASCATEGORYACCOMMODATIONPREFERENCES_TABLE);
            db.execSQL(DatabaseSchema.CREATE_USERHASCATEGORYACTIVITYPREFERENCES_TABLE);
            db.execSQL(DatabaseSchema.CREATE_USERHASFACILITYPREFERENCES_TABLE);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            db.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.ACCOMMODATION_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.ACCOMMODATIONBELONGSCATEGORYACCOMMODATION_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.ACCOMMODATIONHASFACILITY_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.ACTIVITY_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.ACTIVITYBELONGSCATEGORYACTIVITY_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.CATEGORYACCOMMODATION_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.CATEGORYACTIVITY_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.FACILITY_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.RESTAURANT_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.TRANSPORT_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.TRIP_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.TRIPCONTAINSACCOMMODATION_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.TRIPCONTAINSACTIVITY_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.TRIPCONTAINSRESTAURANT_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.TRIPCONTAINSTRANSPORT_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.USER_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.USERHASCATEGORYACCOMMODATIONPREFERENCES_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.USERHASCATEGORYACTIVITYPREFERENCES_TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseSchema.USERHASFACILITYPREFERENCES_TABLE);

            onCreate(db);
        }

    }
}
