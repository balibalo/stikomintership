package internshipstikombali.balitourism.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.User;
import internshipstikombali.balitourism.helper.Database;
import internshipstikombali.balitourism.helper.SessionManager;

public class LogInActivity extends AppCompatActivity {
    private static final String TAG = RegisterActivity.class.getSimpleName();
    private Button btnLogin;
    private Button register;
    private EditText inputUsername;
    private EditText inputPassword;
    private ProgressDialog pDialog;
    private ProgressDialog pDialogAdmin;
    private SessionManager session;
    private SessionManager sessionAdmin;
    private String idOfUser;
    private int idOfTrip = 0;
    private int durationOfTrip;
    private String arrivalDateOfTrip;
    private int isAdmin;
    private ImageButton buttonMusicLogIn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        inputUsername = (EditText) findViewById(R.id.inputUsername);
        inputPassword = (EditText) findViewById(R.id.inputPassword);
        btnLogin = (Button) findViewById(R.id.btnLogIn);
        buttonMusicLogIn = (ImageButton) findViewById(R.id.buttonMusicLogIn);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicLogIn.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicLogIn.setBackgroundResource(R.drawable.play);
        }

        buttonMusicLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicLogIn.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicLogIn.setBackgroundResource(R.drawable.pause);
                }
            }
        });

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        pDialogAdmin = new ProgressDialog(this);
        pDialogAdmin.setCancelable(false);


        // Session manager
        session = new SessionManager(getApplicationContext());
        sessionAdmin = new SessionManager(getApplicationContext());


        // Login button Click Event
        btnLogin.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                String username = inputUsername.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();

                // Check for empty data in the form
                if (!(username.length() == 0) && !(password.length() == 0)) {
                    checkLoginAdmin(username, password);
                }
                else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please enter the credentials!", Toast.LENGTH_LONG)
                            .show();
                }
            }

        });

        // register button click event
        register = (Button) findViewById(R.id.button2);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LogInActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });


    }

    /**
     * function to verify login details in mysql db
     * */
    private void checkLoginAdmin(final String username, final String password) {
        // Tag used to cancel the request
        String tag_string_req = "req_loginAdmin";

        pDialogAdmin.setMessage("Logging in ...");
        if (!pDialogAdmin.isShowing())
            pDialogAdmin.show();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_LOGINADMIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {

                        // user successfully logged in
                        // Create login session
                        sessionAdmin.setLogin(true);

                        // Now store the user in SQLite
                        JSONObject admin = jObj.getJSONObject("admin");
                        int id = admin.getInt("idAdmin");
                        String login = admin.getString("login");
                        isAdmin = 1;
                        if (pDialogAdmin.isShowing()){
                            pDialogAdmin.dismiss();
                        }
                        Intent intent = new Intent(LogInActivity.this,
                                AdministratorMenuActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else {
                        checkLogin(username, password);
                    }
                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                if (pDialogAdmin.isShowing()){
                    pDialogAdmin.dismiss();
                }
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("login", username);
                params.put("password", password);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    private void checkLogin(final String username, final String password) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        String[] partsOfResponse = response.toString().split("idUser\":\"");
                        String[] partsOfSub = partsOfResponse[1].split("\",\"");
                        idOfUser = partsOfSub[0];
                        // user successfully logged in
                        // Create login session
                        session.setLogin(true);

                        // Now store the user in SQLite
                        JSONObject user = jObj.getJSONObject("user");
                        int id = user.getInt("idUser");
                        String username = user.getString("username");

                        // Inserting row in user table
                        Database.myUserDAO.addUser(new User(id, username));

                        getTripByUserId();

                    }
                    else {
                        // Error in login. Get the error message
                        if (pDialogAdmin.isShowing()) {
                            pDialogAdmin.dismiss();
                        }
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                    }
                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                params.put("password", password);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void getTripByUserId() {
        // Tag used to cancel the request
        String tag_string_req = "req_getTripByUserId";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_TRIPBYIDUSER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    // Now store the construction sites in SQLite
                    //Getting each construction site one by one
                    if (!error) {
                        JSONObject trip = jObj.getJSONObject("trip");
                        idOfTrip = trip.getInt("idTrip");
                        arrivalDateOfTrip = trip.getString("arrivalDate");
                        durationOfTrip = trip.getInt("duration");
                    }



                    if (idOfTrip!=0) {
                        if (pDialogAdmin.isShowing()) {
                            pDialogAdmin.dismiss();
                        }
                        Intent intent = new Intent(LogInActivity.this, HomeActivity.class);
                        intent.putExtra("idUser", String.valueOf(idOfUser));
                        intent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                        intent.putExtra("arrivalDate", arrivalDateOfTrip);
                        intent.putExtra("idTrip", String.valueOf(idOfTrip));
                        startActivity(intent);
                        finish();
                    }
                    else {
                        if (pDialogAdmin.isShowing()) {
                            pDialogAdmin.dismiss();
                        }
                        Intent intent = new Intent(LogInActivity.this, FormActivity.class);
                        intent.putExtra("idUser", idOfUser);
                        startActivity(intent);
                        finish();
                    }

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idUser", String.valueOf(idOfUser));
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicLogIn.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicLogIn.setBackgroundResource(R.drawable.play);
        }

        buttonMusicLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicLogIn.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicLogIn.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }

    @Override
    public void onBackPressed()
    {
    }


}
