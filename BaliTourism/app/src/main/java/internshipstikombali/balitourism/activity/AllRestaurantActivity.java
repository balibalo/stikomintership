package internshipstikombali.balitourism.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.business.Restaurant;
import internshipstikombali.balitourism.helper.Database;

public class AllRestaurantActivity extends Activity {

    private int idOfUser;
    private int idOfTrip;
    private int durationOfTrip;
    private String arrivalDateOfTrip;
    List<Restaurant> reqRestaurant;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_restaurant);

        Intent intent = getIntent();
        idOfUser = Integer.parseInt(intent.getStringExtra("idUser"));
        idOfTrip = Integer.parseInt(intent.getStringExtra("idTrip"));
        durationOfTrip = Integer.parseInt(intent.getStringExtra("durationTrip"));
        arrivalDateOfTrip = intent.getStringExtra("arrivalDate");

        reqRestaurant = Database.myRestaurantDAO.fetchAllRestaurants();

        String[] restaurants = new String[reqRestaurant.size()];

        for (int i=0; i<reqRestaurant.size();i++){
            restaurants[i] = reqRestaurant.get(i).getName();
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, restaurants);

        ListView list = (ListView)findViewById(R.id.ListViewRestaurants);

        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent myIntent = new Intent(AllRestaurantActivity.this, RestActivity.class);
                myIntent.putExtra("idRest", String.valueOf(reqRestaurant.get(position).getIdRestaurant()));
                myIntent.putExtra("idUser", String.valueOf(idOfUser));
                myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                myIntent.putExtra("idTrip",String.valueOf(idOfTrip));
                startActivity(myIntent);
            }
        });
    }

    @Override
    public void onBackPressed()
    {
    }

}
