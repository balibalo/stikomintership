package internshipstikombali.balitourism.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.business.Restaurant;
import internshipstikombali.balitourism.helper.Database;

public class AllRestaurantAdminActivity extends AppCompatActivity {

    List<Restaurant> reqRestaurant;
    private Button btnNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_restaurant_admin);

        reqRestaurant = Database.myRestaurantDAO.fetchAllRestaurants();

        String[] restaurants = new String[reqRestaurant.size()];

        for (int i=0; i<reqRestaurant.size();i++){
            restaurants[i] = reqRestaurant.get(i).getName();
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, restaurants);

        ListView list = (ListView)findViewById(R.id.ListViewRestaurantsAdmin);
        btnNew = (Button) findViewById(R.id.buttonNewRestaurant);

        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent myIntent = new Intent(AllRestaurantAdminActivity.this, RestAdminActivity.class);
                myIntent.putExtra("idRest", String.valueOf(reqRestaurant.get(position).getIdRestaurant()));
                startActivity(myIntent);
            }
        });

        // new button Click Event
        btnNew.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                Intent myIntent = new Intent(AllRestaurantAdminActivity.this, AddRestAdminActivity.class);
                startActivity(myIntent);            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        reqRestaurant = Database.myRestaurantDAO.fetchAllRestaurants();
    }

    @Override
    public void onBackPressed()
    {
    }

}
