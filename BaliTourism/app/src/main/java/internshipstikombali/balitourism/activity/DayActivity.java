package internshipstikombali.balitourism.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppController;

public class DayActivity extends AppCompatActivity {

    private int idOfUser;

    public int idOfTrip;
    public int durationOfTrip;
    public String arrivalDateOfTrip;
    public String day;
    private ImageButton buttonMusicDay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day);

        buttonMusicDay = (ImageButton) findViewById(R.id.buttonMusicDay);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicDay.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicDay.setBackgroundResource(R.drawable.play);
        }

        buttonMusicDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicDay.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicDay.setBackgroundResource(R.drawable.pause);
                }
            }
        });


        Intent intent = getIntent();

        idOfUser = Integer.parseInt(intent.getStringExtra("idUser"));
        day = intent.getStringExtra("day");
        durationOfTrip = Integer.parseInt(intent.getStringExtra("durationTrip"));
        arrivalDateOfTrip = intent.getStringExtra("arrivalDate");
        idOfTrip = Integer.parseInt(intent.getStringExtra("idTrip"));

        final TextView textViewToChange = (TextView) findViewById(R.id.day);
        textViewToChange.setText(day);

        String[] items = {"My accommodation", "My activities", "My restaurants", "My transport"};

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);

        ListView list = (ListView)findViewById(R.id.listView);

        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (adapter.getItem(position).equals("My accommodation")){
                    Intent myIntent = new Intent(DayActivity.this, AccommodationActivity.class);
                    myIntent.putExtra("idUser", String.valueOf(idOfUser));
                    myIntent.putExtra("day", day);
                    myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                    myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                    myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                    startActivity(myIntent);
                }
                else if (adapter.getItem(position).equals("My activities")){
                    Intent myIntent = new Intent(DayActivity.this, ActivityActivity.class);
                    myIntent.putExtra("idUser", String.valueOf(idOfUser));
                    myIntent.putExtra("day", day);
                    myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                    myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                    myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                    startActivity(myIntent);
                }
                else if (adapter.getItem(position).equals("My restaurants")){
                    Intent myIntent = new Intent(DayActivity.this, RestaurantActivity.class);
                    myIntent.putExtra("idUser", String.valueOf(idOfUser));
                    myIntent.putExtra("day", day);
                    myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                    myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                    myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                    startActivity(myIntent);
                }
                else if (adapter.getItem(position).equals("My transport")){
                    Intent myIntent = new Intent(DayActivity.this, TransportActivity.class);
                    myIntent.putExtra("idUser", String.valueOf(idOfUser));
                    myIntent.putExtra("day", day);
                    myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                    myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                    myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                    startActivity(myIntent);
                }


            }
        });

    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicDay.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicDay.setBackgroundResource(R.drawable.play);
        }

        buttonMusicDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicDay.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicDay.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }
}
