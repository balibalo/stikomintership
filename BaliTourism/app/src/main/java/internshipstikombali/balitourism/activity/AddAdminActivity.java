package internshipstikombali.balitourism.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;

public class AddAdminActivity extends AppCompatActivity {

    private static final String TAG = AddAdminActivity.class.getSimpleName();
    private Button btnSaveAdmin;
    private EditText inputUsernameAdmin;
    private EditText inputPasswordAdmin;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_admin);

        inputUsernameAdmin = (EditText) findViewById(R.id.inputUsernameAdmin);
        inputPasswordAdmin = (EditText) findViewById(R.id.inputPasswordAdmin);
        btnSaveAdmin = (Button) findViewById(R.id.btnSaveAdmin);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // Register Button Click event
        btnSaveAdmin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String username = inputUsernameAdmin.getText().toString().trim();
                String password = inputPasswordAdmin.getText().toString().trim();

                if (!(username.length() == 0) && !(password.length() == 0)) {
                    addAdmin(username, password);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Please fill both fields !", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });

    }

    /**
     * Function to store admin in MySQL database with post params(tag, name,
     * password) to addadmin url
     * */
    private void addAdmin(final String username,
                              final String password) {
        // Tag used to cancel the request
        String tag_string_req = "req_register";
        pDialog.setMessage("Creating admin ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDADMIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    // User successfully stored in MySQL

                    Toast.makeText(getApplicationContext(), "The admin has been added successfully !", Toast.LENGTH_LONG).show();

                    // Launch login activity
                    Intent intent = new Intent(
                                AddAdminActivity.this,
                                AdministratorMenuActivity.class);
                    startActivity(intent);
                    finish();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to addadmin url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                params.put("password", password);

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }


}
