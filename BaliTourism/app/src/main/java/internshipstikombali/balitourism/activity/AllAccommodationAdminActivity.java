package internshipstikombali.balitourism.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.business.Accommodation;
import internshipstikombali.balitourism.helper.Database;

public class AllAccommodationAdminActivity extends AppCompatActivity {

    List<Accommodation> reqAccommodation;
    private Button btnNew;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_accommodation_admin);

        reqAccommodation = Database.myAccommodationDAO.fetchAllAccommodations();

        String[] accommodations = new String[reqAccommodation.size()];

        for (int i=0; i<reqAccommodation.size();i++){
            accommodations[i] = reqAccommodation.get(i).getName();
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, accommodations);

        ListView list = (ListView)findViewById(R.id.ListViewAccommodationsAdmin);
        btnNew = (Button) findViewById(R.id.buttonNewAccommodation);


        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent myIntent = new Intent(AllAccommodationAdminActivity.this, AccoAdminActivity.class);
                myIntent.putExtra("idAcco", String.valueOf(reqAccommodation.get(position).getIdAccommodation()));
                startActivity(myIntent);
            }
        });

        // new button Click Event
        btnNew.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                Intent myIntent = new Intent(AllAccommodationAdminActivity.this, AddAccoAdminActivity.class);
                startActivity(myIntent);            }
        });


    }

    @Override
    public void onResume(){
        super.onResume();
        reqAccommodation = Database.myAccommodationDAO.fetchAllAccommodations();
    }

    @Override
    public void onBackPressed()
    {
    }

}
