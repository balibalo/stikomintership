package internshipstikombali.balitourism.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.Restaurant;
import internshipstikombali.balitourism.helper.Database;

public class AddRestAdminActivity extends AppCompatActivity {

    private static final String TAG = AddRestAdminActivity.class.getSimpleName();

    private Button buttonAddRest;
    private EditText inputNameRest;
    private EditText inputLongitudeRest;
    private EditText inputLatitudeRest;
    private EditText inputPhoneRest;
    private EditText inputWebsiteRest;
    private EditText inputPriceRest;
    private EditText inputUrlRest;
    private EditText inputDescriptionRest;
    private TimePicker timePickerOpeningAddRest;
    private TimePicker timePickerClosingAddRest;
    private ImageButton buttonMusicAddRestAdmin;


    private int idRestaurant;

    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_rest_admin);
        buttonMusicAddRestAdmin = (ImageButton) findViewById(R.id.buttonMusicAddRestAdmin);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddRestAdmin.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddRestAdmin.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddRestAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddRestAdmin.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddRestAdmin.setBackgroundResource(R.drawable.pause);
                }
            }
        });

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        buttonAddRest = (Button) findViewById(R.id.buttonAddRest);
        inputNameRest = (EditText) findViewById(R.id.inputNameRest);
        inputLongitudeRest = (EditText) findViewById(R.id.inputLongitudeRest);
        inputLatitudeRest = (EditText) findViewById(R.id.inputLatitudeRest);
        inputPhoneRest = (EditText) findViewById(R.id.inputPhoneRest);
        inputWebsiteRest = (EditText) findViewById(R.id.inputWebsiteRest);
        inputPriceRest = (EditText) findViewById(R.id.inputPriceRest);
        inputUrlRest = (EditText) findViewById(R.id.inputUrlRest);
        inputDescriptionRest = (EditText) findViewById(R.id.inputDescriptionRest);
        timePickerOpeningAddRest = (TimePicker) findViewById(R.id.timePickerOpeningAddRest);
        timePickerOpeningAddRest.setIs24HourView(true);
        timePickerClosingAddRest = (TimePicker) findViewById(R.id.timePickerClosingAddRest);
        timePickerClosingAddRest.setIs24HourView(true);

        // Send button Click Event
        buttonAddRest.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                int openingHour;
                int closingHour;
                int openingMinute;
                int closingMinute;
                int currentApiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentApiVersion > android.os.Build.VERSION_CODES.LOLLIPOP_MR1){
                    openingHour = timePickerOpeningAddRest.getHour();
                    closingHour = timePickerClosingAddRest.getHour();
                    openingMinute = timePickerOpeningAddRest.getMinute();
                    closingMinute = timePickerClosingAddRest.getMinute();
                } else {
                    openingHour = timePickerOpeningAddRest.getCurrentHour();
                    closingHour = timePickerClosingAddRest.getCurrentHour();
                    openingMinute = timePickerOpeningAddRest.getCurrentMinute();
                    closingMinute = timePickerClosingAddRest.getCurrentMinute();
                }

                if (!(inputNameRest.getText().toString().trim().length() == 0) && !(inputLongitudeRest.getText().toString().trim().length() == 0) && !(inputLatitudeRest.getText().toString().trim().length() == 0) && !(inputPriceRest.getText().toString().trim().length() == 0)) {
                    if (openingHour<=closingHour){

                        String phone;
                        String website;
                        String url;
                        String description;

                        String name = inputNameRest.getText().toString();
                        String lng = inputLongitudeRest.getText().toString();
                        String lat = inputLatitudeRest.getText().toString();
                        String price = inputPriceRest.getText().toString();

                        if (inputPhoneRest.getText().toString().trim().length() == 0){
                            phone = "no information";
                        }
                        else {
                            phone = inputPhoneRest.getText().toString();
                        }
                        if (inputWebsiteRest.getText().toString().trim().length() == 0){
                            website = "no information";
                        }
                        else {
                            website = inputWebsiteRest.getText().toString();
                        }
                        if (inputUrlRest.getText().toString().trim().length() == 0){
                            url = "no information";
                        }
                        else {
                            url = inputUrlRest.getText().toString();
                        }
                        if (inputDescriptionRest.getText().toString().trim().length() == 0){
                            description = "no information";
                        }
                        else {
                            description = inputDescriptionRest.getText().toString();
                        }

                        if (openingHour==closingHour && openingMinute==closingMinute){
                            openingHour = 0;
                            openingMinute = 0;
                            closingHour = 12;
                            closingMinute = 0;
                        }
                        else {
                            if (openingHour>12){
                                openingHour = 12;
                            }
                            if (closingHour<12){
                                closingHour = 1;
                            }
                            else {
                                closingHour = closingHour-12;
                            }
                        }

                        String oh = String.valueOf(openingHour);
                        String om = String.valueOf(openingMinute);
                        String ch = String.valueOf(closingHour);
                        String cm = String.valueOf(closingMinute);


                        addRestAdmin(name, lng, lat, phone, website, price, url ,description, oh, om, ch, cm);

                    }
                    else {
                        // Prompt user to enter credentials
                        Toast.makeText(getApplicationContext(),
                                "The closing hour must be superior than the opening hour.", Toast.LENGTH_LONG)
                                .show();
                    }
                }
                else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please fill the fields with an (*).", Toast.LENGTH_LONG)
                            .show();
                }

            }

        });

    }

    /**
     * Function to store the new restaurant in MySQL
     * */
    private void addRestAdmin(final String name, final String lng, final String lat, final String phone, final String website, final String price, final String url, final String description, final String oh, final String om, final String ch, final String cm) {
        // Tag used to cancel the request
        String tag_string_req = "req_saveForm";
        pDialog.setMessage("Saving new restaurant ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDRESTAURANTADMIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                        JSONObject restaurant = jObj.getJSONObject("restaurant");
                        idRestaurant = restaurant.getInt("idRestaurant");
                        // Inserting row in Restaurant table
                        Database.myRestaurantDAO.addRestaurant(new Restaurant(idRestaurant,name,lng,lat,phone,website,Integer.parseInt(price),description,url,Integer.parseInt(oh),Integer.parseInt(om),Integer.parseInt(ch),Integer.parseInt(cm)));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(AddRestAdminActivity.this, AdministratorMenuActivity.class);
                startActivity(intent);
                finish();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idRestaurant", String.valueOf(idRestaurant));
                params.put("name", name);
                params.put("longitude", lng);
                params.put("latitude", lat);
                params.put("phoneNumber", phone);
                params.put("website", website);
                params.put("price", price);
                params.put("description", description);
                params.put("picture", url);
                params.put("openingHour", oh);
                params.put("openingMinute", om);
                params.put("closingHour", ch);
                params.put("closingMinute", cm);
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddRestAdmin.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddRestAdmin.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddRestAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddRestAdmin.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddRestAdmin.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }

}
