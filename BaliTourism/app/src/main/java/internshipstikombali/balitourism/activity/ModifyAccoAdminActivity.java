package internshipstikombali.balitourism.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.Accommodation;
import internshipstikombali.balitourism.helper.Database;

public class ModifyAccoAdminActivity extends AppCompatActivity {

    private static final String TAG = ModifyAccoAdminActivity.class.getSimpleName();

    private Button buttonUpdateAcco;
    private EditText inputNameUpdateAcco;
    private EditText inputLongitudeUpdateAcco;
    private EditText inputLatitudeUpdateAcco;
    private EditText inputPhoneUpdateAcco;
    private EditText inputWebsiteUpdateAcco;
    private EditText inputPriceUpdateAcco;
    private EditText inputPlacesUpdateAcco;
    private EditText inputUrlUpdateAcco;
    private EditText inputDescriptionUpdateAcco;
    private ImageButton buttonMusicAddAccoAdminUpdate;


    private int idAcco;

    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_acco_admin);
        buttonMusicAddAccoAdminUpdate = (ImageButton) findViewById(R.id.buttonMusicAddAccoAdminUpdate);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddAccoAdminUpdate.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddAccoAdminUpdate.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddAccoAdminUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddAccoAdminUpdate.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddAccoAdminUpdate.setBackgroundResource(R.drawable.pause);
                }
            }
        });


        Intent intent = getIntent();
        idAcco = Integer.parseInt(intent.getStringExtra("idAcco"));

        Accommodation accommodation = Database.myAccommodationDAO.fetchAccommodationById(idAcco);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        buttonUpdateAcco = (Button) findViewById(R.id.buttonUpdateAcco);
        inputNameUpdateAcco = (EditText) findViewById(R.id.inputNameUpdateAcco);
        inputNameUpdateAcco.setText(accommodation.getName());
        inputLongitudeUpdateAcco = (EditText) findViewById(R.id.inputLongitudeUpdateAcco);
        inputLongitudeUpdateAcco.setText(String.valueOf(accommodation.getLongitude()));
        inputLatitudeUpdateAcco = (EditText) findViewById(R.id.inputLatitudeUpdateAcco);
        inputLatitudeUpdateAcco.setText(String.valueOf(accommodation.getLatitude()));
        inputPhoneUpdateAcco = (EditText) findViewById(R.id.inputPhoneUpdateAcco);
        inputPhoneUpdateAcco.setText(accommodation.getPhoneNumber());
        inputWebsiteUpdateAcco = (EditText) findViewById(R.id.inputWebsiteUpdateAcco);
        inputWebsiteUpdateAcco.setText(accommodation.getWebsite());
        inputPriceUpdateAcco = (EditText) findViewById(R.id.inputPriceUpdateAcco);
        inputPriceUpdateAcco.setText(String.valueOf(accommodation.getPrice()));
        inputPlacesUpdateAcco = (EditText) findViewById(R.id.inputPlacesUpdateAcco);
        inputPlacesUpdateAcco.setText(String.valueOf(accommodation.getNumberOfPlaces()));
        inputUrlUpdateAcco = (EditText) findViewById(R.id.inputUrlUpdateAcco);
        inputUrlUpdateAcco.setText(accommodation.getPicture());
        inputDescriptionUpdateAcco = (EditText) findViewById(R.id.inputDescriptionUpdateAcco);
        inputDescriptionUpdateAcco.setText(accommodation.getDescription());

        // Send button Click Event
        buttonUpdateAcco.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                if (!(inputNameUpdateAcco.getText().toString().trim().length() == 0) && !(inputLongitudeUpdateAcco.getText().toString().trim().length() == 0) && !(inputLatitudeUpdateAcco.getText().toString().trim().length() == 0) && !(inputPriceUpdateAcco.getText().toString().trim().length() == 0) && !(inputPlacesUpdateAcco.getText().toString().trim().length() == 0)) {

                    String phone;
                    String website;
                    String url;
                    String description;

                    String name = inputNameUpdateAcco.getText().toString();
                    String lng = inputLongitudeUpdateAcco.getText().toString();
                    String lat = inputLatitudeUpdateAcco.getText().toString();
                    String price = inputPriceUpdateAcco.getText().toString();
                    String places = inputPlacesUpdateAcco.getText().toString();

                    if (inputPhoneUpdateAcco.getText().toString().trim().length() == 0){
                        phone = "no information";
                    }
                    else {
                        phone = inputPhoneUpdateAcco.getText().toString();
                    }
                    if (inputWebsiteUpdateAcco.getText().toString().trim().length() == 0){
                        website = "no information";
                    }
                    else {
                        website = inputWebsiteUpdateAcco.getText().toString();
                    }
                    if (inputUrlUpdateAcco.getText().toString().trim().length() == 0){
                        url = "no information";
                    }
                    else {
                        url = inputUrlUpdateAcco.getText().toString();
                    }
                    if (inputDescriptionUpdateAcco.getText().toString().trim().length() == 0){
                        description = "no information";
                    }
                    else {
                        description = inputDescriptionUpdateAcco.getText().toString();
                    }

                    updateAccoAdmin(name, lng, lat, phone, website, price, places, url ,description);

                }
                else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please fill the fields with an (*).", Toast.LENGTH_LONG)
                            .show();
                }

            }

        });

    }

    /**
     * Function to store informations contained about the new accommodation in MySQL
     * */
    private void updateAccoAdmin(final String name, final String lng, final String lat, final String phone, final String website, final String price, final String places, final String url, final String description) {
        // Tag used to cancel the request
        String tag_string_req = "req_saveForm";
        pDialog.setMessage("Loading ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATEACCOMMODATIONADMIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                        // updating row in Accommodation table
                        Database.myAccommodationDAO.updateAccommodation(idAcco,name,lng,lat,phone,website,Integer.parseInt(price),description,url,Integer.parseInt(places));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(ModifyAccoAdminActivity.this, ModifyAccoAdminNextActivity.class);
                intent.putExtra("idAccommodation", String.valueOf(idAcco));
                startActivity(intent);
                finish();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idAccommodation", String.valueOf(idAcco));
                params.put("name", name);
                params.put("longitude", lng);
                params.put("latitude", lat);
                params.put("phoneNumber", phone);
                params.put("website", website);
                params.put("price", price);
                params.put("description", description);
                params.put("picture", url);
                params.put("numberOfPlaces", places);

                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddAccoAdminUpdate.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddAccoAdminUpdate.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddAccoAdminUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddAccoAdminUpdate.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddAccoAdminUpdate.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }

}
