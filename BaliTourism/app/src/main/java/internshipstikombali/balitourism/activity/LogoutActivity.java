package internshipstikombali.balitourism.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.helper.SessionManager;

public class LogoutActivity extends AppCompatActivity {

    private SessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout);
        session = new SessionManager(getApplicationContext());
        logoutAdmin();

    }

    /**
     * Logging out the admin. Will set isLoggedIn flag to false in shared
     */
    private void logoutAdmin() {
        session.setLogin(false);
        // Launching the login activity
        Intent intent = new Intent(LogoutActivity.this, LogInActivity.class);
        startActivity(intent);
        finish();
    }

}
