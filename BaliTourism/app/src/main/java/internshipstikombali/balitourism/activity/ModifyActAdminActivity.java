package internshipstikombali.balitourism.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.Activity;
import internshipstikombali.balitourism.helper.Database;

public class ModifyActAdminActivity extends AppCompatActivity {

    private static final String TAG = ModifyActAdminActivity.class.getSimpleName();

    private Button buttonAddActUpdate;
    private EditText inputNameActUpdate;
    private EditText inputLongitudeActUpdate;
    private EditText inputLatitudeActUpdate;
    private EditText inputPhoneActUpdate;
    private EditText inputWebsiteActUpdate;
    private EditText inputPriceActUpdate;
    private EditText inputDurationActUpdate;
    private EditText inputUrlActUpdate;
    private EditText inputDescriptionActUpdate;
    private TimePicker timePickerOpeningAddActUpdate;
    private TimePicker timePickerClosingAddActUpdate;
    private ImageButton buttonMusicAddActAdminUpdate;


    private int idAct;

    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_act_admin);
        buttonMusicAddActAdminUpdate = (ImageButton) findViewById(R.id.buttonMusicAddActAdminUpdate);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddActAdminUpdate.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddActAdminUpdate.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddActAdminUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddActAdminUpdate.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddActAdminUpdate.setBackgroundResource(R.drawable.pause);
                }
            }
        });

        Intent intent = getIntent();
        idAct = Integer.parseInt(intent.getStringExtra("idAct"));

        Activity activity = Database.myActivityDAO.fetchActivityById(idAct);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        buttonAddActUpdate = (Button) findViewById(R.id.buttonAddActUpdate);
        inputNameActUpdate = (EditText) findViewById(R.id.inputNameActUpdate);
        inputNameActUpdate.setText(activity.getName());
        inputLongitudeActUpdate = (EditText) findViewById(R.id.inputLongitudeActUpdate);
        inputLongitudeActUpdate.setText(activity.getLongitude());
        inputLatitudeActUpdate = (EditText) findViewById(R.id.inputLatitudeActUpdate);
        inputLatitudeActUpdate.setText(activity.getLatitude());
        inputPhoneActUpdate = (EditText) findViewById(R.id.inputPhoneActUpdate);
        inputPhoneActUpdate.setText(activity.getPhoneNumber());
        inputWebsiteActUpdate = (EditText) findViewById(R.id.inputWebsiteActUpdate);
        inputWebsiteActUpdate.setText(activity.getWebsite());
        inputPriceActUpdate = (EditText) findViewById(R.id.inputPriceActUpdate);
        inputPriceActUpdate.setText(String.valueOf(activity.getPrice()));
        inputDurationActUpdate = (EditText) findViewById(R.id.inputDurationActUpdate);
        inputDurationActUpdate.setText(String.valueOf(activity.getDuration()));
        inputUrlActUpdate = (EditText) findViewById(R.id.inputUrlActUpdate);
        inputUrlActUpdate.setText(activity.getPicture());
        inputDescriptionActUpdate = (EditText) findViewById(R.id.inputDescriptionActUpdate);
        inputDescriptionActUpdate.setText(activity.getDescription());

        timePickerOpeningAddActUpdate = (TimePicker) findViewById(R.id.timePickerOpeningAddActUpdate);
        timePickerOpeningAddActUpdate.setIs24HourView(true);
        timePickerClosingAddActUpdate = (TimePicker) findViewById(R.id.timePickerClosingAddActUpdate);
        timePickerClosingAddActUpdate.setIs24HourView(true);

        int currentApiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentApiVersion > android.os.Build.VERSION_CODES.LOLLIPOP_MR1){
            timePickerOpeningAddActUpdate.setHour(activity.getOpeningHour());
            timePickerOpeningAddActUpdate.setMinute(activity.getOpeningMinute());
            timePickerClosingAddActUpdate.setHour(activity.getClosingHour()+12);
            timePickerClosingAddActUpdate.setMinute(activity.getClosingMinute());

        } else {
            timePickerOpeningAddActUpdate.setCurrentHour(activity.getOpeningHour());
            timePickerOpeningAddActUpdate.setCurrentMinute(activity.getOpeningMinute());
            timePickerClosingAddActUpdate.setCurrentHour(activity.getClosingHour()+12);
            timePickerClosingAddActUpdate.setCurrentMinute(activity.getClosingMinute());
        }

        // Send button Click Event
        buttonAddActUpdate.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                int openingHour;
                int closingHour;
                int openingMinute;
                int closingMinute;
                int currentApiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentApiVersion > android.os.Build.VERSION_CODES.LOLLIPOP_MR1){
                    openingHour = timePickerOpeningAddActUpdate.getHour();
                    closingHour = timePickerClosingAddActUpdate.getHour();
                    openingMinute = timePickerOpeningAddActUpdate.getMinute();
                    closingMinute = timePickerClosingAddActUpdate.getMinute();
                } else {
                    openingHour = timePickerOpeningAddActUpdate.getCurrentHour();
                    closingHour = timePickerClosingAddActUpdate.getCurrentHour();
                    openingMinute = timePickerOpeningAddActUpdate.getCurrentMinute();
                    closingMinute = timePickerClosingAddActUpdate.getCurrentMinute();
                }

                if (!(inputNameActUpdate.getText().toString().trim().length() == 0) && !(inputLongitudeActUpdate.getText().toString().trim().length() == 0) && !(inputLatitudeActUpdate.getText().toString().trim().length() == 0) && !(inputPriceActUpdate.getText().toString().trim().length() == 0) && !(inputDurationActUpdate.getText().toString().trim().length() == 0)) {
                    if (openingHour<=closingHour){

                        String phone;
                        String website;
                        String url;
                        String description;

                        String name = inputNameActUpdate.getText().toString();
                        String lng = inputLongitudeActUpdate.getText().toString();
                        String lat = inputLatitudeActUpdate.getText().toString();
                        String price = inputPriceActUpdate.getText().toString();
                        String duration = inputDurationActUpdate.getText().toString();

                        if (inputPhoneActUpdate.getText().toString().trim().length() == 0){
                            phone = "no information";
                        }
                        else {
                            phone = inputPhoneActUpdate.getText().toString();
                        }
                        if (inputWebsiteActUpdate.getText().toString().trim().length() == 0){
                            website = "no information";
                        }
                        else {
                            website = inputWebsiteActUpdate.getText().toString();
                        }
                        if (inputUrlActUpdate.getText().toString().trim().length() == 0){
                            url = "no information";
                        }
                        else {
                            url = inputUrlActUpdate.getText().toString();
                        }
                        if (inputDescriptionActUpdate.getText().toString().trim().length() == 0){
                            description = "no information";
                        }
                        else {
                            description = inputDescriptionActUpdate.getText().toString();
                        }

                        if (openingHour==closingHour && openingMinute==closingMinute){
                            openingHour = 0;
                            openingMinute = 0;
                            closingHour = 12;
                            closingMinute = 0;
                        }
                        else {
                            if (openingHour>12){
                                openingHour = 12;
                                openingMinute = 0;
                            }
                            if (closingHour<12){
                                closingHour = 1;
                                closingMinute = 0;
                            }
                            else {
                                closingHour = closingHour-12;
                            }
                        }

                        String oh = String.valueOf(openingHour);
                        String om = String.valueOf(openingMinute);
                        String ch = String.valueOf(closingHour);
                        String cm = String.valueOf(closingMinute);


                        updateActAdmin(name, lng, lat, phone, website, price, duration, url ,description, oh, om, ch, cm);

                    }
                    else {
                        // Prompt user to enter credentials
                        Toast.makeText(getApplicationContext(),
                                "The closing hour must be superior than the opening hour.", Toast.LENGTH_LONG)
                                .show();
                    }
                }
                else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please fill the fields with an (*).", Toast.LENGTH_LONG)
                            .show();
                }

            }

        });

    }

    /**
     * Function to update the activity in MySQL
     * */
    private void updateActAdmin(final String name, final String lng, final String lat, final String phone, final String website, final String price, final String duration, final String url, final String description, final String oh, final String om, final String ch, final String cm) {
        // Tag used to cancel the request
        String tag_string_req = "req_saveForm";
        pDialog.setMessage("Loading ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATEACTIVITYADMIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                        Database.myActivityDAO.updateActivity(idAct,name,lng,lat,phone,website,Integer.parseInt(price),description,url,Integer.parseInt(duration),Integer.parseInt(oh),Integer.parseInt(om),Integer.parseInt(ch),Integer.parseInt(cm));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(ModifyActAdminActivity.this, ModifyActAdminNextActivity.class);
                intent.putExtra("idActivity", String.valueOf(idAct));
                startActivity(intent);
                finish();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idActivity", String.valueOf(idAct));
                params.put("name", name);
                params.put("longitude", lng);
                params.put("latitude", lat);
                params.put("phoneNumber", phone);
                params.put("website", website);
                params.put("price", price);
                params.put("description", description);
                params.put("picture", url);
                params.put("duration", duration);
                params.put("openingHour", oh);
                params.put("openingMinute", om);
                params.put("closingHour", ch);
                params.put("closingMinute", cm);
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddActAdminUpdate.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddActAdminUpdate.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddActAdminUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddActAdminUpdate.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddActAdminUpdate.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }


}
