package internshipstikombali.balitourism.activity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.Restaurant;
import internshipstikombali.balitourism.business.TripContainsRestaurant;
import internshipstikombali.balitourism.helper.Database;

public class RestaurantActivity extends AppCompatActivity {

    private static final String TAG = RestaurantActivity.class.getSimpleName();

    Bitmap bitmap;
    ImageView imageView5;
    ImageView imageView6;

    private ImageButton btnDelete1;
    private ImageButton btnDate1;
    private ImageButton btnDelete2;
    private ImageButton btnDate2;
    private ImageButton buttonMusicRestaurant;

    private int idOfRestaurant1;
    private int idOfRestaurant2;

    private int idOfTrip;
    private String day;
    private int idOfUser;
    private int durationOfTrip;
    private String formatted;
    private String arrivalDateOfTrip;
    private String arrivalTime;
    private String newArrivalTime;

    private int day2;
    private int month;
    private int year;

    private int idRestFormer;

    private DatePickerDialog datePickerDialog;
    private DatePickerDialog datePickerDialog2;

    List<TripContainsRestaurant> reqRestaurant;

    AlertDialog diaBox;
    AlertDialog diaBox2nd;
    AlertDialog diaBox2;
    AlertDialog diaBox3;
    AlertDialog diaBox4;
    AlertDialog diaBox4nd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);

        imageView5 = (ImageView) findViewById(R.id.imageView5);
        imageView6 = (ImageView) findViewById(R.id.imageView6);

        btnDelete1 = (ImageButton) findViewById(R.id.deleteButton5);
        btnDate1 = (ImageButton) findViewById(R.id.dateButton5);
        btnDelete2 = (ImageButton) findViewById(R.id.deleteButton6);
        btnDate2 = (ImageButton) findViewById(R.id.dateButton6);
        buttonMusicRestaurant = (ImageButton) findViewById(R.id.buttonMusicRestaurant);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicRestaurant.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicRestaurant.setBackgroundResource(R.drawable.play);
        }

        buttonMusicRestaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicRestaurant.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicRestaurant.setBackgroundResource(R.drawable.pause);
                }
            }
        });

        Intent intent = getIntent();
        idOfUser = Integer.parseInt(intent.getStringExtra("idUser"));
        day = intent.getStringExtra("day");
        idOfTrip = Integer.parseInt(intent.getStringExtra("idTrip"));
        durationOfTrip = Integer.parseInt(intent.getStringExtra("durationTrip"));
        arrivalDateOfTrip = intent.getStringExtra("arrivalDate");

        // Parametres : (int)idUser, (string)day
        // j'ai besoin des deux restaurants du jour

        diaBox = AskOption();
        diaBox2nd = AskOption2nd();
        diaBox2 = AskOption2();
        diaBox3 = AskOption3();
        diaBox4 = AskOption4();
        diaBox4nd = AskOption4nd();


        idOfRestaurant1 = -1;
        reqRestaurant = Database.myTripContainsRestaurantDAO.fetchAllTripContainsRestaurant();
        for (int i = 0; i<reqRestaurant.size(); i++){
            if ((reqRestaurant.get(i).getIdTrip() == idOfTrip) && reqRestaurant.get(i).getArrivalDate().equals(day) && reqRestaurant.get(i).getArrivalTime().equals("lunch")){
                idOfRestaurant1 = reqRestaurant.get(i).getIdRestaurant();
            }
        }

        if (idOfRestaurant1 == -1){
            final TextView nameRest1 = (TextView) findViewById(R.id.nameRestaurant);
            final TextView priceRest1 = (TextView) findViewById(R.id.priceRestaurant);
            final TextView locationRest1 = (TextView) findViewById(R.id.locationRestaurant);
            final TextView descriptionRest1 = (TextView) findViewById(R.id.descriptionRestaurant);
            final TextView openingHoursRest1 = (TextView) findViewById(R.id.openingHoursRestaurant);
            final TextView phoneRest1 = (TextView) findViewById(R.id.phoneRestaurant);
            final TextView websiteRest1 = (TextView) findViewById(R.id.websiteRestaurant);

            nameRest1.setText("There is no restaurant provided for this day for the lunch.");
            imageView5.setVisibility(View.GONE);
            btnDelete1.setVisibility(View.GONE);
            btnDate1.setVisibility(View.GONE);
            priceRest1.setVisibility(View.GONE);
            locationRest1.setVisibility(View.GONE);
            descriptionRest1.setVisibility(View.GONE);
            openingHoursRest1.setVisibility(View.GONE);
            phoneRest1.setVisibility(View.GONE);
            websiteRest1.setVisibility(View.GONE);
        }
        else {
            Restaurant restaurant1 = Database.myRestaurantDAO.fetchRestaurantById(idOfRestaurant1);
            String nameRestaurant1 = restaurant1.getName();
            String priceRestaurant1 = "Price : " + restaurant1.getPrice() + " irp per person";
            double latitudeRestaurant1 = Double.parseDouble(restaurant1.getLatitude());
            double longitudeRestaurant1 = Double.parseDouble(restaurant1.getLongitude());

            Geocoder geocoder;
            List<Address> addresses = new ArrayList<Address>();
            geocoder = new Geocoder(this, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(latitudeRestaurant1, longitudeRestaurant1, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String addressRestaurant1;
            if (addresses.size()==0){
                addressRestaurant1 = "Location : no information";
            }
            else {
                addressRestaurant1 = "Location : " + addresses.get(0).getAddressLine(0)+", " + addresses.get(0).getLocality();
            }

            String openingHoursRestaurant1 = "Opening hours : " + restaurant1.getOpeningHour()+":"+restaurant1.getOpeningMinute()+"am - "+restaurant1.getClosingHour()+":"+restaurant1.getClosingMinute()+"pm";

            String phoneNumberRestaurant1 = "Phone number : " + restaurant1.getPhoneNumber();

            String websiteRestaurant1 = "Website : " + restaurant1.getWebsite();

            String descriptionRestaurant1 = "Description : " + restaurant1.getDescription();

            String urlRestaurant1 = restaurant1.getPicture();

            final TextView nameRest1 = (TextView) findViewById(R.id.nameRestaurant);
            nameRest1.setText(nameRestaurant1);

            final TextView priceRest1 = (TextView) findViewById(R.id.priceRestaurant);
            priceRest1.setText(priceRestaurant1);

            final TextView locationRest1 = (TextView) findViewById(R.id.locationRestaurant);
            locationRest1.setText(addressRestaurant1);

            final TextView descriptionRest1 = (TextView) findViewById(R.id.descriptionRestaurant);
            descriptionRest1.setText(descriptionRestaurant1);

            final TextView openingHoursRest1 = (TextView) findViewById(R.id.openingHoursRestaurant);
            openingHoursRest1.setText(openingHoursRestaurant1);

            final TextView phoneRest1 = (TextView) findViewById(R.id.phoneRestaurant);
            phoneRest1.setText(phoneNumberRestaurant1);

            final TextView websiteRest1 = (TextView) findViewById(R.id.websiteRestaurant);
            websiteRest1.setText(websiteRestaurant1);

            new LoadImage().execute(urlRestaurant1);

            // delete button Click Event
            btnDelete1.setOnClickListener(new View.OnClickListener()  {
                public void onClick(View view) {
                    diaBox.show();
                }
            });

            String[] partsOfDate = day.split("/");
            day2 = Integer.parseInt(partsOfDate[0]);
            month = Integer.parseInt(partsOfDate[1]);
            year = Integer.parseInt(partsOfDate[2]);

            // date button Click Event
            btnDate1.setOnClickListener(new View.OnClickListener()  {
                public void onClick(View view) {
                    //showStartDateDialog(view);
                    datePickerDialog.setMessage("Change the date");
                    datePickerDialog.updateDate(year, month - 1, day2);
                    datePickerDialog.show();
                }
            });

            Calendar newCalendar = Calendar.getInstance();
            datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    SimpleDateFormat format1 = new SimpleDateFormat("d/M/yyyy");
                    formatted = format1.format(newDate.getTime());
                    SimpleDateFormat sdf = new SimpleDateFormat("d/M/yyyy");

                    Date startTrip = null;
                    try {
                        startTrip = sdf.parse(arrivalDateOfTrip);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    long startTripDate = startTrip.getTime();

                    Date endTrip = null;
                    try {
                        endTrip = sdf.parse(arrivalDateOfTrip);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    long endTripDate = endTrip.getTime()+(86400000*durationOfTrip);

                    Date newDateTrip = null;
                    try {
                        newDateTrip = sdf.parse(formatted);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    long newTripDate = newDateTrip.getTime();

                    if (!(newTripDate>=startTripDate && newTripDate<endTripDate)){
                        diaBox3.show();
                    }
                    else {
                        diaBox4.show();
                    }
                }

            },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        }

        idOfRestaurant2 = -1;
        for (int i = 0; i<reqRestaurant.size(); i++){
            if ((reqRestaurant.get(i).getIdTrip() == idOfTrip) && reqRestaurant.get(i).getArrivalDate().equals(day) && reqRestaurant.get(i).getArrivalTime().equals("dinner")){
                idOfRestaurant2 = reqRestaurant.get(i).getIdRestaurant();
            }
        }

        if (idOfRestaurant2==-1){
            final TextView nameRest2 = (TextView) findViewById(R.id.nameRestaurant2);
            final TextView priceRest2 = (TextView) findViewById(R.id.priceRestaurant2);
            final TextView locationRest2 = (TextView) findViewById(R.id.locationRestaurant2);
            final TextView descriptionRest2 = (TextView) findViewById(R.id.descriptionRestaurant2);
            final TextView openingHoursRest2 = (TextView) findViewById(R.id.openingHoursRestaurant2);
            final TextView phoneRest2 = (TextView) findViewById(R.id.phoneRestaurant2);
            final TextView websiteRest2 = (TextView) findViewById(R.id.websiteRestaurant2);

            nameRest2.setText("There is no restaurant provided for this day for the dinner.");
            imageView6.setVisibility(View.GONE);
            btnDelete2.setVisibility(View.GONE);
            btnDate2.setVisibility(View.GONE);
            priceRest2.setVisibility(View.GONE);
            locationRest2.setVisibility(View.GONE);
            descriptionRest2.setVisibility(View.GONE);
            openingHoursRest2.setVisibility(View.GONE);
            phoneRest2.setVisibility(View.GONE);
            websiteRest2.setVisibility(View.GONE);
        }
        else {
            Restaurant restaurant2 = Database.myRestaurantDAO.fetchRestaurantById(idOfRestaurant2);
            String nameRestaurant2 = restaurant2.getName();
            String priceRestaurant2 = "Price : " + restaurant2.getPrice() + " irp per person";
            double latitudeRestaurant2 = Double.parseDouble(restaurant2.getLatitude());
            double longitudeRestaurant2 = Double.parseDouble(restaurant2.getLongitude());

            Geocoder geocoder2;
            List<Address> addresses2 = new ArrayList<Address>();
            geocoder2 = new Geocoder(this, Locale.getDefault());

            try {
                addresses2 = geocoder2.getFromLocation(latitudeRestaurant2, longitudeRestaurant2, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String addressRestaurant2;
            if (addresses2.size()==0){
                addressRestaurant2 = "Location : no information";
            }
            else {
                addressRestaurant2 = "Location : " + addresses2.get(0).getAddressLine(0)+", " + addresses2.get(0).getLocality();
            }

            String openingHoursRestaurant2 = "Opening hours : " + restaurant2.getOpeningHour()+":"+restaurant2.getOpeningMinute()+"am - "+restaurant2.getClosingHour()+":"+restaurant2.getClosingMinute()+"pm";

            String phoneNumberRestaurant2 = "Phone number : " + restaurant2.getPhoneNumber();

            String websiteRestaurant2 = "Website : " + restaurant2.getWebsite();

            String descriptionRestaurant2 = "Description : " + restaurant2.getDescription();

            String urlRestaurant2 = restaurant2.getPicture();

            final TextView nameRest2 = (TextView) findViewById(R.id.nameRestaurant2);
            nameRest2.setText(nameRestaurant2);

            final TextView priceRest2 = (TextView) findViewById(R.id.priceRestaurant2);
            priceRest2.setText(priceRestaurant2);

            final TextView locationRest2 = (TextView) findViewById(R.id.locationRestaurant2);
            locationRest2.setText(addressRestaurant2);

            final TextView descriptionRest2 = (TextView) findViewById(R.id.descriptionRestaurant2);
            descriptionRest2.setText(descriptionRestaurant2);

            final TextView openingHoursRest2 = (TextView) findViewById(R.id.openingHoursRestaurant2);
            openingHoursRest2.setText(openingHoursRestaurant2);

            final TextView phoneRest2 = (TextView) findViewById(R.id.phoneRestaurant2);
            phoneRest2.setText(phoneNumberRestaurant2);

            final TextView websiteRest2 = (TextView) findViewById(R.id.websiteRestaurant2);
            websiteRest2.setText(websiteRestaurant2);

            if (openingHoursRestaurant2==null){
                openingHoursRest2.setVisibility(View.GONE);
            }
            if (phoneNumberRestaurant2==null){
                phoneRest2.setVisibility(View.GONE);
            }
            if (websiteRestaurant2==null){
                websiteRest2.setVisibility(View.GONE);
            }
            if (urlRestaurant2==null){
                imageView6.setVisibility(View.GONE);
            }
            else {
                new LoadImage2().execute(urlRestaurant2);
            }

            // delete button Click Event
            btnDelete2.setOnClickListener(new View.OnClickListener()  {
                public void onClick(View view) {
                    diaBox2nd.show();
                }
            });

            // date button Click Event
            btnDate2.setOnClickListener(new View.OnClickListener()  {
                public void onClick(View view) {
                    //showStartDateDialog(view);
                    datePickerDialog2.setMessage("Change the date");
                    datePickerDialog2.show();
                }
            });

            Calendar newCalendar = Calendar.getInstance();
            datePickerDialog2 = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    SimpleDateFormat format1 = new SimpleDateFormat("d/M/yyyy");
                    formatted = format1.format(newDate.getTime());
                    SimpleDateFormat sdf = new SimpleDateFormat("d/M/yyyy");

                    Date startTrip = null;
                    try {
                        startTrip = sdf.parse(arrivalDateOfTrip);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    long startTripDate = startTrip.getTime();

                    Date endTrip = null;
                    try {
                        endTrip = sdf.parse(arrivalDateOfTrip);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    long endTripDate = endTrip.getTime()+(86400000*durationOfTrip);

                    Date newDateTrip = null;
                    try {
                        newDateTrip = sdf.parse(formatted);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    long newTripDate = newDateTrip.getTime();

                    if (!(newTripDate>=startTripDate && newTripDate<endTripDate)){
                        diaBox3.show();
                    }
                    else {
                        diaBox4nd.show();
                    }
                }

            },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        }

    }

    @Override
    public void onResume(){
        super.onResume();
        idOfRestaurant1 = -1;
        idOfRestaurant2 = -1;
        reqRestaurant = Database.myTripContainsRestaurantDAO.fetchAllTripContainsRestaurant();
        for (int i = 0; i<reqRestaurant.size(); i++){
            if ((reqRestaurant.get(i).getIdTrip() == idOfTrip) && reqRestaurant.get(i).getArrivalDate().equals(day) && reqRestaurant.get(i).getArrivalTime().equals("lunch")){
                idOfRestaurant1 = reqRestaurant.get(i).getIdRestaurant();
            }
        }
        for (int i = 0; i<reqRestaurant.size(); i++){
            if ((reqRestaurant.get(i).getIdTrip() == idOfTrip) && reqRestaurant.get(i).getArrivalDate().equals(day) && reqRestaurant.get(i).getArrivalTime().equals("dinner")){
                idOfRestaurant2 = reqRestaurant.get(i).getIdRestaurant();
            }
        }
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicRestaurant.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicRestaurant.setBackgroundResource(R.drawable.play);
        }

        buttonMusicRestaurant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicRestaurant.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicRestaurant.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }

    private class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {
            if(image != null){
                imageView5.setImageBitmap(image);
            }else{

            }
        }
    }

    private class LoadImage2 extends AsyncTask<String, String, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {
            if(image != null){
                imageView6.setImageBitmap(image);
            }else{

            }
        }
    }

    /**
     * function to delete the restaurant from the trip in mysql db
     * */
    private void deleteRestaurant() {
        // Tag used to cancel the request
        String tag_string_req = "req_delete";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETERESTAURANT, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                        // delete row in tripContainsRestaurant table
                        if (arrivalTime.equals("lunch")){
                            Database.myTripContainsRestaurantDAO.deleteTripContainsRestaurant(idOfTrip,idOfRestaurant1,day,arrivalTime);
                        }
                        else {
                            Database.myTripContainsRestaurantDAO.deleteTripContainsRestaurant(idOfTrip,idOfRestaurant2,day,arrivalTime);
                        }

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to deletetransport url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idOfTrip));
                if (arrivalTime.equals("lunch")){
                    params.put("idRestaurant", String.valueOf(idOfRestaurant1));
                }
                else {
                    params.put("idRestaurant", String.valueOf(idOfRestaurant2));
                }
                params.put("arrivalDate", day);
                params.put("arrivalTime", arrivalTime);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to delete the former restaurant from the trip in mysql db
     * */
    private void deleteFormerRestaurant() {
        // Tag used to cancel the request
        String tag_string_req = "req_delete";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETERESTAURANT, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json

                        // delete row in tripContainsRestaurant table
                        Database.myTripContainsRestaurantDAO.deleteTripContainsRestaurant(idOfTrip,idRestFormer,formatted, newArrivalTime);

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to deletetransport url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idOfTrip));
                params.put("arrivalDate", formatted);
                params.put("arrivalTime", newArrivalTime);
                params.put("idRestaurant", String.valueOf(idRestFormer));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to update the restaurant from the trip in mysql db
     * */
    private void updateRestaurant() {
        // Tag used to cancel the request
        String tag_string_req = "req_update";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATERESTAURANT, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                        // update row in tripContainsRestaurant table
                        Database.myTripContainsRestaurantDAO.updateTripContainsRestaurant(idOfTrip,day,formatted, arrivalTime, newArrivalTime);

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to updatetransport url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idOfTrip));
                params.put("arrivalDate", day);
                params.put("newDate", formatted);
                params.put("arrivalTime", arrivalTime);
                params.put("newTime", newArrivalTime);


                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to add the restaurant from the trip in mysql db
     * */
    private void addRestaurant() {
        // Tag used to cancel the request
        String tag_string_req = "req_add";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDRESTAURANT, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json

                        if (arrivalTime.equals("lunch")){
                            // add row in tripContainsRestaurant table
                            Database.myTripContainsRestaurantDAO.addTripContainsRestaurant(new TripContainsRestaurant(idOfTrip,idOfRestaurant1,formatted, newArrivalTime));
                        }
                        else {
                            Database.myTripContainsRestaurantDAO.addTripContainsRestaurant(new TripContainsRestaurant(idOfTrip,idOfRestaurant2,formatted, newArrivalTime));
                        }

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to addrestaurant url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idOfTrip));
                if (arrivalTime.equals("lunch")){
                    params.put("idRestaurant", String.valueOf(idOfRestaurant1));
                }
                else {
                    params.put("idRestaurant", String.valueOf(idOfRestaurant2));
                }
                params.put("arrivalDate", formatted);
                params.put("arrivalTime", newArrivalTime);

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to check if there is already a restaurant at this date in mysql db
     * return true if there is already a restaurant
     * */
    private boolean checkRestaurant() {
        reqRestaurant = Database.myTripContainsRestaurantDAO.fetchAllTripContainsRestaurant();
        for (int i = 0; i<reqRestaurant.size(); i++){
            if ((reqRestaurant.get(i).getIdTrip() == idOfTrip) && reqRestaurant.get(i).getArrivalDate().equals(formatted) && reqRestaurant.get(i).getArrivalTime().equals(newArrivalTime)){
                idRestFormer = reqRestaurant.get(i).getIdRestaurant();
                return true;
            }
        }
        return false;
    }

    private AlertDialog AskOption()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setMessage("Do you really want to delete this restaurant for this day ?")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        arrivalTime = "lunch";
                        deleteRestaurant();
                        Toast.makeText(getApplicationContext(),
                                "The restaurant has been deleted successfully !", Toast.LENGTH_LONG)
                                .show();
                        Intent myIntent = new Intent(RestaurantActivity.this, HomeActivity.class);
                        myIntent.putExtra("idUser", String.valueOf(idOfUser));
                        myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                        myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                        myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                        startActivity(myIntent);
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    private AlertDialog AskOption2nd()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setMessage("Do you really want to delete this restaurant for this day ?")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        arrivalTime = "dinner";
                        deleteRestaurant();
                        Toast.makeText(getApplicationContext(),
                                "The restaurant has been deleted successfully !", Toast.LENGTH_LONG)
                                .show();
                        Intent myIntent = new Intent(RestaurantActivity.this, HomeActivity.class);
                        myIntent.putExtra("idUser", String.valueOf(idOfUser));
                        myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                        myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                        myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                        startActivity(myIntent);
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    private AlertDialog AskOption2()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                //set message, title, and icon
                .setMessage("There is already a restaurant at this date, do you want to change it by this one ?")
                .setPositiveButton("Change", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        deleteFormerRestaurant();
                        deleteRestaurant();
                        addRestaurant();
                        Toast.makeText(getApplicationContext(),
                                "The date of the restaurant has been updated successfully !", Toast.LENGTH_LONG)
                                .show();
                        Intent myIntent = new Intent(RestaurantActivity.this, HomeActivity.class);
                        myIntent.putExtra("idUser", String.valueOf(idOfUser));
                        myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                        myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                        myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                        startActivity(myIntent);
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    private AlertDialog AskOption3()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setMessage("You must choose a day according to the dates of your trip.")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    private AlertDialog AskOption4()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                //set message, title, and icon
                .setPositiveButton("Lunch", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        arrivalTime = "lunch";
                        newArrivalTime = "lunch";
                        if (checkRestaurant()) {
                            diaBox2.show();
                        }
                        else {
                            deleteRestaurant();
                            addRestaurant();
                            Toast.makeText(getApplicationContext(),
                                    "The restaurant has been updated successfully !", Toast.LENGTH_LONG)
                                    .show();
                            Intent myIntent = new Intent(RestaurantActivity.this, HomeActivity.class);
                            myIntent.putExtra("idUser", String.valueOf(idOfUser));
                            myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                            myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                            myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                            startActivity(myIntent);
                            finish();
                        }
                    }
                })
                .setNegativeButton("Dinner", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        arrivalTime = "lunch";
                        newArrivalTime = "dinner";
                        if (checkRestaurant()) {
                            diaBox2.show();
                        }
                        else {
                            deleteRestaurant();
                            addRestaurant();
                            Toast.makeText(getApplicationContext(),
                                    "The restaurant has been updated successfully !", Toast.LENGTH_LONG)
                                    .show();
                            Intent myIntent = new Intent(RestaurantActivity.this, HomeActivity.class);
                            myIntent.putExtra("idUser", String.valueOf(idOfUser));
                            myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                            myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                            myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                            startActivity(myIntent);
                            finish();
                        }
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    private AlertDialog AskOption4nd()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                //set message, title, and icon
                .setPositiveButton("Lunch", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        arrivalTime = "dinner";
                        newArrivalTime = "lunch";
                        if (checkRestaurant()) {
                            diaBox2.show();
                        }
                        else {
                            deleteRestaurant();
                            addRestaurant();
                            Toast.makeText(getApplicationContext(),
                                    "The restaurant has been updated successfully !", Toast.LENGTH_LONG)
                                    .show();
                            Intent myIntent = new Intent(RestaurantActivity.this, HomeActivity.class);
                            myIntent.putExtra("idUser", String.valueOf(idOfUser));
                            myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                            myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                            myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                            startActivity(myIntent);
                            finish();
                        }
                    }
                })
                .setNegativeButton("Dinner", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        arrivalTime = "dinner";
                        newArrivalTime = "dinner";
                        if (checkRestaurant()) {
                            diaBox2.show();
                        }
                        else {
                            deleteRestaurant();
                            addRestaurant();
                            Toast.makeText(getApplicationContext(),
                                    "The restaurant has been updated successfully !", Toast.LENGTH_LONG)
                                    .show();
                            Intent myIntent = new Intent(RestaurantActivity.this, HomeActivity.class);
                            myIntent.putExtra("idUser", String.valueOf(idOfUser));
                            myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                            myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                            myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                            startActivity(myIntent);
                            finish();
                        }
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

}
