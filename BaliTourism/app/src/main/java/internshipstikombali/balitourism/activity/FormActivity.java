package internshipstikombali.balitourism.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.Trip;
import internshipstikombali.balitourism.business.UserHasCategoryAccommodationPreferences;
import internshipstikombali.balitourism.business.UserHasCategoryActivityPreferences;
import internshipstikombali.balitourism.business.UserHasFacilityPreferences;
import internshipstikombali.balitourism.helper.Database;
import internshipstikombali.balitourism.helper.SessionManager;

public class FormActivity extends AppCompatActivity {

    private static final String TAG = FormActivity.class.getSimpleName();

    private Button btnLogoutForm;
    private Button btnSend;
    private DatePicker datePicker;
    private NumberPicker numberDays;
    private NumberPicker numberPersons;
    private NumberPicker numberHours;
    private EditText  budget;
    private RadioButton btnYes;
    private RadioButton btnNo;
    private CheckBox guesthouse;
    private CheckBox homestay;
    private CheckBox villa;
    private CheckBox apartement;
    private CheckBox hotel1;
    private CheckBox hotel2;
    private CheckBox hotel3;
    private CheckBox hotel4;
    private CheckBox hotel5;
    private CheckBox swimmingpool;
    private CheckBox laundry;
    private CheckBox television;
    private CheckBox wifi;
    private CheckBox breakfastincluded;
    private CheckBox restaurant;
    private CheckBox bar;
    private CheckBox airconditionner;
    private CheckBox sport;
    private CheckBox relax;
    private CheckBox romantic;
    private CheckBox cultural;
    private CheckBox nature;
    private CheckBox family;
    private int idOfUser;
    private int idOfTrip;
    private ImageButton buttonMusicForm;


    private ProgressDialog pDialog;
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        btnLogoutForm = (Button) findViewById(R.id.btnLogoutForm);
        btnSend = (Button) findViewById(R.id.buttonSend);
        datePicker = (DatePicker) findViewById(R.id.datePicker);
        datePicker.setMinDate(System.currentTimeMillis() - 1000);
        numberDays = (NumberPicker) findViewById(R.id.numberPicker);
        numberDays.setMinValue(0);
        numberDays.setMaxValue(99);
        numberPersons = (NumberPicker) findViewById(R.id.numberPicker2);
        numberPersons.setMinValue(0);
        numberPersons.setMaxValue(50);
        numberHours = (NumberPicker) findViewById(R.id.numberPicker3);
        numberHours.setMinValue(0);
        numberHours.setMaxValue(24);
        budget = (EditText) findViewById(R.id.editText);
        btnYes = (RadioButton) findViewById(R.id.yes);
        btnNo = (RadioButton) findViewById(R.id.no);
        guesthouse = (CheckBox) findViewById(R.id.guesthouse);
        homestay = (CheckBox) findViewById(R.id.homestay);
        villa = (CheckBox) findViewById(R.id.villa);
        apartement = (CheckBox) findViewById(R.id.apartement);
        hotel1 = (CheckBox) findViewById(R.id.hotel1);
        hotel2 = (CheckBox) findViewById(R.id.hotel2);
        hotel3 = (CheckBox) findViewById(R.id.hotel3);
        hotel4 = (CheckBox) findViewById(R.id.hotel4);
        hotel5 = (CheckBox) findViewById(R.id.hotel5);
        swimmingpool = (CheckBox) findViewById(R.id.swimmingpool);
        laundry = (CheckBox) findViewById(R.id.laundry);
        television = (CheckBox) findViewById(R.id.television);
        wifi = (CheckBox) findViewById(R.id.wifi);
        breakfastincluded = (CheckBox) findViewById(R.id.breakfastincluded);
        restaurant = (CheckBox) findViewById(R.id.restaurant);
        bar = (CheckBox) findViewById(R.id.bar);
        airconditionner = (CheckBox) findViewById(R.id.airconditionner);
        sport = (CheckBox) findViewById(R.id.sport);
        relax = (CheckBox) findViewById(R.id.relax);
        romantic = (CheckBox) findViewById(R.id.romantic);
        cultural = (CheckBox) findViewById(R.id.cultural);
        nature = (CheckBox) findViewById(R.id.nature);
        family = (CheckBox) findViewById(R.id.family);
        buttonMusicForm = (ImageButton) findViewById(R.id.buttonMusicForm);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicForm.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicForm.setBackgroundResource(R.drawable.play);
        }

        buttonMusicForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicForm.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicForm.setBackgroundResource(R.drawable.pause);
                }
            }
        });

        Intent intent = getIntent();
        String id = intent.getStringExtra("idUser");
        idOfUser = Integer.parseInt(id);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // Session manager
        session = new SessionManager(getApplicationContext());

        // Logout button click event
        btnLogoutForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutUser();
            }
        });

        // Send button Click Event
        btnSend.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {

                // Check for empty data in the form concerning hard constraints
                if (!(numberDays.getValue() == 0) && !(budget.getText().toString().trim().length() == 0) && (btnYes.isChecked() || btnNo.isChecked())) {

                    int dateDay = datePicker.getDayOfMonth();
                    int dateMonth = datePicker.getMonth()+1;
                    int dateYear = datePicker.getYear();
                    String date = dateDay+"/"+dateMonth+"/"+dateYear;
                    int duration = numberDays.getValue();
                    int numberOfPersons = 1+numberPersons.getValue();
                    String test = budget.getText().toString();
                    int budgetTotal = Integer.parseInt(test);

                    String AccommodationArray[] = new String[9];
                    int cpt = 0;
                    if (guesthouse.isChecked()) {
                        AccommodationArray[cpt] = "guesthouse";
                        cpt++;
                    }
                    if (homestay.isChecked()) {
                        AccommodationArray[cpt] = "homestay";
                        cpt++;
                    }
                    if (villa.isChecked()) {
                        AccommodationArray[cpt] = "villa";
                        cpt++;
                    }
                    if (apartement.isChecked()) {
                        AccommodationArray[cpt] = "apartement";
                        cpt++;
                    }
                    if (hotel1.isChecked()) {
                        AccommodationArray[cpt] = "hotel1";
                        cpt++;
                    }
                    if (hotel2.isChecked()) {
                        AccommodationArray[cpt] = "hotel2";
                        cpt++;
                    }
                    if (hotel3.isChecked()) {
                        AccommodationArray[cpt] = "hotel3";
                        cpt++;
                    }
                    if (hotel4.isChecked()) {
                        AccommodationArray[cpt] = "hotel4";
                        cpt++;
                    }
                    if (hotel5.isChecked()) {
                        AccommodationArray[cpt] = "hotel5";
                        cpt++;
                    }

                    String FacilityArray[] = new String[8];
                    int cpt2 = 0;
                    if (swimmingpool.isChecked()) {
                        FacilityArray[cpt2] = "swimmingpool";
                        cpt2++;
                    }
                    if (airconditionner.isChecked()) {
                        FacilityArray[cpt2] = "airconditionner";
                        cpt2++;
                    }
                    if (laundry.isChecked()) {
                        FacilityArray[cpt2] = "laundry";
                        cpt2++;
                    }
                    if (television.isChecked()) {
                        FacilityArray[cpt2] = "television";
                        cpt2++;
                    }
                    if (breakfastincluded.isChecked()) {
                        FacilityArray[cpt2] = "breakfastincluded";
                        cpt2++;
                    }
                    if (wifi.isChecked()) {
                        FacilityArray[cpt2] = "wifi";
                        cpt2++;
                    }
                    if (restaurant.isChecked()) {
                        FacilityArray[cpt2] = "restaurant";
                        cpt2++;
                    }
                    if (bar.isChecked()) {
                        FacilityArray[cpt2] = "bar";
                        cpt2++;
                    }

                    String ActivityArray[] = new String[6];
                    int cpt3 = 0;
                    if (sport.isChecked()) {
                        ActivityArray[cpt3] = "sport";
                        cpt3++;
                    }
                    if (cultural.isChecked()) {
                        ActivityArray[cpt3] = "cultural";
                        cpt3++;
                    }
                    if (relax.isChecked()) {
                        ActivityArray[cpt3] = "relax";
                        cpt3++;
                    }
                    if (nature.isChecked()) {
                        ActivityArray[cpt3] = "nature";
                        cpt3++;
                    }
                    if (romantic.isChecked()) {
                        ActivityArray[cpt3] = "romantic";
                        cpt3++;
                    }
                    if (family.isChecked()) {
                        ActivityArray[cpt3] = "family";
                        cpt3++;
                    }

                    int time = numberHours.getValue();

                    int drivingLicense;
                    if (btnYes.isChecked()){
                        drivingLicense = 1;
                    }
                    else {
                        drivingLicense = 0;
                    }

                    // save informations concerning the user
                    saveForm(date, duration, numberOfPersons, budgetTotal, AccommodationArray, FacilityArray, ActivityArray, time, drivingLicense);
                }
                else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please answer to the questions with an (*).", Toast.LENGTH_LONG)
                            .show();
                }

            }

        });




    }


    /**
     * Function to store informations contained in the form in MySQL database to newtrip url
     * */
    private void saveForm(final String date, final int duration, final int numberOfPersons, final int budgetTotal, final String[] AccommodationArray, final String[] FacilityArray, final String[] ActivityArray, final int time, final int drivingLicense) {
        // Tag used to cancel the request
        String tag_string_req = "req_saveForm";
        pDialog.setMessage("Saving informations ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_NEWTRIP, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                        // form successfully stored in MySQL
                        // Now store trip in sqlite
                        JSONObject trip = jObj.getJSONObject("trip");
                        idOfTrip = trip.getInt("idTrip");
                        int idUser = trip.getInt("idUser");
                        String date = trip.getString("arrivalDate");
                        int duration = trip.getInt("duration");
                        int numberOfPersons = trip.getInt("numberOfTravellers");
                        int budgetTotal = trip.getInt("budget");

                        // Inserting row in trip table
                        Database.myTripDAO.addTrip(new Trip(idOfTrip,idUser,date,duration,numberOfPersons,budgetTotal));

                        //Toast.makeText(getApplicationContext(), "The trip has been added successfully !", Toast.LENGTH_LONG).show();

                        // Now store user information in sqlite
                        JSONObject user = jObj.getJSONObject("user");
                        int idUser2 = user.getInt("idUser");
                        int time = user.getInt("timeSpentOutside");
                        int drivingLicense = user.getInt("drivingLicense");

                        // Updating row in user table
                        Database.myUserDAO.updateUser(idUser2,time,drivingLicense);

                        //Toast.makeText(getApplicationContext(), "The user has been updated successfully !", Toast.LENGTH_LONG).show();

                        // Now store categories accommodation preferences in sqlite
                        JSONObject userHasCategoryAccommodationPreferences = jObj.getJSONObject("userHasCategoryAccommodationPreferences");
                        int idUser3 = userHasCategoryAccommodationPreferences.getInt("idUser");
                        String nameCategoryAccommodation = userHasCategoryAccommodationPreferences.getString("nameCategoryAccommodation");

                        int cpt =0;
                        while (cpt<AccommodationArray.length){
                            // Inserting row in userHasCategoryAccommodationPreferences table
                            Database.myUserHasCategoryAccommodationPreferencesDAO.addUserHasCategoryAccommodationPreferences(new UserHasCategoryAccommodationPreferences(idUser3,AccommodationArray[cpt]));
                            cpt++;
                        }

                        //Toast.makeText(getApplicationContext(), "The category accommodation preferences have been added successfully !", Toast.LENGTH_LONG).show();

                        // Now store facility preferences in sqlite
                        JSONObject userHasFacilityPreferences = jObj.getJSONObject("userHasFacilityPreferences");
                        int idUser4 = userHasFacilityPreferences.getInt("idUser");
                        String nameFacility = userHasFacilityPreferences.getString("nameFacility");

                        int cpt2 =0;
                        while (cpt2<FacilityArray.length){
                            // Inserting row in userHasFacilityPreferences table
                            Database.myUserHasFacilityPreferencesDAO.addUserHasFacilityPreferences(new UserHasFacilityPreferences(idUser4,FacilityArray[cpt2]));
                            cpt2++;
                        }

                        //Toast.makeText(getApplicationContext(), "The facility preferences have been added successfully !", Toast.LENGTH_LONG).show();

                        // Now store categories activity preferences in sqlite
                        JSONObject userHasCategoryActivityPreferences = jObj.getJSONObject("userHasCategoryActivityPreferences");
                        int idUser5 = userHasCategoryActivityPreferences.getInt("idUser");
                        String nameCategoryActivity = userHasCategoryActivityPreferences.getString("nameCategoryActivity");

                        int cpt3 =0;
                        while (cpt3<ActivityArray.length){
                            // Inserting row in userHasCategoryActivityPreferences table
                            Database.myUserHasCategoryActivityPreferencesDAO.addUserHasCategoryActivityPreferences(new UserHasCategoryActivityPreferences(idUser5,ActivityArray[cpt3]));
                            cpt3++;
                        }

                        //Toast.makeText(getApplicationContext(), "The category activity preferences have been added successfully !", Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                // Launch GeneticAlgorithm activity
                Intent intent = new Intent(
                        FormActivity.this,
                        GeneticAlgorithmActivity.class);

                String idTripNext = String.valueOf(idOfTrip);
                String idUserNext = String.valueOf(idOfUser);
                String durationNext = String.valueOf(duration);
                String numberOfTravellersNext = String.valueOf(numberOfPersons);
                String drivingLicenseNext = String.valueOf(drivingLicense);
                String budgetNext = String.valueOf(budgetTotal);
                String timeSpentOutsideNext = String.valueOf(time);
                String dateNext = date;
                ArrayList<String> prefAccommodationNext = new ArrayList<String>(AccommodationArray.length);
                for (String accommodation : AccommodationArray){
                    prefAccommodationNext.add(accommodation);
                }
                ArrayList<String> prefFacilityNext = new ArrayList<String>(FacilityArray.length);
                for (String facility : FacilityArray){
                    prefFacilityNext.add(facility);
                }
                ArrayList<String> prefActivityNext = new ArrayList<String>(ActivityArray.length);
                for (String activity : ActivityArray){
                    prefActivityNext.add(activity);
                }

                intent.putExtra("idTrip", idTripNext);
                intent.putExtra("idUser", idUserNext);
                intent.putExtra("duration", durationNext);
                intent.putExtra("numberOfTravellers", numberOfTravellersNext);
                intent.putExtra("drivingLicense", drivingLicenseNext);
                intent.putExtra("budget", budgetNext);
                intent.putExtra("timeSpentOutside", timeSpentOutsideNext);
                intent.putExtra("dateOfArrival", dateNext);
                intent.putStringArrayListExtra("prefAccommodation", prefAccommodationNext);
                intent.putStringArrayListExtra("prefFacility", prefFacilityNext);
                intent.putStringArrayListExtra("prefActivity", prefActivityNext);

                startActivity(intent);
                finish();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                String accommodation = "";
                String activity = "";
                String facility = "";


                if (!(AccommodationArray.length == 0)){
                    int cpt =0;
                    while (cpt<(AccommodationArray.length-1) && !(AccommodationArray[cpt+1]== null)){
                        if (!(AccommodationArray[cpt]== null)){
                            accommodation = accommodation + AccommodationArray[cpt] + ",";
                        }
                        cpt++;
                    }
                    if (!(AccommodationArray[cpt]== null)){
                        accommodation = accommodation + AccommodationArray[cpt];
                    }
                }
                if (!(ActivityArray.length == 0)){
                    int cpt2 =0;
                    while (cpt2<(ActivityArray.length-1) && !(ActivityArray[cpt2+1]== null)){
                        if (!(ActivityArray[cpt2]== null)){
                            activity = activity + ActivityArray[cpt2] + ",";
                        }
                        cpt2++;
                    }
                    if (!(ActivityArray[cpt2]== null)){
                        activity = activity + ActivityArray[cpt2];
                    }
                }
                if (!(FacilityArray.length == 0)){
                    int cpt3 =0;
                    while (cpt3<(FacilityArray.length-1) && !(FacilityArray[cpt3+1]== null)){
                        if (!(FacilityArray[cpt3]== null)){
                            facility = facility + FacilityArray[cpt3] + ",";
                        }
                        cpt3++;
                    }
                    if (!(FacilityArray[cpt3]== null)){
                        facility = facility + FacilityArray[cpt3];
                    }
                }


                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idUser", String.valueOf(idOfUser));
                params.put("arrivalDate", date);
                params.put("duration", String.valueOf(duration));
                params.put("numberOfTravellers", String.valueOf(numberOfPersons));
                params.put("budget", String.valueOf(budgetTotal));
                params.put("timeSpentOutside", String.valueOf(time));
                params.put("drivingLicense", String.valueOf(drivingLicense));
                params.put("accommodationArray", accommodation);
                params.put("activityArray", activity);
                params.put("facilityArray", facility);

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     */
    private void logoutUser() {
        session.setLogin(false);
        Database.myUserDAO.deleteAllUsers();
        // Launching the login activity
        Intent intent = new Intent(FormActivity.this, LogInActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicForm.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicForm.setBackgroundResource(R.drawable.play);
        }

        buttonMusicForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicForm.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicForm.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }

    @Override
    public void onBackPressed()
    {
    }

}
