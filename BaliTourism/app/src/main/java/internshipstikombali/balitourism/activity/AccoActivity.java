package internshipstikombali.balitourism.activity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.Accommodation;
import internshipstikombali.balitourism.business.TripContainsAccommodation;
import internshipstikombali.balitourism.helper.Database;

public class AccoActivity extends AppCompatActivity {

    private static final String TAG = AccoActivity.class.getSimpleName();

    Bitmap bitmap;
    ImageView imageView1;
    private ImageButton btnDate;
    private int idOfUser;
    private DatePickerDialog datePickerDialog;
    private int idAcco;
    private String formatted;
    private String arrivalDateOfTrip;
    private int durationOfTrip;
    private int idOfTrip;
    private int idAccoFormer;
    private ImageButton buttonMusicAcco;


    private int day;
    private int month;
    private int year;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acco);

        imageView1 = (ImageView) findViewById(R.id.imageViewAcco);
        btnDate = (ImageButton) findViewById(R.id.dateButtonAcco);
        buttonMusicAcco = (ImageButton) findViewById(R.id.buttonMusicAcco);

        Intent intent = getIntent();
        idAcco = Integer.parseInt(intent.getStringExtra("idAcco"));
        idOfUser = Integer.parseInt(intent.getStringExtra("idUser"));
        idOfTrip = Integer.parseInt(intent.getStringExtra("idTrip"));
        durationOfTrip = Integer.parseInt(intent.getStringExtra("durationTrip"));
        arrivalDateOfTrip = intent.getStringExtra("arrivalDate");

        Accommodation accommodation = Database.myAccommodationDAO.fetchAccommodationById(idAcco);
        String nameAccommodation = accommodation.getName();
        String priceAccommodation = "Average price : " + accommodation.getPrice() + " irp per person";
        double latitudeAccommodation = Double.parseDouble(accommodation.getLatitude());
        double longitudeAccommodation = Double.parseDouble(accommodation.getLongitude());

        String phoneNumber = "Phone number : " + accommodation.getPhoneNumber();

        String website = "Website : " + accommodation.getWebsite();

        String description = "Description : " + accommodation.getDescription();

        Geocoder geocoder;
        List<Address> addresses = new ArrayList<Address>();
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitudeAccommodation, longitudeAccommodation, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String address;
        if (addresses.size()==0){
            address = "Location : no information";
        }
        else {
            address = "Location : " + addresses.get(0).getAddressLine(0)+", " + addresses.get(0).getLocality();
        }

        String url = accommodation.getPicture();

        new LoadImage().execute(url);

        final TextView nameAcco = (TextView) findViewById(R.id.nameAcco);
        nameAcco.setText(nameAccommodation);

        final TextView priceAcco = (TextView) findViewById(R.id.priceAcco);
        priceAcco.setText(priceAccommodation);

        final TextView locationAcco = (TextView) findViewById(R.id.locationAcco);
        locationAcco.setText(address);

        final TextView descriptionAcco = (TextView) findViewById(R.id.descriptionAcco);
        descriptionAcco.setText(description);

        final TextView phoneAcco = (TextView) findViewById(R.id.phoneAcco);
        phoneAcco.setText(phoneNumber);

        final TextView websiteAcco = (TextView) findViewById(R.id.websiteAcco);
        websiteAcco.setText(website);

        final AlertDialog diaBox2 = AskOption2();
        final AlertDialog diaBox3 = AskOption3();

        String[] partsOfDate = arrivalDateOfTrip.split("/");
        day = Integer.parseInt(partsOfDate[0]);
        month = Integer.parseInt(partsOfDate[1]);
        year = Integer.parseInt(partsOfDate[2]);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAcco.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAcco.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAcco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAcco.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAcco.setBackgroundResource(R.drawable.pause);
                }
            }
        });

        // date button Click Event
        btnDate.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                //showStartDateDialog(view);
                datePickerDialog.setMessage("Select a date to add this accommodation to your trip");
                datePickerDialog.updateDate(year, month - 1, day);
                datePickerDialog.show();
            }
        });

        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat format1 = new SimpleDateFormat("d/M/yyyy");
                formatted = format1.format(newDate.getTime());
                SimpleDateFormat sdf = new SimpleDateFormat("d/M/yyyy");


                Date startTrip = null;
                try {
                    startTrip = sdf.parse(arrivalDateOfTrip);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                long startTripDate = startTrip.getTime();

                Date endTrip = null;
                try {
                    endTrip = sdf.parse(arrivalDateOfTrip);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                long endTripDate = endTrip.getTime()+(86400000*durationOfTrip);

                Date newDateTrip = null;
                try {
                    newDateTrip = sdf.parse(formatted);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                long newTripDate = newDateTrip.getTime();

                if (!(newTripDate>=startTripDate && newTripDate<endTripDate)){
                    diaBox3.show();
                }
                else if (checkAccommodation()) {
                    diaBox2.show();
                }
                else {
                    addAccommodation();
                    Toast.makeText(getApplicationContext(),
                            "The accommodation has been added successfully !", Toast.LENGTH_LONG)
                            .show();
                    Intent myIntent = new Intent(AccoActivity.this, HomeActivity.class);
                    myIntent.putExtra("idUser", String.valueOf(idOfUser));
                    myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                    myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                    myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                    startActivity(myIntent);
                    finish();
                }

            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    /**
     * function to check if there is already an accommodation at this date in mysql db
     * return true if there is already an accommodation
     * */
    private boolean checkAccommodation() {
        List<TripContainsAccommodation> reqAccommodation = Database.myTripContainsAccommodationDAO.fetchAllTripContainsAccommodation();
        for (int i = 0; i<reqAccommodation.size(); i++){
            if ((reqAccommodation.get(i).getIdTrip() == idOfTrip) && reqAccommodation.get(i).getArrivalDate().equals(formatted)){
                idAccoFormer = reqAccommodation.get(i).getIdAccommodation();
                return true;
            }
        }
        return false;
    }

    private AlertDialog AskOption2()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                //set message, title, and icon
                .setMessage("There is already an accommodation at this date, do you want to change it by this one ?")
                .setPositiveButton("Change", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        deleteFormerAccommodation();
                        addAccommodation();
                        Toast.makeText(getApplicationContext(),
                                "The accommodation has been changed successfully !", Toast.LENGTH_LONG)
                                .show();
                        Intent myIntent = new Intent(AccoActivity.this, HomeActivity.class);
                        myIntent.putExtra("idUser", String.valueOf(idOfUser));
                        myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                        myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                        myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                        startActivity(myIntent);
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    private AlertDialog AskOption3()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setMessage("You must choose a day according to the dates of your trip.")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    private class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {
            if(image != null){
                imageView1.setImageBitmap(image);
            }else{

            }
        }
    }

    /**
     * function to add the accommodation from the trip in mysql db
     * */
    private void addAccommodation() {
        // Tag used to cancel the request
        String tag_string_req = "req_add";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDACCOMMODATION, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                        // add row in tripContainsAccommodation table
                        Database.myTripContainsAccommodationDAO.addTripContainsAccommodation(new TripContainsAccommodation(idOfTrip,idAcco,formatted));
                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to deleteaccommodation url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idOfTrip));
                params.put("idAccommodation", String.valueOf(idAcco));
                params.put("arrivalDate", formatted);

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to delete the former accommodation from the trip in mysql db
     * */
    private void deleteFormerAccommodation() {
        // Tag used to cancel the request
        String tag_string_req = "req_delete";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETEACCOMMODATION, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json

                        // delete row in tripContainsAccommodation table
                        Database.myTripContainsAccommodationDAO.deleteTripContainsAccommodation(idOfTrip,idAccoFormer,formatted);

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to deleteaccommodation url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idOfTrip));
                params.put("arrivalDate", formatted);
                params.put("idAccommodation", String.valueOf(idAccoFormer));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAcco.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAcco.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAcco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAcco.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAcco.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }
}
