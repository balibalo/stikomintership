package internshipstikombali.balitourism.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.business.Accommodation;
import internshipstikombali.balitourism.business.Activity;
import internshipstikombali.balitourism.business.Restaurant;
import internshipstikombali.balitourism.business.TripContainsAccommodation;
import internshipstikombali.balitourism.business.TripContainsActivity;
import internshipstikombali.balitourism.business.TripContainsRestaurant;
import internshipstikombali.balitourism.helper.Database;

public class MapActivity extends FragmentActivity implements OnMapReadyCallback, AdapterView.OnItemSelectedListener {

    private GoogleMap mMap;
    private int idOfUser;
    private int idOfTrip;
    private String arrivalDateOfTrip;
    private int durationOfTrip;
    List<Accommodation> reqAccommodation;
    List<TripContainsAccommodation> reqMyAccommodation;
    List<Activity> reqActivity;
    List<TripContainsActivity> reqMyActivity;
    List<Restaurant> reqRestaurant;
    List<TripContainsRestaurant> reqMyRestaurant;
    String[] daysOfTrip;
    List<String> filtersDay;
    String day;
    int idAct;
    int idRest;
    int idAcco;
    HashMap<String, String> markerHashMap = new HashMap<String, String>();
    HashMap<Marker, String> intentDayHashMap = new HashMap<Marker, String>();
    private Map<Marker, Class> allMarkersMap = new HashMap<Marker, Class>();
    private  Map<Marker, Integer> markerIdItem = new HashMap<>();
    int i;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        Intent intent = getIntent();
        idOfUser = Integer.parseInt(intent.getStringExtra("idUser"));
        idOfTrip = Integer.parseInt(intent.getStringExtra("idTrip"));
        arrivalDateOfTrip = intent.getStringExtra("arrivalDate");
        durationOfTrip = Integer.parseInt(intent.getStringExtra("durationTrip"));


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Spinner element
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        Spinner spinner2 = (Spinner) findViewById(R.id.spinner2);

        // Spinner click listener
        spinner.setOnItemSelectedListener(this);
        spinner2.setOnItemSelectedListener(this);

        // Spinner Drop down elements
        List<String> filters = new ArrayList<String>();
        filters.add("Filters");
        filters.add("My Trip");
        filters.add("My Accommodations");
        filters.add("My Activities");
        filters.add("My Restaurants");
        filters.add("All Accommodations");
        filters.add("All Activities");
        filters.add("All Restaurants");

        String[] partsOfDate = arrivalDateOfTrip.split("/");
        int day = Integer.parseInt(partsOfDate[0]);
        int month = Integer.parseInt(partsOfDate[1]);
        int year = Integer.parseInt(partsOfDate[2]);

        daysOfTrip = new String[durationOfTrip];
        daysOfTrip[0] = String.valueOf(day)+"/"+String.valueOf(month)+"/"+String.valueOf(year);
        for (int i=1;i<durationOfTrip;i++){
            if (month==2){
                if (year%4==0){
                    if ((day+i)==30){
                        daysOfTrip[i] = "1/3/"+String.valueOf(year);
                        day = -i+1;
                        month = month+1;
                    }
                    else{
                        daysOfTrip[i] = String.valueOf(day+i)+"/"+String.valueOf(month)+"/"+String.valueOf(year);
                    }
                }
                else if ((day+i)==29){
                    daysOfTrip[i] = "1/3/"+String.valueOf(year);
                    day = -i+1;
                    month = month+1;
                }
                else{
                    daysOfTrip[i] = String.valueOf(day+i)+"/"+String.valueOf(month)+"/"+String.valueOf(year);
                }
            }
            else if (month<8) {
                if (month % 2 == 0) {
                    if ((day + i) == 31) {
                        daysOfTrip[i] = "1/" + String.valueOf(month + 1) + "/" + String.valueOf(year);
                        day = -i+1;
                        month = month+1;
                    } else {
                        daysOfTrip[i] = String.valueOf(day + i) + "/" + String.valueOf(month) + "/" + String.valueOf(year);
                    }
                } else if ((day + i) == 32) {
                    daysOfTrip[i] = "1/" + String.valueOf(month + 1) + "/" + String.valueOf(year);
                    day = -i+1;
                    month = month+1;
                } else {
                    daysOfTrip[i] = String.valueOf(day + i) + "/" + String.valueOf(month) + "/" + String.valueOf(year);
                }
            }
            else if (month==12){
                if ((day + i) == 32) {
                    daysOfTrip[i] = "1/1/"+ String.valueOf(year+1);
                    day = -i+1;
                    month = 1;
                    year = year+1;
                }
                else {
                    daysOfTrip[i] = String.valueOf(day + i) + "/" + String.valueOf(month) + "/" + String.valueOf(year);
                }
            }
            else {
                if (month % 2 == 0) {
                    if ((day + i) == 32) {
                        daysOfTrip[i] = "1/" + String.valueOf(month + 1) + "/" + String.valueOf(year);
                        day = -i+1;
                        month = month+1;
                    } else {
                        daysOfTrip[i] = String.valueOf(day + i) + "/" + String.valueOf(month) + "/" + String.valueOf(year);
                    }
                } else if ((day + i) == 31) {
                    daysOfTrip[i] = "1/" + String.valueOf(month + 1) + "/" + String.valueOf(year);
                    day = -i+1;
                    month = month+1;
                } else {
                    daysOfTrip[i] = String.valueOf(day + i) + "/" + String.valueOf(month) + "/" + String.valueOf(year);
                }
            }
        }

        filtersDay = new ArrayList<String>();
        filtersDay.add("Day");
        //filtersDay.add("All");
        for (int i = 0; i<daysOfTrip.length; i++){
            filtersDay.add(daysOfTrip[i]);
        }


        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, filters);
        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, filtersDay);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        spinner2.setAdapter(dataAdapter2);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        // Add a marker in Sydney and move the camera
        LatLng bali = new LatLng(-8.380397, 115.213609);
        mMap.moveCamera( CameraUpdateFactory.newLatLngZoom(bali, 9) );
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();
        String idMarker = null;

        if (item.equals("My Trip")){
            mMap.clear();
            reqAccommodation = Database.myAccommodationDAO.fetchAllAccommodations();
            reqMyAccommodation = Database.myTripContainsAccommodationDAO.fetchAllTripContainsAccommodation();
            for (int i = 0; i<reqMyAccommodation.size(); i++){
                if ((reqMyAccommodation.get(i).getIdTrip() == idOfTrip)){
                    int idMyAccommodation = reqMyAccommodation.get(i).getIdAccommodation();
                    Accommodation myAccommodation = Database.myAccommodationDAO.fetchAccommodationById(idMyAccommodation);
                    double latitudeMyAccommodation = Double.parseDouble(myAccommodation.getLatitude());
                    double longitudeMyAccommodation = Double.parseDouble(myAccommodation.getLongitude());
                    LatLng MyAccommodationLocation = new LatLng(latitudeMyAccommodation, longitudeMyAccommodation);
                    day = reqMyAccommodation.get(i).getArrivalDate();
                    Marker myTripAccoM = mMap.addMarker(new MarkerOptions()
                            .position(MyAccommodationLocation)
                            .title(myAccommodation.getName())
                            .snippet("Arrival date : " + day).icon(BitmapDescriptorFactory.fromResource(R.drawable.accommodation)));
                    mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        @Override
                        public void onInfoWindowClick(Marker marker) {
                            Intent intent = new Intent(MapActivity.this, AccommodationActivity.class);
                            intent.putExtra("idUser", String.valueOf(idOfUser));
                            intent.putExtra("day", day);
                            intent.putExtra("idTrip", String.valueOf(idOfTrip));
                            intent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                            intent.putExtra("arrivalDate", arrivalDateOfTrip);
                            startActivity(intent);
                            finish();
                        }
                    });
                    allMarkersMap.put(myTripAccoM,AccommodationActivity.class);
                    idMarker = myTripAccoM.getId();
                    markerHashMap.put(idMarker, "mytripacco");
                    intentDayHashMap.put(myTripAccoM, day);

                }
            }
            reqActivity = Database.myActivityDAO.fetchAllActivities();
            reqMyActivity = Database.myTripContainsActivityDAO.fetchAllTripContainsActivity();
            for (int i = 0; i<reqMyActivity.size(); i++){
                if ((reqMyActivity.get(i).getIdTrip() == idOfTrip)){
                    int idMyActivity = reqMyActivity.get(i).getIdActivity();
                    Activity myActivity = Database.myActivityDAO.fetchActivityById(idMyActivity);
                    double latitudeMyActivity = Double.parseDouble(myActivity.getLatitude());
                    double longitudeMyActivity = Double.parseDouble(myActivity.getLongitude());
                    LatLng MyActivityLocation = new LatLng(latitudeMyActivity, longitudeMyActivity);
                    day = reqMyActivity.get(i).getArrivalDate();
                    Marker myTripActM = mMap.addMarker(new MarkerOptions()
                            .position(MyActivityLocation)
                            .title(myActivity.getName())
                            .snippet(day + ", " + reqMyActivity.get(i).getArrivalTime()).icon(BitmapDescriptorFactory.fromResource(R.drawable.activity)));
                    mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        @Override
                        public void onInfoWindowClick(Marker marker) {
                            Intent intent = new Intent(MapActivity.this, ActivityActivity.class);
                            intent.putExtra("idUser", String.valueOf(idOfUser));
                            intent.putExtra("day", day);
                            intent.putExtra("idTrip", String.valueOf(idOfTrip));
                            intent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                            intent.putExtra("arrivalDate", arrivalDateOfTrip);
                            startActivity(intent);
                        }
                    });
                    allMarkersMap.put(myTripActM,ActivityActivity.class);
                    idMarker = myTripActM.getId();
                    markerHashMap.put(idMarker, "mytripact");
                    intentDayHashMap.put(myTripActM, day);
                }
            }
            reqRestaurant = Database.myRestaurantDAO.fetchAllRestaurants();
            reqMyRestaurant = Database.myTripContainsRestaurantDAO.fetchAllTripContainsRestaurant();
            for (int i = 0; i<reqMyRestaurant.size(); i++){
                if ((reqMyRestaurant.get(i).getIdTrip() == idOfTrip)){
                    int idMyRestaurant = reqMyRestaurant.get(i).getIdRestaurant();
                    Restaurant myRestaurant = Database.myRestaurantDAO.fetchRestaurantById(idMyRestaurant);
                    double latitudeMyRestaurant = Double.parseDouble(myRestaurant.getLatitude());
                    double longitudeMyRestaurant = Double.parseDouble(myRestaurant.getLongitude());
                    LatLng MyRestaurantLocation = new LatLng(latitudeMyRestaurant, longitudeMyRestaurant);
                    day = reqMyRestaurant.get(i).getArrivalDate();
                    Marker myTripRestM = mMap.addMarker(new MarkerOptions()
                            .position(MyRestaurantLocation)
                            .title(myRestaurant.getName())
                            .snippet(day + ", " + reqMyRestaurant.get(i).getArrivalTime()).icon(BitmapDescriptorFactory.fromResource(R.drawable.restaurant)));
                    mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        @Override
                        public void onInfoWindowClick(Marker marker) {
                            Intent intent = new Intent(MapActivity.this, RestaurantActivity.class);
                            intent.putExtra("idUser", String.valueOf(idOfUser));
                            intent.putExtra("day", day);
                            intent.putExtra("idTrip", String.valueOf(idOfTrip));
                            intent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                            intent.putExtra("arrivalDate", arrivalDateOfTrip);
                            startActivity(intent);
                        }
                    });
                    allMarkersMap.put(myTripRestM,RestaurantActivity.class);
                    idMarker = myTripRestM.getId();
                    markerHashMap.put(idMarker, "mytriprest");
                    intentDayHashMap.put(myTripRestM, day);
                }
            }
        }
        if (item.equals("My Accommodations")){
            mMap.clear();
            reqAccommodation = Database.myAccommodationDAO.fetchAllAccommodations();
            reqMyAccommodation = Database.myTripContainsAccommodationDAO.fetchAllTripContainsAccommodation();
            for (int i = 0; i<reqMyAccommodation.size(); i++){
                if ((reqMyAccommodation.get(i).getIdTrip() == idOfTrip)){
                    int idMyAccommodation = reqMyAccommodation.get(i).getIdAccommodation();
                    Accommodation myAccommodation = Database.myAccommodationDAO.fetchAccommodationById(idMyAccommodation);
                    double latitudeMyAccommodation = Double.parseDouble(myAccommodation.getLatitude());
                    double longitudeMyAccommodation = Double.parseDouble(myAccommodation.getLongitude());
                    LatLng MyAccommodationLocation = new LatLng(latitudeMyAccommodation, longitudeMyAccommodation);
                    day = reqMyAccommodation.get(i).getArrivalDate();
                    Marker myAccoM = mMap.addMarker(new MarkerOptions()
                            .position(MyAccommodationLocation)
                            .title(myAccommodation.getName())
                            .snippet("Arrival date : " + day).icon(BitmapDescriptorFactory.fromResource(R.drawable.accommodation)));
                    allMarkersMap.put(myAccoM,AccommodationActivity.class);
                    idMarker = myAccoM.getId();
                    markerHashMap.put(idMarker, "myacco");
                    intentDayHashMap.put(myAccoM, day);
                }
            }
        }
        if (item.equals("My Activities")){
            mMap.clear();
            reqActivity = Database.myActivityDAO.fetchAllActivities();
            reqMyActivity = Database.myTripContainsActivityDAO.fetchAllTripContainsActivity();
            for (int i = 0; i<reqMyActivity.size(); i++){
                if ((reqMyActivity.get(i).getIdTrip() == idOfTrip)){
                    int idMyActivity = reqMyActivity.get(i).getIdActivity();
                    Activity myActivity = Database.myActivityDAO.fetchActivityById(idMyActivity);
                    double latitudeMyActivity = Double.parseDouble(myActivity.getLatitude());
                    double longitudeMyActivity = Double.parseDouble(myActivity.getLongitude());
                    LatLng MyActivityLocation = new LatLng(latitudeMyActivity, longitudeMyActivity);
                    day = reqMyActivity.get(i).getArrivalDate();
                    Marker myActM = mMap.addMarker(new MarkerOptions()
                            .position(MyActivityLocation)
                            .title(myActivity.getName())
                            .snippet(day + ", " + reqMyActivity.get(i).getArrivalTime()).icon(BitmapDescriptorFactory.fromResource(R.drawable.activity)));
                    mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        @Override
                        public void onInfoWindowClick(Marker marker) {
                            Intent intent = new Intent(MapActivity.this, ActivityActivity.class);
                            intent.putExtra("idUser", String.valueOf(idOfUser));
                            intent.putExtra("day", day);
                            intent.putExtra("idTrip", String.valueOf(idOfTrip));
                            intent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                            intent.putExtra("arrivalDate", arrivalDateOfTrip);
                            startActivity(intent);
                        }
                    });
                    allMarkersMap.put(myActM,ActivityActivity.class);
                    idMarker = myActM.getId();
                    markerHashMap.put(idMarker, "myact");
                    intentDayHashMap.put(myActM, day);
                }
            }
        }
        if (item.equals("My Restaurants")){
            mMap.clear();
            reqRestaurant = Database.myRestaurantDAO.fetchAllRestaurants();
            reqMyRestaurant = Database.myTripContainsRestaurantDAO.fetchAllTripContainsRestaurant();
            for (int i = 0; i<reqMyRestaurant.size(); i++){
                if ((reqMyRestaurant.get(i).getIdTrip() == idOfTrip)){
                    int idMyRestaurant = reqMyRestaurant.get(i).getIdRestaurant();
                    Restaurant myRestaurant = Database.myRestaurantDAO.fetchRestaurantById(idMyRestaurant);
                    double latitudeMyRestaurant = Double.parseDouble(myRestaurant.getLatitude());
                    double longitudeMyRestaurant = Double.parseDouble(myRestaurant.getLongitude());
                    LatLng MyRestaurantLocation = new LatLng(latitudeMyRestaurant, longitudeMyRestaurant);
                    day = reqMyRestaurant.get(i).getArrivalDate();
                    Marker myRestM = mMap.addMarker(new MarkerOptions()
                            .position(MyRestaurantLocation)
                            .title(myRestaurant.getName())
                            .snippet(day + ", " + reqMyRestaurant.get(i).getArrivalTime()).icon(BitmapDescriptorFactory.fromResource(R.drawable.restaurant)));
                    mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        @Override
                        public void onInfoWindowClick(Marker marker) {
                            Intent intent = new Intent(MapActivity.this, RestaurantActivity.class);
                            intent.putExtra("idUser", String.valueOf(idOfUser));
                            intent.putExtra("day", day);
                            intent.putExtra("idTrip", String.valueOf(idOfTrip));
                            intent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                            intent.putExtra("arrivalDate", arrivalDateOfTrip);
                            startActivity(intent);
                        }
                    });
                    allMarkersMap.put(myRestM,RestaurantActivity.class);
                    idMarker = myRestM.getId();
                    markerHashMap.put(idMarker, "myrest");
                    intentDayHashMap.put(myRestM, day);
                }
            }
        }
        if (item.equals("All Activities")){
            mMap.clear();
            reqActivity = Database.myActivityDAO.fetchAllActivities();
            for (int i=0; i<reqActivity.size();i++){
                double latitudeActivity = Double.parseDouble(reqActivity.get(i).getLatitude());
                double longitudeActivity = Double.parseDouble(reqActivity.get(i).getLongitude());
                LatLng activityLocation = new LatLng(latitudeActivity, longitudeActivity);
                idAct = reqActivity.get(i).getIdActivity();
                Marker allActM = mMap.addMarker(new MarkerOptions()
                        .position(activityLocation)
                        .title(reqActivity.get(i).getName())
                        .snippet("Price : " + String.valueOf(reqActivity.get(i).getPrice()) + " Rp").icon(BitmapDescriptorFactory.fromResource(R.drawable.activity)));
                mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        Intent intent = new Intent(MapActivity.this, ActActivity.class);
                        intent.putExtra("idUser", String.valueOf(idOfUser));
                        intent.putExtra("idAct", String.valueOf(idAct));
                        intent.putExtra("idTrip", String.valueOf(idOfTrip));
                        intent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                        intent.putExtra("arrivalDate", arrivalDateOfTrip);
                        startActivity(intent);
                    }
                });
                allMarkersMap.put(allActM,ActActivity.class);
                markerIdItem.put(allActM,idAct);
                idMarker = allActM.getId();
                markerHashMap.put(idMarker, "allact");
            }
        }
        if (item.equals("All Restaurants")){
            mMap.clear();
            reqRestaurant = Database.myRestaurantDAO.fetchAllRestaurants();
            for (int i=0; i<reqRestaurant.size();i++){
                double latitudeRestaurant = Double.parseDouble(reqRestaurant.get(i).getLatitude());
                double longitudeRestaurant = Double.parseDouble(reqRestaurant.get(i).getLongitude());
                LatLng restaurantLocation = new LatLng(latitudeRestaurant, longitudeRestaurant);
                idRest = reqRestaurant.get(i).getIdRestaurant();
                Marker allRestM = mMap.addMarker(new MarkerOptions()
                        .position(restaurantLocation)
                        .title(reqRestaurant.get(i).getName())
                        .snippet("Average price/person : " + String.valueOf(reqRestaurant.get(i).getPrice()) + " Rp").icon(BitmapDescriptorFactory.fromResource(R.drawable.restaurant)));
                mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        Intent intent = new Intent(MapActivity.this, RestActivity.class);
                        intent.putExtra("idUser", String.valueOf(idOfUser));
                        intent.putExtra("idRest", String.valueOf(idRest));
                        intent.putExtra("idTrip", String.valueOf(idOfTrip));
                        intent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                        intent.putExtra("arrivalDate", arrivalDateOfTrip);
                        startActivity(intent);
                    }
                });
                allMarkersMap.put(allRestM,RestActivity.class);
                markerIdItem.put(allRestM,idRest);
                idMarker = allRestM.getId();
                markerHashMap.put(idMarker, "allrest");
            }
        }
        if (item.equals("All Accommodations")){
            mMap.clear();
            reqAccommodation = Database.myAccommodationDAO.fetchAllAccommodations();
            for (int i=0; i<reqAccommodation.size();i++){
                double latitudeAccommodation = Double.parseDouble(reqAccommodation.get(i).getLatitude());
                double longitudeAccommodation = Double.parseDouble(reqAccommodation.get(i).getLongitude());
                LatLng accommodationLocation = new LatLng(latitudeAccommodation, longitudeAccommodation);
                idAcco = reqAccommodation.get(i).getIdAccommodation();
                Marker allAccoM = mMap.addMarker(new MarkerOptions()
                        .position(accommodationLocation)
                        .title(reqAccommodation.get(i).getName())
                        .snippet("Price per person/night : " + String.valueOf(reqAccommodation.get(i).getPrice()) + " Rp").icon(BitmapDescriptorFactory.fromResource(R.drawable.accommodation)));
                allMarkersMap.put(allAccoM,AccoActivity.class);
                markerIdItem.put(allAccoM,idAcco);
                idMarker = allAccoM.getId();
                markerHashMap.put(idMarker, "allacco");
            }
        }

        for (int j = 0; j<daysOfTrip.length; j++){
            if (item.equals(daysOfTrip[j])){
                mMap.clear();
                reqAccommodation = Database.myAccommodationDAO.fetchAllAccommodations();
                reqMyAccommodation = Database.myTripContainsAccommodationDAO.fetchAllTripContainsAccommodation();
                reqActivity = Database.myActivityDAO.fetchAllActivities();
                reqMyActivity = Database.myTripContainsActivityDAO.fetchAllTripContainsActivity();
                reqRestaurant = Database.myRestaurantDAO.fetchAllRestaurants();
                reqMyRestaurant = Database.myTripContainsRestaurantDAO.fetchAllTripContainsRestaurant();
                for (int i = 0; i<reqMyAccommodation.size(); i++){
                    if ((reqMyAccommodation.get(i).getIdTrip() == idOfTrip)){
                        if (reqMyAccommodation.get(i).getArrivalDate().equals(daysOfTrip[j])){
                            int idMyAccommodation = reqMyAccommodation.get(i).getIdAccommodation();
                            Accommodation myAccommodation = Database.myAccommodationDAO.fetchAccommodationById(idMyAccommodation);
                            double latitudeMyAccommodation = Double.parseDouble(myAccommodation.getLatitude());
                            double longitudeMyAccommodation = Double.parseDouble(myAccommodation.getLongitude());
                            LatLng MyAccommodationLocation = new LatLng(latitudeMyAccommodation, longitudeMyAccommodation);
                            day = reqMyAccommodation.get(i).getArrivalDate();
                            Marker dayAccoM = mMap.addMarker(new MarkerOptions()
                                    .position(MyAccommodationLocation)
                                    .title(myAccommodation.getName())
                                    .snippet(myAccommodation.getPhoneNumber() + " - " + myAccommodation.getWebsite()).icon(BitmapDescriptorFactory.fromResource(R.drawable.accommodation)));
                            allMarkersMap.put(dayAccoM,AccommodationActivity.class);
                            idMarker = dayAccoM.getId();
                            markerHashMap.put(idMarker, "dayacco");
                            intentDayHashMap.put(dayAccoM, day);
                        }
                    }
                }
                for (int i = 0; i<reqMyActivity.size(); i++){
                    if ((reqMyActivity.get(i).getIdTrip() == idOfTrip)){
                        if (reqMyActivity.get(i).getArrivalDate().equals(daysOfTrip[j])){
                            int idMyActivity = reqMyActivity.get(i).getIdActivity();
                            Activity myActivity = Database.myActivityDAO.fetchActivityById(idMyActivity);
                            double latitudeMyActivity = Double.parseDouble(myActivity.getLatitude());
                            double longitudeMyActivity = Double.parseDouble(myActivity.getLongitude());
                            LatLng MyActivityLocation = new LatLng(latitudeMyActivity, longitudeMyActivity);
                            day = reqMyActivity.get(i).getArrivalDate();
                            Marker dayActM = mMap.addMarker(new MarkerOptions()
                                    .position(MyActivityLocation)
                                    .title(myActivity.getName())
                                    .snippet(reqMyActivity.get(i).getArrivalTime()).icon(BitmapDescriptorFactory.fromResource(R.drawable.activity)));
                            allMarkersMap.put(dayActM,ActivityActivity.class);
                            idMarker = dayActM.getId();
                            markerHashMap.put(idMarker, "dayact");
                            intentDayHashMap.put(dayActM, day);
                        }
                    }
                }
                for (int i = 0; i<reqMyRestaurant.size(); i++){
                    if ((reqMyRestaurant.get(i).getIdTrip() == idOfTrip)){
                        if (reqMyRestaurant.get(i).getArrivalDate().equals(daysOfTrip[j])){
                            int idMyRestaurant = reqMyRestaurant.get(i).getIdRestaurant();
                            Restaurant myRestaurant = Database.myRestaurantDAO.fetchRestaurantById(idMyRestaurant);
                            double latitudeMyRestaurant = Double.parseDouble(myRestaurant.getLatitude());
                            double longitudeMyRestaurant = Double.parseDouble(myRestaurant.getLongitude());
                            LatLng MyRestaurantLocation = new LatLng(latitudeMyRestaurant, longitudeMyRestaurant);
                            day = reqMyRestaurant.get(i).getArrivalDate();
                            Marker dayRestM = mMap.addMarker(new MarkerOptions()
                                    .position(MyRestaurantLocation)
                                    .title(myRestaurant.getName())
                                    .snippet(reqMyRestaurant.get(i).getArrivalTime()).icon(BitmapDescriptorFactory.fromResource(R.drawable.restaurant)));
                            allMarkersMap.put(dayRestM,RestaurantActivity.class);
                            idMarker = dayRestM.getId();
                            markerHashMap.put(idMarker, "dayrest");
                            intentDayHashMap.put(dayRestM, day);
                        }
                    }
                }
            }
        }

        if (item.equals("Day")){
            mMap.clear();
        }
        if (item.equals("Filters")){
            mMap.clear();
        }

        // Showing selected spinner item
        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener(){

            @Override
            public void onInfoWindowClick(Marker marker) {

                String m = markerHashMap.get(marker.getId());
                if (m.equals("mytripacco")){
                    Class cls = allMarkersMap.get(marker);
                    String dayNext = intentDayHashMap.get(marker);
                    Intent intent = new Intent(MapActivity.this, cls);
                    intent.putExtra("idUser", String.valueOf(idOfUser));
                    intent.putExtra("day", dayNext);
                    intent.putExtra("idTrip", String.valueOf(idOfTrip));
                    intent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                    intent.putExtra("arrivalDate", arrivalDateOfTrip);
                    startActivity(intent);
                }
                else if (m.equals("mytripact")){
                    Class cls = allMarkersMap.get(marker);
                    String dayNext = intentDayHashMap.get(marker);
                    Intent intent = new Intent(MapActivity.this, cls);
                    intent.putExtra("idUser", String.valueOf(idOfUser));
                    intent.putExtra("day", dayNext);
                    intent.putExtra("idTrip", String.valueOf(idOfTrip));
                    intent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                    intent.putExtra("arrivalDate", arrivalDateOfTrip);
                    startActivity(intent);
                }
                else if (m.equals("mytriprest")){
                    Class cls = allMarkersMap.get(marker);
                    String dayNext = intentDayHashMap.get(marker);
                    Intent intent = new Intent(MapActivity.this, cls);
                    intent.putExtra("idUser", String.valueOf(idOfUser));
                    intent.putExtra("day", dayNext);
                    intent.putExtra("idTrip", String.valueOf(idOfTrip));
                    intent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                    intent.putExtra("arrivalDate", arrivalDateOfTrip);
                    startActivity(intent);
                }
                else if (m.equals("myacco")){
                    Class cls = allMarkersMap.get(marker);
                    String dayNext = intentDayHashMap.get(marker);
                    Intent intent = new Intent(MapActivity.this, cls);
                    intent.putExtra("idUser", String.valueOf(idOfUser));
                    intent.putExtra("day", dayNext);
                    intent.putExtra("idTrip", String.valueOf(idOfTrip));
                    intent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                    intent.putExtra("arrivalDate", arrivalDateOfTrip);
                    startActivity(intent);
                }
                else if (m.equals("myact")){
                    Class cls = allMarkersMap.get(marker);
                    String dayNext = intentDayHashMap.get(marker);
                    Intent intent = new Intent(MapActivity.this, cls);
                    intent.putExtra("idUser", String.valueOf(idOfUser));
                    intent.putExtra("day", dayNext);
                    intent.putExtra("idTrip", String.valueOf(idOfTrip));
                    intent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                    intent.putExtra("arrivalDate", arrivalDateOfTrip);
                    startActivity(intent);
                }
                else if (m.equals("myrest")){
                    Class cls = allMarkersMap.get(marker);
                    String dayNext = intentDayHashMap.get(marker);
                    Intent intent = new Intent(MapActivity.this, cls);
                    intent.putExtra("idUser", String.valueOf(idOfUser));
                    intent.putExtra("day", dayNext);
                    intent.putExtra("idTrip", String.valueOf(idOfTrip));
                    intent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                    intent.putExtra("arrivalDate", arrivalDateOfTrip);
                    startActivity(intent);
                }
                else if (m.equals("allact")){
                    Class cls = allMarkersMap.get(marker);
                    int id = markerIdItem.get(marker);
                    Intent intent = new Intent(MapActivity.this, cls);
                    intent.putExtra("idUser", String.valueOf(idOfUser));
                    intent.putExtra("idTrip", String.valueOf(idOfTrip));
                    intent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                    intent.putExtra("arrivalDate", arrivalDateOfTrip);
                    intent.putExtra("idAct", String.valueOf(id));
                    startActivity(intent);
                }
                else if (m.equals("allrest")){
                    Class cls = allMarkersMap.get(marker);
                    int id = markerIdItem.get(marker);
                    Intent intent = new Intent(MapActivity.this, cls);
                    intent.putExtra("idUser", String.valueOf(idOfUser));
                    intent.putExtra("idTrip", String.valueOf(idOfTrip));
                    intent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                    intent.putExtra("arrivalDate", arrivalDateOfTrip);
                    intent.putExtra("idRest", String.valueOf(id));
                    startActivity(intent);
                }
                else if (m.equals("allacco")){
                    Class cls = allMarkersMap.get(marker);
                    int id = markerIdItem.get(marker);
                    Intent intent = new Intent(MapActivity.this, cls);
                    intent.putExtra("idUser", String.valueOf(idOfUser));
                    intent.putExtra("idTrip", String.valueOf(idOfTrip));
                    intent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                    intent.putExtra("arrivalDate", arrivalDateOfTrip);
                    intent.putExtra("idAcco", String.valueOf(id));
                    startActivity(intent);
                }
                else if (m.equals("dayacco")){
                    Class cls = allMarkersMap.get(marker);
                    String dayNext = intentDayHashMap.get(marker);
                    Intent intent = new Intent(MapActivity.this, cls);
                    intent.putExtra("idUser", String.valueOf(idOfUser));
                    intent.putExtra("day", dayNext);
                    intent.putExtra("idTrip", String.valueOf(idOfTrip));
                    intent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                    intent.putExtra("arrivalDate", arrivalDateOfTrip);
                    startActivity(intent);
                }
                else if (m.equals("dayact")){
                    Class cls = allMarkersMap.get(marker);
                    String dayNext = intentDayHashMap.get(marker);
                    Intent intent = new Intent(MapActivity.this, cls);
                    intent.putExtra("idUser", String.valueOf(idOfUser));
                    intent.putExtra("day", dayNext);
                    intent.putExtra("idTrip", String.valueOf(idOfTrip));
                    intent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                    intent.putExtra("arrivalDate", arrivalDateOfTrip);
                    startActivity(intent);
                }
                else if (m.equals("dayrest")){
                    Class cls = allMarkersMap.get(marker);
                    String dayNext = intentDayHashMap.get(marker);
                    Intent intent = new Intent(MapActivity.this, cls);
                    intent.putExtra("idUser", String.valueOf(idOfUser));
                    intent.putExtra("day", dayNext);
                    intent.putExtra("idTrip", String.valueOf(idOfTrip));
                    intent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                    intent.putExtra("arrivalDate", arrivalDateOfTrip);
                    startActivity(intent);
                }

            }
        });
    }

    @Override
    public void onBackPressed()
    {
    }



    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
        Toast.makeText(getApplicationContext(), "Please select a filter or a day", Toast.LENGTH_LONG).show();
    }

}

