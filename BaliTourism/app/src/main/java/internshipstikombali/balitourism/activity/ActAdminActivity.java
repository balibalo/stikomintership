package internshipstikombali.balitourism.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.Activity;
import internshipstikombali.balitourism.helper.Database;

public class ActAdminActivity extends AppCompatActivity {

    private static final String TAG = ActAdminActivity.class.getSimpleName();

    Bitmap bitmap;
    ImageView imageView3;
    private ImageButton btnModify;
    private ImageButton btnDelete;
    private int idAct;
    private ImageButton buttonMusicActAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act_admin);

        imageView3 = (ImageView) findViewById(R.id.imageView3Admin);
        btnDelete = (ImageButton) findViewById(R.id.deleteButton3Admin);
        btnModify = (ImageButton) findViewById(R.id.dateButton3Admin);
        buttonMusicActAdmin = (ImageButton) findViewById(R.id.buttonMusicActAdmin);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicActAdmin.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicActAdmin.setBackgroundResource(R.drawable.play);
        }

        buttonMusicActAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicActAdmin.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicActAdmin.setBackgroundResource(R.drawable.pause);
                }
            }
        });

        Intent intent = getIntent();
        idAct = Integer.parseInt(intent.getStringExtra("idAct"));

        Activity activity1 = Database.myActivityDAO.fetchActivityById(idAct);

        String nameActivity1 = activity1.getName();
        String priceActivity1 = "Price : " + activity1.getPrice() + " irp per person";
        double latitudeActivity1 = Double.parseDouble(activity1.getLatitude());
        double longitudeActivity1 = Double.parseDouble(activity1.getLongitude());

        Geocoder geocoder;
        List<Address> addresses = new ArrayList<Address>();
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitudeActivity1, longitudeActivity1, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }



        String addressActivity1;
        if (addresses.size()==0){
            addressActivity1 = "Location : no information";
        }
        else {
            addressActivity1 = "Location : " + addresses.get(0).getAddressLine(0)+", " + addresses.get(0).getLocality();
        }

        String openingHoursActivity1 = "Opening hours : " + activity1.getOpeningHour()+":"+activity1.getOpeningMinute()+"am - "+activity1.getClosingHour()+":"+activity1.getClosingMinute()+"pm";

        String phoneNumberActivity1 = "Phone number : " + activity1.getPhoneNumber();

        String websiteActivity1 = "Website : " + activity1.getWebsite();

        String descriptionActivity1 = "Description : " + activity1.getDescription();

        String urlActivity1 = activity1.getPicture();

        final TextView nameAct1 = (TextView) findViewById(R.id.nameActivityAdmin);
        nameAct1.setText(nameActivity1);

        final TextView priceAct1 = (TextView) findViewById(R.id.priceActivityAdmin);
        priceAct1.setText(priceActivity1);

        final TextView locationAct1 = (TextView) findViewById(R.id.locationActivityAdmin);
        locationAct1.setText(addressActivity1);

        final TextView descriptionAct1 = (TextView) findViewById(R.id.descriptionActivityAdmin);
        descriptionAct1.setText(descriptionActivity1);

        final TextView openingHoursAct1 = (TextView) findViewById(R.id.openingHoursActivityAdmin);
        openingHoursAct1.setText(openingHoursActivity1);

        final TextView phoneAct1 = (TextView) findViewById(R.id.phoneActivityAdmin);
        phoneAct1.setText(phoneNumberActivity1);

        final TextView websiteAct1 = (TextView) findViewById(R.id.websiteActivityAdmin);
        websiteAct1.setText(websiteActivity1);

        new LoadImage().execute(urlActivity1);

        final AlertDialog diaBox = AskOption();

        // delete button Click Event
        btnDelete.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                diaBox.show();
            }
        });

        // modify button Click Event
        btnModify.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                Intent myIntent = new Intent(ActAdminActivity.this, ModifyActAdminActivity.class);
                myIntent.putExtra("idAct", String.valueOf(idAct));
                startActivity(myIntent);
            }
        });

    }

    private class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {
            if(image != null){
                imageView3.setImageBitmap(image);
            }else{

            }
        }
    }

    /**
     * function to delete the activity in mysql db
     * */
    private void deleteActivityAdmin() {
        // Tag used to cancel the request
        String tag_string_req = "req_delete";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETEACTIVITYADMIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    Database.myActivityDAO.deleteActivity(idAct);

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to deleteactivityadmin url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idActivity", String.valueOf(idAct));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private AlertDialog AskOption()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setMessage("Do you really want to delete this activity from the database ?")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        deleteActivityAdmin();
                        Toast.makeText(getApplicationContext(),
                                "The activity has been deleted successfully !", Toast.LENGTH_LONG)
                                .show();
                        Intent myIntent = new Intent(ActAdminActivity.this, AdministratorMenuActivity.class);
                        startActivity(myIntent);
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicActAdmin.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicActAdmin.setBackgroundResource(R.drawable.play);
        }

        buttonMusicActAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicActAdmin.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicActAdmin.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }



}
