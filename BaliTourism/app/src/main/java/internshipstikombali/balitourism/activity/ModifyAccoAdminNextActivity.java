package internshipstikombali.balitourism.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.AccommodationBelongsCategoryAccommodation;
import internshipstikombali.balitourism.helper.Database;

public class ModifyAccoAdminNextActivity extends AppCompatActivity {

    private static final String TAG = ModifyAccoAdminNextActivity.class.getSimpleName();

    private Button buttonAddNextAccoUpdate;
    private CheckBox guesthouseAccoUpdate;
    private CheckBox homestayAccoUpdate;
    private CheckBox villaAccoUpdate;
    private CheckBox apartementAccoUpdate;
    private CheckBox hotel1AccoUpdate;
    private CheckBox hotel2AccoUpdate;
    private CheckBox hotel3AccoUpdate;
    private CheckBox hotel4AccoUpdate;
    private CheckBox hotel5AccoUpdate;
    private int cpt;
    private ImageButton buttonMusicAddAccoAdminNextUpdate;


    private int idAccommodation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_acco_admin_next);

        buttonAddNextAccoUpdate = (Button) findViewById(R.id.buttonAddNextAccoUpdate);
        guesthouseAccoUpdate = (CheckBox) findViewById(R.id.guesthouseAccoUpdate);
        homestayAccoUpdate = (CheckBox) findViewById(R.id.homestayAccoUpdate);
        villaAccoUpdate = (CheckBox) findViewById(R.id.villaAccoUpdate);
        apartementAccoUpdate = (CheckBox) findViewById(R.id.apartementAccoUpdate);
        hotel1AccoUpdate = (CheckBox) findViewById(R.id.hotel1AccoUpdate);
        hotel2AccoUpdate = (CheckBox) findViewById(R.id.hotel2AccoUpdate);
        hotel3AccoUpdate = (CheckBox) findViewById(R.id.hotel3AccoUpdate);
        hotel4AccoUpdate = (CheckBox) findViewById(R.id.hotel4AccoUpdate);
        hotel5AccoUpdate = (CheckBox) findViewById(R.id.hotel5AccoUpdate);
        buttonMusicAddAccoAdminNextUpdate = (ImageButton) findViewById(R.id.buttonMusicAddAccoAdminNextUpdate);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddAccoAdminNextUpdate.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddAccoAdminNextUpdate.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddAccoAdminNextUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddAccoAdminNextUpdate.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddAccoAdminNextUpdate.setBackgroundResource(R.drawable.pause);
                }
            }
        });

        Intent intent = getIntent();
        String idAcco = intent.getStringExtra("idAccommodation");
        idAccommodation = Integer.parseInt(idAcco);

        String category = Database.myAccommodationBelongsCategoryAccommodationDAO.getCategoryAccommodationByIdAccommodation(idAccommodation);

        if (category.equals("guesthouse")){
            guesthouseAccoUpdate.setChecked(true);
        }
        if (category.equals("homestay")){
            homestayAccoUpdate.setChecked(true);
        }
        if (category.equals("villa")){
            villaAccoUpdate.setChecked(true);
        }
        if (category.equals("apartement")){
            apartementAccoUpdate.setChecked(true);
        }
        if (category.equals("hotel1")){
            hotel1AccoUpdate.setChecked(true);
        }
        if (category.equals("hotel2")){
            hotel2AccoUpdate.setChecked(true);
        }
        if (category.equals("hotel3")){
            hotel3AccoUpdate.setChecked(true);
        }
        if (category.equals("hotel4")){
            hotel4AccoUpdate.setChecked(true);
        }
        if (category.equals("hotel5")){
            hotel5AccoUpdate.setChecked(true);
        }

        deleteAccommodationBelongsCategory();

        // Send button Click Event
        buttonAddNextAccoUpdate.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {

                cpt = 0;
                if (guesthouseAccoUpdate.isChecked()) {
                    cpt = cpt +1;
                }
                if (homestayAccoUpdate.isChecked()) {
                    cpt = cpt +1;
                }
                if (villaAccoUpdate.isChecked()) {
                    cpt = cpt +1;
                }
                if (apartementAccoUpdate.isChecked()) {
                    cpt = cpt +1;
                }
                if (hotel1AccoUpdate.isChecked()) {
                    cpt = cpt +1;
                }
                if (hotel2AccoUpdate.isChecked()) {
                    cpt = cpt +1;
                }
                if (hotel3AccoUpdate.isChecked()) {
                    cpt = cpt +1;
                }
                if (hotel4AccoUpdate.isChecked()) {
                    cpt = cpt +1;
                }
                if (hotel5AccoUpdate.isChecked()) {
                    cpt = cpt +1;
                }

                if (cpt == 1) {
                    if (guesthouseAccoUpdate.isChecked()) {
                        String cat = "guesthouse";
                        addAccommodationBelongsCategory(cat);
                    }
                    if (homestayAccoUpdate.isChecked()) {
                        String cat = "homestay";
                        addAccommodationBelongsCategory(cat);
                    }
                    if (villaAccoUpdate.isChecked()) {
                        String cat = "villa";
                        addAccommodationBelongsCategory(cat);
                    }
                    if (apartementAccoUpdate.isChecked()) {
                        String cat = "apartement";
                        addAccommodationBelongsCategory(cat);
                    }
                    if (hotel1AccoUpdate.isChecked()) {
                        String cat = "hotel1";
                        addAccommodationBelongsCategory(cat);
                    }
                    if (hotel2AccoUpdate.isChecked()) {
                        String cat = "hotel2";
                        addAccommodationBelongsCategory(cat);
                    }
                    if (hotel3AccoUpdate.isChecked()) {
                        String cat = "hotel3";
                        addAccommodationBelongsCategory(cat);
                    }
                    if (hotel4AccoUpdate.isChecked()) {
                        String cat = "hotel4";
                        addAccommodationBelongsCategory(cat);
                    }
                    if (hotel5AccoUpdate.isChecked()) {
                        String cat = "hotel5";
                        addAccommodationBelongsCategory(cat);
                    }

                    Toast.makeText(getApplicationContext(), "The accommodation has been updated successfully !", Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(ModifyAccoAdminNextActivity.this, AdministratorMenuActivity.class);
                    startActivity(intent);
                    finish();
                }
                else if (cpt>1){
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please select only 1 category.", Toast.LENGTH_LONG)
                            .show();
                }
                else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please select 1 category.", Toast.LENGTH_LONG)
                            .show();
                }

            }

        });

    }

    /**
     * Function to delete the former categories of the accommodation in MySQL database to newtrip url
     * */
    private void deleteAccommodationBelongsCategory() {
        // Tag used to cancel the request
        String tag_string_req = "req_saveForm";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETEACCOMMODATIONBELONGSCATEGORY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                        // delete row in AccommodationBelongCat table
                        Database.myAccommodationBelongsCategoryAccommodationDAO.deleteAccommodationBelongsCategoryAccommodation(idAccommodation);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idAccommodation", String.valueOf(idAccommodation));

                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    /**
     * Function to store the categories of the new accommodation in MySQL database to newtrip url
     * */
    private void addAccommodationBelongsCategory(final String category) {
        // Tag used to cancel the request
        String tag_string_req = "req_saveForm";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDACCOMMODATIONBELONGSCATEGORY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                        // Inserting row in AccommodationBelongCat table
                        Database.myAccommodationBelongsCategoryAccommodationDAO.addAccommodationBelongsCategoryAccommodation(new AccommodationBelongsCategoryAccommodation(idAccommodation,category));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idAccommodation", String.valueOf(idAccommodation));
                params.put("nameCategoryAccommodation", category);

                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddAccoAdminNextUpdate.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddAccoAdminNextUpdate.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddAccoAdminNextUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddAccoAdminNextUpdate.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddAccoAdminNextUpdate.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }

}
