package internshipstikombali.balitourism.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.ActivityBelongsCategoryActivity;
import internshipstikombali.balitourism.helper.Database;

public class ModifyActAdminNextActivity extends AppCompatActivity {

    private static final String TAG = ModifyActAdminNextActivity.class.getSimpleName();

    private Button buttonAddNextActUpdate;
    private CheckBox culturalActUpdate;
    private CheckBox familyActUpdate;
    private CheckBox natureActUpdate;
    private CheckBox relaxActUpdate;
    private CheckBox romanticActUpdate;
    private CheckBox sportActUpdate;
    private int cpt;
    private ImageButton buttonMusicAddActAdminNextUpdate;

    private int idActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_act_admin_next);
        buttonMusicAddActAdminNextUpdate = (ImageButton) findViewById(R.id.buttonMusicAddActAdminNextUpdate);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddActAdminNextUpdate.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddActAdminNextUpdate.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddActAdminNextUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddActAdminNextUpdate.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddActAdminNextUpdate.setBackgroundResource(R.drawable.pause);
                }
            }
        });

        buttonAddNextActUpdate = (Button) findViewById(R.id.buttonAddNextActUpdate);
        culturalActUpdate = (CheckBox) findViewById(R.id.culturalActUpdate);
        familyActUpdate = (CheckBox) findViewById(R.id.familyActUpdate);
        natureActUpdate = (CheckBox) findViewById(R.id.natureActUpdate);
        relaxActUpdate = (CheckBox) findViewById(R.id.relaxActUpdate);
        romanticActUpdate = (CheckBox) findViewById(R.id.romanticActUpdate);
        sportActUpdate = (CheckBox) findViewById(R.id.sportActUpdate);

        Intent intent = getIntent();
        String idAct = intent.getStringExtra("idActivity");
        idActivity = Integer.parseInt(idAct);

        ArrayList categories = Database.myActivityBelongsCategoryActivityDAO.fetchAllCategoryActivityByIdActivity(idActivity);

        for (int i=0;i<categories.size();i++){
            if (String.valueOf(categories.get(i)).equals("cultural")){
                culturalActUpdate.setChecked(true);
            }
            if (String.valueOf(categories.get(i)).equals("family")){
                familyActUpdate.setChecked(true);
            }
            if (String.valueOf(categories.get(i)).equals("nature")){
                natureActUpdate.setChecked(true);
            }
            if (String.valueOf(categories.get(i)).equals("relax")){
                relaxActUpdate.setChecked(true);
            }
            if (String.valueOf(categories.get(i)).equals("romantic")){
                romanticActUpdate.setChecked(true);
            }
            if (String.valueOf(categories.get(i)).equals("sport")){
                sportActUpdate.setChecked(true);
            }
        }



        deleteActivityBelongsCategory();

        // Send button Click Event
        buttonAddNextActUpdate.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {

                cpt = 0;
                if (culturalActUpdate.isChecked()) {
                    cpt = cpt +1;
                }
                if (familyActUpdate.isChecked()) {
                    cpt = cpt +1;
                }
                if (natureActUpdate.isChecked()) {
                    cpt = cpt +1;
                }
                if (relaxActUpdate.isChecked()) {
                    cpt = cpt +1;
                }
                if (romanticActUpdate.isChecked()) {
                    cpt = cpt +1;
                }
                if (sportActUpdate.isChecked()) {
                    cpt = cpt +1;
                }

                if (cpt >= 1) {
                    if (culturalActUpdate.isChecked()) {
                        String cat = "cultural";
                        addActivityBelongsCategory(cat);
                    }
                    if (familyActUpdate.isChecked()) {
                        String cat = "family";
                        addActivityBelongsCategory(cat);
                    }
                    if (natureActUpdate.isChecked()) {
                        String cat = "nature";
                        addActivityBelongsCategory(cat);
                    }
                    if (relaxActUpdate.isChecked()) {
                        String cat = "relax";
                        addActivityBelongsCategory(cat);
                    }
                    if (romanticActUpdate.isChecked()) {
                        String cat = "romantic";
                        addActivityBelongsCategory(cat);
                    }
                    if (sportActUpdate.isChecked()) {
                        String cat = "sport";
                        addActivityBelongsCategory(cat);
                    }

                    Toast.makeText(getApplicationContext(), "The activity has been updated successfully !", Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(ModifyActAdminNextActivity.this, AdministratorMenuActivity.class);
                    startActivity(intent);
                    finish();
                }
                else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please select at least 1 category.", Toast.LENGTH_LONG)
                            .show();
                }

            }

        });


    }

    /**
     * Function to store the categories of the new activity in MySQL database to newtrip url
     * */
    private void addActivityBelongsCategory(final String category) {
        // Tag used to cancel the request
        String tag_string_req = "req_saveForm";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDACTIVITYBELONGSCATEGORY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                        // Inserting row in ActivityBelongCat table
                        Database.myActivityBelongsCategoryActivityDAO.addActivityBelongsCategoryActivity(new ActivityBelongsCategoryActivity(idActivity,category));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idActivity", String.valueOf(idActivity));
                params.put("nameCategoryActivity", category);

                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    /**
     * Function to delete the former categories of the activity in MySQL database to newtrip url
     * */
    private void deleteActivityBelongsCategory() {
        // Tag used to cancel the request
        String tag_string_req = "req_saveForm";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETEACTIVITYBELONGSCATEGORY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                        // deleting row in ActivityBelongCat table
                        Database.myActivityBelongsCategoryActivityDAO.deleteActivityBelongsCategoryActivity(idActivity);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idActivity", String.valueOf(idActivity));

                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddActAdminNextUpdate.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddActAdminNextUpdate.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddActAdminNextUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddActAdminNextUpdate.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddActAdminNextUpdate.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }

}
