package internshipstikombali.balitourism.activity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.Activity;
import internshipstikombali.balitourism.business.TripContainsActivity;
import internshipstikombali.balitourism.helper.Database;

public class ActivityActivity extends AppCompatActivity {

    private static final String TAG = ActivityActivity.class.getSimpleName();

    Bitmap bitmap;
    ImageView imageView3;
    ImageView imageView4;
    private ImageButton btnDelete1;
    private ImageButton btnDate1;
    private ImageButton btnDelete2;
    private ImageButton btnDate2;

    private int idOfActivity1;
    private int idOfActivity2;

    private int idOfTrip;
    private String day;
    private int idOfUser;
    private int durationOfTrip;
    private String formatted;
    private String arrivalDateOfTrip;
    private String arrivalTime;
    private String newArrivalTime;
    private int idActFormer;
    private int day2;
    private int month;
    private int year;
    private ImageButton buttonMusicActivity;



    private DatePickerDialog datePickerDialog;
    private DatePickerDialog datePickerDialog2;

    List<TripContainsActivity> reqActivity;

    AlertDialog diaBox;
    AlertDialog diaBox2nd;
    AlertDialog diaBox2;
    AlertDialog diaBox3;
    AlertDialog diaBox4;
    AlertDialog diaBox4nd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity);

        imageView3 = (ImageView) findViewById(R.id.imageView3);
        imageView4 = (ImageView) findViewById(R.id.imageView4);

        btnDelete1 = (ImageButton) findViewById(R.id.deleteButton3);
        btnDate1 = (ImageButton) findViewById(R.id.dateButton3);
        btnDelete2 = (ImageButton) findViewById(R.id.deleteButton4);
        btnDate2 = (ImageButton) findViewById(R.id.dateButton4);
        buttonMusicActivity = (ImageButton) findViewById(R.id.buttonMusicActivity);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicActivity.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicActivity.setBackgroundResource(R.drawable.play);
        }

        buttonMusicActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicActivity.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicActivity.setBackgroundResource(R.drawable.pause);
                }
            }
        });




        Intent intent = getIntent();
        idOfUser = Integer.parseInt(intent.getStringExtra("idUser"));
        day = intent.getStringExtra("day");
        idOfTrip = Integer.parseInt(intent.getStringExtra("idTrip"));
        durationOfTrip = Integer.parseInt(intent.getStringExtra("durationTrip"));
        arrivalDateOfTrip = intent.getStringExtra("arrivalDate");

        // Parametres : (int)idUser, (string)day
        // j'ai besoin des deux activities du jour

        diaBox = AskOption();
        diaBox2nd = AskOption2nd();
        diaBox2 = AskOption2();
        diaBox3 = AskOption3();
        diaBox4 = AskOption4();
        diaBox4nd = AskOption4nd();

        idOfActivity1 = -1;
        reqActivity = Database.myTripContainsActivityDAO.fetchAllTripContainsActivity();
        for (int i = 0; i<reqActivity.size(); i++){
            if ((reqActivity.get(i).getIdTrip() == idOfTrip) && reqActivity.get(i).getArrivalDate().equals(day) && reqActivity.get(i).getArrivalTime().equals("morning")){
                idOfActivity1 = reqActivity.get(i).getIdActivity();
            }
        }

        if (idOfActivity1 == -1){
            final TextView nameAct1 = (TextView) findViewById(R.id.nameActivity);
            final TextView priceAct1 = (TextView) findViewById(R.id.priceActivity);
            final TextView locationAct1 = (TextView) findViewById(R.id.locationActivity);
            final TextView descriptionAct1 = (TextView) findViewById(R.id.descriptionActivity);
            final TextView openingHoursAct1 = (TextView) findViewById(R.id.openingHoursActivity);
            final TextView phoneAct1 = (TextView) findViewById(R.id.phoneActivity);
            final TextView websiteAct1 = (TextView) findViewById(R.id.websiteActivity);

            nameAct1.setText("There is no activity provided for this day in the morning.");
            imageView3.setVisibility(View.GONE);
            btnDelete1.setVisibility(View.GONE);
            btnDate1.setVisibility(View.GONE);
            priceAct1.setVisibility(View.GONE);
            locationAct1.setVisibility(View.GONE);
            descriptionAct1.setVisibility(View.GONE);
            openingHoursAct1.setVisibility(View.GONE);
            phoneAct1.setVisibility(View.GONE);
            websiteAct1.setVisibility(View.GONE);
        }
        else {
            Activity activity1 = Database.myActivityDAO.fetchActivityById(idOfActivity1);
            String nameActivity1 = activity1.getName();
            String priceActivity1 = "Price : " + activity1.getPrice() + " irp per person";
            double latitudeActivity1 = Double.parseDouble(activity1.getLatitude());
            double longitudeActivity1 = Double.parseDouble(activity1.getLongitude());

            Geocoder geocoder;
            List<Address> addresses = new ArrayList<Address>();
            geocoder = new Geocoder(this, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(latitudeActivity1, longitudeActivity1, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String addressActivity1;
            if (addresses.size()==0){
                addressActivity1 = "Location : no information";
            }
            else {
                addressActivity1 = "Location : " + addresses.get(0).getAddressLine(0)+", " + addresses.get(0).getLocality();
            }

            String openingHoursActivity1 = "Opening hours : " + activity1.getOpeningHour()+":"+activity1.getOpeningMinute()+"am - "+activity1.getClosingHour()+":"+activity1.getClosingMinute()+"pm";

            String phoneNumberActivity1 = "Phone number : " + activity1.getPhoneNumber();

            String websiteActivity1 = "Website : " + activity1.getWebsite();

            String descriptionActivity1 = "Description : " + activity1.getDescription();

            String urlActivity1 = activity1.getPicture();

            final TextView nameAct1 = (TextView) findViewById(R.id.nameActivity);
            nameAct1.setText(nameActivity1);

            final TextView priceAct1 = (TextView) findViewById(R.id.priceActivity);
            priceAct1.setText(priceActivity1);

            final TextView locationAct1 = (TextView) findViewById(R.id.locationActivity);
            locationAct1.setText(addressActivity1);

            final TextView descriptionAct1 = (TextView) findViewById(R.id.descriptionActivity);
            descriptionAct1.setText(descriptionActivity1);

            final TextView openingHoursAct1 = (TextView) findViewById(R.id.openingHoursActivity);
            openingHoursAct1.setText(openingHoursActivity1);

            final TextView phoneAct1 = (TextView) findViewById(R.id.phoneActivity);
            phoneAct1.setText(phoneNumberActivity1);

            final TextView websiteAct1 = (TextView) findViewById(R.id.websiteActivity);
            websiteAct1.setText(websiteActivity1);

            new LoadImage().execute(urlActivity1);

            // delete button Click Event
            btnDelete1.setOnClickListener(new View.OnClickListener()  {
                public void onClick(View view) {
                    diaBox.show();
                }
            });

            String[] partsOfDate = day.split("/");
            day2 = Integer.parseInt(partsOfDate[0]);
            month = Integer.parseInt(partsOfDate[1]);
            year = Integer.parseInt(partsOfDate[2]);

            // date button Click Event
            btnDate1.setOnClickListener(new View.OnClickListener()  {
                public void onClick(View view) {
                    //showStartDateDialog(view);
                    datePickerDialog.setMessage("Change the date");
                    datePickerDialog.updateDate(year, month - 1, day2);
                    datePickerDialog.show();
                }
            });

            Calendar newCalendar = Calendar.getInstance();
            datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    SimpleDateFormat format1 = new SimpleDateFormat("d/M/yyyy");
                    formatted = format1.format(newDate.getTime());
                    SimpleDateFormat sdf = new SimpleDateFormat("d/M/yyyy");

                    Date startTrip = null;
                    try {
                        startTrip = sdf.parse(arrivalDateOfTrip);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    long startTripDate = startTrip.getTime();

                    Date endTrip = null;
                    try {
                        endTrip = sdf.parse(arrivalDateOfTrip);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    long endTripDate = endTrip.getTime()+(86400000*durationOfTrip);

                    Date newDateTrip = null;
                    try {
                        newDateTrip = sdf.parse(formatted);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    long newTripDate = newDateTrip.getTime();

                    if (!(newTripDate>=startTripDate && newTripDate<endTripDate)){
                        diaBox3.show();
                    }
                    else {
                        diaBox4.show();
                    }
                }

            },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        }

        idOfActivity2 = -1;
        for (int i = 0; i<reqActivity.size(); i++){
            if ((reqActivity.get(i).getIdTrip() == idOfTrip) && reqActivity.get(i).getArrivalDate().equals(day) && reqActivity.get(i).getArrivalTime().equals("afternoon")){
                idOfActivity2 = reqActivity.get(i).getIdActivity();
            }
        }

        if (idOfActivity2 == -1){
            final TextView nameAct2 = (TextView) findViewById(R.id.nameActivity2);
            final TextView priceAct2 = (TextView) findViewById(R.id.priceActivity2);
            final TextView locationAct2 = (TextView) findViewById(R.id.locationActivity2);
            final TextView descriptionAct2 = (TextView) findViewById(R.id.descriptionActivity2);
            final TextView openingHoursAct2 = (TextView) findViewById(R.id.openingHoursActivity2);
            final TextView phoneAct2 = (TextView) findViewById(R.id.phoneActivity2);
            final TextView websiteAct2 = (TextView) findViewById(R.id.websiteActivity2);

            nameAct2.setText("There is no activity provided for this day in the afternoon.");
            imageView4.setVisibility(View.GONE);
            btnDelete2.setVisibility(View.GONE);
            btnDate2.setVisibility(View.GONE);
            priceAct2.setVisibility(View.GONE);
            locationAct2.setVisibility(View.GONE);
            descriptionAct2.setVisibility(View.GONE);
            openingHoursAct2.setVisibility(View.GONE);
            phoneAct2.setVisibility(View.GONE);
            websiteAct2.setVisibility(View.GONE);
        }
        else {
            Activity activity2 = Database.myActivityDAO.fetchActivityById(idOfActivity2);
            String nameActivity2 = activity2.getName();
            String priceActivity2 = "Price : " + activity2.getPrice() + " irp per person";
            double latitudeActivity2 = Double.parseDouble(activity2.getLatitude());
            double longitudeActivity2 = Double.parseDouble(activity2.getLongitude());

            Geocoder geocoder2;
            List<Address> addresses2 = new ArrayList<Address>();
            geocoder2 = new Geocoder(this, Locale.getDefault());

            try {
                addresses2 = geocoder2.getFromLocation(latitudeActivity2, longitudeActivity2, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String addressActivity2;
            if (addresses2.size()==0){
                addressActivity2 = "Location : no information";
            }
            else {
                addressActivity2 = "Location : " + addresses2.get(0).getAddressLine(0)+", " + addresses2.get(0).getLocality();
            }

            String openingHoursActivity2 = "Opening hours : " + activity2.getOpeningHour()+":"+activity2.getOpeningMinute()+"am - "+activity2.getClosingHour()+":"+activity2.getClosingMinute()+"pm";

            String phoneNumberActivity2 = "Phone number : " + activity2.getPhoneNumber();

            String websiteActivity2 = "Website : " + activity2.getWebsite();

            String descriptionActivity2 = "Description : " + activity2.getDescription();

            String urlActivity2 = activity2.getPicture();

            final TextView nameAct2 = (TextView) findViewById(R.id.nameActivity2);
            nameAct2.setText(nameActivity2);

            final TextView priceAct2 = (TextView) findViewById(R.id.priceActivity2);
            priceAct2.setText(priceActivity2);

            final TextView locationAct2 = (TextView) findViewById(R.id.locationActivity2);
            locationAct2.setText(addressActivity2);

            final TextView descriptionAct2 = (TextView) findViewById(R.id.descriptionActivity2);
            descriptionAct2.setText(descriptionActivity2);

            final TextView openingHoursAct2 = (TextView) findViewById(R.id.openingHoursActivity2);
            openingHoursAct2.setText(openingHoursActivity2);

            final TextView phoneAct2 = (TextView) findViewById(R.id.phoneActivity2);
            phoneAct2.setText(phoneNumberActivity2);

            final TextView websiteAct2 = (TextView) findViewById(R.id.websiteActivity2);
            websiteAct2.setText(websiteActivity2);

            if (openingHoursActivity2==null){
                openingHoursAct2.setVisibility(View.GONE);
            }
            if (phoneNumberActivity2==null){
                phoneAct2.setVisibility(View.GONE);
            }
            if (websiteActivity2==null){
                websiteAct2.setVisibility(View.GONE);
            }
            if (urlActivity2==null){
                imageView4.setVisibility(View.GONE);
            }
            else {
                new LoadImage2().execute(urlActivity2);
            }

            // delete button Click Event
            btnDelete2.setOnClickListener(new View.OnClickListener()  {
                public void onClick(View view) {
                    diaBox2nd.show();
                }
            });


            // date button Click Event
            btnDate2.setOnClickListener(new View.OnClickListener()  {
                public void onClick(View view) {
                    //showStartDateDialog(view);
                    datePickerDialog2.setMessage("Change the date");
                    datePickerDialog2.show();
                }
            });

            Calendar newCalendar = Calendar.getInstance();
            datePickerDialog2 = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    SimpleDateFormat format1 = new SimpleDateFormat("d/M/yyyy");
                    formatted = format1.format(newDate.getTime());
                    SimpleDateFormat sdf = new SimpleDateFormat("d/M/yyyy");

                    Date startTrip = null;
                    try {
                        startTrip = sdf.parse(arrivalDateOfTrip);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    long startTripDate = startTrip.getTime();

                    Date endTrip = null;
                    try {
                        endTrip = sdf.parse(arrivalDateOfTrip);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    long endTripDate = endTrip.getTime()+(86400000*durationOfTrip);

                    Date newDateTrip = null;
                    try {
                        newDateTrip = sdf.parse(formatted);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    long newTripDate = newDateTrip.getTime();

                    if (!(newTripDate>=startTripDate && newTripDate<endTripDate)){
                        diaBox3.show();
                    }
                    else {
                        diaBox4nd.show();
                    }
                }

            },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        }

    }

    @Override
    public void onResume(){
        super.onResume();
        idOfActivity1 = -1;
        idOfActivity2 = -1;
        reqActivity = Database.myTripContainsActivityDAO.fetchAllTripContainsActivity();
        for (int i = 0; i<reqActivity.size(); i++){
            if ((reqActivity.get(i).getIdTrip() == idOfTrip) && reqActivity.get(i).getArrivalDate().equals(day) && reqActivity.get(i).getArrivalTime().equals("morning")){
                idOfActivity1 = reqActivity.get(i).getIdActivity();
            }
        }
        for (int i = 0; i<reqActivity.size(); i++){
            if ((reqActivity.get(i).getIdTrip() == idOfTrip) && reqActivity.get(i).getArrivalDate().equals(day) && reqActivity.get(i).getArrivalTime().equals("afternoon")){
                idOfActivity2 = reqActivity.get(i).getIdActivity();
            }
        }
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicActivity.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicActivity.setBackgroundResource(R.drawable.play);
        }

        buttonMusicActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicActivity.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicActivity.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }

    private class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {
            if(image != null){
                imageView3.setImageBitmap(image);
            }else{

            }
        }
    }

    private class LoadImage2 extends AsyncTask<String, String, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {
            if(image != null){
                imageView4.setImageBitmap(image);
            }else{

            }
        }
    }

    /**
     * function to delete the activity from the trip in mysql db
     * */
    private void deleteActivity() {
        // Tag used to cancel the request
        String tag_string_req = "req_delete";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETEACTIVITY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                        // delete row in tripContainsActivity table
                        if (arrivalTime.equals("morning")){
                            Database.myTripContainsActivityDAO.deleteTripContainsActivity(idOfTrip,idOfActivity1,day,arrivalTime);
                        }
                        else {
                            Database.myTripContainsActivityDAO.deleteTripContainsActivity(idOfTrip,idOfActivity2,day,arrivalTime);
                        }

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to deletetransport url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idOfTrip));
                params.put("arrivalDate", day);
                params.put("arrivalTime", arrivalTime);
                if (arrivalTime.equals("morning")){
                    params.put("idActivity", String.valueOf(idOfActivity1));
                }
                else {
                    params.put("idActivity", String.valueOf(idOfActivity2));
                }
                    return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to delete the former activity from the trip in mysql db
     * */
    private void deleteFormerActivity() {
        // Tag used to cancel the request
        String tag_string_req = "req_delete";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETEACTIVITY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                        // delete row in tripContainsActivity table
                        Database.myTripContainsActivityDAO.deleteTripContainsActivity(idOfTrip,idActFormer,formatted, newArrivalTime);

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to deletetransport url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idOfTrip));
                params.put("arrivalDate", formatted);
                params.put("arrivalTime", newArrivalTime);
                params.put("idActivity", String.valueOf(idActFormer));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to update the activity from the trip in mysql db
     * */
    private void updateActivity() {
        // Tag used to cancel the request
        String tag_string_req = "req_update";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATEACTIVITY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    // Check for error node in json
                        // update row in tripContainsActivity table
                        Database.myTripContainsActivityDAO.updateTripContainsActivity(idOfTrip,day,formatted, arrivalTime, newArrivalTime);

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to updatetransport url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idOfTrip));
                params.put("arrivalDate", day);
                params.put("newDate", formatted);
                params.put("arrivalTime", arrivalTime);
                params.put("newTime", newArrivalTime);


                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to add the activity from the trip in mysql db
     * */
    private void addActivity() {
        // Tag used to cancel the request
        String tag_string_req = "req_add";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDACTIVITY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                        if (arrivalTime.equals("morning")){
                            // add row in tripContainsActivity table
                            Database.myTripContainsActivityDAO.addTripContainsActivity(new TripContainsActivity(idOfTrip,idOfActivity1,formatted, newArrivalTime));
                        }
                        else {
                            Database.myTripContainsActivityDAO.addTripContainsActivity(new TripContainsActivity(idOfTrip,idOfActivity2,formatted, newArrivalTime));
                        }

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to addactivity url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idOfTrip));
                if (arrivalTime.equals("morning")){
                    params.put("idActivity", String.valueOf(idOfActivity1));
                }
                else {
                    params.put("idActivity", String.valueOf(idOfActivity2));
                }
                params.put("arrivalDate", formatted);
                params.put("arrivalTime", newArrivalTime);

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to check if there is already an activity at this date in mysql db
     * return true if there is already an activity
     * */
    private boolean checkActivity() {
        reqActivity = Database.myTripContainsActivityDAO.fetchAllTripContainsActivity();
        for (int i = 0; i<reqActivity.size(); i++){
            if ((reqActivity.get(i).getIdTrip() == idOfTrip) && reqActivity.get(i).getArrivalDate().equals(formatted) && reqActivity.get(i).getArrivalTime().equals(newArrivalTime)){
                idActFormer = reqActivity.get(i).getIdActivity();
                return true;
            }
        }
        return false;
    }

    private AlertDialog AskOption()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setMessage("Do you really want to delete this activity for this day ?")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        arrivalTime = "morning";
                        deleteActivity();
                        Toast.makeText(getApplicationContext(),
                                "The activity has been deleted successfully !", Toast.LENGTH_LONG)
                                .show();
                        Intent myIntent = new Intent(ActivityActivity.this, HomeActivity.class);
                        myIntent.putExtra("idUser", String.valueOf(idOfUser));
                        myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                        myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                        myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                        startActivity(myIntent);
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    private AlertDialog AskOption2nd()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setMessage("Do you really want to delete this activity for this day ?")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        arrivalTime = "afternoon";
                        deleteActivity();
                        Toast.makeText(getApplicationContext(),
                                "The activity has been deleted successfully !", Toast.LENGTH_LONG)
                                .show();
                        Intent myIntent = new Intent(ActivityActivity.this, HomeActivity.class);
                        myIntent.putExtra("idUser", String.valueOf(idOfUser));
                        myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                        myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                        myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                        startActivity(myIntent);
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    private AlertDialog AskOption2()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                //set message, title, and icon
                .setMessage("There is already an activity at this date, do you want to change it by this one ?")
                .setPositiveButton("Change", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        deleteFormerActivity();
                        deleteActivity();
                        addActivity();
                        Toast.makeText(getApplicationContext(),
                                "The date of the activity has been updated successfully !", Toast.LENGTH_LONG)
                                .show();
                        Intent myIntent = new Intent(ActivityActivity.this, HomeActivity.class);
                        myIntent.putExtra("idUser", String.valueOf(idOfUser));
                        myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                        myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                        myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                        startActivity(myIntent);
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    private AlertDialog AskOption3()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setMessage("You must choose a day according to the dates of your trip.")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    private AlertDialog AskOption4()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                //set message, title, and icon
                .setPositiveButton("Morning", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        arrivalTime = "morning";
                        newArrivalTime = "morning";
                        if (checkActivity()) {
                            diaBox2.show();
                        }
                        else {
                            updateActivity();
                            Toast.makeText(getApplicationContext(),
                                    "The activity has been updated successfully !", Toast.LENGTH_LONG)
                                    .show();
                            Intent myIntent = new Intent(ActivityActivity.this, HomeActivity.class);
                            myIntent.putExtra("idUser", String.valueOf(idOfUser));
                            myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                            myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                            myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                            startActivity(myIntent);
                            finish();
                        }
                    }
                })
                .setNegativeButton("Afternoon", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        arrivalTime = "morning";
                        newArrivalTime = "afternoon";
                        if (checkActivity()) {
                            diaBox2.show();
                        }
                        else {
                            updateActivity();
                            Toast.makeText(getApplicationContext(),
                                    "The activity has been updated successfully !", Toast.LENGTH_LONG)
                                    .show();
                            Intent myIntent = new Intent(ActivityActivity.this, HomeActivity.class);
                            myIntent.putExtra("idUser", String.valueOf(idOfUser));
                            myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                            myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                            myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                            startActivity(myIntent);
                            finish();
                        }
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    private AlertDialog AskOption4nd()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                //set message, title, and icon
                .setPositiveButton("Morning", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        arrivalTime = "afternoon";
                        newArrivalTime = "morning";
                        if (checkActivity()) {
                            diaBox2.show();
                        }
                        else {
                            updateActivity();
                            Toast.makeText(getApplicationContext(),
                                    "The activity has been updated successfully !", Toast.LENGTH_LONG)
                                    .show();
                            Intent myIntent = new Intent(ActivityActivity.this, HomeActivity.class);
                            myIntent.putExtra("idUser", String.valueOf(idOfUser));
                            myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                            myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                            myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                            startActivity(myIntent);
                            finish();
                        }
                    }
                })
                .setNegativeButton("Afternoon", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        arrivalTime = "afternoon";
                        newArrivalTime = "afternoon";
                        if (checkActivity()) {
                            diaBox2.show();
                        }
                        else {
                            updateActivity();
                            Toast.makeText(getApplicationContext(),
                                    "The activity has been updated successfully !", Toast.LENGTH_LONG)
                                    .show();
                            Intent myIntent = new Intent(ActivityActivity.this, HomeActivity.class);
                            myIntent.putExtra("idUser", String.valueOf(idOfUser));
                            myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                            myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                            myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                            startActivity(myIntent);
                            finish();
                        }
                    }
                })
                .create();
        return myQuittingDialogBox;

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

}
