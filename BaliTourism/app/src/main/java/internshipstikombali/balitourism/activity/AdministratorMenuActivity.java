package internshipstikombali.balitourism.activity;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TabHost;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppController;

public class AdministratorMenuActivity extends TabActivity {
    private ImageButton buttonMusicAdministratorMenu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_administrator_menu);

        buttonMusicAdministratorMenu = (ImageButton) findViewById(R.id.buttonMusicAdministratorMenu);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAdministratorMenu.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAdministratorMenu.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAdministratorMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAdministratorMenu.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAdministratorMenu.setBackgroundResource(R.drawable.pause);
                }
            }
        });


        TabHost mTabHost = getTabHost();

        mTabHost.addTab(mTabHost.newTabSpec("accommodations").setIndicator("Accommodations").setContent(new Intent(AdministratorMenuActivity.this, AllAccommodationAdminActivity.class)));
        mTabHost.addTab(mTabHost.newTabSpec("activities").setIndicator("Activities").setContent(new Intent(AdministratorMenuActivity.this, AllActivityAdminActivity.class)));
        mTabHost.addTab(mTabHost.newTabSpec("restaurants").setIndicator("Restaurants").setContent(new Intent(AdministratorMenuActivity.this, AllRestaurantAdminActivity.class)));
        mTabHost.addTab(mTabHost.newTabSpec("transports").setIndicator("Transports").setContent(new Intent(AdministratorMenuActivity.this, AllTransportAdminActivity.class)));
        mTabHost.addTab(mTabHost.newTabSpec("addAdmin").setIndicator("Add Admin").setContent(new Intent(AdministratorMenuActivity.this, AddAdminActivity.class)));
        mTabHost.addTab(mTabHost.newTabSpec("logout").setIndicator("Logout").setContent(new Intent(AdministratorMenuActivity.this, LogoutActivity.class)));


        mTabHost.setCurrentTab(0);
    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAdministratorMenu.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAdministratorMenu.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAdministratorMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAdministratorMenu.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAdministratorMenu.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }

    @Override
    public void onBackPressed()
    {
    }


}
