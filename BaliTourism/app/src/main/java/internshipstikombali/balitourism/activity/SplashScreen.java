package internshipstikombali.balitourism.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.Accommodation;
import internshipstikombali.balitourism.business.AccommodationBelongsCategoryAccommodation;
import internshipstikombali.balitourism.business.AccommodationHasFacility;
import internshipstikombali.balitourism.business.ActivityBelongsCategoryActivity;
import internshipstikombali.balitourism.business.Restaurant;
import internshipstikombali.balitourism.business.Transport;
import internshipstikombali.balitourism.business.TripContainsAccommodation;
import internshipstikombali.balitourism.business.TripContainsActivity;
import internshipstikombali.balitourism.business.TripContainsRestaurant;
import internshipstikombali.balitourism.business.TripContainsTransport;
import internshipstikombali.balitourism.helper.Database;

public class SplashScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        Thread timerThread = new Thread(){
            public void run(){
                try{
                    storeAllDatabase();
                    sleep(15000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally{
                    // Storing data into database
                    Intent intent = new Intent(SplashScreen.this,LogInActivity.class);
                    startActivity(intent);
                }
            }
        };
        timerThread.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    /**
     * Function to store all accommodations
     */
    private void storeAllAccommodations() {
        // Tag used to cancel the request
        String tag_string_req = "req_getAllAccommodations";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ACCOMMODATIONS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                        // Now store the accommodations in SQLite
                        JSONArray accommodations = jObj.getJSONArray("accommodations");
                        //Getting each accommodation one by one
                        for(int i=0; i<=accommodations.length()-1; i++){
                            JSONObject accommodationsObj = accommodations.getJSONObject(i);
                            JSONObject accommodation = accommodationsObj.getJSONObject("accommodation");
                            int idAccommodation = accommodation.getInt("idAccommodation");
                            String name = accommodation.getString("name");
                            String longitude = accommodation.getString("longitude");
                            String latitude = accommodation.getString("latitude");
                            String phoneNumber = accommodation.getString("phoneNumber");
                            String website = accommodation.getString("website");
                            int price = accommodation.getInt("price");
                            String description = accommodation.getString("description");
                            String picture = accommodation.getString("picture");
                            int numberOfPlaces = accommodation.getInt("numberOfPlaces");

                            // Insert the row in SQLite database
                            Database.myAccommodationDAO.addAccommodation(new Accommodation(idAccommodation, name, longitude, latitude, phoneNumber, website, price, description, picture, numberOfPlaces));
                        }

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to accommodations url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", "all");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * Function to store all activities
     */
    private void storeAllActivities() {
        // Tag used to cancel the request
        String tag_string_req = "req_getAllActivities";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ACTIVITIES, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                        // Now store the activities in SQLite
                        JSONArray activities = jObj.getJSONArray("activities");
                        //Getting each activity one by one
                        for(int i=0; i<=activities.length()-1; i++){
                            JSONObject activitiesObj = activities.getJSONObject(i);
                            JSONObject activity = activitiesObj.getJSONObject("activity");
                            int idActivity = activity.getInt("idActivity");
                            String name = activity.getString("name");
                            String longitude = activity.getString("longitude");
                            String latitude = activity.getString("latitude");
                            String phoneNumber = activity.getString("phoneNumber");
                            String website = activity.getString("website");
                            int price = activity.getInt("price");
                            String description = activity.getString("description");
                            String picture = activity.getString("picture");
                            int duration = activity.getInt("duration");
                            int openingHour = activity.getInt("openingHour");
                            int openingMinute = activity.getInt("openingMinute");
                            int closingHour = activity.getInt("closingHour");
                            int closingMinute = activity.getInt("closingMinute");

                            // Insert the row in SQLite database
                            Database.myActivityDAO.addActivity(new internshipstikombali.balitourism.business.Activity(idActivity, name, longitude, latitude, phoneNumber, website, price, description, picture, duration, openingHour, openingMinute, closingHour, closingMinute));
                        }

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to accommodations url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", "all");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * Function to store all restaurants
     */
    private void storeAllRestaurants() {
        // Tag used to cancel the request
        String tag_string_req = "req_getAllRestaurants";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_RESTAURANTS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                        // Now store the restaurants in SQLite
                        JSONArray restaurants = jObj.getJSONArray("restaurants");
                        //Getting each restaurant one by one
                        for(int i=0; i<=restaurants.length()-1; i++){
                            JSONObject restaurantsObj = restaurants.getJSONObject(i);
                            JSONObject restaurant = restaurantsObj.getJSONObject("restaurant");
                            int idRestaurant = restaurant.getInt("idRestaurant");
                            String name = restaurant.getString("name");
                            String longitude = restaurant.getString("longitude");
                            String latitude = restaurant.getString("latitude");
                            String phoneNumber = restaurant.getString("phoneNumber");
                            String website = restaurant.getString("website");
                            int price = restaurant.getInt("price");
                            String description = restaurant.getString("description");
                            String picture = restaurant.getString("picture");
                            int openingHour = restaurant.getInt("openingHour");
                            int openingMinute = restaurant.getInt("openingMinute");
                            int closingHour = restaurant.getInt("closingHour");
                            int closingMinute = restaurant.getInt("closingMinute");

                            // Insert the row in SQLite database
                            Database.myRestaurantDAO.addRestaurant(new Restaurant(idRestaurant, name, longitude, latitude, phoneNumber, website, price, description, picture, openingHour, openingMinute, closingHour, closingMinute));
                        }

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                   //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to restaurants url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", "all");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * Function to store all transports
     */
    private void storeAllTransports() {
        // Tag used to cancel the request
        String tag_string_req = "req_getAllTransports";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_TRANSPORTS, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                        // Now store the transports in SQLite
                        JSONArray transports = jObj.getJSONArray("transports");
                        //Getting each transport one by one
                        for(int i=0; i<=transports.length()-1; i++){
                            JSONObject transportsObj = transports.getJSONObject(i);
                            JSONObject transport = transportsObj.getJSONObject("transport");
                            int idTransport = transport.getInt("idTransport");
                            String name = transport.getString("name");
                            String description = transport.getString("description");
                            String picture = transport.getString("picture");
                            int numberOfPlaces = transport.getInt("numberOfPlaces");
                            int needDrivingLicense = transport.getInt("needDrivingLicense");
                            int price = transport.getInt("price");

                            // Insert the row in SQLite database
                            Database.myTransportDAO.addTransport(new Transport(idTransport, name, description, picture, numberOfPlaces, needDrivingLicense, price));
                        }

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to transports url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", "all");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * Function to store all activities
     */
    private void storeAllActivityBelongsCategoryActivity() {
        // Tag used to cancel the request
        String tag_string_req = "req_getAllActivityBCA";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ACTIVITYBCA, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                        // Now store the activities in SQLite
                        JSONArray activitiesBelongsCategoryActivity = jObj.getJSONArray("activitiesBelongsCategoryActivity");
                        //Getting each activity one by one
                        for(int i=0; i<=activitiesBelongsCategoryActivity.length()-1; i++){
                            JSONObject activitiesBCAObj = activitiesBelongsCategoryActivity.getJSONObject(i);
                            JSONObject activityBelongsCategoryActivity = activitiesBCAObj.getJSONObject("activityBelongsCategoryActivity");
                            int idActivity = activityBelongsCategoryActivity.getInt("idActivity");
                            String nameCategoryActivity = activityBelongsCategoryActivity.getString("nameCategoryActivity");


                            // Insert the row in SQLite database
                            Database.myActivityBelongsCategoryActivityDAO.addActivityBelongsCategoryActivity(new ActivityBelongsCategoryActivity(idActivity, nameCategoryActivity));
                        }

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to accommodations url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", "all");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    /**
     * Function to store all accommodationBCA
     */
    private void storeAllAccommodationBelongsCategoryAccommodation() {
        // Tag used to cancel the request
        String tag_string_req = "req_getAllAccommodationBCA";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ACCOMMODATIONBCA, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                        // Now store the activities in SQLite
                        JSONArray accommodationsBelongsCategoryAccommodation = jObj.getJSONArray("accommodationsBelongsCategoryAccommodation");
                        //Getting each activity one by one
                        for(int i=0; i<=accommodationsBelongsCategoryAccommodation.length()-1; i++){
                            JSONObject accommodationsBCAObj = accommodationsBelongsCategoryAccommodation.getJSONObject(i);
                            JSONObject accommodationBelongsCategoryAccommodation = accommodationsBCAObj.getJSONObject("accommodationBelongsCategoryAccommodation");
                            int idAccommodation = accommodationBelongsCategoryAccommodation.getInt("idAccommodation");
                            String nameCategoryAccommodation = accommodationBelongsCategoryAccommodation.getString("nameCategoryAccommodation");


                            // Insert the row in SQLite database
                            Database.myAccommodationBelongsCategoryAccommodationDAO.addAccommodationBelongsCategoryAccommodation(new AccommodationBelongsCategoryAccommodation(idAccommodation, nameCategoryAccommodation));
                        }

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to accommodations url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", "all");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * Function to store all accommodationHasFacility
     */
    private void storeAllAccommodationHasFacility() {
        // Tag used to cancel the request
        String tag_string_req = "req_getAllAccommodationHasFacility";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ACCOMMODATIONHASFACILITY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                        // Now store the activities in SQLite
                        JSONArray accommodationsHasFacility = jObj.getJSONArray("accommodationsHasFacility");
                        //Getting each activity one by one
                        for(int i=0; i<=accommodationsHasFacility.length()-1; i++){
                            JSONObject accommodationsHasFacilityObj = accommodationsHasFacility.getJSONObject(i);
                            JSONObject accommodationHasFacility = accommodationsHasFacilityObj.getJSONObject("accommodationHasFacility");
                            int idAccommodation = accommodationHasFacility.getInt("idAccommodation");
                            String nameFacility = accommodationHasFacility.getString("nameFacility");


                            // Insert the row in SQLite database
                            Database.myAccommodationHasFacilityDAO.addAccommodationHasFacility(new AccommodationHasFacility(idAccommodation, nameFacility));
                        }

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to accommodations url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", "all");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * Function to store all tripContainsTransport
     */
    private void storeAllTripContainsTransport() {
        // Tag used to cancel the request
        String tag_string_req = "req_getAllTripContainsTransport";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_TRIPCONTAINSTRANSPORT, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    // Now store the transports in SQLite
                    JSONArray tripContainsTransports = jObj.getJSONArray("tripContainsTransports");

                    //Getting each transport one by one
                    for(int i=0; i<=tripContainsTransports.length()-1; i++){
                        JSONObject tripContainsTransportsObj = tripContainsTransports.getJSONObject(i);
                        JSONObject tripContainsTransport = tripContainsTransportsObj.getJSONObject("tripContainsTransport");
                        int idTrip = tripContainsTransport.getInt("idTrip");
                        int idTransport = tripContainsTransport.getInt("idTransport");
                        String dateTransport = tripContainsTransport.getString("dateTransport");

                        // Insert the row in SQLite database
                        Database.myTripContainsTransportDAO.addTripContainsTransport(new TripContainsTransport(idTrip, idTransport, dateTransport, 0, 0));
                    }

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to accommodations url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", "all");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * Function to store all tripContainsAccommodation
     */
    private void storeAllTripContainsAccommodation() {
        // Tag used to cancel the request
        String tag_string_req = "req_getAllTripContainsAccommodation";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_TRIPCONTAINSACCOMMODATION, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    // Now store the activities in SQLite
                    JSONArray tripContainsAccommodations = jObj.getJSONArray("tripContainsAccommodations");

                    //Getting each activity one by one
                    for(int i=0; i<=tripContainsAccommodations.length()-1; i++){
                        JSONObject tripContainsAccommodationsObj = tripContainsAccommodations.getJSONObject(i);
                        JSONObject tripContainsAccommodation = tripContainsAccommodationsObj.getJSONObject("tripContainsAccommodation");
                        int idTrip = tripContainsAccommodation.getInt("idTrip");
                        int idAccommodation = tripContainsAccommodation.getInt("idAccommodation");
                        String arrivalDate = tripContainsAccommodation.getString("arrivalDate");


                        // Insert the row in SQLite database
                        Database.myTripContainsAccommodationDAO.addTripContainsAccommodation(new TripContainsAccommodation(idTrip, idAccommodation, arrivalDate));
                    }

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to accommodations url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", "all");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * Function to store all tripContainsActivity
     */
    private void storeAllTripContainsActivity() {
        // Tag used to cancel the request
        String tag_string_req = "req_getAllTripContainsActivity";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_TRIPCONTAINSACTIVITY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    // Now store the activities in SQLite
                    JSONArray tripContainsActivities = jObj.getJSONArray("tripContainsActivities");

                    //Getting each activity one by one
                    for(int i=0; i<=tripContainsActivities.length()-1; i++){
                        JSONObject tripContainsActivitiesObj = tripContainsActivities.getJSONObject(i);
                        JSONObject tripContainsActivity = tripContainsActivitiesObj.getJSONObject("tripContainsActivity");
                        int idTrip = tripContainsActivity.getInt("idTrip");
                        int idActivity = tripContainsActivity.getInt("idActivity");
                        String arrivalDate = tripContainsActivity.getString("arrivalDate");
                        String arrivalTime = tripContainsActivity.getString("arrivalTime");

                        // Insert the row in SQLite database
                        Database.myTripContainsActivityDAO.addTripContainsActivity(new TripContainsActivity(idTrip, idActivity, arrivalDate, arrivalTime));
                    }

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to accommodations url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", "all");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * Function to store all tripContainsRestaurant
     */
    private void storeAllTripContainsRestaurant() {
        // Tag used to cancel the request
        String tag_string_req = "req_getAllTripContainsRestaurant";
        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_TRIPCONTAINSRESTAURANT, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    // Now store the restaurants in SQLite
                    JSONArray tripContainsRestaurants = jObj.getJSONArray("tripContainsRestaurants");

                    //Getting each restaurant one by one
                    for(int i=0; i<=tripContainsRestaurants.length()-1; i++){
                        JSONObject tripContainsRestaurantsObj = tripContainsRestaurants.getJSONObject(i);
                        JSONObject tripContainsRestaurant = tripContainsRestaurantsObj.getJSONObject("tripContainsRestaurant");
                        int idTrip = tripContainsRestaurant.getInt("idTrip");
                        int idRestaurant = tripContainsRestaurant.getInt("idRestaurant");
                        String arrivalDate = tripContainsRestaurant.getString("arrivalDate");
                        String arrivalTime = tripContainsRestaurant.getString("arrivalTime");

                        // Insert the row in SQLite database
                        Database.myTripContainsRestaurantDAO.addTripContainsRestaurant(new TripContainsRestaurant(idTrip, idRestaurant, arrivalDate, arrivalTime));
                    }

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to accommodations url
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", "all");

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void storeAllDatabase() {
        if(isOnline()) {
            //Database.myTripDAO.deleteAllTrips();
            Database.myAccommodationDAO.deleteAllAccommodations();
            Database.myActivityDAO.deleteAllActivities();
            Database.myRestaurantDAO.deleteAllRestaurants();
            Database.myTransportDAO.deleteAllTransports();
            Database.myActivityBelongsCategoryActivityDAO.deleteAllActivityBelongsCategoryActivity();
            Database.myAccommodationBelongsCategoryAccommodationDAO.deleteAllAccommodationBelongsCategoryAccommodation();
            Database.myAccommodationHasFacilityDAO.deleteAllAccommodationHasFacility();
            Database.myTripContainsAccommodationDAO.deleteAllTripContainsAccommodation();
            Database.myTripContainsActivityDAO.deleteAllTripContainsActivity();
            Database.myTripContainsRestaurantDAO.deleteAllTripContainsRestaurant();
            Database.myTripContainsTransportDAO.deleteAllTripContainsTransport();

            //this.storeAllTrips();
            this.storeAllAccommodations();
            this.storeAllActivities();
            this.storeAllRestaurants();
            this.storeAllTransports();
            this.storeAllActivityBelongsCategoryActivity();
            this.storeAllAccommodationBelongsCategoryAccommodation();
            this.storeAllAccommodationHasFacility();
            this.storeAllTripContainsAccommodation();
            this.storeAllTripContainsActivity();
            this.storeAllTripContainsRestaurant();
            this.storeAllTripContainsTransport();
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
