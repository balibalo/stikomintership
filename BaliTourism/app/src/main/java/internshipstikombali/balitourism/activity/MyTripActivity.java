package internshipstikombali.balitourism.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.helper.Database;
import internshipstikombali.balitourism.helper.SessionManager;

public class MyTripActivity extends Activity {

    private static final String TAG = MyTripActivity.class.getSimpleName();

    private Button btnLogout;
    private Button btnPreferences;
    private SessionManager session;
    private int idOfUser;
    public int idOfTrip;
    public int durationOfTrip;
    public String arrivalDateOfTrip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_trip);
        btnLogout = (Button) findViewById(R.id.btnLogout);
        btnPreferences = (Button) findViewById(R.id.btnPreferences);
        final AlertDialog diaBox = AskOption();

        // session manager
        session = new SessionManager(getApplicationContext());

        if (getIntent().hasExtra("idUser")){
            idOfUser = Integer.parseInt(getIntent().getStringExtra("idUser"));
            idOfTrip = Integer.parseInt(getIntent().getStringExtra("idTrip"));
            durationOfTrip = Integer.parseInt(getIntent().getStringExtra("durationTrip"));
            arrivalDateOfTrip = getIntent().getStringExtra("arrivalDate");
        }
        else {
            idOfUser = Integer.parseInt(getParent().getIntent().getStringExtra("idUser"));
            idOfTrip = Integer.parseInt(getParent().getIntent().getStringExtra("idTrip"));
            durationOfTrip = Integer.parseInt(getParent().getIntent().getStringExtra("durationTrip"));
            arrivalDateOfTrip = getParent().getIntent().getStringExtra("arrivalDate");
        }
/*
        Intent intent = getIntent();
        idOfUser = Integer.parseInt(intent.getStringExtra("idUser"));
        idOfTrip = Integer.parseInt(intent.getStringExtra("idTrip"));
        durationOfTrip = Integer.parseInt(intent.getStringExtra("durationTrip"));
        arrivalDateOfTrip = intent.getStringExtra("arrivalDate");
        */

        if (!session.isLoggedIn()) {
            logoutUser();
        }

        // Logout button click event
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logoutUser();
            }
        });

        // Preferences button click event
        btnPreferences.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                diaBox.show();
            }
        });

        //final Intent intent = getIntent();
        //final int idOfUser = Integer.parseInt(intent.getStringExtra("idUser"));

        /*
        if(getIntent().hasExtra("idUser")) {
            idOfUser = Integer.parseInt(getIntent().getStringExtra("idUser"));
            idOfTrip = Integer.parseInt(getIntent().getStringExtra("idTrip"));
            durationOfTrip = Integer.parseInt(getIntent().getStringExtra("durationTrip"));
            arrivalDateOfTrip = getIntent().getStringExtra("arrivalDate");
        }
        else {
            idOfUser = Integer.parseInt(getParent().getIntent().getStringExtra("idUser"));
        }
        */


        // parametres : (int) idUser
        // j'ai besoin de la durée du trip et de la date d'arrivée


        String[] partsOfDate = arrivalDateOfTrip.split("/");
        int day = Integer.parseInt(partsOfDate[0]);
        int month = Integer.parseInt(partsOfDate[1]);
        int year = Integer.parseInt(partsOfDate[2]);

        String[] daysOfTrip = new String[durationOfTrip];
        daysOfTrip[0] = String.valueOf(day)+"/"+String.valueOf(month)+"/"+String.valueOf(year);
        for (int i=1;i<durationOfTrip;i++){
            if (month==2){
                if (year%4==0){
                    if ((day+i)==30){
                        daysOfTrip[i] = "1/3/"+String.valueOf(year);
                        day = -i+1;
                        month = month+1;
                    }
                    else{
                        daysOfTrip[i] = String.valueOf(day+i)+"/"+String.valueOf(month)+"/"+String.valueOf(year);
                    }
                }
                else if ((day+i)==29){
                    daysOfTrip[i] = "1/3/"+String.valueOf(year);
                    day = -i+1;
                    month = month+1;
                }
                else{
                    daysOfTrip[i] = String.valueOf(day+i)+"/"+String.valueOf(month)+"/"+String.valueOf(year);
                }
            }
            else if (month<8) {
                if (month % 2 == 0) {
                    if ((day + i) == 31) {
                        daysOfTrip[i] = "1/" + String.valueOf(month + 1) + "/" + String.valueOf(year);
                        day = -i+1;
                        month = month+1;
                    } else {
                        daysOfTrip[i] = String.valueOf(day + i) + "/" + String.valueOf(month) + "/" + String.valueOf(year);
                    }
                } else if ((day + i) == 32) {
                    daysOfTrip[i] = "1/" + String.valueOf(month + 1) + "/" + String.valueOf(year);
                    day = -i+1;
                    month = month+1;
                } else {
                    daysOfTrip[i] = String.valueOf(day + i) + "/" + String.valueOf(month) + "/" + String.valueOf(year);
                }
            }
            else if (month==12){
                if ((day + i) == 32) {
                    daysOfTrip[i] = "1/1/"+ String.valueOf(year+1);
                    day = -i+1;
                    month = 1;
                    year = year+1;
                }
                else {
                    daysOfTrip[i] = String.valueOf(day + i) + "/" + String.valueOf(month) + "/" + String.valueOf(year);
                }
            }
            else {
                if (month % 2 == 0) {
                    if ((day + i) == 32) {
                        daysOfTrip[i] = "1/" + String.valueOf(month + 1) + "/" + String.valueOf(year);
                        day = -i+1;
                        month = month+1;
                    } else {
                        daysOfTrip[i] = String.valueOf(day + i) + "/" + String.valueOf(month) + "/" + String.valueOf(year);
                    }
                } else if ((day + i) == 31) {
                    daysOfTrip[i] = "1/" + String.valueOf(month + 1) + "/" + String.valueOf(year);
                    day = -i+1;
                    month = month+1;
                } else {
                    daysOfTrip[i] = String.valueOf(day + i) + "/" + String.valueOf(month) + "/" + String.valueOf(year);
                }
            }
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(MyTripActivity.this, android.R.layout.simple_list_item_1, daysOfTrip);

        ListView list = (ListView)findViewById(R.id.ListView);

        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent myIntent = new Intent(MyTripActivity.this, DayActivity.class);
                myIntent.putExtra("day", adapter.getItem(position));
                myIntent.putExtra("idUser", String.valueOf(idOfUser));
                myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                myIntent.putExtra("idTrip",String.valueOf(idOfTrip));
                startActivity(myIntent);
            }
        });
    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     */
    private void logoutUser() {
        session.setLogin(false);
        Database.myUserDAO.deleteAllUsers();
        // Launching the login activity
        Intent intent = new Intent(MyTripActivity.this, LogInActivity.class);
        startActivity(intent);
        finish();
    }

    private AlertDialog AskOption()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setMessage("Be careful ! If you modify your preferences, your actual trip will be deleted and a new one will be created.")
                .setPositiveButton("Modify", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        deleteTrip();
                        Intent myIntent = new Intent(MyTripActivity.this, FormActivity.class);
                        myIntent.putExtra("idUser", String.valueOf(idOfUser));
                        startActivity(myIntent);
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    /**
     * function to delete the former trip in mysql db
     * */
    private void deleteTrip() {
        // Tag used to cancel the request
        String tag_string_req = "req_delete";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETETRIP, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                        // delete row in Trip table
                        Database.myTripDAO.deleteTripById(idOfTrip);
                        Database.myTripContainsRestaurantDAO.deleteTripContainsRestaurantById(idOfTrip);
                        Database.myTripContainsTransportDAO.deleteTripContainsTransportById(idOfTrip);
                        Database.myTripContainsActivityDAO.deleteTripContainsActivityById(idOfTrip);
                        Database.myTripContainsAccommodationDAO.deleteTripContainsAccommodationById(idOfTrip);

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to deletetrip url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idOfTrip));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    public void onBackPressed()
    {
    }


}
