package internshipstikombali.balitourism.activity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.Restaurant;
import internshipstikombali.balitourism.business.TripContainsRestaurant;
import internshipstikombali.balitourism.helper.Database;

public class RestActivity extends AppCompatActivity {

    private static final String TAG = RestaurantActivity.class.getSimpleName();

    Bitmap bitmap;
    ImageView imageView5;
    private ImageButton btnDate1;

    private int idOfRest;

    private int idOfTrip;
    private int idOfUser;
    private int durationOfTrip;
    private String formatted;
    private String arrivalDateOfTrip;
    private String newArrivalTime;
    private int day;
    private int month;
    private int year;
    private ImageButton buttonMusicRest;

    private DatePickerDialog datePickerDialog;

    List<TripContainsRestaurant> reqRestaurant;

    AlertDialog diaBox2;
    AlertDialog diaBox3;
    AlertDialog diaBox4;

    private int idRestFormer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rest);

        imageView5 = (ImageView) findViewById(R.id.imageViewRest);
        btnDate1 = (ImageButton) findViewById(R.id.dateButtonRest);
        buttonMusicRest = (ImageButton) findViewById(R.id.buttonMusicRest);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicRest.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicRest.setBackgroundResource(R.drawable.play);
        }

        buttonMusicRest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicRest.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicRest.setBackgroundResource(R.drawable.pause);
                }
            }
        });

        Intent intent = getIntent();
        idOfUser = Integer.parseInt(intent.getStringExtra("idUser"));
        idOfRest = Integer.parseInt(intent.getStringExtra("idRest"));
        idOfTrip = Integer.parseInt(intent.getStringExtra("idTrip"));
        durationOfTrip = Integer.parseInt(intent.getStringExtra("durationTrip"));
        arrivalDateOfTrip = intent.getStringExtra("arrivalDate");

        diaBox2 = AskOption2();
        diaBox3 = AskOption3();
        diaBox4 = AskOption4();

        Restaurant restaurant1 = Database.myRestaurantDAO.fetchRestaurantById(idOfRest);

        String nameRestaurant1 = restaurant1.getName();
        String priceRestaurant1 = "Price : " + restaurant1.getPrice() + " irp per person";
        double latitudeRestaurant1 = Double.parseDouble(restaurant1.getLatitude());
        double longitudeRestaurant1 = Double.parseDouble(restaurant1.getLongitude());

        Geocoder geocoder;
        List<Address> addresses = new ArrayList<Address>();
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitudeRestaurant1, longitudeRestaurant1, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String addressRestaurant1;
        if (addresses.size()==0){
            addressRestaurant1 = "Location : no information";
        }
        else {
            addressRestaurant1 = "Location : " + addresses.get(0).getAddressLine(0)+", " + addresses.get(0).getLocality();
        }

        String openingHoursRestaurant1 = "Opening hours : " + restaurant1.getOpeningHour()+":"+restaurant1.getOpeningMinute()+"am - "+restaurant1.getClosingHour()+":"+restaurant1.getClosingMinute()+"pm";

        String phoneNumberRestaurant1 = "Phone number : " + restaurant1.getPhoneNumber();

        String websiteRestaurant1 = "Website : " + restaurant1.getWebsite();

        String descriptionRestaurant1 = "Description : " + restaurant1.getDescription();

        String urlRestaurant1 = restaurant1.getPicture();

        final TextView nameRest1 = (TextView) findViewById(R.id.nameRest);
        nameRest1.setText(nameRestaurant1);

        final TextView priceRest1 = (TextView) findViewById(R.id.priceRest);
        priceRest1.setText(priceRestaurant1);

        final TextView locationRest1 = (TextView) findViewById(R.id.locationRest);
        locationRest1.setText(addressRestaurant1);

        final TextView descriptionRest1 = (TextView) findViewById(R.id.descriptionRest);
        descriptionRest1.setText(descriptionRestaurant1);

        final TextView openingHoursRest1 = (TextView) findViewById(R.id.openingHoursRest);
        openingHoursRest1.setText(openingHoursRestaurant1);

        final TextView phoneRest1 = (TextView) findViewById(R.id.phoneRest);
        phoneRest1.setText(phoneNumberRestaurant1);

        final TextView websiteRest1 = (TextView) findViewById(R.id.websiteRest);
        websiteRest1.setText(websiteRestaurant1);

        new LoadImage().execute(urlRestaurant1);

        String[] partsOfDate = arrivalDateOfTrip.split("/");
        day = Integer.parseInt(partsOfDate[0]);
        month = Integer.parseInt(partsOfDate[1]);
        year = Integer.parseInt(partsOfDate[2]);

        // date button Click Event
        btnDate1.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                //showStartDateDialog(view);
                datePickerDialog.setMessage("Change the date");
                datePickerDialog.updateDate(year, month - 1, day);
                datePickerDialog.show();
            }
        });

        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat format1 = new SimpleDateFormat("d/M/yyyy");
                formatted = format1.format(newDate.getTime());
                SimpleDateFormat sdf = new SimpleDateFormat("d/M/yyyy");

                Date startTrip = null;
                try {
                    startTrip = sdf.parse(arrivalDateOfTrip);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                long startTripDate = startTrip.getTime();

                Date endTrip = null;
                try {
                    endTrip = sdf.parse(arrivalDateOfTrip);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                long endTripDate = endTrip.getTime()+(86400000*durationOfTrip);

                Date newDateTrip = null;
                try {
                    newDateTrip = sdf.parse(formatted);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                long newTripDate = newDateTrip.getTime();

                if (!(newTripDate>=startTripDate && newTripDate<endTripDate)){
                    diaBox3.show();
                }
                else {
                    diaBox4.show();
                }
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    private class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {
            if(image != null){
                imageView5.setImageBitmap(image);
            }else{

            }
        }
    }

    /**
     * function to delete the former restaurant from the trip in mysql db
     * */
    private void deleteFormerRestaurant() {
        // Tag used to cancel the request
        String tag_string_req = "req_delete";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETERESTAURANT, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json

                        // delete row in tripContainsRestaurant table
                        Database.myTripContainsRestaurantDAO.deleteTripContainsRestaurant(idOfTrip,idRestFormer,formatted, newArrivalTime);

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to deletetransport url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idOfTrip));
                params.put("arrivalDate", formatted);
                params.put("arrivalTime", newArrivalTime);
                params.put("idRestaurant", String.valueOf(idRestFormer));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to add the restaurant from the trip in mysql db
     * */
    private void addRestaurant() {
        // Tag used to cancel the request
        String tag_string_req = "req_add";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDRESTAURANT, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                            // add row in tripContainsRestaurant table
                            Database.myTripContainsRestaurantDAO.addTripContainsRestaurant(new TripContainsRestaurant(idOfTrip,idOfRest,formatted, newArrivalTime));

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to addrestaurant url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idOfTrip));
                params.put("idRestaurant", String.valueOf(idOfRest));
                params.put("arrivalDate", formatted);
                params.put("arrivalTime", newArrivalTime);

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to check if there is already a restaurant at this date in mysql db
     * return true if there is already a restaurant
     * */
    private boolean checkRestaurant() {
        reqRestaurant = Database.myTripContainsRestaurantDAO.fetchAllTripContainsRestaurant();
        for (int i = 0; i<reqRestaurant.size(); i++){
            if ((reqRestaurant.get(i).getIdTrip() == idOfTrip) && reqRestaurant.get(i).getArrivalDate().equals(formatted) && reqRestaurant.get(i).getArrivalTime().equals(newArrivalTime)){
                idRestFormer = reqRestaurant.get(i).getIdRestaurant();
                return true;
            }
        }
        return false;
    }

    private AlertDialog AskOption2()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                //set message, title, and icon
                .setMessage("There is already a restaurant at this date, do you want to change it by this one ?")
                .setPositiveButton("Change", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        deleteFormerRestaurant();
                        addRestaurant();
                        Toast.makeText(getApplicationContext(),
                                "The restaurant has been changed successfully !", Toast.LENGTH_LONG)
                                .show();
                        Intent myIntent = new Intent(RestActivity.this, HomeActivity.class);
                        myIntent.putExtra("idUser", String.valueOf(idOfUser));
                        myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                        myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                        myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                        startActivity(myIntent);
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    private AlertDialog AskOption3()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setMessage("You must choose a day according to the dates of your trip.")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    private AlertDialog AskOption4()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                //set message, title, and icon
                .setPositiveButton("Lunch", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        newArrivalTime = "lunch";
                        if (checkRestaurant()) {
                            diaBox2.show();
                        }
                        else {
                            addRestaurant();
                            Toast.makeText(getApplicationContext(),
                                    "The restaurant has been added successfully !", Toast.LENGTH_LONG)
                                    .show();
                            Intent myIntent = new Intent(RestActivity.this, HomeActivity.class);
                            myIntent.putExtra("idUser", String.valueOf(idOfUser));
                            myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                            myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                            myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                            startActivity(myIntent);
                            finish();
                        }
                    }
                })
                .setNegativeButton("Dinner", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        newArrivalTime = "dinner";
                        if (checkRestaurant()) {
                            diaBox2.show();
                        }
                        else {
                            addRestaurant();
                            Toast.makeText(getApplicationContext(),
                                    "The restaurant has been added successfully !", Toast.LENGTH_LONG)
                                    .show();
                            Intent myIntent = new Intent(RestActivity.this, HomeActivity.class);
                            myIntent.putExtra("idUser", String.valueOf(idOfUser));
                            myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                            myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                            myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                            startActivity(myIntent);
                            finish();
                        }
                    }
                })
                .create();
        return myQuittingDialogBox;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicRest.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicRest.setBackgroundResource(R.drawable.play);
        }

        buttonMusicRest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicRest.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicRest.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }

}
