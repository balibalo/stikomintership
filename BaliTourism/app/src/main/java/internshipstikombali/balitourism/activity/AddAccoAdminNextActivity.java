package internshipstikombali.balitourism.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.AccommodationBelongsCategoryAccommodation;
import internshipstikombali.balitourism.helper.Database;

public class AddAccoAdminNextActivity extends AppCompatActivity {

    private static final String TAG = AddAccoAdminNextActivity.class.getSimpleName();

    private Button buttonAddNextAcco;
    private CheckBox guesthouseAcco;
    private CheckBox homestayAcco;
    private CheckBox villaAcco;
    private CheckBox apartementAcco;
    private CheckBox hotel1Acco;
    private CheckBox hotel2Acco;
    private CheckBox hotel3Acco;
    private CheckBox hotel4Acco;
    private CheckBox hotel5Acco;
    private int cpt;
    private ImageButton buttonMusicAddAccoAdminNext;


    private int idAccommodation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_acco_admin_next);

        buttonAddNextAcco = (Button) findViewById(R.id.buttonAddNextAcco);
        guesthouseAcco = (CheckBox) findViewById(R.id.guesthouseAcco);
        homestayAcco = (CheckBox) findViewById(R.id.homestayAcco);
        villaAcco = (CheckBox) findViewById(R.id.villaAcco);
        apartementAcco = (CheckBox) findViewById(R.id.apartementAcco);
        hotel1Acco = (CheckBox) findViewById(R.id.hotel1Acco);
        hotel2Acco = (CheckBox) findViewById(R.id.hotel2Acco);
        hotel3Acco = (CheckBox) findViewById(R.id.hotel3Acco);
        hotel4Acco = (CheckBox) findViewById(R.id.hotel4Acco);
        hotel5Acco = (CheckBox) findViewById(R.id.hotel5Acco);
        buttonMusicAddAccoAdminNext = (ImageButton) findViewById(R.id.buttonMusicAddAccoAdminNext);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddAccoAdminNext.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddAccoAdminNext.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddAccoAdminNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddAccoAdminNext.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddAccoAdminNext.setBackgroundResource(R.drawable.pause);
                }
            }
        });

        Intent intent = getIntent();
        String idAcco = intent.getStringExtra("idAccommodation");
        idAccommodation = Integer.parseInt(idAcco);


        // Send button Click Event
        buttonAddNextAcco.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {

                cpt = 0;
                if (guesthouseAcco.isChecked()) {
                    cpt = cpt +1;
                }
                if (homestayAcco.isChecked()) {
                    cpt = cpt +1;
                }
                if (villaAcco.isChecked()) {
                    cpt = cpt +1;
                }
                if (apartementAcco.isChecked()) {
                    cpt = cpt +1;
                }
                if (hotel1Acco.isChecked()) {
                    cpt = cpt +1;
                }
                if (hotel2Acco.isChecked()) {
                    cpt = cpt +1;
                }
                if (hotel3Acco.isChecked()) {
                    cpt = cpt +1;
                }
                if (hotel4Acco.isChecked()) {
                    cpt = cpt +1;
                }
                if (hotel5Acco.isChecked()) {
                    cpt = cpt +1;
                }

                if (cpt == 1) {
                    if (guesthouseAcco.isChecked()) {
                        String cat = "guesthouse";
                        addAccommodationBelongsCategory(cat);
                    }
                    if (homestayAcco.isChecked()) {
                        String cat = "homestay";
                        addAccommodationBelongsCategory(cat);
                    }
                    if (villaAcco.isChecked()) {
                        String cat = "villa";
                        addAccommodationBelongsCategory(cat);
                    }
                    if (apartementAcco.isChecked()) {
                        String cat = "apartement";
                        addAccommodationBelongsCategory(cat);
                    }
                    if (hotel1Acco.isChecked()) {
                        String cat = "hotel1";
                        addAccommodationBelongsCategory(cat);
                    }
                    if (hotel2Acco.isChecked()) {
                        String cat = "hotel2";
                        addAccommodationBelongsCategory(cat);
                    }
                    if (hotel3Acco.isChecked()) {
                        String cat = "hotel3";
                        addAccommodationBelongsCategory(cat);
                    }
                    if (hotel4Acco.isChecked()) {
                        String cat = "hotel4";
                        addAccommodationBelongsCategory(cat);
                    }
                    if (hotel5Acco.isChecked()) {
                        String cat = "hotel5";
                        addAccommodationBelongsCategory(cat);
                    }

                    Toast.makeText(getApplicationContext(), "The accommodation has been created successfully !", Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(AddAccoAdminNextActivity.this, AdministratorMenuActivity.class);
                    startActivity(intent);
                    finish();
                }
                else if (cpt>1){
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please select only 1 category.", Toast.LENGTH_LONG)
                            .show();
                }
                else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please select 1 category.", Toast.LENGTH_LONG)
                            .show();
                }

            }

        });

    }

    /**
     * Function to store the categories of the new accommodation in MySQL database to newtrip url
     * */
    private void addAccommodationBelongsCategory(final String category) {
        // Tag used to cancel the request
        String tag_string_req = "req_saveForm";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDACCOMMODATIONBELONGSCATEGORY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                        // Inserting row in AccommodationBelongCat table
                        Database.myAccommodationBelongsCategoryAccommodationDAO.addAccommodationBelongsCategoryAccommodation(new AccommodationBelongsCategoryAccommodation(idAccommodation,category));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idAccommodation", String.valueOf(idAccommodation));
                params.put("nameCategoryAccommodation", category);

                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddAccoAdminNext.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddAccoAdminNext.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddAccoAdminNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddAccoAdminNext.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddAccoAdminNext.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }


}
