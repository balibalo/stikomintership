package internshipstikombali.balitourism.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.Transport;
import internshipstikombali.balitourism.helper.Database;

public class AddTransAdminActivity extends AppCompatActivity {

    private static final String TAG = AddTransAdminActivity.class.getSimpleName();

    private Button buttonAddTrans;
    private EditText inputNameTrans;
    private EditText inputPriceTrans;
    private EditText inputPlacesTrans;
    private EditText inputUrlTrans;
    private EditText inputDescriptionTrans;
    private RadioButton btnYes;
    private RadioButton btnNo;
    private ImageButton buttonMusicAddTransAdmin;


    private int idTransport;

    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_trans_admin);
        buttonMusicAddTransAdmin = (ImageButton) findViewById(R.id.buttonMusicAddTransAdmin);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddTransAdmin.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddTransAdmin.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddTransAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddTransAdmin.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddTransAdmin.setBackgroundResource(R.drawable.pause);
                }
            }
        });

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        buttonAddTrans = (Button) findViewById(R.id.buttonAddTrans);
        inputNameTrans = (EditText) findViewById(R.id.inputNameTrans);
        inputPriceTrans = (EditText) findViewById(R.id.inputPriceTrans);
        inputPlacesTrans = (EditText) findViewById(R.id.inputPlacesTrans);
        inputUrlTrans = (EditText) findViewById(R.id.inputUrlTrans);
        inputDescriptionTrans = (EditText) findViewById(R.id.inputDescriptionTrans);
        btnYes = (RadioButton) findViewById(R.id.yesTrans);
        btnNo = (RadioButton) findViewById(R.id.noTrans);

        // Send button Click Event
        buttonAddTrans.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                if (!(inputNameTrans.getText().toString().trim().length() == 0) && !(inputPriceTrans.getText().toString().trim().length() == 0) && !(inputPlacesTrans.getText().toString().trim().length() == 0) && (btnYes.isChecked() || btnNo.isChecked())) {

                    String url;
                    String description;

                    String name = inputNameTrans.getText().toString();
                    String price = inputPriceTrans.getText().toString();
                    String places = inputPlacesTrans.getText().toString();

                    if (inputUrlTrans.getText().toString().trim().length() == 0){
                        url = "no information";
                    }
                    else {
                        url = inputUrlTrans.getText().toString();
                    }
                    if (inputDescriptionTrans.getText().toString().trim().length() == 0){
                        description = "no information";
                    }
                    else {
                        description = inputDescriptionTrans.getText().toString();
                    }

                    int drivingLicense;
                    if (btnYes.isChecked()){
                        drivingLicense = 1;
                    }
                    else {
                        drivingLicense = 0;
                    }

                    addTransAdmin(name, price, places, url ,description,drivingLicense);

                }
                else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please fill the fields with an (*).", Toast.LENGTH_LONG)
                            .show();
                }

            }

        });

    }

    /**
     * Function to store the new transport in MySQL database
     * */
    private void addTransAdmin(final String name, final String price, final String places, final String url, final String description, final int drivingLicense) {
        // Tag used to cancel the request
        String tag_string_req = "req_saveForm";
        pDialog.setMessage("Saving new transport ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDTRANSPORTADMIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                        JSONObject transport = jObj.getJSONObject("transport");
                        idTransport = transport.getInt("idTransport");

                        // Inserting row in Transport table
                        Database.myTransportDAO.addTransport(new Transport(idTransport,name,description,url,Integer.parseInt(places),drivingLicense,Integer.parseInt(price)));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(AddTransAdminActivity.this, AdministratorMenuActivity.class);
                startActivity(intent);
                finish();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTransport", String.valueOf(idTransport));
                params.put("name", name);
                params.put("price", price);
                params.put("description", description);
                params.put("picture", url);
                params.put("numberOfPlaces", places);
                params.put("needDrivingLicense", String.valueOf(drivingLicense));

                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddTransAdmin.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddTransAdmin.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddTransAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddTransAdmin.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddTransAdmin.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }

}
