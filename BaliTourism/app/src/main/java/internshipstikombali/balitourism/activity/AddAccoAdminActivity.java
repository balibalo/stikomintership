package internshipstikombali.balitourism.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.Accommodation;
import internshipstikombali.balitourism.helper.Database;

public class AddAccoAdminActivity extends AppCompatActivity {

    private static final String TAG = AddAccoAdminActivity.class.getSimpleName();

    private Button buttonAddAcco;
    private EditText inputNameAcco;
    private EditText inputLongitudeAcco;
    private EditText inputLatitudeAcco;
    private EditText inputPhoneAcco;
    private EditText inputWebsiteAcco;
    private EditText inputPriceAcco;
    private EditText inputPlacesAcco;
    private EditText inputUrlAcco;
    private EditText inputDescriptionAcco;

    private int idAccommodation;
    private ImageButton buttonMusicAddAccoAdmin;


    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_acco_admin);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        buttonAddAcco = (Button) findViewById(R.id.buttonAddAcco);
        inputNameAcco = (EditText) findViewById(R.id.inputNameAcco);
        inputLongitudeAcco = (EditText) findViewById(R.id.inputLongitudeAcco);
        inputLatitudeAcco = (EditText) findViewById(R.id.inputLatitudeAcco);
        inputPhoneAcco = (EditText) findViewById(R.id.inputPhoneAcco);
        inputWebsiteAcco = (EditText) findViewById(R.id.inputWebsiteAcco);
        inputPriceAcco = (EditText) findViewById(R.id.inputPriceAcco);
        inputPlacesAcco = (EditText) findViewById(R.id.inputPlacesAcco);
        inputUrlAcco = (EditText) findViewById(R.id.inputUrlAcco);
        inputDescriptionAcco = (EditText) findViewById(R.id.inputDescriptionAcco);
        buttonMusicAddAccoAdmin = (ImageButton) findViewById(R.id.buttonMusicAddAccoAdmin);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddAccoAdmin.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddAccoAdmin.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddAccoAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddAccoAdmin.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddAccoAdmin.setBackgroundResource(R.drawable.pause);
                }
            }
        });

        // Send button Click Event
        buttonAddAcco.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                if (!(inputNameAcco.getText().toString().trim().length() == 0) && !(inputLongitudeAcco.getText().toString().trim().length() == 0) && !(inputLatitudeAcco.getText().toString().trim().length() == 0) && !(inputPriceAcco.getText().toString().trim().length() == 0) && !(inputPlacesAcco.getText().toString().trim().length() == 0)) {

                    String phone;
                    String website;
                    String url;
                    String description;

                    String name = inputNameAcco.getText().toString();
                    String lng = inputLongitudeAcco.getText().toString();
                    String lat = inputLatitudeAcco.getText().toString();
                    String price = inputPriceAcco.getText().toString();
                    String places = inputPlacesAcco.getText().toString();

                    if (inputPhoneAcco.getText().toString().trim().length() == 0){
                        phone = "no information";
                    }
                    else {
                        phone = inputPhoneAcco.getText().toString();
                    }
                    if (inputWebsiteAcco.getText().toString().trim().length() == 0){
                        website = "no information";
                    }
                    else {
                        website = inputWebsiteAcco.getText().toString();
                    }
                    if (inputUrlAcco.getText().toString().trim().length() == 0){
                        url = "no information";
                    }
                    else {
                        url = inputUrlAcco.getText().toString();
                    }
                    if (inputDescriptionAcco.getText().toString().trim().length() == 0){
                        description = "no information";
                    }
                    else {
                        description = inputDescriptionAcco.getText().toString();
                    }

                    addAccoAdmin(name, lng, lat, phone, website, price, places, url ,description);

                }
                else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please fill the fields with an (*).", Toast.LENGTH_LONG)
                            .show();
                }

            }

        });

    }

    /**
     * Function to store informations contained about the new accommodation in MySQL
     * */
    private void addAccoAdmin(final String name, final String lng, final String lat, final String phone, final String website, final String price, final String places, final String url, final String description) {
        // Tag used to cancel the request
        String tag_string_req = "req_saveForm";
        pDialog.setMessage("Loading ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDACCOMMODATIONADMIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                        JSONObject accommodation = jObj.getJSONObject("accommodation");
                        idAccommodation = accommodation.getInt("idAccommodation");

                        // Inserting row in Accommodation table
                        Database.myAccommodationDAO.addAccommodation(new Accommodation(idAccommodation,name,lng,lat,phone,website,Integer.parseInt(price),description,url,Integer.parseInt(places)));


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(AddAccoAdminActivity.this, AddAccoAdminNextActivity.class);
                intent.putExtra("idAccommodation", String.valueOf(idAccommodation));
                startActivity(intent);
                finish();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idAccommodation", String.valueOf(idAccommodation));
                params.put("name", name);
                params.put("longitude", lng);
                params.put("latitude", lat);
                params.put("phoneNumber", phone);
                params.put("website", website);
                params.put("price", price);
                params.put("description", description);
                params.put("picture", url);
                params.put("numberOfPlaces", places);

                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddAccoAdmin.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddAccoAdmin.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddAccoAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddAccoAdmin.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddAccoAdmin.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }


}
