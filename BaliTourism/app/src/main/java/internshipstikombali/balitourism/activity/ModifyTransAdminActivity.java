package internshipstikombali.balitourism.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.Transport;
import internshipstikombali.balitourism.helper.Database;

public class ModifyTransAdminActivity extends AppCompatActivity {

    private static final String TAG = ModifyTransAdminActivity.class.getSimpleName();

    private Button buttonAddTransUpdate;
    private EditText inputNameTransUpdate;
    private EditText inputPriceTransUpdate;
    private EditText inputPlacesTransUpdate;
    private EditText inputUrlTransUpdate;
    private EditText inputDescriptionTransUpdate;
    private RadioButton btnYesUpdate;
    private RadioButton btnNoUpdate;
    private ImageButton buttonMusicAddTransAdminUpdate;


    private int idTransport;

    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_trans_admin);
        buttonMusicAddTransAdminUpdate = (ImageButton) findViewById(R.id.buttonMusicAddTransAdminUpdate);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddTransAdminUpdate.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddTransAdminUpdate.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddTransAdminUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddTransAdminUpdate.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddTransAdminUpdate.setBackgroundResource(R.drawable.pause);
                }
            }
        });

        Intent intent = getIntent();
        idTransport = Integer.parseInt(intent.getStringExtra("idTrans"));


        Transport transport = Database.myTransportDAO.fetchTransportById(idTransport);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        buttonAddTransUpdate = (Button) findViewById(R.id.buttonAddTransUpdate);
        inputNameTransUpdate = (EditText) findViewById(R.id.inputNameTransUpdate);
        inputNameTransUpdate.setText(transport.getName());
        inputPriceTransUpdate = (EditText) findViewById(R.id.inputPriceTransUpdate);
        inputPriceTransUpdate.setText(String.valueOf(transport.getPrice()));
        inputPlacesTransUpdate = (EditText) findViewById(R.id.inputPlacesTransUpdate);
        inputPlacesTransUpdate.setText(String.valueOf(transport.getNumberOfPlaces()));
        inputUrlTransUpdate = (EditText) findViewById(R.id.inputUrlTransUpdate);
        inputUrlTransUpdate.setText(transport.getPicture());
        inputDescriptionTransUpdate = (EditText) findViewById(R.id.inputDescriptionTransUpdate);
        inputDescriptionTransUpdate.setText(transport.getDescription());
        btnYesUpdate = (RadioButton) findViewById(R.id.yesTransUpdate);
        btnNoUpdate = (RadioButton) findViewById(R.id.noTransUpdate);

        if (transport.getNeedDrivingLicense()==1){
            btnYesUpdate.setChecked(true);
        }
        else {
            btnNoUpdate.setChecked(true);
        }

        // Send button Click Event
        buttonAddTransUpdate.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                if (!(inputNameTransUpdate.getText().toString().trim().length() == 0) && !(inputPriceTransUpdate.getText().toString().trim().length() == 0) && !(inputPlacesTransUpdate.getText().toString().trim().length() == 0) && (btnYesUpdate.isChecked() || btnNoUpdate.isChecked())) {

                    String url;
                    String description;

                    String name = inputNameTransUpdate.getText().toString();
                    String price = inputPriceTransUpdate.getText().toString();
                    String places = inputPlacesTransUpdate.getText().toString();

                    if (inputUrlTransUpdate.getText().toString().trim().length() == 0){
                        url = "no information";
                    }
                    else {
                        url = inputUrlTransUpdate.getText().toString();
                    }
                    if (inputDescriptionTransUpdate.getText().toString().trim().length() == 0){
                        description = "no information";
                    }
                    else {
                        description = inputDescriptionTransUpdate.getText().toString();
                    }

                    int drivingLicense;
                    if (btnYesUpdate.isChecked()){
                        drivingLicense = 1;
                    }
                    else {
                        drivingLicense = 0;
                    }

                    updateTransAdmin(name, price, places, url ,description,drivingLicense);

                }
                else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please fill the fields with an (*).", Toast.LENGTH_LONG)
                            .show();
                }
            }

        });
    }

    /**
     * Function to update the transport in MySQL database
     * */
    private void updateTransAdmin(final String name, final String price, final String places, final String url, final String description, final int drivingLicense) {
        // Tag used to cancel the request
        String tag_string_req = "req_saveForm";
        pDialog.setMessage("Updating the transport ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATETRANSPORTADMIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                        // Updating row in Transport table
                        Database.myTransportDAO.updateTransport(idTransport,name,description,url,Integer.parseInt(places),drivingLicense,Integer.parseInt(price));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(ModifyTransAdminActivity.this, AdministratorMenuActivity.class);
                startActivity(intent);
                finish();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTransport", String.valueOf(idTransport));
                params.put("name", name);
                params.put("price", price);
                params.put("description", description);
                params.put("picture", url);
                params.put("numberOfPlaces", places);
                params.put("needDrivingLicense", String.valueOf(drivingLicense));

                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddTransAdminUpdate.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddTransAdminUpdate.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddTransAdminUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddTransAdminUpdate.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddTransAdminUpdate.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }

}
