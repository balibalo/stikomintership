package internshipstikombali.balitourism.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.Restaurant;
import internshipstikombali.balitourism.helper.Database;

public class ModifyRestAdminActivity extends AppCompatActivity {

    private static final String TAG = ModifyRestAdminActivity.class.getSimpleName();

    private Button buttonAddRestUpdate;
    private EditText inputNameRestUpdate;
    private EditText inputLongitudeRestUpdate;
    private EditText inputLatitudeRestUpdate;
    private EditText inputPhoneRestUpdate;
    private EditText inputWebsiteRestUpdate;
    private EditText inputPriceRestUpdate;
    private EditText inputUrlRestUpdate;
    private EditText inputDescriptionRestUpdate;
    private TimePicker timePickerOpeningAddRestUpdate;
    private TimePicker timePickerClosingAddRestUpdate;
    private ImageButton buttonMusicAddRestAdminUpdate;


    private int idRest;

    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_rest_admin);
        buttonMusicAddRestAdminUpdate = (ImageButton) findViewById(R.id.buttonMusicAddRestAdminUpdate);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddRestAdminUpdate.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddRestAdminUpdate.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddRestAdminUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddRestAdminUpdate.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddRestAdminUpdate.setBackgroundResource(R.drawable.pause);
                }
            }
        });

        Intent intent = getIntent();
        idRest = Integer.parseInt(intent.getStringExtra("idRest"));

        Restaurant restaurant = Database.myRestaurantDAO.fetchRestaurantById(idRest);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        buttonAddRestUpdate = (Button) findViewById(R.id.buttonAddRestUpdate);
        inputNameRestUpdate = (EditText) findViewById(R.id.inputNameRestUpdate);
        inputNameRestUpdate.setText(restaurant.getName());
        inputLongitudeRestUpdate = (EditText) findViewById(R.id.inputLongitudeRestUpdate);
        inputLongitudeRestUpdate.setText(restaurant.getLongitude());
        inputLatitudeRestUpdate = (EditText) findViewById(R.id.inputLatitudeRestUpdate);
        inputLatitudeRestUpdate.setText(restaurant.getLatitude());
        inputPhoneRestUpdate = (EditText) findViewById(R.id.inputPhoneRestUpdate);
        inputPhoneRestUpdate.setText(restaurant.getPhoneNumber());
        inputWebsiteRestUpdate = (EditText) findViewById(R.id.inputWebsiteRestUpdate);
        inputWebsiteRestUpdate.setText(restaurant.getWebsite());
        inputPriceRestUpdate = (EditText) findViewById(R.id.inputPriceRestUpdate);
        inputPriceRestUpdate.setText(String.valueOf(restaurant.getPrice()));
        inputUrlRestUpdate = (EditText) findViewById(R.id.inputUrlRestUpdate);
        inputUrlRestUpdate.setText(restaurant.getPicture());
        inputDescriptionRestUpdate = (EditText) findViewById(R.id.inputDescriptionRestUpdate);
        inputDescriptionRestUpdate.setText(restaurant.getDescription());

        timePickerOpeningAddRestUpdate = (TimePicker) findViewById(R.id.timePickerOpeningAddRestUpdate);
        timePickerOpeningAddRestUpdate.setIs24HourView(true);
        timePickerClosingAddRestUpdate = (TimePicker) findViewById(R.id.timePickerClosingAddRestUpdate);
        timePickerClosingAddRestUpdate.setIs24HourView(true);

        int currentApiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentApiVersion > android.os.Build.VERSION_CODES.LOLLIPOP_MR1){
            timePickerOpeningAddRestUpdate.setHour(restaurant.getOpeningHour());
            timePickerOpeningAddRestUpdate.setMinute(restaurant.getOpeningMinute());
            timePickerClosingAddRestUpdate.setHour(restaurant.getClosingHour()+12);
            timePickerClosingAddRestUpdate.setMinute(restaurant.getClosingMinute());

        } else {
            timePickerOpeningAddRestUpdate.setCurrentHour(restaurant.getOpeningHour());
            timePickerOpeningAddRestUpdate.setCurrentMinute(restaurant.getOpeningMinute());
            timePickerClosingAddRestUpdate.setCurrentHour(restaurant.getClosingHour()+12);
            timePickerClosingAddRestUpdate.setCurrentMinute(restaurant.getClosingMinute());
        }

        // Send button Click Event
        buttonAddRestUpdate.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                int openingHour;
                int closingHour;
                int openingMinute;
                int closingMinute;
                int currentApiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentApiVersion > android.os.Build.VERSION_CODES.LOLLIPOP_MR1){
                    openingHour = timePickerOpeningAddRestUpdate.getHour();
                    closingHour = timePickerClosingAddRestUpdate.getHour();
                    openingMinute = timePickerOpeningAddRestUpdate.getMinute();
                    closingMinute = timePickerClosingAddRestUpdate.getMinute();
                } else {
                    openingHour = timePickerOpeningAddRestUpdate.getCurrentHour();
                    closingHour = timePickerClosingAddRestUpdate.getCurrentHour();
                    openingMinute = timePickerOpeningAddRestUpdate.getCurrentMinute();
                    closingMinute = timePickerClosingAddRestUpdate.getCurrentMinute();
                }

                if (!(inputNameRestUpdate.getText().toString().trim().length() == 0) && !(inputLongitudeRestUpdate.getText().toString().trim().length() == 0) && !(inputLatitudeRestUpdate.getText().toString().trim().length() == 0) && !(inputPriceRestUpdate.getText().toString().trim().length() == 0)) {
                    if (openingHour<=closingHour){

                        String phone;
                        String website;
                        String url;
                        String description;

                        String name = inputNameRestUpdate.getText().toString();
                        String lng = inputLongitudeRestUpdate.getText().toString();
                        String lat = inputLatitudeRestUpdate.getText().toString();
                        String price = inputPriceRestUpdate.getText().toString();

                        if (inputPhoneRestUpdate.getText().toString().trim().length() == 0){
                            phone = "no information";
                        }
                        else {
                            phone = inputPhoneRestUpdate.getText().toString();
                        }
                        if (inputWebsiteRestUpdate.getText().toString().trim().length() == 0){
                            website = "no information";
                        }
                        else {
                            website = inputWebsiteRestUpdate.getText().toString();
                        }
                        if (inputUrlRestUpdate.getText().toString().trim().length() == 0){
                            url = "no information";
                        }
                        else {
                            url = inputUrlRestUpdate.getText().toString();
                        }
                        if (inputDescriptionRestUpdate.getText().toString().trim().length() == 0){
                            description = "no information";
                        }
                        else {
                            description = inputDescriptionRestUpdate.getText().toString();
                        }

                        if (openingHour==closingHour && openingMinute==closingMinute){
                            openingHour = 0;
                            openingMinute = 0;
                            closingHour = 12;
                            closingMinute = 0;
                        }
                        else {
                            if (openingHour>12){
                                openingHour = 12;
                                openingMinute = 0;
                            }
                            if (closingHour<12){
                                closingHour = 1;
                                closingMinute = 0;
                            }
                            else {
                                closingHour = closingHour-12;
                            }
                        }

                        String oh = String.valueOf(openingHour);
                        String om = String.valueOf(openingMinute);
                        String ch = String.valueOf(closingHour);
                        String cm = String.valueOf(closingMinute);


                        updateRestAdmin(name, lng, lat, phone, website, price, url ,description, oh, om, ch, cm);

                    }
                    else {
                        // Prompt user to enter credentials
                        Toast.makeText(getApplicationContext(),
                                "The closing hour must be superior than the opening hour.", Toast.LENGTH_LONG)
                                .show();
                    }
                }
                else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please fill the fields with an (*).", Toast.LENGTH_LONG)
                            .show();
                }

            }

        });


    }

    /**
     * Function to update the restaurant in MySQL
     * */
    private void updateRestAdmin(final String name, final String lng, final String lat, final String phone, final String website, final String price, final String url, final String description, final String oh, final String om, final String ch, final String cm) {
        // Tag used to cancel the request
        String tag_string_req = "req_saveForm";
        pDialog.setMessage("Updating restaurant ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATERESTAURANTADMIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                        Database.myRestaurantDAO.updateRestaurant(idRest,name,lng,lat,phone,website,Integer.parseInt(price),description,url,Integer.parseInt(oh),Integer.parseInt(om),Integer.parseInt(ch),Integer.parseInt(cm));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(ModifyRestAdminActivity.this, AdministratorMenuActivity.class);
                startActivity(intent);
                finish();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idRestaurant", String.valueOf(idRest));
                params.put("name", name);
                params.put("longitude", lng);
                params.put("latitude", lat);
                params.put("phoneNumber", phone);
                params.put("website", website);
                params.put("price", price);
                params.put("description", description);
                params.put("picture", url);
                params.put("openingHour", oh);
                params.put("openingMinute", om);
                params.put("closingHour", ch);
                params.put("closingMinute", cm);
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddRestAdminUpdate.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddRestAdminUpdate.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddRestAdminUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddRestAdminUpdate.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddRestAdminUpdate.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }


}
