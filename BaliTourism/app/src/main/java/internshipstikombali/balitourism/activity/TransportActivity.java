package internshipstikombali.balitourism.activity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.Transport;
import internshipstikombali.balitourism.business.TripContainsTransport;
import internshipstikombali.balitourism.helper.Database;
import internshipstikombali.balitourism.helper.SessionManager;

public class TransportActivity extends AppCompatActivity {

    private static final String TAG = TransportActivity.class.getSimpleName();

    Bitmap bitmap;
    ImageView imageView2;
    private ImageButton btnDelete;
    private ImageButton btnDate;
    private SessionManager session;
    private int idOfTrip;
    private String day;
    private int idOfUser;
    private int idOfTransport;
    private int durationOfTrip;
    private String formatted;
    private String arrivalDateOfTrip;
    private int idTransFormer;
    private ImageButton buttonMusicTransport;


    private int day2;
    private int month;
    private int year;


    List<TripContainsTransport> reqTransport;


    private DatePickerDialog datePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transport);

        // Session manager
        session = new SessionManager(getApplicationContext());

        imageView2 = (ImageView) findViewById(R.id.imageView2);
        btnDelete = (ImageButton) findViewById(R.id.deleteButton2);
        btnDate = (ImageButton) findViewById(R.id.dateButton2);
        buttonMusicTransport = (ImageButton) findViewById(R.id.buttonMusicTransport);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicTransport.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicTransport.setBackgroundResource(R.drawable.play);
        }

        buttonMusicTransport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicTransport.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicTransport.setBackgroundResource(R.drawable.pause);
                }
            }
        });

        Intent intent = getIntent();
        idOfUser = Integer.parseInt(intent.getStringExtra("idUser"));
        day = intent.getStringExtra("day");
        idOfTrip = Integer.parseInt(intent.getStringExtra("idTrip"));
        durationOfTrip = Integer.parseInt(intent.getStringExtra("durationTrip"));
        arrivalDateOfTrip = intent.getStringExtra("arrivalDate");

        // Parametres : (int)idUser, (string)day
        // j'ai besoin du transport du jour

        idOfTransport = -1;
        reqTransport = Database.myTripContainsTransportDAO.fetchAllTripContainsTransport();
        for (int i = 0; i<reqTransport.size(); i++){
            if ((reqTransport.get(i).getIdTrip() == idOfTrip) && reqTransport.get(i).getDateTransport().equals(day)){
                idOfTransport = reqTransport.get(i).getIdTransport();
            }
        }

        if (idOfTransport == -1){
            final TextView nameTransp = (TextView) findViewById(R.id.nameTransport);
            final TextView priceTransp = (TextView) findViewById(R.id.priceTransport);
            final TextView descriptionTransp = (TextView) findViewById(R.id.descriptionTransport);
            final TextView drivingLicenseTransp = (TextView) findViewById(R.id.drivingLicenseTransport);

            nameTransp.setText("There is no transport provided for this day.");
            imageView2.setVisibility(View.GONE);
            btnDelete.setVisibility(View.GONE);
            btnDate.setVisibility(View.GONE);
            priceTransp.setVisibility(View.GONE);
            descriptionTransp.setVisibility(View.GONE);
            drivingLicenseTransp.setVisibility(View.GONE);
        }
        else {
            Transport transport = Database.myTransportDAO.fetchTransportById(idOfTransport);
            String nameTransport = transport.getName();
            String priceTransport;
            if (nameTransport.equals("taxi")) {
                priceTransport = "Average price : " + transport.getPrice() + " irp per minute";
            }
            else {
                priceTransport = "Average price : " + transport.getPrice() + " irp per day";
            }

            String description = "Description : " + transport.getDescription();

            String url = transport.getPicture();

            String needDrivingLicense = String.valueOf(transport.getNeedDrivingLicense());

            String drivingLicense = "Be careful ! You need driving license to use this vehicle !!!";

            new LoadImage().execute(url);

            final TextView nameTransp = (TextView) findViewById(R.id.nameTransport);
            nameTransp.setText(nameTransport);

            final TextView priceTransp = (TextView) findViewById(R.id.priceTransport);
            priceTransp.setText(priceTransport);

            final TextView descriptionTransp = (TextView) findViewById(R.id.descriptionTransport);
            descriptionTransp.setText(description);

            final TextView drivingLicenseTransp = (TextView) findViewById(R.id.drivingLicenseTransport);

            if (needDrivingLicense.equals("1")){
                drivingLicenseTransp.setText(drivingLicense);
                drivingLicenseTransp.setVisibility(View.VISIBLE);
            }
            else {
                drivingLicenseTransp.setVisibility(View.GONE);
            }

            final AlertDialog diaBox = AskOption();
            final AlertDialog diaBox2 = AskOption2();
            final AlertDialog diaBox3 = AskOption3();

            // delete button Click Event
            btnDelete.setOnClickListener(new View.OnClickListener()  {
                public void onClick(View view) {
                    diaBox.show();
                }
            });

            String[] partsOfDate = day.split("/");
            day2 = Integer.parseInt(partsOfDate[0]);
            month = Integer.parseInt(partsOfDate[1]);
            year = Integer.parseInt(partsOfDate[2]);

            // date button Click Event
            btnDate.setOnClickListener(new View.OnClickListener()  {
                public void onClick(View view) {
                    //showStartDateDialog(view);
                    datePickerDialog.setMessage("Change the date");
                    datePickerDialog.updateDate(year, month - 1, day2);
                    datePickerDialog.show();
                }
            });

            Calendar newCalendar = Calendar.getInstance();
            datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    SimpleDateFormat format1 = new SimpleDateFormat("d/M/yyyy");
                    formatted = format1.format(newDate.getTime());
                    SimpleDateFormat sdf = new SimpleDateFormat("d/M/yyyy");

                    Date startTrip = null;
                    try {
                        startTrip = sdf.parse(arrivalDateOfTrip);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    long startTripDate = startTrip.getTime();

                    Date endTrip = null;
                    try {
                        endTrip = sdf.parse(arrivalDateOfTrip);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    long endTripDate = endTrip.getTime()+(86400000*durationOfTrip);

                    Date newDateTrip = null;
                    try {
                        newDateTrip = sdf.parse(formatted);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    long newTripDate = newDateTrip.getTime();

                    if (!(newTripDate>=startTripDate && newTripDate<endTripDate)){
                        diaBox3.show();
                    }
                    else if (checkTransport()) {
                        diaBox2.show();
                    }
                    else {
                        updateTransport();
                        Toast.makeText(getApplicationContext(),
                                "The transport has been updated successfully !", Toast.LENGTH_LONG)
                                .show();
                        Intent myIntent = new Intent(TransportActivity.this, HomeActivity.class);
                        myIntent.putExtra("idUser", String.valueOf(idOfUser));
                        myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                        myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                        myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                        startActivity(myIntent);
                        finish();
                    }

                }

            },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        idOfTransport = -1;
        reqTransport = Database.myTripContainsTransportDAO.fetchAllTripContainsTransport();
        for (int i = 0; i<reqTransport.size(); i++){
            if ((reqTransport.get(i).getIdTrip() == idOfTrip) && reqTransport.get(i).getDateTransport().equals(day)){
                idOfTransport = reqTransport.get(i).getIdTransport();
            }
        }
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicTransport.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicTransport.setBackgroundResource(R.drawable.play);
        }

        buttonMusicTransport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicTransport.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicTransport.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }

    private class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {
            if(image != null){
                imageView2.setImageBitmap(image);
            }else{

            }
        }
    }

    /**
     * function to check if there is already a transport at this date in mysql db
     * return true if there is already a transport
     * */
    private boolean checkTransport() {
        reqTransport = Database.myTripContainsTransportDAO.fetchAllTripContainsTransport();
        for (int i = 0; i<reqTransport.size(); i++){
            if ((reqTransport.get(i).getIdTrip() == idOfTrip) && reqTransport.get(i).getDateTransport().equals(formatted)){
                idTransFormer = reqTransport.get(i).getIdTransport();
                return true;
            }
        }
        return false;
    }

    /**
     * function to delete the transport from the trip in mysql db
     * */
    private void deleteTransport() {
        // Tag used to cancel the request
        String tag_string_req = "req_delete";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETETRANSPORT, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                        // delete row in tripContainsTransport table
                        Database.myTripContainsTransportDAO.deleteTripContainsTransport(idOfTrip,idOfTransport,day);

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to deletetransport url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idOfTrip));
                params.put("dateTransport", day);
                params.put("idTransport", String.valueOf(idOfTransport));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to delete the former transport from the trip in mysql db
     * */
    private void deleteFormerTransport() {
        // Tag used to cancel the request
        String tag_string_req = "req_delete";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETETRANSPORT, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json

                        // delete row in tripContainsTransport table
                        Database.myTripContainsTransportDAO.deleteTripContainsTransport(idOfTrip,idTransFormer,formatted);

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to deletetransport url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idOfTrip));
                params.put("dateTransport", formatted);
                params.put("idTransport", String.valueOf(idTransFormer));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to add the transport from the trip in mysql db
     * */
    private void addTransport() {
        // Tag used to cancel the request
        String tag_string_req = "req_add";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDTRANSPORT, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                        // add row in tripContainsTransport table
                        Database.myTripContainsTransportDAO.addTripContainsTransport(new TripContainsTransport(idOfTrip,idOfTransport,formatted, 0, 0));

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to deleteaccommodation url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idOfTrip));
                params.put("idTransport", String.valueOf(idOfTransport));
                params.put("dateTransport", formatted);

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to update the transport from the trip in mysql db
     * */
    private void updateTransport() {
        // Tag used to cancel the request
        String tag_string_req = "req_update";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATETRANSPORT, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                        // update row in tripContainsTransport table
                        Database.myTripContainsTransportDAO.updateTripContainsTransport(idOfTrip,day,formatted);
                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to updatetransport url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idOfTrip));
                params.put("dateTransport", day);
                params.put("newDate", formatted);

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    private AlertDialog AskOption()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setMessage("Do you really want to delete this transport for this day ?")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        deleteTransport();
                        Toast.makeText(getApplicationContext(),
                                "The transport has been deleted successfully !", Toast.LENGTH_LONG)
                                .show();
                        Intent myIntent = new Intent(TransportActivity.this, HomeActivity.class);
                        myIntent.putExtra("idUser", String.valueOf(idOfUser));
                        myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                        myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                        myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                        startActivity(myIntent);
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    private AlertDialog AskOption2()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                //set message, title, and icon
                .setMessage("There is already a transport at this date, do you want to change it by this one ?")
                .setPositiveButton("Change", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        deleteFormerTransport();
                        deleteTransport();
                        addTransport();
                        Toast.makeText(getApplicationContext(),
                                "The date of the transport has been updated successfully !", Toast.LENGTH_LONG)
                                .show();
                        Intent myIntent = new Intent(TransportActivity.this, HomeActivity.class);
                        myIntent.putExtra("idUser", String.valueOf(idOfUser));
                        myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                        myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                        myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                        startActivity(myIntent);
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    private AlertDialog AskOption3()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setMessage("You must choose a day according to the dates of your trip.")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
