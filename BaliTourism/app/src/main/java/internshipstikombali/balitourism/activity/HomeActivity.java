package internshipstikombali.balitourism.activity;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TabHost;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppController;

public class HomeActivity extends TabActivity {

    private int idOfUser;
    private int idOfTrip;
    private int durationOfTrip;
    private String arrivalDateOfTrip;
    private ImageButton buttonMusicHome;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        buttonMusicHome = (ImageButton) findViewById(R.id.buttonMusicHome);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicHome.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicHome.setBackgroundResource(R.drawable.play);
        }

        buttonMusicHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicHome.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicHome.setBackgroundResource(R.drawable.pause);
                }
            }
        });

        Intent intent = getIntent();
        idOfUser = Integer.parseInt(intent.getStringExtra("idUser"));
        idOfTrip = Integer.parseInt(intent.getStringExtra("idTrip"));
        durationOfTrip = Integer.parseInt(intent.getStringExtra("durationTrip"));
        arrivalDateOfTrip = intent.getStringExtra("arrivalDate");

        TabHost mTabHost = getTabHost();

        Intent intentAllAccommodation = new Intent(HomeActivity.this, AllAccommodationActivity.class);
        intentAllAccommodation.putExtra("durationTrip", String.valueOf(durationOfTrip));
        intentAllAccommodation.putExtra("arrivalDate", arrivalDateOfTrip);
        intentAllAccommodation.putExtra("idTrip", String.valueOf(idOfTrip));
        intentAllAccommodation.putExtra("idUser", String.valueOf(idOfUser));

        Intent intentAllActivity = new Intent(HomeActivity.this, AllActivityActivity.class);
        intentAllActivity.putExtra("durationTrip", String.valueOf(durationOfTrip));
        intentAllActivity.putExtra("arrivalDate", arrivalDateOfTrip);
        intentAllActivity.putExtra("idTrip", String.valueOf(idOfTrip));
        intentAllActivity.putExtra("idUser", String.valueOf(idOfUser));

        Intent intentAllRestaurant = new Intent(HomeActivity.this, AllRestaurantActivity.class);
        intentAllRestaurant.putExtra("durationTrip", String.valueOf(durationOfTrip));
        intentAllRestaurant.putExtra("arrivalDate", arrivalDateOfTrip);
        intentAllRestaurant.putExtra("idTrip", String.valueOf(idOfTrip));
        intentAllRestaurant.putExtra("idUser", String.valueOf(idOfUser));

        Intent intentAllTransport = new Intent(HomeActivity.this, AllTransportActivity.class);
        intentAllTransport.putExtra("durationTrip", String.valueOf(durationOfTrip));
        intentAllTransport.putExtra("arrivalDate", arrivalDateOfTrip);
        intentAllTransport.putExtra("idTrip", String.valueOf(idOfTrip));
        intentAllTransport.putExtra("idUser", String.valueOf(idOfUser));

        Intent intentMap = new Intent(HomeActivity.this, MapActivity.class);
        intentMap.putExtra("durationTrip", String.valueOf(durationOfTrip));
        intentMap.putExtra("arrivalDate", arrivalDateOfTrip);
        intentMap.putExtra("idTrip", String.valueOf(idOfTrip));
        intentMap.putExtra("idUser", String.valueOf(idOfUser));


        mTabHost.addTab(mTabHost.newTabSpec("myTrip").setIndicator("My Trip").setContent(new Intent(HomeActivity.this, MyTripActivity.class)));
        mTabHost.addTab(mTabHost.newTabSpec("accommodations").setIndicator("Accommodations").setContent(intentAllAccommodation));
        mTabHost.addTab(mTabHost.newTabSpec("activities").setIndicator("Activities").setContent(intentAllActivity));
        mTabHost.addTab(mTabHost.newTabSpec("restaurants").setIndicator("Restaurants").setContent(intentAllRestaurant));
        mTabHost.addTab(mTabHost.newTabSpec("transports").setIndicator("Transports").setContent(intentAllTransport));
        mTabHost.addTab(mTabHost.newTabSpec("map").setIndicator("Map").setContent(intentMap));
        mTabHost.setCurrentTab(0);
    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicHome.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicHome.setBackgroundResource(R.drawable.play);
        }

        buttonMusicHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicHome.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicHome.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }

    @Override
    public void onBackPressed()
    {
    }

}
