package internshipstikombali.balitourism.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.Accommodation;
import internshipstikombali.balitourism.helper.Database;

public class AccoAdminActivity extends AppCompatActivity {

    private static final String TAG = AccoAdminActivity.class.getSimpleName();

    Bitmap bitmap;
    ImageView imageView1;
    private ImageButton btnModify;
    private ImageButton btnDelete;
    private int idAcco;
    private ImageButton buttonMusicAccoAdmin;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acco_admin);

        imageView1 = (ImageView) findViewById(R.id.imageViewAdmin);
        btnDelete = (ImageButton) findViewById(R.id.deleteButtonAdmin);
        btnModify = (ImageButton) findViewById(R.id.dateButtonAdmin);
        buttonMusicAccoAdmin = (ImageButton) findViewById(R.id.buttonMusicAccoAdmin);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAccoAdmin.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAccoAdmin.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAccoAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAccoAdmin.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAccoAdmin.setBackgroundResource(R.drawable.pause);
                }
            }
        });





        Intent intent = getIntent();
        idAcco = Integer.parseInt(intent.getStringExtra("idAcco"));

        Accommodation accommodation = Database.myAccommodationDAO.fetchAccommodationById(idAcco);
        String nameAccommodation = accommodation.getName();
        String priceAccommodation = "Average price : " + accommodation.getPrice() + " irp per person";
        double latitudeAccommodation = Double.parseDouble(accommodation.getLatitude());
        double longitudeAccommodation = Double.parseDouble(accommodation.getLongitude());

        String phoneNumber = "Phone number : " + accommodation.getPhoneNumber();

        String website = "Website : " + accommodation.getWebsite();

        String description = "Description : " + accommodation.getDescription();

        Geocoder geocoder;
        List<Address> addresses = new ArrayList<Address>();
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitudeAccommodation, longitudeAccommodation, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String address;
        if (addresses.size()==0){
            address = "Location : no information";
        }
        else {
            address = "Location : " + addresses.get(0).getAddressLine(0)+", " + addresses.get(0).getLocality();
        }

        String url = accommodation.getPicture();

        new LoadImage().execute(url);

        final TextView nameAcco = (TextView) findViewById(R.id.nameAccommodationAdmin);
        nameAcco.setText(nameAccommodation);

        final TextView priceAcco = (TextView) findViewById(R.id.priceAccommodationAdmin);
        priceAcco.setText(priceAccommodation);

        final TextView locationAcco = (TextView) findViewById(R.id.locationAccommodationAdmin);
        locationAcco.setText(address);

        final TextView descriptionAcco = (TextView) findViewById(R.id.descriptionAccommodationAdmin);
        descriptionAcco.setText(description);

        final TextView phoneAcco = (TextView) findViewById(R.id.phoneAccommodationAdmin);
        phoneAcco.setText(phoneNumber);

        final TextView websiteAcco = (TextView) findViewById(R.id.websiteAccommodationAdmin);
        websiteAcco.setText(website);

        final AlertDialog diaBox = AskOption();

        // delete button Click Event
        btnDelete.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                diaBox.show();
            }
        });

        // modify button Click Event
        btnModify.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                Intent myIntent = new Intent(AccoAdminActivity.this, ModifyAccoAdminActivity.class);
                myIntent.putExtra("idAcco", String.valueOf(idAcco));
                startActivity(myIntent);
            }
        });

    }

    private class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {
            if(image != null){
                imageView1.setImageBitmap(image);
            }else{

            }
        }
    }

    /**
     * function to delete the accommodation in mysql db
     * */
    private void deleteAccommodationAdmin() {
        // Tag used to cancel the request
        String tag_string_req = "req_delete";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETEACCOMMODATIONADMIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    Database.myAccommodationDAO.deleteAccommodation(idAcco);

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to deleteaccommodationadmin url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idAccommodation", String.valueOf(idAcco));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private AlertDialog AskOption()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setMessage("Do you really want to delete this accommodation from the database ?")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        deleteAccommodationAdmin();

                        Toast.makeText(getApplicationContext(), "The accommodation has been deleted successfully !", Toast.LENGTH_LONG).show();
                        Intent myIntent = new Intent(AccoAdminActivity.this, AdministratorMenuActivity.class);
                        startActivity(myIntent);
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAccoAdmin.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAccoAdmin.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAccoAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAccoAdmin.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAccoAdmin.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }


}
