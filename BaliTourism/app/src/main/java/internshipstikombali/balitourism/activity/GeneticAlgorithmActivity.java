package internshipstikombali.balitourism.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.Accommodation;
import internshipstikombali.balitourism.business.Activity;
import internshipstikombali.balitourism.business.Restaurant;
import internshipstikombali.balitourism.business.Transport;
import internshipstikombali.balitourism.business.TripContainsAccommodation;
import internshipstikombali.balitourism.business.TripContainsActivity;
import internshipstikombali.balitourism.business.TripContainsRestaurant;
import internshipstikombali.balitourism.business.TripContainsTransport;
import internshipstikombali.balitourism.helper.Database;
import internshipstikombali.balitourism.helper.DirectionsJSONParser;
import internshipstikombali.balitourism.helper.SessionManager;

/*
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
*/

public class GeneticAlgorithmActivity extends AppCompatActivity {

    private static final String TAG = GeneticAlgorithmActivity.class.getSimpleName();
    private SessionManager session;
    private int time;
    private ProgressDialog pDialog;



    private final int replacementsByGeneration = 1;
    private final int crossoverProbability = 30;
    private final int mutationProbability = 70;
    private final int nbOfGenerations = 1;
    private final int initialPopulationSize = 2 ;
    private final int bestGroupSize = 2;
    private List<Integer> bestTrips = new ArrayList<Integer>();
    private List<ArrayList> trips = new ArrayList<ArrayList>();
    private ArrayList bestTrip = new ArrayList();

    int idOfUser;
    int durationNext;
    int numberOfTravellersNext;
    int drivingLicenseNext;
    int budgetNext;
    int timeSpentOutsideNext;
    int idOfTrip;
    String dateOfArrivalNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_genetic_algorithm);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        pDialog.setMessage("We are now creating your trip. It will take a few minutes ...");
        if (!pDialog.isShowing()) {
            pDialog.show();
        }



        Thread timerThread = new Thread(){
            public void run(){
                try{
                    sleep(2000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = getIntent();
                        String idTripN = intent.getStringExtra("idTrip");
                        String idN = intent.getStringExtra("idUser");
                        String durationN = intent.getStringExtra("duration");
                        String numberOfTravellersN = intent.getStringExtra("numberOfTravellers");
                        String drivingLicenseN = intent.getStringExtra("drivingLicense");
                        String budgetN = intent.getStringExtra("budget");
                        String timeSpentOutsideN = intent.getStringExtra("timeSpentOutside");
                        ArrayList<String> prefAccommodationN = intent.getStringArrayListExtra("prefAccommodation");
                        ArrayList<String> prefFacilityN = intent.getStringArrayListExtra("prefFacility");
                        ArrayList<String> prefActivityN = intent.getStringArrayListExtra("prefActivity");

                        idOfTrip = Integer.parseInt(idTripN);
                        idOfUser = Integer.parseInt(idN);
                        durationNext = Integer.parseInt(durationN);
                        numberOfTravellersNext = Integer.parseInt(numberOfTravellersN);
                        drivingLicenseNext = Integer.parseInt(drivingLicenseN);
                        budgetNext = Integer.parseInt(budgetN);
                        timeSpentOutsideNext = Integer.parseInt(timeSpentOutsideN);
                        dateOfArrivalNext = intent.getStringExtra("dateOfArrival");

                        // Session manager
                        session = new SessionManager(getApplicationContext());

                        // save informations concerning the user
                        saveIdTrip(idOfUser);

                        // create initial population
                        ArrayList initialPopulation = new ArrayList();
                        for (int i=0;i<initialPopulationSize;i++){
                            //initial population
                            Accommodation AccommodationArrayInitial[] = new Accommodation[durationNext-1];
                            Restaurant RestaurantArrayInitial[] = new Restaurant[(durationNext-2)*2];
                            Activity ActivityArrayInitial[] = new Activity[(durationNext-2)*2];
                            Transport TransportArrayInitial[] = new Transport[(durationNext-2)];
                            Integer TravelTimeInitial[] = new Integer[(durationNext-2)];
                            // get all restaurants, activities, accommodations and transports in the database
                            List<Restaurant> restaurantList = new ArrayList<Restaurant>();
                            List<Activity> activityList = new ArrayList<Activity>();
                            List<Accommodation> accommodationList = new ArrayList<Accommodation>();
                            List<Transport> transportList = new ArrayList<Transport>();
                            for (Accommodation accommodation : Database.myAccommodationDAO.fetchAllAccommodations()) {
                                accommodationList.add(accommodation);
                            }
                            for (Activity activity : Database.myActivityDAO.fetchAllActivities()) {
                                activityList.add(activity);
                            }
                            for (Restaurant restaurant : Database.myRestaurantDAO.fetchAllRestaurants()) {
                                restaurantList.add(restaurant);
                            }
                            for (Transport transport : Database.myTransportDAO.fetchAllTransports()) {
                                transportList.add(transport);
                            }
                            initialPopulation.add(generateRandom(restaurantList, activityList, accommodationList, transportList, AccommodationArrayInitial, ActivityArrayInitial, RestaurantArrayInitial, TransportArrayInitial, prefAccommodationN, prefActivityN, prefFacilityN,i,TravelTimeInitial));
                        }

                        int currentGeneration = 0;
                        Random rand = new Random();
                        int tripIndex;
                        ArrayList parent1;
                        ArrayList parent2;
                        ArrayList parentClone;
                        ArrayList child;
                        float tripScore;
                        float tripScoreMutation;
                        float tripScoreCrossOver;

                        List<Integer> bestTripsCopy;

                        List<Restaurant> restaurantListDB = new ArrayList<Restaurant>();
                        List<Activity> activityListDB = new ArrayList<Activity>();
                        List<Accommodation> accommodationListDB = new ArrayList<Accommodation>();
                        List<Transport> transportListDB = new ArrayList<Transport>();
                        for (Accommodation accommodation : Database.myAccommodationDAO.fetchAllAccommodations()) {
                            accommodationListDB.add(accommodation);
                        }
                        for (Activity activity : Database.myActivityDAO.fetchAllActivities()) {
                            activityListDB.add(activity);
                        }
                        for (Restaurant restaurant : Database.myRestaurantDAO.fetchAllRestaurants()) {
                            restaurantListDB.add(restaurant);
                        }
                        for (Transport transport : Database.myTransportDAO.fetchAllTransports()) {
                            transportListDB.add(transport);
                        }

                        while (currentGeneration < nbOfGenerations) {
                            for (int j = 0; j < replacementsByGeneration; j++) {
                                //crossover
                                tripIndex = rand.nextInt(bestTrips.size());
                                parent1 = trips.get(bestTrips.get(tripIndex));

                                if (bestTrips.size() == 1) {
                                    parent2 = trips.get(rand.nextInt(trips.size()));
                                } else {
                                    bestTripsCopy = (ArrayList<Integer>) ((ArrayList<Integer>) bestTrips).clone();
                                    bestTripsCopy.remove(tripIndex);
                                    tripIndex = rand.nextInt(bestTripsCopy.size());
                                    parent2 = trips.get(bestTrips.get(tripIndex));
                                }

                                child = crossover(parent1, parent2);

                                if (child != null){
                                    tripIndex = rand.nextInt(initialPopulationSize);
                                    while (isInBest(tripIndex)){
                                        tripIndex = rand.nextInt(initialPopulationSize);
                                    }
                                    trips.remove(tripIndex);
                                    tripScoreCrossOver = evaluationTrip(budgetNext,prefFacilityN,prefActivityN,prefAccommodationN,(Accommodation[])child.get(0),(Activity[])child.get(1),(double)child.get(5),timeSpentOutsideNext,(Integer[])child.get(6));
                                    child.add(6, tripScoreCrossOver);
                                    trips.add(tripIndex,child);
                                    addToBest(tripIndex);
                                }

                                //mutation
                                tripIndex = rand.nextInt(bestTrips.size());
                                parent1 = trips.get(bestTrips.get(tripIndex));
                                parentClone = (ArrayList) parent1.clone();
                                tripScore = (float)parent1.get(6);

                                if (mutation(parentClone,accommodationListDB,activityListDB,restaurantListDB,transportListDB)){
                                    tripScoreMutation = evaluationTrip(budgetNext,prefFacilityN,prefActivityN,prefAccommodationN,(Accommodation[])parentClone.get(0),(Activity[])parentClone.get(1),(double)parentClone.get(5),timeSpentOutsideNext,(Integer[])parentClone.get(6));

                                    parentClone.add(6,tripScoreMutation);

                                    //if (tripScoreMutation>tripScore){
                                    if (tripScoreMutation>tripScore){
                                        if (isInBest(tripIndex)){
                                            bestTrips.remove((Object)tripIndex);
                                        }
                                        trips.remove(tripIndex);
                                        trips.add(tripIndex,parentClone);
                                        addToBest(tripIndex);

                                    }
                                }
                            }
                            currentGeneration = currentGeneration+1;
                        }

                        bestTrip = getBestTrip();


                        //store best trip in online db
                        //store in tripContainsAccommodation
                        String[] partsOfDate = dateOfArrivalNext.split("/");
                        int day = Integer.parseInt(partsOfDate[0]);
                        int month = Integer.parseInt(partsOfDate[1]);
                        int year = Integer.parseInt(partsOfDate[2]);

                        String[] daysOfTrip = new String[durationNext];
                        daysOfTrip[0] = String.valueOf(day)+"/"+String.valueOf(month)+"/"+String.valueOf(year);
                        for (int i=1;i<durationNext;i++){
                            if (month==2){
                                if (year%4==0){
                                    if ((day+i)==30){
                                        daysOfTrip[i] = "1/3/"+String.valueOf(year);
                                        day = -i+1;
                                        month = month+1;
                                    }
                                    else{
                                        daysOfTrip[i] = String.valueOf(day+i)+"/"+String.valueOf(month)+"/"+String.valueOf(year);
                                    }
                                }
                                else if ((day+i)==29){
                                    daysOfTrip[i] = "1/3/"+String.valueOf(year);
                                    day = -i+1;
                                    month = month+1;
                                }
                                else{
                                    daysOfTrip[i] = String.valueOf(day+i)+"/"+String.valueOf(month)+"/"+String.valueOf(year);
                                }
                            }
                            else if (month<8) {
                                if (month % 2 == 0) {
                                    if ((day + i) == 31) {
                                        daysOfTrip[i] = "1/" + String.valueOf(month + 1) + "/" + String.valueOf(year);
                                        day = -i+1;
                                        month = month+1;
                                    } else {
                                        daysOfTrip[i] = String.valueOf(day + i) + "/" + String.valueOf(month) + "/" + String.valueOf(year);
                                    }
                                } else if ((day + i) == 32) {
                                    daysOfTrip[i] = "1/" + String.valueOf(month + 1) + "/" + String.valueOf(year);
                                    day = -i+1;
                                    month = month+1;
                                } else {
                                    daysOfTrip[i] = String.valueOf(day + i) + "/" + String.valueOf(month) + "/" + String.valueOf(year);
                                }
                            }
                            else if (month==12){
                                if ((day + i) == 32) {
                                    daysOfTrip[i] = "1/1/"+ String.valueOf(year+1);
                                    day = -i+1;
                                    month = 1;
                                    year = year+1;
                                }
                                else {
                                    daysOfTrip[i] = String.valueOf(day + i) + "/" + String.valueOf(month) + "/" + String.valueOf(year);
                                }
                            }
                            else {
                                if (month % 2 == 0) {
                                    if ((day + i) == 32) {
                                        daysOfTrip[i] = "1/" + String.valueOf(month + 1) + "/" + String.valueOf(year);
                                        day = -i+1;
                                        month = month+1;
                                    } else {
                                        daysOfTrip[i] = String.valueOf(day + i) + "/" + String.valueOf(month) + "/" + String.valueOf(year);
                                    }
                                } else if ((day + i) == 31) {
                                    daysOfTrip[i] = "1/" + String.valueOf(month + 1) + "/" + String.valueOf(year);
                                    day = -i+1;
                                    month = month+1;
                                } else {
                                    daysOfTrip[i] = String.valueOf(day + i) + "/" + String.valueOf(month) + "/" + String.valueOf(year);
                                }
                            }
                        }
                        for (int i=0; i<((Accommodation[]) bestTrip.get(0)).length; i++){
                            storeBestTripAccommodation(idOfUser, ((Accommodation[])bestTrip.get(0))[i].getIdAccommodation(), daysOfTrip[i]);
                        }

                        int dayActivity = 1;
                        for (int k = 0; k<((Activity[]) bestTrip.get(1)).length; k++){
                            if (k==0){
                                storeBestTripActivity(idOfUser, ((Activity[])bestTrip.get(1))[k].getIdActivity(), daysOfTrip[1], "morning");
                            }
                            if (k%2==1){
                                storeBestTripActivity(idOfUser, ((Activity[])bestTrip.get(1))[k].getIdActivity(), daysOfTrip[dayActivity], "afternoon");
                            }
                            if (k%2==0 && k!=0){
                                dayActivity = dayActivity+1;
                                storeBestTripActivity(idOfUser, ((Activity[])bestTrip.get(1))[k].getIdActivity(), daysOfTrip[dayActivity], "morning");
                            }
                        }
                        for (int k=0;k<((Restaurant[])bestTrip.get(2)).length;k++){
                        }        int dayRestaurant = 1;
                        for (int j = 0; j<((Restaurant[]) bestTrip.get(2)).length; j++){
                            if (j==0){
                                storeBestTripRestaurant(idOfUser, ((Restaurant[])bestTrip.get(2))[j].getIdRestaurant(), daysOfTrip[1], "lunch");
                            }
                            if (j%2==1){
                                storeBestTripRestaurant(idOfUser, ((Restaurant[])bestTrip.get(2))[j].getIdRestaurant(), daysOfTrip[dayRestaurant], "dinner");
                            }
                            if (j%2==0 && j!=0){
                                dayRestaurant = dayRestaurant+1;
                                storeBestTripRestaurant(idOfUser, ((Restaurant[])bestTrip.get(2))[j].getIdRestaurant(), daysOfTrip[dayRestaurant], "lunch");
                            }
                        }

                        int dayTransport = 1;
                        for (int p = 0; p<((Transport[]) bestTrip.get(3)).length; p++){
                            storeBestTripTransport(idOfUser, ((Transport[]) bestTrip.get(3))[p].getIdTransport(), daysOfTrip[dayTransport]);
                            dayTransport = dayTransport + 1;
                        }

                        if (pDialog.isShowing()){
                            pDialog.dismiss();
                        }

                        Intent intentNext = new Intent(GeneticAlgorithmActivity.this, HomeActivity.class);
                        intentNext.putExtra("idUser", String.valueOf(idOfUser));
                        intentNext.putExtra("idTrip", String.valueOf(idOfTrip));
                        intentNext.putExtra("durationTrip", String.valueOf(durationNext));
                        intentNext.putExtra("arrivalDate",dateOfArrivalNext);
                        startActivity(intentNext);
                        finish();
                    }
                });
            }
        };
        timerThread.start();

    }

    private ArrayList getBestTrip() {
        return trips.get(bestTrips.get(0));
    }

    private boolean isInBest(int tripIndex) {
        return bestTrips.contains(tripIndex);
    }

    // function to generate random trip for the first population
    public ArrayList generateRandom(List<Restaurant> restaurantList, List<Activity> activityList, List<Accommodation> accommodationList, List<Transport> transportList, Accommodation[] AccommodationArrayInitial, Activity[] ActivityArrayInitial, Restaurant[] RestaurantArrayInitial, Transport[] TransportArrayInitial, ArrayList<String> prefAccommodationN, ArrayList<String> prefActivityN, ArrayList<String> prefFacilityN, int tripIndex, Integer[] TravelTimeInitial) {
        Random rand = new Random();
        Random rand2 = new Random();
        Random rand3 = new Random();
        Random rand4 = new Random();
        Random rand5 = new Random();
        Random rand6 = new Random();

        List<Activity> activityList1 = activityList;
        List<Accommodation> accommodationList1 = accommodationList;
        List<Restaurant> restaurantList1 = restaurantList;
        List<Transport> transportList1 = transportList;

        Activity activityMorning;
        Activity activityAfternoon;
        Restaurant restaurantLunch;
        Restaurant restaurantDinner;
        Accommodation accommodation;
        Transport transport;

        ArrayList initialPop = new ArrayList();

        Accommodation AccommodationArray[] = new Accommodation[durationNext-1];
        Restaurant RestaurantArray[] = new Restaurant[(durationNext-2)*2];
        Activity ActivityArray[] = new Activity[(durationNext-2)*2];
        Transport TransportArray[] = new Transport[(durationNext-2)];
        Integer TravelTimeArray[] = new Integer[(durationNext-2)];

        int duration1 = 0;
        int duration2 = 0;
        int duration3 = 0;
        int duration4 = 0;
        double priceTrip = 1000000000;
        int travelTimeTotal = 0;
        int priceDay = 0;
        int j =0;
        int indexActivityMorning;
        int indexRestaurantLunch;
        int indexActivityAfternoon;
        int indexRestaurantDinner;
        int indexAccommodation;
        int indexTransport;

        while (priceTrip>budgetNext){
            activityList1 = activityList;
            accommodationList1 = accommodationList;
            restaurantList1 = restaurantList;
            transportList1 = transportList;

            // first day only accommodation for the night
            int indexAccommodation0 = rand.nextInt((accommodationList1.size()));
            accommodation = accommodationList1.get(indexAccommodation0);
            AccommodationArray[0] = accommodation;

            priceTrip = accommodation.getPrice();
            travelTimeTotal = 0;
            int travelTime = 0;
            for (int i = 0;i<durationNext-2;i++){
                do {
                    j=i;
                    indexActivityMorning = rand.nextInt((activityList1.size()));
                    activityMorning = activityList1.get(indexActivityMorning);
                    ActivityArray[i+j] = activityMorning;
                    indexRestaurantLunch = rand2.nextInt((restaurantList1.size()));
                    restaurantLunch = restaurantList1.get(indexRestaurantLunch);
                    RestaurantArray[i+j] = restaurantLunch;
                    j = i+1;
                    indexActivityAfternoon = rand3.nextInt((activityList1.size()));
                    activityAfternoon = activityList1.get(indexActivityAfternoon);
                    ActivityArray[i+j] = activityAfternoon;
                    indexRestaurantDinner = rand4.nextInt((restaurantList1.size()));
                    restaurantDinner = restaurantList1.get(indexRestaurantDinner);
                    RestaurantArray[i+j] = restaurantDinner;
                    indexAccommodation = rand5.nextInt((accommodationList1.size()));
                    accommodation = accommodationList1.get(indexAccommodation);
                    AccommodationArray[i+1] = accommodation;
                    indexTransport = rand6.nextInt((transportList1.size()));
                    transport = transportList1.get(indexTransport);
                    TransportArray[i] = transport;

                    // Get latitude and longitude
                    String latitudeAccommodationPrevious = AccommodationArray[i].getLatitude();
                    String longitudeAccommodationPrevious = AccommodationArray[i].getLongitude();
                    String latitudeActivityMorning = activityMorning.getLatitude();
                    String longitudeActivityMorning = activityMorning.getLongitude();
                    String latitudeRestaurantLunch = restaurantLunch.getLatitude();
                    String longitudeRestaurantLunch = restaurantLunch.getLongitude();
                    String latitudeActivityAfternoon = activityAfternoon.getLatitude();
                    String longitudeActivityAfternoon = activityAfternoon.getLongitude();
                    String latitudeRestaurantDinner = restaurantDinner.getLatitude();
                    String longitudeRestaurantDinner = restaurantDinner.getLongitude();
                    String latitudeAccommodationDay = accommodation.getLatitude();
                    String longitudeAccommodationDay = accommodation.getLongitude();

                    int durationOfTravelling1 = calculateTravelTime(latitudeAccommodationPrevious, longitudeAccommodationPrevious,latitudeActivityMorning, longitudeActivityMorning);
                    travelTime = durationOfTravelling1;
                    duration1 = durationOfTravelling1+activityMorning.getDuration();

                    int durationOfTravelling2 = calculateTravelTime(latitudeActivityMorning, longitudeActivityMorning,latitudeRestaurantLunch, longitudeRestaurantLunch);
                    travelTime = travelTime+durationOfTravelling2;
                    duration2 = duration1+durationOfTravelling2;

                    int durationOfTravelling3 = calculateTravelTime(latitudeRestaurantLunch, longitudeRestaurantLunch,latitudeActivityAfternoon, longitudeActivityAfternoon);
                    travelTime = travelTime+durationOfTravelling3;
                    duration3 = duration2+60+durationOfTravelling3+activityAfternoon.getDuration();

                    int durationOfTravelling4 = calculateTravelTime(latitudeActivityAfternoon, longitudeActivityAfternoon,latitudeRestaurantDinner, longitudeRestaurantDinner);
                    travelTime = travelTime+durationOfTravelling4;
                    duration4 = duration3+durationOfTravelling4;

                    int durationOfTravelling5 = calculateTravelTime(latitudeRestaurantDinner, longitudeRestaurantDinner,latitudeAccommodationDay, longitudeAccommodationDay);
                    travelTime = travelTime+durationOfTravelling5;

                    priceDay = (activityMorning.getPrice()+restaurantLunch.getPrice()+activityAfternoon.getPrice()+restaurantDinner.getPrice()+accommodation.getPrice())*numberOfTravellersNext;
                    int numberOfTransport = (numberOfTravellersNext/transport.getNumberOfPlaces())+1;
                    if (transport.getName().equals("taxi")){
                        priceDay = priceDay + numberOfTransport*(transport.getPrice()*travelTime);
                    }
                    else {
                        priceDay = priceDay + numberOfTransport*transport.getPrice();
                    }

                } while (!(travelTime<=420 && accommodation.getNumberOfPlaces()>=numberOfTravellersNext && transport.getNeedDrivingLicense()<=drivingLicenseNext && 480+duration1<=(((activityMorning.getClosingHour()+12)*60)+activityMorning.getClosingMinute()) && 480+duration2<=(((restaurantLunch.getClosingHour()+12)*60)+restaurantLunch.getClosingMinute()) && 480+duration3<=(((activityAfternoon.getClosingHour()+12)*60)+activityAfternoon.getClosingMinute()) && 480+duration4<=(((restaurantDinner.getClosingHour()+12)*60)+restaurantDinner.getClosingMinute()) && indexActivityMorning!=indexActivityAfternoon));
                priceTrip = priceTrip + priceDay;
                if (indexActivityMorning<indexActivityAfternoon){
                    activityList1.remove(indexActivityAfternoon);
                    activityList1.remove(indexActivityMorning);
                }
                else {
                    activityList1.remove(indexActivityMorning);
                    activityList1.remove(indexActivityAfternoon);
                }
                TravelTimeArray[i] = travelTime;
                travelTimeTotal = travelTimeTotal + travelTime;
            }
        }

        for (int i = 0;i<durationNext-1;i++){
            AccommodationArrayInitial[i] = AccommodationArray[i];
        }
        for (int i = 0;i<(durationNext-2)*2;i++){
            RestaurantArrayInitial[i] = RestaurantArray[i];
        }
        for (int i = 0;i<(durationNext-2)*2;i++){
            ActivityArrayInitial[i] = ActivityArray[i];
        }
        for (int i = 0;i<(durationNext-2);i++){
            TransportArrayInitial[i] = TransportArray[i];
        }
        for (int i = 0;i<(durationNext-2);i++){
            TravelTimeInitial[i] = TravelTimeArray[i];
        }

        initialPop.add(AccommodationArrayInitial);
        initialPop.add(ActivityArrayInitial);
        initialPop.add(RestaurantArrayInitial);
        initialPop.add(TransportArrayInitial);
        initialPop.add(travelTimeTotal);
        initialPop.add(priceTrip);

        float score = evaluationTrip(budgetNext, prefFacilityN, prefActivityN, prefAccommodationN, (Accommodation[])initialPop.get(0), (Activity[])initialPop.get(1), (double)initialPop.get(5), timeSpentOutsideNext, TravelTimeInitial);

        initialPop.add(score);
        initialPop.add(TravelTimeInitial);

        trips.add(initialPop);
        addToBest(tripIndex);
        return initialPop;
    }

    public boolean addToBest(int tripIndex){
        if (bestTrips == null){
            bestTrips.add(tripIndex);
            return true;
        }
        else if (bestTrips.size()==0){
            bestTrips.add(tripIndex);
            return true;
        }
        else if (bestTrips.size()<15 && !isInBest(tripIndex)){
            int i = bestTrips.size();
            boolean placeFound = false;

            while (i>0 && !placeFound) {
                if ((float)trips.get(bestTrips.get(i-1)).get(6)>(float)trips.get(tripIndex).get(6)){
                    placeFound = true;
                }
                else {
                    i=i-1;
                }
            }
            bestTrips.add(i,tripIndex);
        }
        else if ((float)trips.get(tripIndex).get(6) > (float)trips.get(bestTrips.get(bestTrips.size() - 1)).get(6) && !isInBest(tripIndex)){
            int i = bestTrips.size()-1;
            boolean placeFound = false;

            while (i>0 && !placeFound) {
                if ((float)trips.get(bestTrips.get(i-1)).get(6)>(float)trips.get(tripIndex).get(6)){
                    placeFound = true;
                }
                else {
                    i=i-1;
                }
            }
            bestTrips.add(i,tripIndex);

            if (bestTrips.size()>bestGroupSize){
                bestTrips.remove(bestTrips.size()-1);
            }
            return true;
        }
        return false;
    }

    public int calculateTravelTime(String lat1, String lng1, String lat2, String lng2) {
        Location location1 = new Location("");
        location1.setLatitude(Double.parseDouble(lat1));
        location1.setLongitude(Double.parseDouble(lng1));

        Location location2 = new Location("");
        location2.setLatitude(Double.parseDouble(lat2));
        location2.setLongitude(Double.parseDouble(lng2));

        double distanceInMeters = location1.distanceTo(location2);

        double coeff = 0.0047;
        int estimatedDriveTimeInMinutes = (int) (distanceInMeters * coeff);
        return estimatedDriveTimeInMinutes;
    }

        /**
         * Function to store idTrip in user table in MySQL database to geneticalgorithm url
         */
        private void saveIdTrip(final int idUser) {
            // Tag used to cancel the request
            String tag_string_req = "req_saveIdTrip";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_GENETICALGORITHM, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                }
            }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idUser", String.valueOf(idUser));
                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }


    //evaluation
    public float evaluationTrip(int budgetNext, ArrayList<String> prefFacilityArray, ArrayList<String> prefActivityArray,
                                ArrayList<String> prefAccommodationArray, Accommodation[] AccommodationArray,
                                Activity[] ActivityArray, double budget,
                                int timeSpentOutsideNext, Integer[] travelTime) {

        final int PONDACTIVITE = 5;
        final int PONDACCOMMODATION = 30;
        final int PONDFACILITIES = 1;
        final int PONDBUDGET = 3 * ActivityArray.length;
        final int PONDTRAJET = 1;
        final int PONDTIME = 4;

        double score = 0;
        float fitness;
        double scoreMax;
        int cptPrefAcc = 0;
        int cptPrefFac = 0;
        int cptPrefAct = 0;
        while (prefAccommodationArray.get(cptPrefAcc) != null){
            cptPrefAcc = cptPrefAcc + 1;
        }

        scoreMax = (prefActivityArray.size() * 2 * PONDACTIVITE + PONDACCOMMODATION + prefFacilityArray.size() * PONDFACILITIES + 9 * PONDTRAJET + 2 * PONDTIME) * (prefAccommodationArray.size() / 2.0) + 2 * PONDBUDGET;

        //evaluation of the accommodations
        for (int i = 0; i < AccommodationArray.length; i++) {
            Accommodation accommodation = AccommodationArray[i];
            String catAccommodation = Database.myAccommodationBelongsCategoryAccommodationDAO.getCategoryAccommodationByIdAccommodation(accommodation.getIdAccommodation());
            for (int j = 0; j < cptPrefAcc; j++) {
                if (prefAccommodationArray.get(j) != null){
                    if (prefAccommodationArray.get(j).equals(catAccommodation)) {
                        score = score + PONDACCOMMODATION;
                    }
                }
            }
        }
        //evaluation of the facilities
        for (int i = 0; i < AccommodationArray.length; i++) {
            if (Database.myAccommodationHasFacilityDAO.fetchAllAccommodationHasFacility().size()>0){
                for (int j = 0; j<Database.myAccommodationHasFacilityDAO.fetchAllAccommodationHasFacility().size(); j++){
                    if (AccommodationArray[i].getIdAccommodation() == Database.myAccommodationHasFacilityDAO.fetchAllAccommodationHasFacility().get(j).getIdAccommodation()) {
                        for (int k = 0; k < cptPrefFac; k++) {
                            if (Database.myAccommodationHasFacilityDAO.fetchAllAccommodationHasFacility().get(j).getNameFacility().equals(prefFacilityArray.get(k))) {
                                score = score + PONDFACILITIES;
                            }
                        }
                    }
                }
            }
        }
        //evaluation of time spent outside
        int totalTimeSpentOutside = ActivityArray.length;
        for(int i = 0; i<travelTime.length;i++){
            totalTimeSpentOutside = totalTimeSpentOutside + travelTime[i];
        }
        for (int i = 0; i < ActivityArray.length; i++) {
            totalTimeSpentOutside = totalTimeSpentOutside + ActivityArray[i].getDuration();
        }
        double averageDayTimeSpentOutside = totalTimeSpentOutside / (ActivityArray.length / 2.0);
        //double averageTimeTravel = travelTime / (ActivityArray.length / 2.0);
        int scoreTravel = 0;
        for(int i = 0; i<travelTime.length;i++){
            if (travelTime[i] <= 120.0) {
                scoreTravel = scoreTravel + 9 * PONDTRAJET;
            }
            else if (travelTime[i] <= 150.0) {
                scoreTravel = scoreTravel + 8 *PONDTRAJET;
            }
            else if (travelTime[i] <= 180.0) {
                scoreTravel = scoreTravel + 7 *PONDTRAJET;
            }
            else if (travelTime[i] <= 210.0) {
                scoreTravel = scoreTravel + 6 * PONDTRAJET;
            }
            else if (travelTime[i] <= 240.0) {
                scoreTravel = scoreTravel + 5 * PONDTRAJET;
            }
            else if (travelTime[i] <= 270.0) {
                scoreTravel = scoreTravel + 4 * PONDTRAJET;
            }
            else if (travelTime[i] <= 300.0) {
                scoreTravel = scoreTravel + 3 * PONDTRAJET;
            }
            else if (travelTime[i] <= 330.0) {
                scoreTravel = scoreTravel + 2 * PONDTRAJET;
            }
            else {
                scoreTravel = scoreTravel - 1 * PONDTRAJET;
            }
        }
        score = score + (int) Math.ceil((float)scoreTravel / ActivityArray.length);
        if (Math.abs(averageDayTimeSpentOutside - timeSpentOutsideNext) < 30.0) {
            score = score + 2 * PONDTIME;
        }
        else if (Math.abs(averageDayTimeSpentOutside - timeSpentOutsideNext) < 60.0) {
            score = score + PONDTIME;
        }

        //evaluation of the budget
        if (budget >= ((3.0 / 4) * budgetNext)) {
            score = score + 2 * PONDBUDGET;
        }
        else if (budget >= 1.0 / 2 * budgetNext) {
            score = score + PONDBUDGET;
        }

        fitness = (float) (score / scoreMax);
        return fitness;
    }

    private boolean mutation(ArrayList trip, List<Accommodation> accommodationListDB, List<Activity> activityListDB, List<Restaurant> restaurantListDB, List<Transport> transportListDB) {
        Random rand = new Random();
        // Check the probability of a mutation to occur
        if (rand.nextInt(101) > mutationProbability) {
            return false;
        }
        int chooseArray = 0;
        chooseArray = rand.nextInt(4);
        int chooseItem = 0;
        int chooseItem2 = 0;
        boolean alreadyChosen = true;
        if (chooseArray==0){
            chooseItem = rand.nextInt(((Accommodation[])(trip.get(chooseArray))).length);
            chooseItem2 = rand.nextInt(accommodationListDB.size());
            while (accommodationListDB.get(chooseItem2).getIdAccommodation() == ((Accommodation[])(trip.get(chooseArray)))[chooseItem].getIdAccommodation()){
                chooseItem2 = rand.nextInt(accommodationListDB.size());
            }
            ((Accommodation[])trip.get(chooseArray))[chooseItem] = accommodationListDB.get(chooseItem2);
        }
        else if (chooseArray==1){
            // index of activity in the trip
            chooseItem = rand.nextInt(((Activity[])(trip.get(chooseArray))).length);
            // index of new activity
            while (alreadyChosen){
                chooseItem2 = rand.nextInt(activityListDB.size());
                // for every activity in the trip
                for (int i=0;i<((Activity[])(trip.get(chooseArray))).length;i++){
                    if (activityListDB.get(chooseItem2).getIdActivity() != ((Activity[])(trip.get(chooseArray)))[i].getIdActivity()){
                        alreadyChosen = false;
                    }
                    else {
                        alreadyChosen = true;
                    }
                }
            }
            ((Activity[])(trip.get(chooseArray)))[chooseItem] = activityListDB.get(chooseItem2);
        }
        else if (chooseArray==2){
            chooseItem = rand.nextInt(((Restaurant[])(trip.get(chooseArray))).length);
            chooseItem2 = rand.nextInt(restaurantListDB.size());
            while (restaurantListDB.get(chooseItem2).getIdRestaurant() == ((Restaurant[])(trip.get(chooseArray)))[chooseItem].getIdRestaurant()){
                chooseItem2 = rand.nextInt(restaurantListDB.size());
            }
            ((Restaurant[])trip.get(chooseArray))[chooseItem] = restaurantListDB.get(chooseItem2);
        }
        else {
            chooseItem = rand.nextInt(((Transport[])(trip.get(chooseArray))).length);
            chooseItem2 = rand.nextInt(transportListDB.size());
            while (transportListDB.get(chooseItem2).getIdTransport() == ((Transport[])(trip.get(chooseArray)))[chooseItem].getIdTransport()){
                chooseItem2 = rand.nextInt(transportListDB.size());
            }
            ((Transport[])trip.get(chooseArray))[chooseItem] = transportListDB.get(chooseItem2);
        }
        int travelDuration = 0;
        Integer DurationDay[] = new Integer[(durationNext-2)];
        DurationDay = calculateTravelDuration(trip);
        for(int i = 0; i<DurationDay.length;i++){
            travelDuration= travelDuration + DurationDay[i];
        }
        double priceTrip = calculatePrice(trip, travelDuration);

        trip.set(4,travelDuration);
        trip.set(5,priceTrip);
        trip.set(6,DurationDay);

        boolean check = checkConstraints(trip);

        if (priceTrip>budgetNext){
            return false;
        }
        else if (!check){
            return false;
        }

        return true;
    }

    public ArrayList crossover (ArrayList parent1, ArrayList parent2){
        Random random = new Random();

        //check the probability of a crossover to occur
        if (random.nextInt(101) > crossoverProbability){
            return null;
        }
        int chooseItem1 = 0;
        int chooseItem2 = 0;
        while (chooseItem1 == chooseItem2){
            chooseItem1 = random.nextInt(4);
            chooseItem2 = random.nextInt(4);
        }
        ArrayList child = new ArrayList();

        if ((chooseItem1==0 && chooseItem2==1) || (chooseItem1==1 && chooseItem2==0)){
            child.add(0, parent1.get(0));
            child.add(1, parent1.get(1));
            child.add(2, parent2.get(2));
            child.add(3, parent2.get(3));
        }
        if ((chooseItem1==0 && chooseItem2==2) || (chooseItem1==2 || chooseItem2==0)){
            child.add(0, parent1.get(0));
            child.add(1, parent2.get(1));
            child.add(2, parent1.get(2));
            child.add(3, parent2.get(3));
        }
        if ((chooseItem1==0 && chooseItem2==3) || (chooseItem1==3 && chooseItem2==0)){
            child.add(0, parent1.get(0));
            child.add(1, parent2.get(1));
            child.add(2, parent2.get(2));
            child.add(3, parent1.get(3));
        }
        if ((chooseItem1==1 && chooseItem2==2) || (chooseItem1==2 && chooseItem2==1)){
            child.add(0, parent2.get(0));
            child.add(1, parent1.get(1));
            child.add(2, parent1.get(2));
            child.add(3, parent2.get(3));
        }
        if ((chooseItem1==1 && chooseItem2==3) || (chooseItem1==3 && chooseItem2==1)){
            child.add(0, parent2.get(0));
            child.add(1, parent1.get(1));
            child.add(2, parent2.get(2));
            child.add(3, parent1.get(3));
        }
        if ((chooseItem1==2 && chooseItem2==3) || (chooseItem1==3 && chooseItem2==2)){
            child.add(0, parent2.get(0));
            child.add(1, parent2.get(1));
            child.add(2, parent1.get(2));
            child.add(3, parent1.get(3));
        }
        int travelDuration = 0;
        Integer DurationDayCross[] = new Integer[(durationNext-2)];
        DurationDayCross = calculateTravelDuration(child);
        for(int i = 0; i<DurationDayCross.length;i++){
            travelDuration= travelDuration + DurationDayCross[i];
        }

        double priceChild = calculatePrice(child, travelDuration);
        child.add(4, travelDuration);
        child.add(5, priceChild);
        child.add(6, DurationDayCross);
        boolean check = checkConstraints(child);


        if (priceChild>budgetNext){
            return null;
        }
        else if (!check){
            return null;
        }
        return child;
    }

    public boolean checkConstraints(ArrayList child) {
        boolean check = true;
        int duration1 = 0;
        int duration2 = 0;
        int duration3 = 0;
        int duration4 = 0;
        String lat1;
        String lng1;
        String lat2;
        String lng2;
        String lat3;
        String lng3;
        String lat4;
        String lng4;
        String lat5;
        String lng5;
        String lat6;
        String lng6;
        int travelDay = 0;
        for (int i = 0; i < durationNext - 2; i++) {
            travelDay = ((Integer[]) child.get(6))[i];
            lat1 = (((Accommodation[]) child.get(0))[i]).getLatitude();
            lng1 = (((Accommodation[]) child.get(0))[i]).getLongitude();
            lat2 = (((Activity[]) child.get(1))[i]).getLatitude();
            lng2 = (((Activity[]) child.get(1))[i]).getLongitude();
            lat3 = (((Restaurant[]) child.get(2))[i]).getLatitude();
            lng3 = (((Restaurant[]) child.get(2))[i]).getLongitude();
            lat4 = (((Activity[]) child.get(1))[i + 1]).getLatitude();
            lng4 = (((Activity[]) child.get(1))[i + 1]).getLongitude();
            lat5 = (((Restaurant[]) child.get(2))[i + 1]).getLatitude();
            lng5 = (((Restaurant[]) child.get(2))[i + 1]).getLongitude();
            lat6 = (((Accommodation[]) child.get(0))[i + 1]).getLatitude();
            lng6 = (((Accommodation[]) child.get(0))[i + 1]).getLongitude();
            duration1 = calculateTravelTime(lat1, lng1, lat2, lng2) + (((Activity[]) child.get(1))[i]).getDuration();
            duration2 = duration1 + calculateTravelTime(lat2, lng2, lat3, lng3);
            duration3 = duration2 + 60 + calculateTravelTime(lat3, lng3, lat4, lng4) + (((Activity[]) child.get(1))[i + 1]).getDuration();
            duration4 = duration3 + calculateTravelTime(lat4, lng4, lat5, lng5);
            if (travelDay >420 || 480 + duration1 > ((((((Activity[]) child.get(1))[i]).getClosingHour() + 12) * 60) + (((Activity[]) child.get(1))[i]).getClosingMinute()) || 480 + duration2 > ((((((Restaurant[]) child.get(2))[i]).getClosingHour() + 12) * 60) + (((Restaurant[]) child.get(2))[i]).getClosingMinute()) || 480 + duration3 > ((((((Activity[]) child.get(1))[i + 1]).getClosingHour() + 12) * 60) + (((Activity[]) child.get(1))[i + 1]).getClosingMinute()) || 480 + duration4 > ((((((Restaurant[]) child.get(2))[i + 1]).getClosingHour() + 12) * 60) + (((Restaurant[]) child.get(2))[i + 1]).getClosingMinute())) {
                check = false;
            }
        }
        return check;
    }

    public Integer[] calculateTravelDuration(ArrayList child) {
        Integer DurationDay[] = new Integer[(durationNext-2)];
        String lat1;
        String lng1;
        String lat2;
        String lng2;
        String lat3;
        String lng3;
        String lat4;
        String lng4;
        String lat5;
        String lng5;
        String lat6;
        String lng6;
        for (int i = 0; i < durationNext - 2; i++) {
            lat1 = (((Accommodation[]) child.get(0))[i]).getLatitude();
            lng1 = (((Accommodation[]) child.get(0))[i]).getLongitude();
            lat2 = (((Activity[]) child.get(1))[i]).getLatitude();
            lng2 = (((Activity[]) child.get(1))[i]).getLongitude();
            lat3 = (((Restaurant[]) child.get(2))[i]).getLatitude();
            lng3 = (((Restaurant[]) child.get(2))[i]).getLongitude();
            lat4 = (((Activity[]) child.get(1))[i + 1]).getLatitude();
            lng4 = (((Activity[]) child.get(1))[i + 1]).getLongitude();
            lat5 = (((Restaurant[]) child.get(2))[i + 1]).getLatitude();
            lng5 = (((Restaurant[]) child.get(2))[i + 1]).getLongitude();
            lat6 = (((Accommodation[]) child.get(0))[i + 1]).getLatitude();
            lng6 = (((Accommodation[]) child.get(0))[i + 1]).getLongitude();
            DurationDay[i] = calculateTravelTime(lat1, lng1, lat2, lng2) + calculateTravelTime(lat2, lng2, lat3, lng3) + calculateTravelTime(lat3, lng3, lat4, lng4) + calculateTravelTime(lat4, lng4, lat5, lng5) + calculateTravelTime(lat5, lng5, lat6, lng6);
        }
        return DurationDay;
    }

    public double calculatePrice(ArrayList child, int travelDuration) {
        double price = 0;
        int numberOfTransport = 0;
        for (int i = 0; i < ((Accommodation[]) child.get(0)).length; i++) {
            price = price + (((Accommodation[]) child.get(0))[i]).getPrice();
        }
        for (int i = 0; i < ((Activity[]) child.get(1)).length; i++) {
            price = price + ((((Activity[]) child.get(1))[i]).getPrice()) * numberOfTravellersNext;
        }
        for (int i = 0; i < ((Restaurant[]) child.get(2)).length; i++) {
            price = price + ((((Restaurant[]) child.get(2))[i]).getPrice()) * numberOfTravellersNext;
        }
        for (int i = 0; i < ((Transport[]) child.get(3)).length; i++) {
            numberOfTransport = (numberOfTravellersNext / ((((Transport[]) child.get(3))[i]).getNumberOfPlaces())) + 1;
            if ((((Transport[]) child.get(3))[i]).getName().equals("taxi")) {
                price = price + numberOfTransport * ((((Transport[]) child.get(3))[i]).getPrice() * travelDuration);
            } else {
                price = price + numberOfTransport * (((Transport[]) child.get(3))[i]).getPrice();
            }
        }
        return price;
    }



    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb  = new StringBuffer();

            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        }catch(Exception e){
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }


    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> >{

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }
    }

    private void storeBestTripAccommodation(final int idUser,
                                            final int idAccommodation, final String arrivalDateAccommodation) {
        // Tag used to cancel the request
        String tag_string_req = "req_storeBestTripAccommodation";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_BESTTRIPACCOMMODATION, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                        JSONObject tripContainsAccommodation = jObj.getJSONObject("tripContainsAccommodation");
                        int idTrip = tripContainsAccommodation.getInt("idTrip");
                        int idAccommodation = tripContainsAccommodation.getInt("idAccommodation");
                        String arrivalDate = tripContainsAccommodation.getString("arrivalDate");

                        // Inserting row in constructionSite table
                        Database.myTripContainsAccommodationDAO.addTripContainsAccommodation(new TripContainsAccommodation(idTrip, idAccommodation, arrivalDate));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idUser", String.valueOf(idUser));
                params.put("idAccommodation", String.valueOf(idAccommodation));
                params.put("arrivalDateAccommodation", arrivalDateAccommodation);

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void storeBestTripActivity(final int idUser,
                                       final int idActivity, final String arrivalDateActivity, final String arrivalTimeActivity) {
        // Tag used to cancel the request
        String tag_string_req = "req_storeBestTripActivity";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_BESTTRIPACTIVITY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    JSONObject tripContainsActivity = jObj.getJSONObject("tripContainsActivity");
                    int idTrip = tripContainsActivity.getInt("idTrip");
                    int idActivity = tripContainsActivity.getInt("idActivity");
                    String arrivalDate = tripContainsActivity.getString("arrivalDate");
                    String arrivalTime = tripContainsActivity.getString("arrivalTime");

                    // Inserting row in table
                    Database.myTripContainsActivityDAO.addTripContainsActivity(new TripContainsActivity(idTrip, idActivity, arrivalDate,arrivalTime));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idUser", String.valueOf(idUser));
                params.put("idActivity", String.valueOf(idActivity));
                params.put("arrivalDateActivity", arrivalDateActivity);
                params.put("arrivalTimeActivity", arrivalTimeActivity);

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void storeBestTripRestaurant(final int idUser,
                                         final int idRestaurant, final String arrivalDateRestaurant, final String arrivalTimeRestaurant) {
        // Tag used to cancel the request
        String tag_string_req = "req_storeBestTripRestaurant";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_BESTTRIPRESTAURANT, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                    JSONObject tripContainsRestaurant = jObj.getJSONObject("tripContainsRestaurant");
                    int idTrip = tripContainsRestaurant.getInt("idTrip");
                    int idRestaurant = tripContainsRestaurant.getInt("idRestaurant");
                    String arrivalDate = tripContainsRestaurant.getString("arrivalDate");
                    String arrivalTime = tripContainsRestaurant.getString("arrivalTime");

                    // Inserting row in table
                    Database.myTripContainsRestaurantDAO.addTripContainsRestaurant(new TripContainsRestaurant(idTrip, idRestaurant, arrivalDate,arrivalTime));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idUser", String.valueOf(idUser));
                params.put("idRestaurant", String.valueOf(idRestaurant));
                params.put("arrivalDateRestaurant", arrivalDateRestaurant);
                params.put("arrivalTimeRestaurant", arrivalTimeRestaurant);

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void storeBestTripTransport(final int idUser,
                                        final int idTransport, final String dateTransport) {
        // Tag used to cancel the request
        String tag_string_req = "req_storeBestTripTransport";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_BESTTRIPTRANSPORT, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    JSONObject tripContainsTransport = jObj.getJSONObject("tripContainsTransport");
                    int idTrip = tripContainsTransport.getInt("idTrip");
                    int idTransport = tripContainsTransport.getInt("idTransport");
                    String dateTransport = tripContainsTransport.getString("dateTransport");

                    // Inserting row in table
                    Database.myTripContainsTransportDAO.addTripContainsTransport(new TripContainsTransport(idTrip, idTransport, dateTransport));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idUser", String.valueOf(idUser));
                params.put("idTransport", String.valueOf(idTransport));
                params.put("dateTransport", dateTransport);

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    @Override
    public void onBackPressed()
    {
    }





}




