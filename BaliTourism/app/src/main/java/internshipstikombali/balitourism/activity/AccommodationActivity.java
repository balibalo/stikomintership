package internshipstikombali.balitourism.activity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.Accommodation;
import internshipstikombali.balitourism.business.TripContainsAccommodation;
import internshipstikombali.balitourism.helper.Database;
import internshipstikombali.balitourism.helper.SessionManager;

public class AccommodationActivity extends AppCompatActivity {

    private static final String TAG = AccommodationActivity.class.getSimpleName();
    Bitmap bitmap;
    ImageView imageView1;
    private ImageButton btnDelete;
    private ImageButton btnDate;
    private SessionManager session;
    private int idOfTrip;
    private String day;
    private int idOfUser;
    private int idOfAccommodation;
    private int durationOfTrip;
    private String formatted;
    private String arrivalDateOfTrip;
    private int idOfAccoFormer;
    private ImageButton buttonMusicAccommodation;


    private int day2;
    private int month;
    private int year;

    private DatePickerDialog datePickerDialog;
    List<TripContainsAccommodation> reqAccommodation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accommodation);

        // Session manager
        session = new SessionManager(getApplicationContext());

        imageView1 = (ImageView) findViewById(R.id.imageView);
        btnDelete = (ImageButton) findViewById(R.id.deleteButton);
        btnDate = (ImageButton) findViewById(R.id.dateButton);
        buttonMusicAccommodation = (ImageButton) findViewById(R.id.buttonMusicAccommodation);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAccommodation.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAccommodation.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAccommodation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAccommodation.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAccommodation.setBackgroundResource(R.drawable.pause);
                }
            }
        });





        Intent intent = getIntent();
        idOfUser = Integer.parseInt(intent.getStringExtra("idUser"));
        day = intent.getStringExtra("day");
        idOfTrip = Integer.parseInt(intent.getStringExtra("idTrip"));
        durationOfTrip = Integer.parseInt(intent.getStringExtra("durationTrip"));
        arrivalDateOfTrip = intent.getStringExtra("arrivalDate");

        // Parametres : (int)idUser, (string)day
        // j'ai besoin de l'accommodation du jour
        idOfAccommodation = -1;
        reqAccommodation = Database.myTripContainsAccommodationDAO.fetchAllTripContainsAccommodation();
        for (int i = 0; i<reqAccommodation.size(); i++){
            if ((reqAccommodation.get(i).getIdTrip() == idOfTrip) && reqAccommodation.get(i).getArrivalDate().equals(day)){
                idOfAccommodation = reqAccommodation.get(i).getIdAccommodation();
            }
        }



        if (idOfAccommodation == -1){
            final TextView nameAcco = (TextView) findViewById(R.id.nameAccommodation);
            final TextView priceAcco = (TextView) findViewById(R.id.priceAccommodation);
            final TextView descriptionAcco = (TextView) findViewById(R.id.descriptionAccommodation);
            final TextView phoneAcco = (TextView) findViewById(R.id.phoneAccommodation);
            final TextView websiteAcco = (TextView) findViewById(R.id.websiteAccommodation);
            final TextView locationAcco = (TextView) findViewById(R.id.locationAccommodation);

            nameAcco.setText("There is no accommodation provided for this day.");
            imageView1.setVisibility(View.GONE);
            btnDelete.setVisibility(View.GONE);
            btnDate.setVisibility(View.GONE);
            priceAcco.setVisibility(View.GONE);
            descriptionAcco.setVisibility(View.GONE);
            phoneAcco.setVisibility(View.GONE);
            websiteAcco.setVisibility(View.GONE);
            locationAcco.setVisibility(View.GONE);
        }
        else {
            Accommodation accommodation = Database.myAccommodationDAO.fetchAccommodationById(idOfAccommodation);
            String nameAccommodation = accommodation.getName();
            String priceAccommodation = "Average price : " + accommodation.getPrice() + " irp per person";
            double latitudeAccommodation = Double.parseDouble(accommodation.getLatitude());
            double longitudeAccommodation = Double.parseDouble(accommodation.getLongitude());

            String phoneNumber = "Phone number : " + accommodation.getPhoneNumber();

            String website = "Website : " + accommodation.getWebsite();

            String description = "Description : " + accommodation.getDescription();

            Geocoder geocoder;
            List<Address> addresses = new ArrayList<Address>();
            geocoder = new Geocoder(this, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(latitudeAccommodation, longitudeAccommodation, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String address;
            if (addresses.size()==0){
                address = "Location : no information";
            }
            else {
                address = "Location : " + addresses.get(0).getAddressLine(0)+", " + addresses.get(0).getLocality();
            }

            String url = accommodation.getPicture();

            new LoadImage().execute(url);

            final TextView nameAcco = (TextView) findViewById(R.id.nameAccommodation);
            nameAcco.setText(nameAccommodation);

            final TextView priceAcco = (TextView) findViewById(R.id.priceAccommodation);
            priceAcco.setText(priceAccommodation);

            final TextView locationAcco = (TextView) findViewById(R.id.locationAccommodation);
            locationAcco.setText(address);

            final TextView descriptionAcco = (TextView) findViewById(R.id.descriptionAccommodation);
            descriptionAcco.setText(description);

            final TextView phoneAcco = (TextView) findViewById(R.id.phoneAccommodation);
            phoneAcco.setText(phoneNumber);

            final TextView websiteAcco = (TextView) findViewById(R.id.websiteAccommodation);
            websiteAcco.setText(website);

            final AlertDialog diaBox = AskOption();
            final AlertDialog diaBox2 = AskOption2();
            final AlertDialog diaBox3 = AskOption3();

            // delete button Click Event
            btnDelete.setOnClickListener(new View.OnClickListener()  {
                public void onClick(View view) {
                    diaBox.show();
                }
            });

            String[] partsOfDate = day.split("/");
            day2 = Integer.parseInt(partsOfDate[0]);
            month = Integer.parseInt(partsOfDate[1]);
            year = Integer.parseInt(partsOfDate[2]);

            // date button Click Event
            btnDate.setOnClickListener(new View.OnClickListener()  {
                public void onClick(View view) {
                    //showStartDateDialog(view);
                    datePickerDialog.setMessage("Change the date");
                    datePickerDialog.updateDate(year, month - 1, day2);
                    datePickerDialog.show();
                }
            });

            Calendar newCalendar = Calendar.getInstance();
            datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    SimpleDateFormat format1 = new SimpleDateFormat("d/M/yyyy");
                    formatted = format1.format(newDate.getTime());
                    SimpleDateFormat sdf = new SimpleDateFormat("d/M/yyyy");

                    Date startTrip = null;
                    try {
                        startTrip = sdf.parse(arrivalDateOfTrip);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    long startTripDate = startTrip.getTime();

                    Date endTrip = null;
                    try {
                        endTrip = sdf.parse(arrivalDateOfTrip);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    long endTripDate = endTrip.getTime()+(86400000*durationOfTrip);

                    Date newDateTrip = null;
                    try {
                        newDateTrip = sdf.parse(formatted);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    long newTripDate = newDateTrip.getTime();


                    if (!(newTripDate>=startTripDate && newTripDate<endTripDate)){
                        diaBox3.show();
                    }
                    else if (checkAccommodation()) {
                        diaBox2.show();
                    }
                    else {
                        updateAccommodation();
                        Toast.makeText(getApplicationContext(),
                                "The accommodation has been updated successfully !", Toast.LENGTH_LONG)
                                .show();
                        Intent myIntent = new Intent(AccommodationActivity.this, HomeActivity.class);
                        myIntent.putExtra("idUser", String.valueOf(idOfUser));
                        myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                        myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                        myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                        startActivity(myIntent);
                        finish();
                    }

                }

            },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        }

    }

    @Override
    public void onResume(){
        super.onResume();
        idOfAccommodation = -1;
        reqAccommodation = Database.myTripContainsAccommodationDAO.fetchAllTripContainsAccommodation();
        for (int i = 0; i<reqAccommodation.size(); i++){
            if ((reqAccommodation.get(i).getIdTrip() == idOfTrip) && reqAccommodation.get(i).getArrivalDate().equals(day)){
                idOfAccommodation = reqAccommodation.get(i).getIdAccommodation();
            }
        }
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAccommodation.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAccommodation.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAccommodation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAccommodation.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAccommodation.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }

    private class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {
            if(image != null){
                imageView1.setImageBitmap(image);
            }else{

            }
        }
    }

    /**
     * function to check if there is already an accommodation at this date in mysql db
     * return true if there is already an accommodation
     * */
    private boolean checkAccommodation() {
        List<TripContainsAccommodation> reqAccommodation = Database.myTripContainsAccommodationDAO.fetchAllTripContainsAccommodation();
        for (int i = 0; i<reqAccommodation.size(); i++){
            if ((reqAccommodation.get(i).getIdTrip() == idOfTrip) && reqAccommodation.get(i).getArrivalDate().equals(formatted)){
                idOfAccoFormer = reqAccommodation.get(i).getIdAccommodation();
                return true;
            }
        }
        return false;
    }

    /**
     * function to delete the accommodation from the trip in mysql db
     * */
    private void deleteAccommodation() {
        // Tag used to cancel the request
        String tag_string_req = "req_delete";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETEACCOMMODATION, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                        // delete row in tripContainsAccommodation table
                        Database.myTripContainsAccommodationDAO.deleteTripContainsAccommodation(idOfTrip,idOfAccommodation,day);

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to deleteaccommodation url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idOfTrip));
                params.put("arrivalDate", day);
                params.put("idAccommodation", String.valueOf(idOfAccommodation));


                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to delete the former accommodation from the trip in mysql db
     * */
    private void deleteFormerAccommodation() {
        // Tag used to cancel the request
        String tag_string_req = "req_delete";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETEACCOMMODATION, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                        // delete row in tripContainsAccommodation table
                        Database.myTripContainsAccommodationDAO.deleteTripContainsAccommodation(idOfTrip,idOfAccoFormer,formatted);

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to deleteaccommodation url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idOfTrip));
                params.put("arrivalDate", formatted);
                params.put("idAccommodation", String.valueOf(idOfAccoFormer));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to add the accommodation from the trip in mysql db
     * */
    private void addAccommodation() {
        // Tag used to cancel the request
        String tag_string_req = "req_add";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDACCOMMODATION, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                        // add row in tripContainsAccommodation table
                        Database.myTripContainsAccommodationDAO.addTripContainsAccommodation(new TripContainsAccommodation(idOfTrip,idOfAccommodation,formatted));

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to deleteaccommodation url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idOfTrip));
                params.put("idAccommodation", String.valueOf(idOfAccommodation));
                params.put("arrivalDate", formatted);

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to update the accommodation from the trip in mysql db
     * */
    private void updateAccommodation() {
        // Tag used to cancel the request
        String tag_string_req = "req_update";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_UPDATEACCOMMODATION, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json

                        JSONObject tripContainsAccommodation = jObj.getJSONObject("tripContainsAccommodation");
                        int idTrip = tripContainsAccommodation.getInt("idTrip");
                        int idAccommodation = tripContainsAccommodation.getInt("idAccommodation");
                        String arrivalDate = tripContainsAccommodation.getString("arrivalDate");

                        // update row in tripContainsAccommodation table
                        Database.myTripContainsAccommodationDAO.updateTripContainsAccommodation(idOfTrip,day,formatted);

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to updateaccommodation url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idOfTrip));
                params.put("arrivalDate", day);
                params.put("newDate", formatted);

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }



    private AlertDialog AskOption()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setMessage("Do you really want to delete this accommodation for this day ?")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        deleteAccommodation();
                        Toast.makeText(getApplicationContext(),
                                "The accommodation has been deleted successfully !", Toast.LENGTH_LONG)
                                .show();
                        Intent myIntent = new Intent(AccommodationActivity.this, HomeActivity.class);
                        myIntent.putExtra("idUser", String.valueOf(idOfUser));
                        myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                        myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                        myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                        startActivity(myIntent);
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    private AlertDialog AskOption2()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                //set message, title, and icon
                .setMessage("There is already an accommodation at this date, do you want to change it by this one ?")
                .setPositiveButton("Change", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        deleteFormerAccommodation();
                        deleteAccommodation();
                        addAccommodation();
                        Toast.makeText(getApplicationContext(),
                                "The date of the accommodation has been updated successfully !", Toast.LENGTH_LONG)
                                .show();
                        Intent myIntent = new Intent(AccommodationActivity.this, HomeActivity.class);
                        myIntent.putExtra("idUser", String.valueOf(idOfUser));
                        myIntent.putExtra("day", day);
                        myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                        myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                        myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                        startActivity(myIntent);
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    private AlertDialog AskOption3()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setMessage("You must choose a day according to the dates of your trip.")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }


    private void getTripContainsAccoByIdTripDate(final int idTrip, final String arrivalDate) {
        // Tag used to cancel the request
        String tag_string_req = "req_getTripContainsAccoByIdTripDate";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ACCOBYTRIPDATE, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json

                        // Now store the construction sites in SQLite
                        //Getting each construction site one by one

                        JSONObject trip = jObj.getJSONObject("trip");
                        int idTrip = trip.getInt("idTrip");
                        idOfAccommodation = trip.getInt("idAccommodation");
                        String arrivalDate = trip.getString("arrivalDate");

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idTrip));
                params.put("arrivalDate", String.valueOf(arrivalDate));
                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }
}
