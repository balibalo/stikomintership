package internshipstikombali.balitourism.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.business.Transport;
import internshipstikombali.balitourism.helper.Database;

public class AllTransportAdminActivity extends AppCompatActivity {

    List<Transport> reqTransport;
    private Button btnNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_transport_admin);

        reqTransport = Database.myTransportDAO.fetchAllTransports();

        String[] transports = new String[reqTransport.size()];

        for (int i=0; i<reqTransport.size();i++){
            transports[i] = reqTransport.get(i).getName();
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, transports);

        ListView list = (ListView)findViewById(R.id.ListViewTransportsAdmin);
        btnNew = (Button) findViewById(R.id.buttonNewTransport);

        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent myIntent = new Intent(AllTransportAdminActivity.this, TransAdminActivity.class);
                myIntent.putExtra("idTrans", String.valueOf(reqTransport.get(position).getIdTransport()));
                startActivity(myIntent);
            }
        });

        // new button Click Event
        btnNew.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                Intent myIntent = new Intent(AllTransportAdminActivity.this, AddTransAdminActivity.class);
                startActivity(myIntent);            }
        });

    }

    @Override
    public void onResume(){
        super.onResume();
        reqTransport = Database.myTransportDAO.fetchAllTransports();
    }

    @Override
    public void onBackPressed()
    {
    }

}
