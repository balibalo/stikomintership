package internshipstikombali.balitourism.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.Restaurant;
import internshipstikombali.balitourism.helper.Database;

public class RestAdminActivity extends AppCompatActivity {

    private static final String TAG = RestAdminActivity.class.getSimpleName();

    Bitmap bitmap;
    ImageView imageView5;
    private ImageButton btnDelete;
    private ImageButton btnModify;
    private int idRest;
    private ImageButton buttonMusicRestAdmin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rest_admin);

        imageView5 = (ImageView) findViewById(R.id.imageView5Admin);
        btnDelete = (ImageButton) findViewById(R.id.deleteButton5Admin);
        btnModify = (ImageButton) findViewById(R.id.dateButton5Admin);
        buttonMusicRestAdmin = (ImageButton) findViewById(R.id.buttonMusicRestAdmin);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicRestAdmin.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicRestAdmin.setBackgroundResource(R.drawable.play);
        }

        buttonMusicRestAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicRestAdmin.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicRestAdmin.setBackgroundResource(R.drawable.pause);
                }
            }
        });

        Intent intent = getIntent();
        idRest = Integer.parseInt(intent.getStringExtra("idRest"));

        Restaurant restaurant1 = Database.myRestaurantDAO.fetchRestaurantById(idRest);

        String nameRestaurant1 = restaurant1.getName();
        String priceRestaurant1 = "Price : " + restaurant1.getPrice() + " irp per person";
        double latitudeRestaurant1 = Double.parseDouble(restaurant1.getLatitude());
        double longitudeRestaurant1 = Double.parseDouble(restaurant1.getLongitude());

        Geocoder geocoder;
        List<Address> addresses = new ArrayList<Address>();
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitudeRestaurant1, longitudeRestaurant1, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String addressRestaurant1;
        if (addresses.size()==0){
            addressRestaurant1 = "Location : no information";
        }
        else {
            addressRestaurant1 = "Location : " + addresses.get(0).getAddressLine(0)+", " + addresses.get(0).getLocality();
        }

        String openingHoursRestaurant1 = "Opening hours : " + restaurant1.getOpeningHour()+":"+restaurant1.getOpeningMinute()+"am - "+restaurant1.getClosingHour()+":"+restaurant1.getClosingMinute()+"pm";

        String phoneNumberRestaurant1 = "Phone number : " + restaurant1.getPhoneNumber();

        String websiteRestaurant1 = "Website : " + restaurant1.getWebsite();

        String descriptionRestaurant1 = "Description : " + restaurant1.getDescription();

        String urlRestaurant1 = restaurant1.getPicture();

        final TextView nameRest1 = (TextView) findViewById(R.id.nameRestaurantAdmin);
        nameRest1.setText(nameRestaurant1);

        final TextView priceRest1 = (TextView) findViewById(R.id.priceRestaurantAdmin);
        priceRest1.setText(priceRestaurant1);

        final TextView locationRest1 = (TextView) findViewById(R.id.locationRestaurantAdmin);
        locationRest1.setText(addressRestaurant1);

        final TextView descriptionRest1 = (TextView) findViewById(R.id.descriptionRestaurantAdmin);
        descriptionRest1.setText(descriptionRestaurant1);

        final TextView openingHoursRest1 = (TextView) findViewById(R.id.openingHoursRestaurantAdmin);
        openingHoursRest1.setText(openingHoursRestaurant1);

        final TextView phoneRest1 = (TextView) findViewById(R.id.phoneRestaurantAdmin);
        phoneRest1.setText(phoneNumberRestaurant1);

        final TextView websiteRest1 = (TextView) findViewById(R.id.websiteRestaurantAdmin);
        websiteRest1.setText(websiteRestaurant1);

        new LoadImage().execute(urlRestaurant1);

        final AlertDialog diaBox = AskOption();

        // delete button Click Event
        btnDelete.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                diaBox.show();
            }
        });

        // modify button Click Event
        btnModify.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                Intent myIntent = new Intent(RestAdminActivity.this, ModifyRestAdminActivity.class);
                myIntent.putExtra("idRest", String.valueOf(idRest));
                startActivity(myIntent);
            }
        });

    }

    private class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {
            if(image != null){
                imageView5.setImageBitmap(image);
            }else{

            }
        }
    }

    /**
     * function to delete the restaurant in mysql db
     * */
    private void deleteRestaurantAdmin() {
        // Tag used to cancel the request
        String tag_string_req = "req_delete";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETERESTAURANTADMIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    Database.myRestaurantDAO.deleteRestaurant(idRest);

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to deleterestaurantadmin url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idRestaurant", String.valueOf(idRest));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private AlertDialog AskOption()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setMessage("Do you really want to delete this restaurant from the database ?")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        deleteRestaurantAdmin();
                        Toast.makeText(getApplicationContext(),
                                "The restaurant has been deleted successfully !", Toast.LENGTH_LONG)
                                .show();
                        Intent myIntent = new Intent(RestAdminActivity.this, AdministratorMenuActivity.class);
                        startActivity(myIntent);
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicRestAdmin.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicRestAdmin.setBackgroundResource(R.drawable.play);
        }

        buttonMusicRestAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicRestAdmin.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicRestAdmin.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }



}
