package internshipstikombali.balitourism.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.helper.Database;

public class AllActivityActivity extends Activity {

    private int idOfUser;
    private int idOfTrip;
    private int durationOfTrip;
    private String arrivalDateOfTrip;
    List<internshipstikombali.balitourism.business.Activity> reqActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_activity);

        Intent intent = getIntent();
        idOfUser = Integer.parseInt(intent.getStringExtra("idUser"));
        idOfTrip = Integer.parseInt(intent.getStringExtra("idTrip"));
        durationOfTrip = Integer.parseInt(intent.getStringExtra("durationTrip"));
        arrivalDateOfTrip = intent.getStringExtra("arrivalDate");

        reqActivity = Database.myActivityDAO.fetchAllActivities();

        String[] activities = new String[reqActivity.size()];

        for (int i=0; i<reqActivity.size();i++){
            activities[i] = reqActivity.get(i).getName();
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, activities);

        ListView list = (ListView)findViewById(R.id.ListViewActivities);

        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent myIntent = new Intent(AllActivityActivity.this, ActActivity.class);
                myIntent.putExtra("idAct", String.valueOf(reqActivity.get(position).getIdActivity()));
                myIntent.putExtra("idUser", String.valueOf(idOfUser));
                myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                myIntent.putExtra("idTrip",String.valueOf(idOfTrip));
                startActivity(myIntent);
            }
        });

    }

    @Override
    public void onBackPressed()
    {
    }

}
