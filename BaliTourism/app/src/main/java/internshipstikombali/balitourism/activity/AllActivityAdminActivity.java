package internshipstikombali.balitourism.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.List;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.business.Activity;
import internshipstikombali.balitourism.helper.Database;

public class AllActivityAdminActivity extends AppCompatActivity {

    List<Activity> reqActivity;
    private Button btnNew;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_activity_admin);

        reqActivity = Database.myActivityDAO.fetchAllActivities();

        String[] activities = new String[reqActivity.size()];

        for (int i=0; i<reqActivity.size();i++){
            activities[i] = reqActivity.get(i).getName();
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, activities);

        ListView list = (ListView)findViewById(R.id.ListViewActivitiesAdmin);
        btnNew = (Button) findViewById(R.id.buttonNewActivity);

        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent myIntent = new Intent(AllActivityAdminActivity.this, ActAdminActivity.class);
                myIntent.putExtra("idAct", String.valueOf(reqActivity.get(position).getIdActivity()));
                startActivity(myIntent);
            }
        });

        // new button Click Event
        btnNew.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                Intent myIntent = new Intent(AllActivityAdminActivity.this, AddActAdminActivity.class);
                startActivity(myIntent);            }
        });
    }

    @Override
    public void onResume(){
        super.onResume();
        reqActivity = Database.myActivityDAO.fetchAllActivities();
    }

    @Override
    public void onBackPressed()
    {
    }

}
