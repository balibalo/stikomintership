package internshipstikombali.balitourism.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.Activity;
import internshipstikombali.balitourism.helper.Database;

public class AddActAdminActivity extends AppCompatActivity {

    private static final String TAG = AddActAdminActivity.class.getSimpleName();

    private Button buttonAddAct;
    private EditText inputNameAct;
    private EditText inputLongitudeAct;
    private EditText inputLatitudeAct;
    private EditText inputPhoneAct;
    private EditText inputWebsiteAct;
    private EditText inputPriceAct;
    private EditText inputDurationAct;
    private EditText inputUrlAct;
    private EditText inputDescriptionAct;
    private TimePicker timePickerOpeningAddAct;
    private TimePicker timePickerClosingAddAct;
    private ImageButton buttonMusicAddActAdmin;


    private int idActivity;

    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_act_admin);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        buttonAddAct = (Button) findViewById(R.id.buttonAddAct);
        inputNameAct = (EditText) findViewById(R.id.inputNameAct);
        inputLongitudeAct = (EditText) findViewById(R.id.inputLongitudeAct);
        inputLatitudeAct = (EditText) findViewById(R.id.inputLatitudeAct);
        inputPhoneAct = (EditText) findViewById(R.id.inputPhoneAct);
        inputWebsiteAct = (EditText) findViewById(R.id.inputWebsiteAct);
        inputPriceAct = (EditText) findViewById(R.id.inputPriceAct);
        inputDurationAct = (EditText) findViewById(R.id.inputDurationAct);
        inputUrlAct = (EditText) findViewById(R.id.inputUrlAct);
        inputDescriptionAct = (EditText) findViewById(R.id.inputDescriptionAct);
        timePickerOpeningAddAct = (TimePicker) findViewById(R.id.timePickerOpeningAddAct);
        timePickerOpeningAddAct.setIs24HourView(true);
        timePickerClosingAddAct = (TimePicker) findViewById(R.id.timePickerClosingAddAct);
        timePickerClosingAddAct.setIs24HourView(true);
        buttonMusicAddActAdmin = (ImageButton) findViewById(R.id.buttonMusicAddActAdmin);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddActAdmin.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddActAdmin.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddActAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddActAdmin.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddActAdmin.setBackgroundResource(R.drawable.pause);
                }
            }
        });

        // Send button Click Event
        buttonAddAct.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                int openingHour;
                int closingHour;
                int openingMinute;
                int closingMinute;
                int currentApiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentApiVersion > android.os.Build.VERSION_CODES.LOLLIPOP_MR1){
                    openingHour = timePickerOpeningAddAct.getHour();
                    closingHour = timePickerClosingAddAct.getHour();
                    openingMinute = timePickerOpeningAddAct.getMinute();
                    closingMinute = timePickerClosingAddAct.getMinute();
                } else {
                    openingHour = timePickerOpeningAddAct.getCurrentHour();
                    closingHour = timePickerClosingAddAct.getCurrentHour();
                    openingMinute = timePickerOpeningAddAct.getCurrentMinute();
                    closingMinute = timePickerClosingAddAct.getCurrentMinute();
                }

                if (!(inputNameAct.getText().toString().trim().length() == 0) && !(inputLongitudeAct.getText().toString().trim().length() == 0) && !(inputLatitudeAct.getText().toString().trim().length() == 0) && !(inputPriceAct.getText().toString().trim().length() == 0) && !(inputDurationAct.getText().toString().trim().length() == 0)) {
                    if (openingHour<=closingHour){

                        String phone;
                        String website;
                        String url;
                        String description;

                        String name = inputNameAct.getText().toString();
                        String lng = inputLongitudeAct.getText().toString();
                        String lat = inputLatitudeAct.getText().toString();
                        String price = inputPriceAct.getText().toString();
                        String duration = inputDurationAct.getText().toString();

                        if (inputPhoneAct.getText().toString().trim().length() == 0){
                            phone = "no information";
                        }
                        else {
                            phone = inputPhoneAct.getText().toString();
                        }
                        if (inputWebsiteAct.getText().toString().trim().length() == 0){
                            website = "no information";
                        }
                        else {
                            website = inputWebsiteAct.getText().toString();
                        }
                        if (inputUrlAct.getText().toString().trim().length() == 0){
                            url = "no information";
                        }
                        else {
                            url = inputUrlAct.getText().toString();
                        }
                        if (inputDescriptionAct.getText().toString().trim().length() == 0){
                            description = "no information";
                        }
                        else {
                            description = inputDescriptionAct.getText().toString();
                        }

                        if (openingHour==closingHour && openingMinute==closingMinute){
                            openingHour = 0;
                            openingMinute = 0;
                            closingHour = 12;
                            closingMinute = 0;
                        }
                        else {
                            if (openingHour>12){
                                openingHour = 12;
                            }
                            if (closingHour<12){
                                closingHour = 1;
                            }
                            else {
                                closingHour = closingHour-12;
                            }
                        }

                        String oh = String.valueOf(openingHour);
                        String om = String.valueOf(openingMinute);
                        String ch = String.valueOf(closingHour);
                        String cm = String.valueOf(closingMinute);


                        addActAdmin(name, lng, lat, phone, website, price, duration, url ,description, oh, om, ch, cm);

                    }
                    else {
                        // Prompt user to enter credentials
                        Toast.makeText(getApplicationContext(),
                                "The closing hour must be superior than the opening hour.", Toast.LENGTH_LONG)
                                .show();
                    }
                }
                else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please fill the fields with an (*).", Toast.LENGTH_LONG)
                            .show();
                }

            }

        });

    }

    /**
     * Function to store informations contained about the new activity in MySQL
     * */
    private void addActAdmin(final String name, final String lng, final String lat, final String phone, final String website, final String price, final String duration, final String url, final String description, final String oh, final String om, final String ch, final String cm) {
        // Tag used to cancel the request
        String tag_string_req = "req_saveForm";
        pDialog.setMessage("Loading ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDACTIVITYADMIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");
                        JSONObject activity = jObj.getJSONObject("activity");
                        idActivity = activity.getInt("idActivity");
                        // Inserting row in Activity table
                        Database.myActivityDAO.addActivity(new Activity(idActivity,name,lng,lat,phone,website,Integer.parseInt(price),description,url,Integer.parseInt(duration),Integer.parseInt(oh),Integer.parseInt(om),Integer.parseInt(ch),Integer.parseInt(cm)));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Intent intent = new Intent(AddActAdminActivity.this, AddActAdminNextActivity.class);
                intent.putExtra("idActivity", String.valueOf(idActivity));
                startActivity(intent);
                finish();

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idActivity", String.valueOf(idActivity));
                params.put("name", name);
                params.put("longitude", lng);
                params.put("latitude", lat);
                params.put("phoneNumber", phone);
                params.put("website", website);
                params.put("price", price);
                params.put("description", description);
                params.put("picture", url);
                params.put("duration", duration);
                params.put("openingHour", oh);
                params.put("openingMinute", om);
                params.put("closingHour", ch);
                params.put("closingMinute", cm);
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddActAdmin.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddActAdmin.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddActAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddActAdmin.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddActAdmin.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }


}
