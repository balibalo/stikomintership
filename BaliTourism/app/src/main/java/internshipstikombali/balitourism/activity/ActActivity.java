package internshipstikombali.balitourism.activity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.Activity;
import internshipstikombali.balitourism.business.TripContainsActivity;
import internshipstikombali.balitourism.helper.Database;

public class ActActivity extends AppCompatActivity {

    private static final String TAG = ActivityActivity.class.getSimpleName();

    Bitmap bitmap;
    ImageView imageView3;
    private ImageButton btnDate1;

    private int idOfAct;

    private int idOfTrip;
    private int idOfUser;
    private int durationOfTrip;
    private String formatted;
    private String arrivalDateOfTrip;
    private String newArrivalTime;
    private int idActFormer;
    private int day;
    private int month;
    private int year;
    private ImageButton buttonMusicAct;


    private DatePickerDialog datePickerDialog;

    List<TripContainsActivity> reqActivity;

    AlertDialog diaBox2;
    AlertDialog diaBox3;
    AlertDialog diaBox4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_act);

        imageView3 = (ImageView) findViewById(R.id.imageViewAct);
        btnDate1 = (ImageButton) findViewById(R.id.dateButtonAct);
        buttonMusicAct = (ImageButton) findViewById(R.id.buttonMusicAct);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAct.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAct.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAct.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAct.setBackgroundResource(R.drawable.pause);
                }
            }
        });


        Intent intent = getIntent();
        idOfUser = Integer.parseInt(intent.getStringExtra("idUser"));
        idOfAct = Integer.parseInt(intent.getStringExtra("idAct"));
        idOfTrip = Integer.parseInt(intent.getStringExtra("idTrip"));
        durationOfTrip = Integer.parseInt(intent.getStringExtra("durationTrip"));
        arrivalDateOfTrip = intent.getStringExtra("arrivalDate");

        diaBox2 = AskOption2();
        diaBox3 = AskOption3();
        diaBox4 = AskOption4();

        Activity activity1 = Database.myActivityDAO.fetchActivityById(idOfAct);

        String nameActivity1 = activity1.getName();
        String priceActivity1 = "Price : " + activity1.getPrice() + " irp per person";
        double latitudeActivity1 = Double.parseDouble(activity1.getLatitude());
        double longitudeActivity1 = Double.parseDouble(activity1.getLongitude());

        Geocoder geocoder;
        List<Address> addresses = new ArrayList<Address>();
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitudeActivity1, longitudeActivity1, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String addressActivity1;
        if (addresses.size()==0){
            addressActivity1 = "Location : no information";
        }
        else {
            addressActivity1 = "Location : " + addresses.get(0).getAddressLine(0)+", " + addresses.get(0).getLocality();
        }

        String openingHoursActivity1 = "Opening hours : " + activity1.getOpeningHour()+":"+activity1.getOpeningMinute()+"am - "+activity1.getClosingHour()+":"+activity1.getClosingMinute()+"pm";

        String phoneNumberActivity1 = "Phone number : " + activity1.getPhoneNumber();

        String websiteActivity1 = "Website : " + activity1.getWebsite();

        String descriptionActivity1 = "Description : " + activity1.getDescription();

        String urlActivity1 = activity1.getPicture();

        final TextView nameAct1 = (TextView) findViewById(R.id.nameAct);
        nameAct1.setText(nameActivity1);

        final TextView priceAct1 = (TextView) findViewById(R.id.priceAct);
        priceAct1.setText(priceActivity1);

        final TextView locationAct1 = (TextView) findViewById(R.id.locationAct);
        locationAct1.setText(addressActivity1);

        final TextView descriptionAct1 = (TextView) findViewById(R.id.descriptionAct);
        descriptionAct1.setText(descriptionActivity1);

        final TextView openingHoursAct1 = (TextView) findViewById(R.id.openingHoursAct);
        openingHoursAct1.setText(openingHoursActivity1);

        final TextView phoneAct1 = (TextView) findViewById(R.id.phoneAct);
        phoneAct1.setText(phoneNumberActivity1);

        final TextView websiteAct1 = (TextView) findViewById(R.id.websiteAct);
        websiteAct1.setText(websiteActivity1);

        new LoadImage().execute(urlActivity1);

        String[] partsOfDate = arrivalDateOfTrip.split("/");
        day = Integer.parseInt(partsOfDate[0]);
        month = Integer.parseInt(partsOfDate[1]);
        year = Integer.parseInt(partsOfDate[2]);

        // date button Click Event
        btnDate1.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                //showStartDateDialog(view);
                datePickerDialog.setMessage("Select a date to add this activity to your trip");
                datePickerDialog.updateDate(year, month - 1, day);
                datePickerDialog.show();
            }
        });

        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                SimpleDateFormat format1 = new SimpleDateFormat("d/M/yyyy");
                formatted = format1.format(newDate.getTime());
                SimpleDateFormat sdf = new SimpleDateFormat("d/M/yyyy");

                Date startTrip = null;
                try {
                    startTrip = sdf.parse(arrivalDateOfTrip);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                long startTripDate = startTrip.getTime();

                Date endTrip = null;
                try {
                    endTrip = sdf.parse(arrivalDateOfTrip);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                long endTripDate = endTrip.getTime()+(86400000*durationOfTrip);

                Date newDateTrip = null;
                try {
                    newDateTrip = sdf.parse(formatted);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                long newTripDate = newDateTrip.getTime();

                if (!(newTripDate>=startTripDate && newTripDate<endTripDate)){
                    diaBox3.show();
                }
                else {
                    diaBox4.show();
                }
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    private class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {
            if(image != null){
                imageView3.setImageBitmap(image);
            }else{

            }
        }
    }

    /**
     * function to delete the former activity from the trip in mysql db
     * */
    private void deleteFormerActivity() {
        // Tag used to cancel the request
        String tag_string_req = "req_delete";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETEACTIVITY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json

                        // delete row in tripContainsActivity table
                        Database.myTripContainsActivityDAO.deleteTripContainsActivity(idOfTrip,idActFormer,formatted, newArrivalTime);

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to deleteactivity url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idOfTrip));
                params.put("arrivalDate", formatted);
                params.put("arrivalTime", newArrivalTime);
                params.put("idActivity", String.valueOf(idActFormer));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to add the activity from the trip in mysql db
     * */
    private void addActivity() {
        // Tag used to cancel the request
        String tag_string_req = "req_add";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDACTIVITY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                            // add row in tripContainsActivity table
                            Database.myTripContainsActivityDAO.addTripContainsActivity(new TripContainsActivity(idOfTrip,idOfAct,formatted, newArrivalTime));

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to addactivity url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTrip", String.valueOf(idOfTrip));
                params.put("idActivity", String.valueOf(idOfAct));
                params.put("arrivalDate", formatted);
                params.put("arrivalTime", newArrivalTime);

                return params;
            }

        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    /**
     * function to check if there is already an activity at this date in mysql db
     * return true if there is already an activity
     * */
    private boolean checkActivity() {
        reqActivity = Database.myTripContainsActivityDAO.fetchAllTripContainsActivity();
        for (int i = 0; i<reqActivity.size(); i++){
            if ((reqActivity.get(i).getIdTrip() == idOfTrip) && reqActivity.get(i).getArrivalDate().equals(formatted) && reqActivity.get(i).getArrivalTime().equals(newArrivalTime)){
                idActFormer = reqActivity.get(i).getIdActivity();
                return true;
            }
        }
        return false;
    }

    private AlertDialog AskOption2()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                //set message, title, and icon
                .setMessage("There is already an activity at this date, do you want to change it by this one ?")
                .setPositiveButton("Change", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        deleteFormerActivity();
                        addActivity();
                        Toast.makeText(getApplicationContext(),
                                "The activity has been changed successfully !", Toast.LENGTH_LONG)
                                .show();
                        Intent myIntent = new Intent(ActActivity.this, HomeActivity.class);
                        myIntent.putExtra("idUser", String.valueOf(idOfUser));
                        myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                        myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                        myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                        startActivity(myIntent);
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    private AlertDialog AskOption3()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setMessage("You must choose a day according to the dates of your trip.")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;
    }

    private AlertDialog AskOption4()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                //set message, title, and icon
                .setPositiveButton("Morning", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        newArrivalTime = "morning";
                        if (checkActivity()) {
                            diaBox2.show();
                        }
                        else {
                            addActivity();
                            Toast.makeText(getApplicationContext(),
                                    "The activity has been added successfully !", Toast.LENGTH_LONG)
                                    .show();
                            Intent myIntent = new Intent(ActActivity.this, HomeActivity.class);
                            myIntent.putExtra("idUser", String.valueOf(idOfUser));
                            myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                            myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                            myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                            startActivity(myIntent);
                            finish();
                        }
                    }
                })
                .setNegativeButton("Afternoon", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        newArrivalTime = "afternoon";
                        if (checkActivity()) {
                            diaBox2.show();
                        }
                        else {
                            addActivity();
                            Toast.makeText(getApplicationContext(),
                                    "The activity has been added successfully !", Toast.LENGTH_LONG)
                                    .show();
                            Intent myIntent = new Intent(ActActivity.this, HomeActivity.class);
                            myIntent.putExtra("idUser", String.valueOf(idOfUser));
                            myIntent.putExtra("durationTrip", String.valueOf(durationOfTrip));
                            myIntent.putExtra("arrivalDate",arrivalDateOfTrip);
                            myIntent.putExtra("idTrip", String.valueOf(idOfTrip));
                            startActivity(myIntent);
                            finish();
                        }
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAct.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAct.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAct.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAct.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }

}
