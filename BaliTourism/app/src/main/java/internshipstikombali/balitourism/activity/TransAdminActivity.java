package internshipstikombali.balitourism.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.Transport;
import internshipstikombali.balitourism.helper.Database;

public class TransAdminActivity extends AppCompatActivity {

    private static final String TAG = TransAdminActivity.class.getSimpleName();

    Bitmap bitmap;
    ImageView imageView2;
    private ImageButton btnModify;
    private ImageButton btnDelete;
    private int idTrans;
    private ImageButton buttonMusicTransAdmin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trans_admin);

        imageView2 = (ImageView) findViewById(R.id.imageView2Admin);
        btnDelete = (ImageButton) findViewById(R.id.deleteButton2Admin);
        btnModify = (ImageButton) findViewById(R.id.dateButton2Admin);
        buttonMusicTransAdmin = (ImageButton) findViewById(R.id.buttonMusicTransAdmin);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicTransAdmin.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicTransAdmin.setBackgroundResource(R.drawable.play);
        }

        buttonMusicTransAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicTransAdmin.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicTransAdmin.setBackgroundResource(R.drawable.pause);
                }
            }
        });

        Intent intent = getIntent();
        idTrans = Integer.parseInt(intent.getStringExtra("idTrans"));

        Transport transport = Database.myTransportDAO.fetchTransportById(idTrans);

        String nameTransport = transport.getName();
        String priceTransport;
        if (nameTransport.equals("taxi")) {
            priceTransport = "Average price : " + transport.getPrice() + " irp per minute";
        }
        else {
            priceTransport = "Average price : " + transport.getPrice() + " irp per day";
        }

        String description = "Description : " + transport.getDescription();

        String url = transport.getPicture();

        String needDrivingLicense = String.valueOf(transport.getNeedDrivingLicense());

        String drivingLicense = "Be careful ! You need driving license to use this vehicle !!!";

        new LoadImage().execute(url);

        final TextView nameTransp = (TextView) findViewById(R.id.nameTransportAdmin);
        nameTransp.setText(nameTransport);

        final TextView priceTransp = (TextView) findViewById(R.id.priceTransportAdmin);
        priceTransp.setText(priceTransport);

        final TextView descriptionTransp = (TextView) findViewById(R.id.descriptionTransportAdmin);
        descriptionTransp.setText(description);

        final TextView drivingLicenseTransp = (TextView) findViewById(R.id.drivingLicenseTransportAdmin);

        if (needDrivingLicense.equals("1")){
            drivingLicenseTransp.setText(drivingLicense);
            drivingLicenseTransp.setVisibility(View.VISIBLE);
        }
        else {
            drivingLicenseTransp.setVisibility(View.GONE);
        }

        final AlertDialog diaBox = AskOption();

        // delete button Click Event
        btnDelete.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                diaBox.show();
            }
        });

        // modify button Click Event
        btnModify.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {
                Intent myIntent = new Intent(TransAdminActivity.this, ModifyTransAdminActivity.class);
                myIntent.putExtra("idTrans", String.valueOf(idTrans));
                startActivity(myIntent);
            }
        });

    }

    private class LoadImage extends AsyncTask<String, String, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {
            if(image != null){
                imageView2.setImageBitmap(image);
            }else{

            }
        }
    }

    /**
     * function to delete the transport in mysql db
     * */
    private void deleteTransportAdmin() {
        // Tag used to cancel the request
        String tag_string_req = "req_delete";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_DELETETRANSPORTADMIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    Database.myTransportDAO.deleteTransport(idTrans);

                }
                catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to deletetransportadmin url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idTransport", String.valueOf(idTrans));

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private AlertDialog AskOption()
    {
        AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
                .setMessage("Do you really want to delete this transport from the database ?")
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        deleteTransportAdmin();
                        Toast.makeText(getApplicationContext(),
                                "The transport has been deleted successfully !", Toast.LENGTH_LONG)
                                .show();
                        Intent myIntent = new Intent(TransAdminActivity.this, AdministratorMenuActivity.class);
                        startActivity(myIntent);
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        return myQuittingDialogBox;

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
        {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicTransAdmin.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicTransAdmin.setBackgroundResource(R.drawable.play);
        }

        buttonMusicTransAdmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicTransAdmin.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicTransAdmin.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }

}
