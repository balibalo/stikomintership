package internshipstikombali.balitourism.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.app.AppConfig;
import internshipstikombali.balitourism.app.AppController;
import internshipstikombali.balitourism.business.ActivityBelongsCategoryActivity;
import internshipstikombali.balitourism.helper.Database;

public class AddActAdminNextActivity extends AppCompatActivity {

    private static final String TAG = AddActAdminNextActivity.class.getSimpleName();

    private Button buttonAddNextAct;
    private CheckBox culturalAct;
    private CheckBox familyAct;
    private CheckBox natureAct;
    private CheckBox relaxAct;
    private CheckBox romanticAct;
    private CheckBox sportAct;
    private int cpt;
    private ImageButton buttonMusicAddActAdminNext;


    private int idActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_act_admin_next);

        buttonAddNextAct = (Button) findViewById(R.id.buttonAddNextAct);
        culturalAct = (CheckBox) findViewById(R.id.culturalAct);
        familyAct = (CheckBox) findViewById(R.id.familyAct);
        natureAct = (CheckBox) findViewById(R.id.natureAct);
        relaxAct = (CheckBox) findViewById(R.id.relaxAct);
        romanticAct = (CheckBox) findViewById(R.id.romanticAct);
        sportAct = (CheckBox) findViewById(R.id.sportAct);
        buttonMusicAddActAdminNext = (ImageButton) findViewById(R.id.buttonMusicAddActAdminNext);

        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddActAdminNext.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddActAdminNext.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddActAdminNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddActAdminNext.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddActAdminNext.setBackgroundResource(R.drawable.pause);
                }
            }
        });

        Intent intent = getIntent();
        String idAct = intent.getStringExtra("idActivity");
        idActivity = Integer.parseInt(idAct);

        // Send button Click Event
        buttonAddNextAct.setOnClickListener(new View.OnClickListener()  {
            public void onClick(View view) {

                cpt = 0;
                if (culturalAct.isChecked()) {
                    cpt = cpt +1;
                }
                if (familyAct.isChecked()) {
                    cpt = cpt +1;
                }
                if (natureAct.isChecked()) {
                    cpt = cpt +1;
                }
                if (relaxAct.isChecked()) {
                    cpt = cpt +1;
                }
                if (romanticAct.isChecked()) {
                    cpt = cpt +1;
                }
                if (sportAct.isChecked()) {
                    cpt = cpt +1;
                }

                if (cpt >= 1) {
                    if (culturalAct.isChecked()) {
                        String cat = "cultural";
                        addActivityBelongsCategory(cat);
                    }
                    if (familyAct.isChecked()) {
                        String cat = "family";
                        addActivityBelongsCategory(cat);
                    }
                    if (natureAct.isChecked()) {
                        String cat = "nature";
                        addActivityBelongsCategory(cat);
                    }
                    if (relaxAct.isChecked()) {
                        String cat = "relax";
                        addActivityBelongsCategory(cat);
                    }
                    if (romanticAct.isChecked()) {
                        String cat = "romantic";
                        addActivityBelongsCategory(cat);
                    }
                    if (sportAct.isChecked()) {
                        String cat = "sport";
                        addActivityBelongsCategory(cat);
                    }

                    Toast.makeText(getApplicationContext(), "The activity has been created successfully !", Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(AddActAdminNextActivity.this, AdministratorMenuActivity.class);
                    startActivity(intent);
                    finish();
                }
                else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please select at least 1 category.", Toast.LENGTH_LONG)
                            .show();
                }

            }

        });

    }

    /**
     * Function to store the categories of the new activity in MySQL database to newtrip url
     * */
    private void addActivityBelongsCategory(final String category) {
        // Tag used to cancel the request
        String tag_string_req = "req_saveForm";

        StringRequest strReq = new StringRequest(Request.Method.POST,
                AppConfig.URL_ADDACTIVITYBELONGSCATEGORY, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                        // Inserting row in ActivityBelongCat table
                        Database.myActivityBelongsCategoryActivityDAO.addActivityBelongsCategoryActivity(new ActivityBelongsCategoryActivity(idActivity,category));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {

                // Posting params to register url
                Map<String, String> params = new HashMap<String, String>();
                params.put("idActivity", String.valueOf(idActivity));
                params.put("nameCategoryActivity", category);

                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    @Override
    public void onResume(){
        super.onResume();
        if (((AppController) getApplication()).getPlayer().isPlaying()) {
            buttonMusicAddActAdminNext.setBackgroundResource(R.drawable.pause);
        }
        else {
            buttonMusicAddActAdminNext.setBackgroundResource(R.drawable.play);
        }

        buttonMusicAddActAdminNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((AppController) getApplication()).getPlayer().isPlaying()) {
                    ((AppController) getApplication()).onPause();
                    buttonMusicAddActAdminNext.setBackgroundResource(R.drawable.play);
                }
                else {
                    ((AppController) getApplication()).onStart();
                    buttonMusicAddActAdminNext.setBackgroundResource(R.drawable.pause);
                }
            }
        });
    }

}
