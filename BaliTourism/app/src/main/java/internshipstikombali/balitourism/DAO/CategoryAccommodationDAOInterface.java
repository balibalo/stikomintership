package internshipstikombali.balitourism.DAO;

import internshipstikombali.balitourism.business.CategoryAccommodation;

/**
 * Created by Pierre on 22/06/2016.
 */
public interface CategoryAccommodationDAOInterface {
    public boolean addCategoryAccommodation(CategoryAccommodation categoryAccommodation);
    public void deleteAllCategoriesAccommodation();
}
