package internshipstikombali.balitourism.DAO;

import internshipstikombali.balitourism.business.UserHasCategoryAccommodationPreferences;

/**
 * Created by Pierre on 22/06/2016.
 */
public interface UserHasCategoryAccommodationPreferencesDAOInterface {
    public boolean addUserHasCategoryAccommodationPreferences(UserHasCategoryAccommodationPreferences userHasCategoryAccommodationPreferences);
    public void deleteAllUserHasCategoryAccommodationPreferences();
}
