package internshipstikombali.balitourism.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import internshipstikombali.balitourism.business.Activity;
import internshipstikombali.balitourism.helper.DatabaseContentProvider;
import internshipstikombali.balitourism.helper.DatabaseSchema;

/**
 * Created by Pierre on 22/06/2016.
 */
public class ActivityDAO extends DatabaseContentProvider implements ActivityDAOInterface, DatabaseSchema {

    private Cursor cursor;
    private ContentValues initialValues;

    public ActivityDAO(SQLiteDatabase db) {
        super(db);
    }

    private void setContentValue(Activity activity) {
        initialValues = new ContentValues();
        initialValues.put(KEY_ACTIVITY_IDACTIVITY, activity.getIdActivity());
        initialValues.put(KEY_ACTIVITY_NAME, activity.getName());
        initialValues.put(KEY_ACTIVITY_LONGITUDE, activity.getLongitude());
        initialValues.put(KEY_ACTIVITY_LATITUDE, activity.getLatitude());
        initialValues.put(KEY_ACTIVITY_PHONENUMBER, activity.getPhoneNumber());
        initialValues.put(KEY_ACTIVITY_WEBSITE, activity.getWebsite());
        initialValues.put(KEY_ACTIVITY_PRICE, activity.getPrice());
        initialValues.put(KEY_ACTIVITY_DESCRIPTION, activity.getDescription());
        initialValues.put(KEY_ACTIVITY_PICTURE, activity.getPicture());
        initialValues.put(KEY_ACTIVITY_DURATION, activity.getDuration());
        initialValues.put(KEY_ACTIVITY_OPENINGHOUR, activity.getOpeningHour());
        initialValues.put(KEY_ACTIVITY_OPENINGMINUTE, activity.getOpeningMinute());
        initialValues.put(KEY_ACTIVITY_CLOSINGHOUR, activity.getClosingHour());
        initialValues.put(KEY_ACTIVITY_CLOSINGMINUTE, activity.getClosingMinute());

    }

    private ContentValues getContentValue() {
        return initialValues;
    }

    @Override
    public boolean addActivity(Activity activity) {
        setContentValue(activity);
        try {
            return super.insert(ACTIVITY_TABLE, getContentValue()) > 0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public boolean updateActivity(int idAct, String name, String longitude, String latitude, String phone, String website, int price, String description, String url, int duration, int oh, int om, int ch, int cm){
        ContentValues cv = new ContentValues();
        cv.put("name",name);
        cv.put("longitude",longitude);
        cv.put("latitude",latitude);
        cv.put("phoneNumber",phone);
        cv.put("website",website);
        cv.put("price",price);
        cv.put("description",description);
        cv.put("picture",url);
        cv.put("duration",duration);
        cv.put("openingHour",oh);
        cv.put("openingMinute",om);
        cv.put("closingHour",ch);
        cv.put("closingMinute",cm);


        try {
            return super.update(ACTIVITY_TABLE,cv, "idActivity = ?",new String[]{String.valueOf(idAct)}) >0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public void deleteAllActivities() {
        super.delete(ACTIVITY_TABLE, null, null);
    }

    @Override
    public boolean deleteActivity(int idActivity) {
        try {
            return super.delete(ACTIVITY_TABLE, "idActivity = ?", new String[]{String.valueOf(idActivity)}) >0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public List<Activity> fetchAllActivities() {
        List<Activity> activityList = new ArrayList<Activity>();
        cursor = super.query(ACTIVITY_TABLE, ACTIVITY_COLUMNS, null, null, KEY_ACTIVITY_IDACTIVITY);
        if(cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Activity activity = cursorToEntity(cursor);
                activityList.add(activity);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return activityList;
    }

    @Override
    public Activity fetchActivityById(int idActivity){
        final String selectionArgs[] = {String.valueOf(idActivity)};
        final String selection = KEY_ACTIVITY_IDACTIVITY + " = ?";
        Activity activity = new Activity();
        cursor = super.query(ACTIVITY_TABLE, ACTIVITY_COLUMNS, selection, selectionArgs, KEY_ACTIVITY_IDACTIVITY);
        if(cursor != null) {
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                activity = cursorToEntity(cursor);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return activity;
    }


    @Override
    protected Activity cursorToEntity(Cursor cursor) {
        Activity activity = new Activity();

        int idActivityIndex;
        int nameIndex;
        int longitudeIndex;
        int latitudeIndex;
        int phoneNumberIndex;
        int websiteIndex;
        int priceIndex;
        int descriptionIndex;
        int pictureIndex;
        int durationIndex;
        int openingHourIndex;
        int openingMinuteIndex;
        int closingHourIndex;
        int closingMinuteIndex;

        if(cursor != null) {
            if(cursor.getColumnIndex(KEY_ACTIVITY_IDACTIVITY) != -1) {
                idActivityIndex = cursor.getColumnIndexOrThrow(KEY_ACTIVITY_IDACTIVITY);
                activity.setIdActivity(cursor.getInt(idActivityIndex));
            }
            if(cursor.getColumnIndex(KEY_ACTIVITY_NAME) != -1) {
                nameIndex = cursor.getColumnIndexOrThrow(KEY_ACTIVITY_NAME);
                activity.setName(cursor.getString(nameIndex));
            }
            if(cursor.getColumnIndex(KEY_ACTIVITY_LONGITUDE) != -1) {
                longitudeIndex = cursor.getColumnIndexOrThrow(KEY_ACTIVITY_LONGITUDE);
                activity.setLongitude(cursor.getString(longitudeIndex));
            }
            if(cursor.getColumnIndex(KEY_ACTIVITY_LATITUDE) != -1) {
                latitudeIndex = cursor.getColumnIndexOrThrow(KEY_ACTIVITY_LATITUDE);
                activity.setLatitude(cursor.getString(latitudeIndex));
            }
            if(cursor.getColumnIndex(KEY_ACTIVITY_PHONENUMBER) != -1) {
                phoneNumberIndex = cursor.getColumnIndexOrThrow(KEY_ACTIVITY_PHONENUMBER);
                activity.setPhoneNumber(cursor.getString(phoneNumberIndex));
            }
            if(cursor.getColumnIndex(KEY_ACTIVITY_WEBSITE) != -1) {
                websiteIndex = cursor.getColumnIndexOrThrow(KEY_ACTIVITY_WEBSITE);
                activity.setWebsite(cursor.getString(websiteIndex));
            }
            if(cursor.getColumnIndex(KEY_ACTIVITY_PRICE) != -1) {
                priceIndex = cursor.getColumnIndexOrThrow(KEY_ACTIVITY_PRICE);
                activity.setPrice(cursor.getInt(priceIndex));
            }
            if(cursor.getColumnIndex(KEY_ACTIVITY_DESCRIPTION) != -1) {
                descriptionIndex = cursor.getColumnIndexOrThrow(KEY_ACTIVITY_DESCRIPTION);
                activity.setDescription(cursor.getString(descriptionIndex));
            }
            if(cursor.getColumnIndex(KEY_ACTIVITY_PICTURE) != -1) {
                pictureIndex = cursor.getColumnIndexOrThrow(KEY_ACTIVITY_PICTURE);
                activity.setPicture(cursor.getString(pictureIndex));
            }
            if(cursor.getColumnIndex(KEY_ACTIVITY_DURATION) != -1) {
                durationIndex = cursor.getColumnIndexOrThrow(KEY_ACTIVITY_DURATION);
                activity.setDuration(cursor.getInt(durationIndex));
            }
            if(cursor.getColumnIndex(KEY_ACTIVITY_OPENINGHOUR) != -1) {
                openingHourIndex = cursor.getColumnIndexOrThrow(KEY_ACTIVITY_OPENINGHOUR);
                activity.setOpeningHour(cursor.getInt(openingHourIndex));
            }
            if(cursor.getColumnIndex(KEY_ACTIVITY_OPENINGMINUTE) != -1) {
                openingMinuteIndex = cursor.getColumnIndexOrThrow(KEY_ACTIVITY_OPENINGMINUTE);
                activity.setOpeningMinute(cursor.getInt(openingMinuteIndex));
            }
            if(cursor.getColumnIndex(KEY_ACTIVITY_CLOSINGHOUR) != -1) {
                closingHourIndex = cursor.getColumnIndexOrThrow(KEY_ACTIVITY_CLOSINGHOUR);
                activity.setClosingHour(cursor.getInt(closingHourIndex));
            }
            if(cursor.getColumnIndex(KEY_ACTIVITY_CLOSINGMINUTE) != -1) {
                closingMinuteIndex = cursor.getColumnIndexOrThrow(KEY_ACTIVITY_CLOSINGMINUTE);
                activity.setClosingMinute(cursor.getInt(closingMinuteIndex));
            }
        }
        return activity;
    }
}
