package internshipstikombali.balitourism.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import internshipstikombali.balitourism.business.TripContainsActivity;
import internshipstikombali.balitourism.helper.DatabaseContentProvider;
import internshipstikombali.balitourism.helper.DatabaseSchema;

/**
 * Created by Pierre on 22/06/2016.
 */
public class TripContainsActivityDAO extends DatabaseContentProvider implements TripContainsActivityDAOInterface, DatabaseSchema {
    private Cursor cursor;
    private ContentValues initialValues;

    public TripContainsActivityDAO(SQLiteDatabase db) {
        super(db);
    }

    private void setContentValue(TripContainsActivity tripContainsActivity) {
        initialValues = new ContentValues();
        initialValues.put(KEY_TRIPCONTAINSACTIVITY_IDTRIP, tripContainsActivity.getIdTrip());
        initialValues.put(KEY_TRIPCONTAINSACTIVITY_IDACTIVITY, tripContainsActivity.getIdActivity());
        initialValues.put(KEY_TRIPCONTAINSACTIVITY_ARRIVALDATE, String.valueOf(tripContainsActivity.getArrivalDate()));
        initialValues.put(KEY_TRIPCONTAINSACTIVITY_ARRIVALTIME, String.valueOf(tripContainsActivity.getArrivalTime()));
    }

    private ContentValues getContentValue() {
        return initialValues;
    }

    @Override
    public boolean addTripContainsActivity(TripContainsActivity tripContainsActivity) {
        setContentValue(tripContainsActivity);
        try {
            return super.insert(TRIPCONTAINSACTIVITY_TABLE, getContentValue()) > 0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public void deleteAllTripContainsActivity() {
        super.delete(TRIPCONTAINSACTIVITY_TABLE, null, null);
    }

    @Override
    public List<TripContainsActivity> fetchAllTripContainsActivity(){
        List<TripContainsActivity> resList = new ArrayList<TripContainsActivity>();
        cursor = super.query(TRIPCONTAINSACTIVITY_TABLE, TRIPCONTAINSACTIVITY_COLUMNS, null, null, null);
        if(cursor != null){
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                TripContainsActivity tripContainsActivity = cursorToEntity(cursor);
                resList.add(tripContainsActivity);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return resList;
    }

    @Override
    public boolean deleteTripContainsActivity(int idTrip,int idActivity, String day, String arrivalTime) {
        try {
            return super.delete(TRIPCONTAINSACTIVITY_TABLE, "idTrip = ? and idActivity = ? and arrivalDate = ? and arrivalTime = ?", new String[]{String.valueOf(idTrip), String.valueOf(idActivity),day,arrivalTime}) >0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public boolean deleteTripContainsActivityById(int idTrip) {
        try {
            return super.delete(TRIPCONTAINSACTIVITY_TABLE, "idTrip = ?", new String[]{String.valueOf(idTrip)})>0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public boolean updateTripContainsActivity(int idTrip, String day, String formatted, String arrivalTime, String newArrivalTime) {
        ContentValues cv = new ContentValues();
        cv.put("arrivalDate",formatted);
        cv.put("arrivalTime",newArrivalTime);

        try {
            return super.update(TRIPCONTAINSACTIVITY_TABLE, cv, "idTrip = ? and arrivalDate = ? and arrivalTime = ?",new String[]{String.valueOf(idTrip),day,arrivalTime})>0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    protected TripContainsActivity cursorToEntity(Cursor cursor) {
        TripContainsActivity tripContainsActivity = new TripContainsActivity();

        int idTripIndex;
        int idActivityIndex;
        int arrivalDateIndex;
        int arrivalTimeIndex;


        if(cursor != null) {
            if(cursor.getColumnIndex(KEY_TRIPCONTAINSACTIVITY_IDTRIP) != -1) {
                idTripIndex = cursor.getColumnIndexOrThrow(KEY_TRIPCONTAINSACTIVITY_IDTRIP);
                tripContainsActivity.setIdTrip(cursor.getInt(idTripIndex));
            }
            if(cursor.getColumnIndex(KEY_TRIPCONTAINSACTIVITY_IDACTIVITY) != -1) {
                idActivityIndex = cursor.getColumnIndexOrThrow(KEY_TRIPCONTAINSACTIVITY_IDACTIVITY);
                tripContainsActivity.setIdActivity(cursor.getInt(idActivityIndex));
            }
            if(cursor.getColumnIndex(KEY_TRIPCONTAINSACTIVITY_ARRIVALDATE) != -1) {
                arrivalDateIndex = cursor.getColumnIndexOrThrow(KEY_TRIPCONTAINSACTIVITY_ARRIVALDATE);
                tripContainsActivity.setArrivalDate(cursor.getString(arrivalDateIndex));
            }
            if(cursor.getColumnIndex(KEY_TRIPCONTAINSACTIVITY_ARRIVALTIME) != -1) {
                arrivalTimeIndex = cursor.getColumnIndexOrThrow(KEY_TRIPCONTAINSACTIVITY_ARRIVALTIME);
                tripContainsActivity.setArrivalTime(cursor.getString(arrivalTimeIndex));
            }
        }
        return tripContainsActivity;
    }
}
