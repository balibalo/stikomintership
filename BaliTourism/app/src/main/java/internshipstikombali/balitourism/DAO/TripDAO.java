package internshipstikombali.balitourism.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import internshipstikombali.balitourism.business.Trip;
import internshipstikombali.balitourism.helper.DatabaseContentProvider;
import internshipstikombali.balitourism.helper.DatabaseSchema;

/**
 * Created by Pierre on 22/06/2016.
 */
public class TripDAO extends DatabaseContentProvider implements TripDAOInterface, DatabaseSchema {

    private Cursor cursor;
    private ContentValues initialValues;

    public TripDAO(SQLiteDatabase db) {
        super(db);
    }

    private void setContentValue(Trip trip) {
        initialValues = new ContentValues();
        initialValues.put(KEY_TRIP_IDTRIP, trip.getIdTrip());
        initialValues.put(KEY_TRIP_IDUSER, trip.getIdUser());
        initialValues.put(KEY_TRIP_ARRIVALDATE, trip.getArrivalDate());
        initialValues.put(KEY_TRIP_DURATION, trip.getDuration());
        initialValues.put(KEY_TRIP_NUMBEROFTRAVELLERS, trip.getNumberOfTravellers());
        initialValues.put(KEY_TRIP_BUDGET, trip.getBudget());
    }

    private ContentValues getContentValue() {
        return initialValues;
    }

    @Override
    public boolean addTrip(Trip trip) {
        setContentValue(trip);
        try {
            return super.insert(TRIP_TABLE, getContentValue()) > 0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public void deleteAllTrips() {
        super.delete(TRIP_TABLE, null, null);
    }


    @Override
    public List<Trip> fetchAllTrip(){
        List<Trip> resList = new ArrayList<Trip>();
        cursor = super.query(TRIP_TABLE, TRIP_COLUMNS, null, null, KEY_TRIP_IDTRIP);
        if(cursor != null){
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                Trip trip = cursorToEntity(cursor);
                resList.add(trip);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return resList;
    }

    @Override
    public Trip fetchTripByIdUser(int idUser){
        final String selectionArgs[] = {String.valueOf(idUser)};
        final String selection = KEY_TRIP_IDUSER + " = ?";
        Trip trip = new Trip();
        cursor = super.query(TRIP_TABLE, TRIP_COLUMNS, selection, selectionArgs, KEY_TRIP_IDUSER);
        if(cursor != null) {
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                trip = cursorToEntity(cursor);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return trip;
    }

    @Override
    public boolean deleteTripById(int idTrip) {
        try {
            return super.delete(TRIP_TABLE, "idTrip = ?", new String[]{String.valueOf(idTrip)}) >0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    protected Trip cursorToEntity(Cursor cursor) {
        Trip trip = new Trip();

        int idTripIndex;
        int idUserIndex;
        int arrivalDateIndex;
        int durationIndex;
        int numberOfTravellersIndex;
        int budgetIndex;

        if(cursor != null) {
            if(cursor.getColumnIndex(KEY_TRIP_IDTRIP) != -1) {
                idTripIndex = cursor.getColumnIndexOrThrow(KEY_TRIP_IDTRIP);
                trip.setIdTrip(cursor.getInt(idTripIndex));
            }
            if(cursor.getColumnIndex(KEY_TRIP_IDUSER) != -1) {
                idUserIndex = cursor.getColumnIndexOrThrow(KEY_TRIP_IDUSER);
                trip.setIdUser(cursor.getInt(idUserIndex));
            }
            if(cursor.getColumnIndex(KEY_TRIP_ARRIVALDATE) != -1) {
                arrivalDateIndex = cursor.getColumnIndexOrThrow(KEY_TRIP_ARRIVALDATE);
                trip.setArrivalDate(cursor.getString(arrivalDateIndex));
            }
            if(cursor.getColumnIndex(KEY_TRIP_DURATION) != -1) {
                durationIndex = cursor.getColumnIndexOrThrow(KEY_TRIP_DURATION);
                trip.setDuration(cursor.getInt(durationIndex));
            }
            if(cursor.getColumnIndex(KEY_TRIP_NUMBEROFTRAVELLERS) != -1) {
                numberOfTravellersIndex = cursor.getColumnIndexOrThrow(KEY_TRIP_NUMBEROFTRAVELLERS);
                trip.setNumberOfTravellers(cursor.getInt(numberOfTravellersIndex));
            }
            if(cursor.getColumnIndex(KEY_TRIP_BUDGET) != -1) {
                budgetIndex = cursor.getColumnIndexOrThrow(KEY_TRIP_BUDGET);
                trip.setBudget(cursor.getInt(budgetIndex));
            }
        }
        return trip;
    }
}