package internshipstikombali.balitourism.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import internshipstikombali.balitourism.business.Restaurant;
import internshipstikombali.balitourism.helper.DatabaseContentProvider;
import internshipstikombali.balitourism.helper.DatabaseSchema;

/**
 * Created by Pierre on 22/06/2016.
 */
public class RestaurantDAO extends DatabaseContentProvider implements RestaurantDAOInterface, DatabaseSchema {

    private Cursor cursor;
    private ContentValues initialValues;

    public RestaurantDAO(SQLiteDatabase db) {
        super(db);
    }

    private void setContentValue(Restaurant restaurant) {
        initialValues = new ContentValues();
        initialValues.put(KEY_RESTAURANT_IDRESTAURANT, restaurant.getIdRestaurant());
        initialValues.put(KEY_RESTAURANT_NAME, restaurant.getName());
        initialValues.put(KEY_RESTAURANT_LONGITUDE, restaurant.getLongitude());
        initialValues.put(KEY_RESTAURANT_LATITUDE, restaurant.getLatitude());
        initialValues.put(KEY_RESTAURANT_PHONENUMBER, restaurant.getPhoneNumber());
        initialValues.put(KEY_RESTAURANT_WEBSITE, restaurant.getWebsite());
        initialValues.put(KEY_RESTAURANT_PRICE, restaurant.getPrice());
        initialValues.put(KEY_RESTAURANT_DESCRIPTION, restaurant.getDescription());
        initialValues.put(KEY_RESTAURANT_PICTURE, restaurant.getPicture());
        initialValues.put(KEY_RESTAURANT_OPENINGHOUR, restaurant.getOpeningHour());
        initialValues.put(KEY_RESTAURANT_OPENINGMINUTE, restaurant.getOpeningMinute());
        initialValues.put(KEY_RESTAURANT_CLOSINGHOUR, restaurant.getClosingHour());
        initialValues.put(KEY_RESTAURANT_CLOSINGMINUTE, restaurant.getClosingMinute());
    }

    private ContentValues getContentValue() {
        return initialValues;
    }

    @Override
    public boolean addRestaurant(Restaurant restaurant) {
        setContentValue(restaurant);
        try {
            return super.insert(RESTAURANT_TABLE, getContentValue()) > 0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public boolean updateRestaurant(int idRest, String name, String longitude, String latitude, String phone, String website, int price, String description, String url, int oh, int om, int ch, int cm){
        ContentValues cv = new ContentValues();
        cv.put("name",name);
        cv.put("longitude",longitude);
        cv.put("latitude",latitude);
        cv.put("phoneNumber",phone);
        cv.put("website",website);
        cv.put("price",price);
        cv.put("description",description);
        cv.put("picture",url);
        cv.put("openingHour",oh);
        cv.put("openingMinute",om);
        cv.put("closingHour",ch);
        cv.put("closingMinute",cm);


        try {
            return super.update(RESTAURANT_TABLE,cv, "idRestaurant = ?",new String[]{String.valueOf(idRest)}) >0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public void deleteAllRestaurants() {
        super.delete(RESTAURANT_TABLE, null, null);
    }

    @Override
    public boolean deleteRestaurant(int idRestaurant) {
        try {
            return super.delete(RESTAURANT_TABLE, "idRestaurant = ?", new String[]{String.valueOf(idRestaurant)}) >0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public List<Restaurant> fetchAllRestaurants() {
        List<Restaurant> restaurantList = new ArrayList<Restaurant>();
        cursor = super.query(RESTAURANT_TABLE, RESTAURANT_COLUMNS, null, null, KEY_RESTAURANT_IDRESTAURANT);
        if(cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Restaurant restaurant = cursorToEntity(cursor);
                restaurantList.add(restaurant);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return restaurantList;
    }

    @Override
    public Restaurant fetchRestaurantById(int idRestaurant){
        final String selectionArgs[] = {String.valueOf(idRestaurant)};
        final String selection = KEY_RESTAURANT_IDRESTAURANT + " = ?";
        Restaurant restaurant = new Restaurant();
        cursor = super.query(RESTAURANT_TABLE, RESTAURANT_COLUMNS, selection, selectionArgs, KEY_RESTAURANT_IDRESTAURANT);
        if(cursor != null) {
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                restaurant = cursorToEntity(cursor);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return restaurant;
    }


    @Override
    protected Restaurant cursorToEntity(Cursor cursor) {
        Restaurant restaurant = new Restaurant();

        int idRestaurantIndex;
        int nameIndex;
        int longitudeIndex;
        int latitudeIndex;
        int phoneNumberIndex;
        int websiteIndex;
        int priceIndex;
        int descriptionIndex;
        int pictureIndex;
        int openingHourIndex;
        int openingMinuteIndex;
        int closingHourIndex;
        int closingMinuteIndex;

        if(cursor != null) {
            if(cursor.getColumnIndex(KEY_RESTAURANT_IDRESTAURANT) != -1) {
                idRestaurantIndex = cursor.getColumnIndexOrThrow(KEY_RESTAURANT_IDRESTAURANT);
                restaurant.setIdRestaurant(cursor.getInt(idRestaurantIndex));
            }
            if(cursor.getColumnIndex(KEY_RESTAURANT_NAME) != -1) {
                nameIndex = cursor.getColumnIndexOrThrow(KEY_RESTAURANT_NAME);
                restaurant.setName(cursor.getString(nameIndex));
            }
            if(cursor.getColumnIndex(KEY_RESTAURANT_LONGITUDE) != -1) {
                longitudeIndex = cursor.getColumnIndexOrThrow(KEY_RESTAURANT_LONGITUDE);
                restaurant.setLongitude(cursor.getString(longitudeIndex));
            }
            if(cursor.getColumnIndex(KEY_RESTAURANT_LATITUDE) != -1) {
                latitudeIndex = cursor.getColumnIndexOrThrow(KEY_RESTAURANT_LATITUDE);
                restaurant.setLatitude(cursor.getString(latitudeIndex));
            }
            if(cursor.getColumnIndex(KEY_RESTAURANT_PHONENUMBER) != -1) {
                phoneNumberIndex = cursor.getColumnIndexOrThrow(KEY_RESTAURANT_PHONENUMBER);
                restaurant.setPhoneNumber(cursor.getString(phoneNumberIndex));
            }
            if(cursor.getColumnIndex(KEY_RESTAURANT_WEBSITE) != -1) {
                websiteIndex = cursor.getColumnIndexOrThrow(KEY_RESTAURANT_WEBSITE);
                restaurant.setWebsite(cursor.getString(websiteIndex));
            }
            if(cursor.getColumnIndex(KEY_RESTAURANT_PRICE) != -1) {
                priceIndex = cursor.getColumnIndexOrThrow(KEY_RESTAURANT_PRICE);
                restaurant.setPrice(cursor.getInt(priceIndex));
            }
            if(cursor.getColumnIndex(KEY_RESTAURANT_DESCRIPTION) != -1) {
                descriptionIndex = cursor.getColumnIndexOrThrow(KEY_RESTAURANT_DESCRIPTION);
                restaurant.setDescription(cursor.getString(descriptionIndex));
            }
            if(cursor.getColumnIndex(KEY_RESTAURANT_PICTURE) != -1) {
                pictureIndex = cursor.getColumnIndexOrThrow(KEY_RESTAURANT_PICTURE);
                restaurant.setPicture(cursor.getString(pictureIndex));
            }
            if(cursor.getColumnIndex(KEY_RESTAURANT_OPENINGHOUR) != -1) {
                openingHourIndex = cursor.getColumnIndexOrThrow(KEY_RESTAURANT_OPENINGHOUR);
                restaurant.setOpeningHour(cursor.getInt(openingHourIndex));
            }
            if(cursor.getColumnIndex(KEY_RESTAURANT_OPENINGMINUTE) != -1) {
                openingMinuteIndex = cursor.getColumnIndexOrThrow(KEY_RESTAURANT_OPENINGMINUTE);
                restaurant.setOpeningMinute(cursor.getInt(openingMinuteIndex));
            }
            if(cursor.getColumnIndex(KEY_RESTAURANT_CLOSINGHOUR) != -1) {
                closingHourIndex = cursor.getColumnIndexOrThrow(KEY_RESTAURANT_CLOSINGHOUR);
                restaurant.setClosingHour(cursor.getInt(closingHourIndex));
            }
            if(cursor.getColumnIndex(KEY_RESTAURANT_CLOSINGMINUTE) != -1) {
                closingMinuteIndex = cursor.getColumnIndexOrThrow(KEY_RESTAURANT_CLOSINGMINUTE);
                restaurant.setClosingMinute(cursor.getInt(closingMinuteIndex));
            }
        }
        return restaurant;
    }
}
