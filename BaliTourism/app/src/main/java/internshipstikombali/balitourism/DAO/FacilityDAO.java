package internshipstikombali.balitourism.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import internshipstikombali.balitourism.business.Facility;
import internshipstikombali.balitourism.helper.DatabaseContentProvider;
import internshipstikombali.balitourism.helper.DatabaseSchema;

/**
 * Created by Pierre on 22/06/2016.
 */
public class FacilityDAO extends DatabaseContentProvider implements FacilityDAOInterface, DatabaseSchema {

    private Cursor cursor;
    private ContentValues initialValues;

    public FacilityDAO(SQLiteDatabase db) {
        super(db);
    }

    private void setContentValue(Facility facility) {
        initialValues = new ContentValues();
        initialValues.put(KEY_FACILITY_NAMEFACILITY, facility.getNameFacility());
        initialValues.put(KEY_FACILITY_DESCRIPTION, facility.getDescription());
    }

    private ContentValues getContentValue() {
        return initialValues;
    }

    @Override
    public boolean addFacility(Facility facility) {
        setContentValue(facility);
        try {
            return super.insert(FACILITY_TABLE, getContentValue()) > 0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public void deleteAllFacilities() {
        super.delete(FACILITY_TABLE, null, null);
    }

    @Override
    protected Facility cursorToEntity(Cursor cursor) {
        Facility facility = new Facility();

        int nameFacilityIndex;
        int descriptionIndex;

        if(cursor != null) {
            if(cursor.getColumnIndex(KEY_FACILITY_NAMEFACILITY) != -1) {
                nameFacilityIndex = cursor.getColumnIndexOrThrow(KEY_FACILITY_NAMEFACILITY);
                facility.setNameFacility(cursor.getString(nameFacilityIndex));
            }
            if(cursor.getColumnIndex(KEY_FACILITY_DESCRIPTION) != -1) {
                descriptionIndex = cursor.getColumnIndexOrThrow(KEY_FACILITY_DESCRIPTION);
                facility.setDescription(cursor.getString(descriptionIndex));
            }
        }
        return facility;
    }
}
