package internshipstikombali.balitourism.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import internshipstikombali.balitourism.business.TripContainsTransport;
import internshipstikombali.balitourism.helper.DatabaseContentProvider;
import internshipstikombali.balitourism.helper.DatabaseSchema;

/**
 * Created by Pierre on 22/06/2016.
 */
public class TripContainsTransportDAO extends DatabaseContentProvider implements TripContainsTransportDAOInterface, DatabaseSchema {
    private Cursor cursor;
    private ContentValues initialValues;

    public TripContainsTransportDAO(SQLiteDatabase db) {
        super(db);
    }

    private void setContentValue(TripContainsTransport tripContainsTransport) {
        initialValues = new ContentValues();
        initialValues.put(KEY_TRIPCONTAINSTRANSPORT_IDTRIP, tripContainsTransport.getIdTrip());
        initialValues.put(KEY_TRIPCONTAINSTRANSPORT_IDTRANSPORT, tripContainsTransport.getIdTransport());
        initialValues.put(KEY_TRIPCONTAINSTRANSPORT_DATETRANSPORT, String.valueOf(tripContainsTransport.getDateTransport()));
        initialValues.put(KEY_TRIPCONTAINSTRANSPORT_DURATION, tripContainsTransport.getDuration());
        initialValues.put(KEY_TRIPCONTAINSTRANSPORT_PRICE, tripContainsTransport.getPrice());
    }

    private ContentValues getContentValue() {
        return initialValues;
    }

    @Override
    public boolean addTripContainsTransport(TripContainsTransport tripContainsTransport) {
        setContentValue(tripContainsTransport);
        try {
            return super.insert(TRIPCONTAINSTRANSPORT_TABLE, getContentValue()) > 0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public void deleteAllTripContainsTransport() {
        super.delete(TRIPCONTAINSTRANSPORT_TABLE, null, null);
    }

    @Override
    public boolean deleteTripContainsTransport(int idTrip,int idTransport, String day) {
        try {
            return super.delete(TRIPCONTAINSTRANSPORT_TABLE, "idTrip = ? and idTransport = ? and dateTransport = ?", new String[]{String.valueOf(idTrip), String.valueOf(idTransport),day}) >0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public boolean deleteTripContainsTransportById(int idTrip) {
        try {
            return super.delete(TRIPCONTAINSTRANSPORT_TABLE, "idTrip = ?", new String[]{String.valueOf(idTrip)}) >0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public boolean updateTripContainsTransport(int idTrip, String day, String formatted) {
        ContentValues cv = new ContentValues();
        cv.put("dateTransport",formatted);
        try {
            return super.update(TRIPCONTAINSTRANSPORT_TABLE,cv, "idTrip = ? and dateTransport = ?",new String[]{String.valueOf(idTrip),day}) >0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public List<TripContainsTransport> fetchAllTripContainsTransport(){
        List<TripContainsTransport> resList = new ArrayList<TripContainsTransport>();
        cursor = super.query(TRIPCONTAINSTRANSPORT_TABLE, TRIPCONTAINSTRANSPORT_COLUMNS, null, null, null);
        if(cursor != null){
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                TripContainsTransport tripContainsTransport = cursorToEntity(cursor);
                resList.add(tripContainsTransport);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return resList;
    }

    @Override
    protected TripContainsTransport cursorToEntity(Cursor cursor) {
        TripContainsTransport tripContainsTransport = new TripContainsTransport();

        int idTripIndex;
        int idTransportIndex;
        int dateIndex;
        int durationIndex;
        int priceIndex;


        if(cursor != null) {
            if(cursor.getColumnIndex(KEY_TRIPCONTAINSTRANSPORT_IDTRIP) != -1) {
                idTripIndex = cursor.getColumnIndexOrThrow(KEY_TRIPCONTAINSTRANSPORT_IDTRIP);
                tripContainsTransport.setIdTrip(cursor.getInt(idTripIndex));
            }
            if(cursor.getColumnIndex(KEY_TRIPCONTAINSTRANSPORT_IDTRANSPORT) != -1) {
                idTransportIndex = cursor.getColumnIndexOrThrow(KEY_TRIPCONTAINSTRANSPORT_IDTRANSPORT);
                tripContainsTransport.setIdTransport(cursor.getInt(idTransportIndex));
            }
            if(cursor.getColumnIndex(KEY_TRIPCONTAINSTRANSPORT_DATETRANSPORT) != -1) {
                dateIndex = cursor.getColumnIndexOrThrow(KEY_TRIPCONTAINSTRANSPORT_DATETRANSPORT);
                tripContainsTransport.setDateTransport(cursor.getString(dateIndex));
            }
            if(cursor.getColumnIndex(KEY_TRIPCONTAINSTRANSPORT_DURATION) != -1) {
                durationIndex = cursor.getColumnIndexOrThrow(KEY_TRIPCONTAINSTRANSPORT_DURATION);
                tripContainsTransport.setDuration(cursor.getInt(durationIndex));
            }
            if(cursor.getColumnIndex(KEY_TRIPCONTAINSTRANSPORT_PRICE) != -1) {
                priceIndex = cursor.getColumnIndexOrThrow(KEY_TRIPCONTAINSTRANSPORT_PRICE);
                tripContainsTransport.setPrice(cursor.getInt(priceIndex));
            }
        }
        return tripContainsTransport;
    }
}
