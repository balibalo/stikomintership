package internshipstikombali.balitourism.DAO;

import java.util.List;

import internshipstikombali.balitourism.business.TripContainsTransport;

/**
 * Created by Pierre on 22/06/2016.
 */
public interface TripContainsTransportDAOInterface {
    public boolean addTripContainsTransport(TripContainsTransport tripContainsTransport);
    public void deleteAllTripContainsTransport();
    public boolean deleteTripContainsTransport(int idTrip,int idTransport, String day);
    public boolean deleteTripContainsTransportById(int idTrip);
    public boolean updateTripContainsTransport(int idTrip, String day, String formatted);
    public List<TripContainsTransport> fetchAllTripContainsTransport();

}
