package internshipstikombali.balitourism.DAO;

import internshipstikombali.balitourism.business.AccommodationBelongsCategoryAccommodation;

/**
 * Created by Pierre on 22/06/2016.
 */
public interface AccommodationBelongsCategoryAccommodationDAOInterface {
    public boolean addAccommodationBelongsCategoryAccommodation(AccommodationBelongsCategoryAccommodation accommodationBelongsCategoryAccommodation);
    public void deleteAllAccommodationBelongsCategoryAccommodation();
    public boolean deleteAccommodationBelongsCategoryAccommodation(int idAccommodation);
    public boolean updateAccommodationBelongsCategoryAccommodation(int idAccommodation, String category);
    public String getCategoryAccommodationByIdAccommodation(int idAccommodation);
}
