package internshipstikombali.balitourism.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import internshipstikombali.balitourism.business.Transport;
import internshipstikombali.balitourism.helper.DatabaseContentProvider;
import internshipstikombali.balitourism.helper.DatabaseSchema;

/**
 * Created by Pierre on 22/06/2016.
 */
public class TransportDAO extends DatabaseContentProvider implements TransportDAOInterface, DatabaseSchema {

    private Cursor cursor;
    private ContentValues initialValues;

    public TransportDAO(SQLiteDatabase db) {
        super(db);
    }

    private void setContentValue(Transport transport) {
        initialValues = new ContentValues();
        initialValues.put(KEY_TRANSPORT_IDTRANSPORT, transport.getIdTransport());
        initialValues.put(KEY_TRANSPORT_NAME, transport.getName());
        initialValues.put(KEY_TRANSPORT_DESCRIPTION, transport.getDescription());
        initialValues.put(KEY_TRANSPORT_PICTURE, transport.getPicture());
        initialValues.put(KEY_TRANSPORT_NUMBEROFPLACES, transport.getNumberOfPlaces());
        initialValues.put(KEY_TRANSPORT_NEEDDRIVINGLICENSE, transport.getNeedDrivingLicense());
        initialValues.put(KEY_TRANSPORT_PRICE, transport.getPrice());
    }

    private ContentValues getContentValue() {
        return initialValues;
    }

    @Override
    public boolean addTransport(Transport transport) {
        setContentValue(transport);
        try {
            return super.insert(TRANSPORT_TABLE, getContentValue()) > 0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public boolean updateTransport(int idTransport, String name, String description, String url, int places,  int License, int price){
        ContentValues cv = new ContentValues();
        cv.put("name",name);
        cv.put("price",price);
        cv.put("description",description);
        cv.put("picture",url);
        cv.put("numberOfPlaces",places);
        cv.put("needDrivingLicense",License);
        try {
            return super.update(TRANSPORT_TABLE,cv, "idTransport = ?",new String[]{String.valueOf(idTransport)}) >0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public void deleteAllTransports() {
        super.delete(TRANSPORT_TABLE, null, null);
    }

    @Override
    public boolean deleteTransport(int idTransport) {
        try {
            return super.delete(TRANSPORT_TABLE, "idTransport = ?", new String[]{String.valueOf(idTransport)}) >0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public List<Transport> fetchAllTransports() {
        List<Transport> transportList = new ArrayList<Transport>();
        cursor = super.query(TRANSPORT_TABLE, TRANSPORT_COLUMNS, null, null, KEY_TRANSPORT_IDTRANSPORT);
        if(cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Transport transport = cursorToEntity(cursor);
                transportList.add(transport);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return transportList;
    }

    @Override
    public Transport fetchTransportById(int idTransport){
        final String selectionArgs[] = {String.valueOf(idTransport)};
        final String selection = KEY_TRANSPORT_IDTRANSPORT + " = ?";
        Transport transport = new Transport();
        cursor = super.query(TRANSPORT_TABLE, TRANSPORT_COLUMNS, selection, selectionArgs, KEY_TRANSPORT_IDTRANSPORT);
        if(cursor != null) {
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                transport = cursorToEntity(cursor);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return transport;
    }


    @Override
    protected Transport cursorToEntity(Cursor cursor) {
        Transport transport = new Transport();

        int idTransportIndex;
        int nameIndex;
        int descriptionIndex;
        int pictureIndex;
        int numberOfPlacesIndex;
        int needDrivingLicenseIndex;
        int priceIndex;

        if(cursor != null) {
            if(cursor.getColumnIndex(KEY_TRANSPORT_IDTRANSPORT) != -1) {
                idTransportIndex = cursor.getColumnIndexOrThrow(KEY_TRANSPORT_IDTRANSPORT);
                transport.setIdTransport(cursor.getInt(idTransportIndex));
            }
            if(cursor.getColumnIndex(KEY_TRANSPORT_NAME) != -1) {
                nameIndex = cursor.getColumnIndexOrThrow(KEY_TRANSPORT_NAME);
                transport.setName(cursor.getString(nameIndex));
            }
            if(cursor.getColumnIndex(KEY_TRANSPORT_DESCRIPTION) != -1) {
                descriptionIndex = cursor.getColumnIndexOrThrow(KEY_TRANSPORT_DESCRIPTION);
                transport.setDescription(cursor.getString(descriptionIndex));
            }
            if(cursor.getColumnIndex(KEY_TRANSPORT_PICTURE) != -1) {
                pictureIndex = cursor.getColumnIndexOrThrow(KEY_TRANSPORT_PICTURE);
                transport.setPicture(cursor.getString(pictureIndex));
            }
            if(cursor.getColumnIndex(KEY_TRANSPORT_NUMBEROFPLACES) != -1) {
                numberOfPlacesIndex = cursor.getColumnIndexOrThrow(KEY_TRANSPORT_NUMBEROFPLACES);
                transport.setNumberOfPlaces(cursor.getInt(numberOfPlacesIndex));
            }
            if(cursor.getColumnIndex(KEY_TRANSPORT_NEEDDRIVINGLICENSE) != -1) {
                needDrivingLicenseIndex = cursor.getColumnIndexOrThrow(KEY_TRANSPORT_NEEDDRIVINGLICENSE);
                transport.setNeedDrivingLicense(cursor.getInt(needDrivingLicenseIndex));
            }
            if(cursor.getColumnIndex(KEY_TRANSPORT_PRICE) != -1) {
                priceIndex = cursor.getColumnIndexOrThrow(KEY_TRANSPORT_PRICE);
                transport.setPrice(cursor.getInt(priceIndex));
            }
        }
        return transport;
    }
}