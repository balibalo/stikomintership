package internshipstikombali.balitourism.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import internshipstikombali.balitourism.business.CategoryAccommodation;
import internshipstikombali.balitourism.helper.DatabaseContentProvider;
import internshipstikombali.balitourism.helper.DatabaseSchema;

/**
 * Created by Pierre on 22/06/2016.
 */
public class CategoryAccommodationDAO extends DatabaseContentProvider implements CategoryAccommodationDAOInterface, DatabaseSchema {

    private Cursor cursor;
    private ContentValues initialValues;

    public CategoryAccommodationDAO(SQLiteDatabase db) {
        super(db);
    }

    private void setContentValue(CategoryAccommodation categoryAccommodation) {
        initialValues = new ContentValues();
        initialValues.put(KEY_CATEGORYACCOMMODATION_NAMECATEGORYACCOMMODATION, categoryAccommodation.getNameCategoryAccommodation());
        initialValues.put(KEY_CATEGORYACCOMMODATION_DESCRIPTION, categoryAccommodation.getDescription());
    }

    private ContentValues getContentValue() {
        return initialValues;
    }

    @Override
    public boolean addCategoryAccommodation(CategoryAccommodation categoryAccommodation) {
        setContentValue(categoryAccommodation);
        try {
            return super.insert(CATEGORYACCOMMODATION_TABLE, getContentValue()) > 0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public void deleteAllCategoriesAccommodation() {
        super.delete(CATEGORYACCOMMODATION_TABLE, null, null);
    }

    @Override
    protected CategoryAccommodation cursorToEntity(Cursor cursor) {
        CategoryAccommodation categoryAccommodation = new CategoryAccommodation();

        int nameCategoryAccommodationIndex;
        int descriptionIndex;

        if(cursor != null) {
            if(cursor.getColumnIndex(KEY_CATEGORYACCOMMODATION_NAMECATEGORYACCOMMODATION) != -1) {
                nameCategoryAccommodationIndex = cursor.getColumnIndexOrThrow(KEY_CATEGORYACCOMMODATION_NAMECATEGORYACCOMMODATION);
                categoryAccommodation.setNameCategoryAccommodation(cursor.getString(nameCategoryAccommodationIndex));
            }
            if(cursor.getColumnIndex(KEY_CATEGORYACCOMMODATION_DESCRIPTION) != -1) {
                descriptionIndex = cursor.getColumnIndexOrThrow(KEY_CATEGORYACCOMMODATION_DESCRIPTION);
                categoryAccommodation.setDescription(cursor.getString(descriptionIndex));
            }
        }
        return categoryAccommodation;
    }
}
