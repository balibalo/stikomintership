package internshipstikombali.balitourism.DAO;

import java.util.List;

import internshipstikombali.balitourism.business.TripContainsAccommodation;

/**
 * Created by Pierre on 22/06/2016.
 */
public interface TripContainsAccommodationDAOInterface {
    public boolean addTripContainsAccommodation(TripContainsAccommodation tripContainsAccommodation);
    public void deleteAllTripContainsAccommodation();
    public boolean deleteTripContainsAccommodation(int idTrip,int idAccommodation,String day);
    public boolean deleteTripContainsAccommodationById(int idTrip);
    public boolean updateTripContainsAccommodation(int idTrip, String day, String formatted);
    public List<TripContainsAccommodation> fetchAllTripContainsAccommodation();
}
