package internshipstikombali.balitourism.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import internshipstikombali.balitourism.business.TripContainsRestaurant;
import internshipstikombali.balitourism.helper.DatabaseContentProvider;
import internshipstikombali.balitourism.helper.DatabaseSchema;

/**
 * Created by Pierre on 22/06/2016.
 */
public class TripContainsRestaurantDAO extends DatabaseContentProvider implements TripContainsRestaurantDAOInterface, DatabaseSchema {
    private Cursor cursor;
    private ContentValues initialValues;

    public TripContainsRestaurantDAO(SQLiteDatabase db) {
        super(db);
    }

    private void setContentValue(TripContainsRestaurant tripContainsRestaurant) {
        initialValues = new ContentValues();
        initialValues.put(KEY_TRIPCONTAINSRESTAURANT_IDTRIP, tripContainsRestaurant.getIdTrip());
        initialValues.put(KEY_TRIPCONTAINSRESTAURANT_IDRESTAURANT, tripContainsRestaurant.getIdRestaurant());
        initialValues.put(KEY_TRIPCONTAINSRESTAURANT_ARRIVALDATE, String.valueOf(tripContainsRestaurant.getArrivalDate()));
        initialValues.put(KEY_TRIPCONTAINSRESTAURANT_ARRIVALTIME, String.valueOf(tripContainsRestaurant.getArrivalTime()));
    }

    private ContentValues getContentValue() {
        return initialValues;
    }

    @Override
    public boolean addTripContainsRestaurant(TripContainsRestaurant tripContainsRestaurant) {
        setContentValue(tripContainsRestaurant);
        try {
            return super.insert(TRIPCONTAINSRESTAURANT_TABLE, getContentValue()) > 0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public void deleteAllTripContainsRestaurant() {
        super.delete(TRIPCONTAINSRESTAURANT_TABLE, null, null);
    }

    @Override
    public List<TripContainsRestaurant> fetchAllTripContainsRestaurant(){
        List<TripContainsRestaurant> resList = new ArrayList<TripContainsRestaurant>();
        cursor = super.query(TRIPCONTAINSRESTAURANT_TABLE, TRIPCONTAINSRESTAURANT_COLUMNS, null, null, null);
        if(cursor != null){
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                TripContainsRestaurant tripContainsRestaurant = cursorToEntity(cursor);
                resList.add(tripContainsRestaurant);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return resList;
    }

    @Override
    public boolean deleteTripContainsRestaurant(int idTrip,int idRestaurant, String day, String arrivalTime) {
        try {
            return super.delete(TRIPCONTAINSRESTAURANT_TABLE, "idTrip = ? and idRestaurant = ? and arrivalDate = ? and arrivalTime = ?", new String[]{String.valueOf(idTrip), String.valueOf(idRestaurant),day,arrivalTime}) >0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public boolean deleteTripContainsRestaurantById(int idTrip) {
        try {
            return super.delete(TRIPCONTAINSRESTAURANT_TABLE, "idTrip = ?", new String[]{String.valueOf(idTrip)}) >0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public boolean updateTripContainsRestaurant(int idTrip, String day, String formatted, String arrivalTime, String newArrivalTime) {
        ContentValues cv = new ContentValues();
        cv.put("arrivalDate",formatted);
        cv.put("arrivalTime",newArrivalTime);

        try {
            return super.update(TRIPCONTAINSRESTAURANT_TABLE,cv, "idTrip = ? and arrivalDate = ? and arrivalTime = ?",new String[]{String.valueOf(idTrip),day,arrivalTime}) >0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    protected TripContainsRestaurant cursorToEntity(Cursor cursor) {
        TripContainsRestaurant tripContainsRestaurant = new TripContainsRestaurant();

        int idTripIndex;
        int idRestaurantIndex;
        int arrivalDateIndex;
        int arrivalTimeIndex;


        if(cursor != null) {
            if(cursor.getColumnIndex(KEY_TRIPCONTAINSRESTAURANT_IDTRIP) != -1) {
                idTripIndex = cursor.getColumnIndexOrThrow(KEY_TRIPCONTAINSRESTAURANT_IDTRIP);
                tripContainsRestaurant.setIdTrip(cursor.getInt(idTripIndex));
            }
            if(cursor.getColumnIndex(KEY_TRIPCONTAINSRESTAURANT_IDRESTAURANT) != -1) {
                idRestaurantIndex = cursor.getColumnIndexOrThrow(KEY_TRIPCONTAINSRESTAURANT_IDRESTAURANT);
                tripContainsRestaurant.setIdRestaurant(cursor.getInt(idRestaurantIndex));
            }
            if(cursor.getColumnIndex(KEY_TRIPCONTAINSRESTAURANT_ARRIVALDATE) != -1) {
                arrivalDateIndex = cursor.getColumnIndexOrThrow(KEY_TRIPCONTAINSRESTAURANT_ARRIVALDATE);
                tripContainsRestaurant.setArrivalDate(cursor.getString(arrivalDateIndex));
            }
            if(cursor.getColumnIndex(KEY_TRIPCONTAINSRESTAURANT_ARRIVALTIME) != -1) {
                arrivalTimeIndex = cursor.getColumnIndexOrThrow(KEY_TRIPCONTAINSRESTAURANT_ARRIVALTIME);
                tripContainsRestaurant.setArrivalTime(cursor.getString(arrivalTimeIndex));
            }
        }
        return tripContainsRestaurant;
    }
}
