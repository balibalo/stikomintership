package internshipstikombali.balitourism.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import internshipstikombali.balitourism.business.UserHasCategoryActivityPreferences;
import internshipstikombali.balitourism.helper.DatabaseContentProvider;
import internshipstikombali.balitourism.helper.DatabaseSchema;

/**
 * Created by Pierre on 22/06/2016.
 */
public class UserHasCategoryActivityPreferencesDAO extends DatabaseContentProvider implements UserHasCategoryActivityPreferencesDAOInterface, DatabaseSchema {
    private Cursor cursor;
    private ContentValues initialValues;

    public UserHasCategoryActivityPreferencesDAO(SQLiteDatabase db) {
        super(db);
    }

    private void setContentValue(UserHasCategoryActivityPreferences userHasCategoryActivityPreferences) {
        initialValues = new ContentValues();
        initialValues.put(KEY_USERHASCATEGORYACTIVITYPREFERENCES_IDUSER, userHasCategoryActivityPreferences.getIdUser());
        initialValues.put(KEY_USERHASCATEGORYACTIVITYPREFERENCES_NAMECATEGORYACTIVITY, userHasCategoryActivityPreferences.getNameCategoryActivity());
    }

    private ContentValues getContentValue() {
        return initialValues;
    }

    @Override
    public boolean addUserHasCategoryActivityPreferences(UserHasCategoryActivityPreferences userHasCategoryActivityPreferences) {
        setContentValue(userHasCategoryActivityPreferences);
        try {
            return super.insert(USERHASCATEGORYACTIVITYPREFERENCES_TABLE, getContentValue()) > 0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public void deleteAllUserHasCategoryActivityPreferences() {
        super.delete(USERHASCATEGORYACTIVITYPREFERENCES_TABLE, null, null);
    }

    @Override
    protected UserHasCategoryActivityPreferences cursorToEntity(Cursor cursor) {
        UserHasCategoryActivityPreferences userHasCategoryActivityPreferences = new UserHasCategoryActivityPreferences();

        int idUserIndex;
        int nameCategoryActivityIndex;

        if(cursor != null) {
            if(cursor.getColumnIndex(KEY_USERHASCATEGORYACTIVITYPREFERENCES_IDUSER) != -1) {
                idUserIndex = cursor.getColumnIndexOrThrow(KEY_USERHASCATEGORYACTIVITYPREFERENCES_IDUSER);
                userHasCategoryActivityPreferences.setIdUser(cursor.getInt(idUserIndex));
            }
            if(cursor.getColumnIndex(KEY_USERHASCATEGORYACTIVITYPREFERENCES_NAMECATEGORYACTIVITY) != -1) {
                nameCategoryActivityIndex = cursor.getColumnIndexOrThrow(KEY_USERHASCATEGORYACTIVITYPREFERENCES_NAMECATEGORYACTIVITY);
                userHasCategoryActivityPreferences.setNameCategoryActivity(cursor.getString(nameCategoryActivityIndex));
            }

        }
        return userHasCategoryActivityPreferences;
    }
}
