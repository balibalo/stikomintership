package internshipstikombali.balitourism.DAO;

import internshipstikombali.balitourism.business.Facility;

/**
 * Created by Pierre on 22/06/2016.
 */
public interface FacilityDAOInterface {
    public boolean addFacility(Facility facility);
    public void deleteAllFacilities();
}
