package internshipstikombali.balitourism.DAO;

import internshipstikombali.balitourism.business.UserHasCategoryActivityPreferences;

/**
 * Created by Pierre on 22/06/2016.
 */
public interface UserHasCategoryActivityPreferencesDAOInterface {
    public boolean addUserHasCategoryActivityPreferences(UserHasCategoryActivityPreferences userHasCategoryActivityPreferences);
    public void deleteAllUserHasCategoryActivityPreferences();
}
