package internshipstikombali.balitourism.DAO;

import internshipstikombali.balitourism.business.CategoryActivity;

/**
 * Created by Pierre on 22/06/2016.
 */
public interface CategoryActivityDAOInterface {
    public boolean addCategoryActivity(CategoryActivity categoryActivity);
    public void deleteAllCategoriesActivity();
}
