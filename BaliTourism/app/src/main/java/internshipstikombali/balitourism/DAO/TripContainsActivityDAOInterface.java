package internshipstikombali.balitourism.DAO;

import java.util.List;

import internshipstikombali.balitourism.business.TripContainsActivity;

/**
 * Created by Pierre on 22/06/2016.
 */
public interface TripContainsActivityDAOInterface {
    public boolean addTripContainsActivity(TripContainsActivity tripContainsActivity);
    public void deleteAllTripContainsActivity();
    public List<TripContainsActivity> fetchAllTripContainsActivity();
    public boolean deleteTripContainsActivity(int idTrip,int idActivity, String day, String arrivalTime);
    public boolean deleteTripContainsActivityById(int idTrip);
    public boolean updateTripContainsActivity(int idTrip, String day, String formatted, String arrivalTime, String newArrivalTime);
}
