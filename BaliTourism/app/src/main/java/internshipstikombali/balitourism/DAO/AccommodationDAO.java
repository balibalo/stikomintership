package internshipstikombali.balitourism.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import internshipstikombali.balitourism.business.Accommodation;
import internshipstikombali.balitourism.helper.DatabaseContentProvider;
import internshipstikombali.balitourism.helper.DatabaseSchema;

/**
 * Created by Pierre on 22/06/2016.
 */
public class AccommodationDAO extends DatabaseContentProvider implements AccommodationDAOInterface, DatabaseSchema {
    private Cursor cursor;
    private ContentValues initialValues;

    public AccommodationDAO(SQLiteDatabase db) {
        super(db);
    }

    private void setContentValue(Accommodation accommodation) {
        initialValues = new ContentValues();
        initialValues.put(KEY_ACCOMMODATION_IDACCOMMODATION, accommodation.getIdAccommodation());
        initialValues.put(KEY_ACCOMMODATION_NAME, accommodation.getName());
        initialValues.put(KEY_ACCOMMODATION_LONGITUDE, accommodation.getLongitude());
        initialValues.put(KEY_ACCOMMODATION_LATITUDE, accommodation.getLatitude());
        initialValues.put(KEY_ACCOMMODATION_PHONENUMBER, accommodation.getPhoneNumber());
        initialValues.put(KEY_ACCOMMODATION_WEBSITE, accommodation.getWebsite());
        initialValues.put(KEY_ACCOMMODATION_PRICE, accommodation.getPrice());
        initialValues.put(KEY_ACCOMMODATION_DESCRIPTION, accommodation.getDescription());
        initialValues.put(KEY_ACCOMMODATION_PICTURE, accommodation.getPicture());
        initialValues.put(KEY_ACCOMMODATION_NUMBEROFPLACES, accommodation.getNumberOfPlaces());
    }

    private ContentValues getContentValue() {
        return initialValues;
    }

    @Override
    public boolean addAccommodation(Accommodation accommodation) {
        setContentValue(accommodation);
        try {
            return super.insert(ACCOMMODATION_TABLE, getContentValue()) > 0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public void deleteAllAccommodations() {
        super.delete(ACCOMMODATION_TABLE, null, null);
    }

    @Override
    public boolean deleteAccommodation(int idAccommodation) {
        try {
            return super.delete(ACCOMMODATION_TABLE, "idAccommodation = ?", new String[]{String.valueOf(idAccommodation)}) >0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public boolean updateAccommodation(int idAcco, String name, String longitude, String latitude, String phone, String website, int price, String description, String url, int places){
        ContentValues cv = new ContentValues();
        cv.put("name",name);
        cv.put("longitude",longitude);
        cv.put("latitude",latitude);
        cv.put("phoneNumber",phone);
        cv.put("website",website);
        cv.put("price",price);
        cv.put("description",description);
        cv.put("picture",url);
        cv.put("numberOfPlaces",places);

        try {
            return super.update(ACCOMMODATION_TABLE,cv, "idAccommodation = ?",new String[]{String.valueOf(idAcco)}) >0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public List<Accommodation> fetchAllAccommodations() {
        List<Accommodation> accommodationList = new ArrayList<Accommodation>();
        cursor = super.query(ACCOMMODATION_TABLE, ACCOMMODATION_COLUMNS, null, null, KEY_ACCOMMODATION_PRICE);
        if(cursor != null) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                Accommodation accommodation = cursorToEntity(cursor);
                accommodationList.add(accommodation);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return accommodationList;
    }

    @Override
    public Accommodation fetchAccommodationById(int idAccommodation){
        final String selectionArgs[] = {String.valueOf(idAccommodation)};
        final String selection = KEY_ACCOMMODATION_IDACCOMMODATION + " = ?";
        Accommodation accommodation = new Accommodation();
        cursor = super.query(ACCOMMODATION_TABLE, ACCOMMODATION_COLUMNS, selection, selectionArgs, KEY_ACCOMMODATION_IDACCOMMODATION);
        if(cursor != null) {
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                accommodation = cursorToEntity(cursor);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return accommodation;
    }

    @Override
    protected Accommodation cursorToEntity(Cursor cursor) {
        Accommodation accommodation = new Accommodation();

        int idAccommodationIndex;
        int nameIndex;
        int longitudeIndex;
        int latitudeIndex;
        int phoneNumberIndex;
        int websiteIndex;
        int priceIndex;
        int descriptionIndex;
        int pictureIndex;
        int numberOfPlacesIndex;

        if(cursor != null) {
            if(cursor.getColumnIndex(KEY_ACCOMMODATION_IDACCOMMODATION) != -1) {
                idAccommodationIndex = cursor.getColumnIndexOrThrow(KEY_ACCOMMODATION_IDACCOMMODATION);
                accommodation.setIdAccommodation(cursor.getInt(idAccommodationIndex));
            }
            if(cursor.getColumnIndex(KEY_ACCOMMODATION_NAME) != -1) {
                nameIndex = cursor.getColumnIndexOrThrow(KEY_ACCOMMODATION_NAME);
                accommodation.setName(cursor.getString(nameIndex));
            }
            if(cursor.getColumnIndex(KEY_ACCOMMODATION_LONGITUDE) != -1) {
                longitudeIndex = cursor.getColumnIndexOrThrow(KEY_ACCOMMODATION_LONGITUDE);
                accommodation.setLongitude(cursor.getString(longitudeIndex));
            }
            if(cursor.getColumnIndex(KEY_ACCOMMODATION_LATITUDE) != -1) {
                latitudeIndex = cursor.getColumnIndexOrThrow(KEY_ACCOMMODATION_LATITUDE);
                accommodation.setLatitude(cursor.getString(latitudeIndex));
            }
            if(cursor.getColumnIndex(KEY_ACCOMMODATION_PHONENUMBER) != -1) {
                phoneNumberIndex = cursor.getColumnIndexOrThrow(KEY_ACCOMMODATION_PHONENUMBER);
                accommodation.setPhoneNumber(cursor.getString(phoneNumberIndex));
            }
            if(cursor.getColumnIndex(KEY_ACCOMMODATION_WEBSITE) != -1) {
                websiteIndex = cursor.getColumnIndexOrThrow(KEY_ACCOMMODATION_WEBSITE);
                accommodation.setWebsite(cursor.getString(websiteIndex));
            }
            if(cursor.getColumnIndex(KEY_ACCOMMODATION_PRICE) != -1) {
                priceIndex = cursor.getColumnIndexOrThrow(KEY_ACCOMMODATION_PRICE);
                accommodation.setPrice(cursor.getInt(priceIndex));
            }
            if(cursor.getColumnIndex(KEY_ACCOMMODATION_DESCRIPTION) != -1) {
                descriptionIndex = cursor.getColumnIndexOrThrow(KEY_ACCOMMODATION_DESCRIPTION);
                accommodation.setDescription(cursor.getString(descriptionIndex));
            }
            if(cursor.getColumnIndex(KEY_ACCOMMODATION_PICTURE) != -1) {
                pictureIndex = cursor.getColumnIndexOrThrow(KEY_ACCOMMODATION_PICTURE);
                accommodation.setPicture(cursor.getString(pictureIndex));
            }
            if(cursor.getColumnIndex(KEY_ACCOMMODATION_NUMBEROFPLACES) != -1) {
                numberOfPlacesIndex = cursor.getColumnIndexOrThrow(KEY_ACCOMMODATION_NUMBEROFPLACES);
                accommodation.setNumberOfPlaces(cursor.getInt(numberOfPlacesIndex));
            }
        }
        return accommodation;
    }
}
