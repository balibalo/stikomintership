package internshipstikombali.balitourism.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import internshipstikombali.balitourism.business.TripContainsAccommodation;
import internshipstikombali.balitourism.helper.DatabaseContentProvider;
import internshipstikombali.balitourism.helper.DatabaseSchema;

/**
 * Created by Pierre on 22/06/2016.
 */
public class TripContainsAccommodationDAO extends DatabaseContentProvider implements TripContainsAccommodationDAOInterface, DatabaseSchema {
    private Cursor cursor;
    private ContentValues initialValues;

    public TripContainsAccommodationDAO(SQLiteDatabase db) {
        super(db);
    }

    private void setContentValue(TripContainsAccommodation tripContainsAccommodation) {
        initialValues = new ContentValues();
        initialValues.put(KEY_TRIPCONTAINSACCOMMODATION_IDTRIP, tripContainsAccommodation.getIdTrip());
        initialValues.put(KEY_TRIPCONTAINSACCOMMODATION_IDACCOMMODATION, tripContainsAccommodation.getIdAccommodation());
        initialValues.put(KEY_TRIPCONTAINSACCOMMODATION_ARRIVALDATE, String.valueOf(tripContainsAccommodation.getArrivalDate()));
    }

    private ContentValues getContentValue() {
        return initialValues;
    }

    @Override
    public boolean addTripContainsAccommodation(TripContainsAccommodation tripContainsAccommodation) {
        setContentValue(tripContainsAccommodation);
        try {
            return super.insert(TRIPCONTAINSACCOMMODATION_TABLE, getContentValue()) > 0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public boolean deleteTripContainsAccommodation(int idTrip,int idAccommodation,String day) {
        try {
            return super.delete(TRIPCONTAINSACCOMMODATION_TABLE, "idTrip = ? and idAccommodation = ? and arrivalDate = ?", new String[]{String.valueOf(idTrip), String.valueOf(idAccommodation), day}) >0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public boolean deleteTripContainsAccommodationById(int idTrip) {
        try {
            return super.delete(TRIPCONTAINSACCOMMODATION_TABLE, "idTrip = ?", new String[]{String.valueOf(idTrip)}) >0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public boolean updateTripContainsAccommodation(int idTrip, String day, String formatted) {
        ContentValues cv = new ContentValues();
        cv.put("arrivalDate",formatted);
        try {
            return super.update(TRIPCONTAINSACCOMMODATION_TABLE,cv, "idTrip = ? and arrivalDate = ?",new String[]{String.valueOf(idTrip), day}) >0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public void deleteAllTripContainsAccommodation() {
        super.delete(TRIPCONTAINSACCOMMODATION_TABLE, null, null);
    }

    @Override
    public List<TripContainsAccommodation> fetchAllTripContainsAccommodation(){
        List<TripContainsAccommodation> resList = new ArrayList<TripContainsAccommodation>();
        cursor = super.query(TRIPCONTAINSACCOMMODATION_TABLE, TRIPCONTAINSACCOMMODATION_COLUMNS, null, null, null);
        if(cursor != null){
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                TripContainsAccommodation tripContainsAccommodation = cursorToEntity(cursor);
                resList.add(tripContainsAccommodation);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return resList;
    }

    @Override
    protected TripContainsAccommodation cursorToEntity(Cursor cursor) {
        TripContainsAccommodation tripContainsAccommodation = new TripContainsAccommodation();

        int idTripIndex;
        int idAccommodationIndex;
        int arrivalDateIndex;
        int numberOfNightsIndex;


        if(cursor != null) {
            if(cursor.getColumnIndex(KEY_TRIPCONTAINSACCOMMODATION_IDTRIP) != -1) {
                idTripIndex = cursor.getColumnIndexOrThrow(KEY_TRIPCONTAINSACCOMMODATION_IDTRIP);
                tripContainsAccommodation.setIdTrip(cursor.getInt(idTripIndex));
            }
            if(cursor.getColumnIndex(KEY_TRIPCONTAINSACCOMMODATION_IDACCOMMODATION) != -1) {
                idAccommodationIndex = cursor.getColumnIndexOrThrow(KEY_TRIPCONTAINSACCOMMODATION_IDACCOMMODATION);
                tripContainsAccommodation.setIdAccommodation(cursor.getInt(idAccommodationIndex));
            }
            if(cursor.getColumnIndex(KEY_TRIPCONTAINSACCOMMODATION_ARRIVALDATE) != -1) {
                arrivalDateIndex = cursor.getColumnIndexOrThrow(KEY_TRIPCONTAINSACCOMMODATION_ARRIVALDATE);
                tripContainsAccommodation.setArrivalDate(cursor.getString(arrivalDateIndex));
            }

        }
        return tripContainsAccommodation;
    }
}
