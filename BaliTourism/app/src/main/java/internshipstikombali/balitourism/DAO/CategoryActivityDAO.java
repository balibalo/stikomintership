package internshipstikombali.balitourism.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import internshipstikombali.balitourism.business.CategoryActivity;
import internshipstikombali.balitourism.helper.DatabaseContentProvider;
import internshipstikombali.balitourism.helper.DatabaseSchema;

/**
 * Created by Pierre on 22/06/2016.
 */
public class CategoryActivityDAO extends DatabaseContentProvider implements CategoryActivityDAOInterface, DatabaseSchema {

    private Cursor cursor;
    private ContentValues initialValues;

    public CategoryActivityDAO(SQLiteDatabase db) {
        super(db);
    }

    private void setContentValue(CategoryActivity categoryActivity) {
        initialValues = new ContentValues();
        initialValues.put(KEY_CATEGORYACTIVITY_NAMECATEGORYACTIVITY, categoryActivity.getNameCategoryActivity());
        initialValues.put(KEY_CATEGORYACTIVITY_DESCRIPTION, categoryActivity.getDescription());
    }

    private ContentValues getContentValue() {
        return initialValues;
    }

    @Override
    public boolean addCategoryActivity(CategoryActivity categoryActivity) {
        setContentValue(categoryActivity);
        try {
            return super.insert(CATEGORYACTIVITY_TABLE, getContentValue()) > 0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public void deleteAllCategoriesActivity() {
        super.delete(CATEGORYACTIVITY_TABLE, null, null);
    }

    @Override
    protected CategoryActivity cursorToEntity(Cursor cursor) {
        CategoryActivity categoryActivity = new CategoryActivity();

        int nameCategoryActivityIndex;
        int descriptionIndex;

        if(cursor != null) {
            if(cursor.getColumnIndex(KEY_CATEGORYACTIVITY_NAMECATEGORYACTIVITY) != -1) {
                nameCategoryActivityIndex = cursor.getColumnIndexOrThrow(KEY_CATEGORYACTIVITY_NAMECATEGORYACTIVITY);
                categoryActivity.setNameCategoryActivity(cursor.getString(nameCategoryActivityIndex));
            }
            if(cursor.getColumnIndex(KEY_CATEGORYACTIVITY_DESCRIPTION) != -1) {
                descriptionIndex = cursor.getColumnIndexOrThrow(KEY_CATEGORYACTIVITY_DESCRIPTION);
                categoryActivity.setDescription(cursor.getString(descriptionIndex));
            }
        }
        return categoryActivity;
    }
}
