package internshipstikombali.balitourism.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import internshipstikombali.balitourism.business.User;
import internshipstikombali.balitourism.helper.DatabaseContentProvider;
import internshipstikombali.balitourism.helper.DatabaseSchema;

/**
 * Created by Pierre on 21/06/2016.
 */
public class UserDAO extends DatabaseContentProvider implements UserDAOInterface, DatabaseSchema {

    private Cursor cursor;
    private ContentValues initialValues;

    public UserDAO(SQLiteDatabase db) {
        super(db);
    }

    private void setContentValue(User user) {
        initialValues = new ContentValues();
        initialValues.put(KEY_USER_IDUSER, user.getIdUser());
        initialValues.put(KEY_USER_IDTRIP, user.getIdTrip());
        initialValues.put(KEY_USER_USERNAME, user.getUsername());
        initialValues.put(KEY_USER_ENCRYPTEDPASSWORD, user.getEncryptedPassword());
        initialValues.put(KEY_USER_EMAILADDRESS, user.getEmailAddress());
        initialValues.put(KEY_USER_DRIVINGLICENSE, user.getDrivingLicense());
        initialValues.put(KEY_USER_TIMESPENTOUTSIDE, user.getTimeSpentOutside());
    }

    private ContentValues getContentValue() {
        return initialValues;
    }

    @Override
    public boolean addUser(User user) {
        setContentValue(user);
        try {
            return super.insert(USER_TABLE, getContentValue()) > 0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public boolean updateUser(int idUser, int time, int drivingLicense) {
        ContentValues data=new ContentValues();
        data.put("timeSpentOutside",time);
        data.put("drivingLicense",drivingLicense);
        try {
            return super.update(USER_TABLE, data, "idUser = ?", new String[]{String.valueOf(idUser)}) > 0;

        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public void deleteAllUsers() {
        super.delete(USER_TABLE, null, null);
    }

    @Override
    public User fetchUserById(int idUser){
        final String selectionArgs[] = {String.valueOf(idUser)};
        final String selection = KEY_USER_IDUSER + " = ?";
        User user = new User();
        cursor = super.query(USER_TABLE, USER_COLUMNS, selection, selectionArgs, KEY_USER_IDUSER);
        if(cursor != null) {
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                user = cursorToEntity(cursor);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return user;
    }


    @Override
    protected User cursorToEntity(Cursor cursor) {
        User user = new User();

        int idUserIndex;
        int idTripIndex;
        int usernameIndex;
        int encryptedPasswordIndex;
        int emailAddressIndex;
        int drivingLicenseIndex;
        int timeSpentOutsideIndex;

        if(cursor != null) {
            if(cursor.getColumnIndex(KEY_USER_IDUSER) != -1) {
                idUserIndex = cursor.getColumnIndexOrThrow(KEY_USER_IDUSER);
                user.setIdUser(cursor.getInt(idUserIndex));
            }
            if(cursor.getColumnIndex(KEY_USER_IDTRIP) != -1) {
                idTripIndex = cursor.getColumnIndexOrThrow(KEY_USER_IDTRIP);
                user.setIdTrip(cursor.getInt(idTripIndex));
            }
            if(cursor.getColumnIndex(KEY_USER_USERNAME) != -1) {
                usernameIndex = cursor.getColumnIndexOrThrow(KEY_USER_USERNAME);
                user.setUsername(cursor.getString(usernameIndex));
            }
            if(cursor.getColumnIndex(KEY_USER_ENCRYPTEDPASSWORD) != -1) {
                encryptedPasswordIndex = cursor.getColumnIndexOrThrow(KEY_USER_ENCRYPTEDPASSWORD);
                user.setEncryptedPassword(cursor.getString(encryptedPasswordIndex));
            }
            if(cursor.getColumnIndex(KEY_USER_EMAILADDRESS) != -1) {
                emailAddressIndex = cursor.getColumnIndexOrThrow(KEY_USER_EMAILADDRESS);
                user.setEmailAddress(cursor.getString(emailAddressIndex));
            }
            if(cursor.getColumnIndex(KEY_USER_DRIVINGLICENSE) != -1) {
                drivingLicenseIndex = cursor.getColumnIndexOrThrow(KEY_USER_DRIVINGLICENSE);
                user.setDrivingLicense(cursor.getInt(drivingLicenseIndex));
            }
            if(cursor.getColumnIndex(KEY_USER_TIMESPENTOUTSIDE) != -1) {
                timeSpentOutsideIndex = cursor.getColumnIndexOrThrow(KEY_USER_TIMESPENTOUTSIDE);
                user.setTimeSpentOutside(cursor.getInt(timeSpentOutsideIndex));
            }
        }
        return user;
    }
}