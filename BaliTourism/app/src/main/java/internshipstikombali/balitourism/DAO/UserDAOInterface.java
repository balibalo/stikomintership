package internshipstikombali.balitourism.DAO;

import internshipstikombali.balitourism.business.User;

/**
 * Created by Pierre on 21/06/2016.
 */
public interface UserDAOInterface {
    public boolean addUser(User user);
    public void deleteAllUsers();
    public User fetchUserById(int idUser);
    public boolean updateUser(int idUser, int time, int drivingLicense);
}
