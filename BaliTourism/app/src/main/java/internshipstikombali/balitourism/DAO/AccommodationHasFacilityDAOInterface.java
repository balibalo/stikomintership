package internshipstikombali.balitourism.DAO;

import java.util.ArrayList;
import java.util.List;

import internshipstikombali.balitourism.business.AccommodationHasFacility;

/**
 * Created by Pierre on 22/06/2016.
 */
public interface AccommodationHasFacilityDAOInterface {
    public boolean addAccommodationHasFacility(AccommodationHasFacility accommodationHasFacility);
    public void deleteAllAccommodationHasFacility();
    public List<AccommodationHasFacility> fetchAllAccommodationHasFacility();
        public ArrayList<String> fetchAllFacilitiesByIdAccommodation(int idAccommodation);
}
