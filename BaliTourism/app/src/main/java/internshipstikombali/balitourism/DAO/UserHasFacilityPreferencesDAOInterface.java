package internshipstikombali.balitourism.DAO;

import internshipstikombali.balitourism.business.UserHasFacilityPreferences;

/**
 * Created by Pierre on 22/06/2016.
 */
public interface UserHasFacilityPreferencesDAOInterface {
    public boolean addUserHasFacilityPreferences(UserHasFacilityPreferences userHasFacilityPreferences);
    public void deleteAllUserHasFacilityPreferences();
}
