package internshipstikombali.balitourism.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import internshipstikombali.balitourism.business.AccommodationBelongsCategoryAccommodation;
import internshipstikombali.balitourism.helper.DatabaseContentProvider;
import internshipstikombali.balitourism.helper.DatabaseSchema;

/**
 * Created by Pierre on 22/06/2016.
 */
public class AccommodationBelongsCategoryAccommodationDAO extends DatabaseContentProvider implements AccommodationBelongsCategoryAccommodationDAOInterface, DatabaseSchema {
    private Cursor cursor;
    private ContentValues initialValues;

    public AccommodationBelongsCategoryAccommodationDAO(SQLiteDatabase db) {
        super(db);
    }

    private void setContentValue(AccommodationBelongsCategoryAccommodation accommodationBelongsCategoryAccommodation) {
        initialValues = new ContentValues();
        initialValues.put(KEY_ACCOMMODATIONBELONGSCATEGORYACCOMMODATION_IDACCOMMODATION, accommodationBelongsCategoryAccommodation.getIdAccommodation());
        initialValues.put(KEY_ACCOMMODATIONBELONGSCATEGORYACCOMMODATION_NAMECATEGORYACCOMMODATION, accommodationBelongsCategoryAccommodation.getNameCategoryAccommodation());
    }

    private ContentValues getContentValue() {
        return initialValues;
    }

    @Override
    public boolean addAccommodationBelongsCategoryAccommodation(AccommodationBelongsCategoryAccommodation accommodationBelongsCategoryAccommodation) {
        setContentValue(accommodationBelongsCategoryAccommodation);
        try {
            return super.insert(ACCOMMODATIONBELONGSCATEGORYACCOMMODATION_TABLE, getContentValue()) > 0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public void deleteAllAccommodationBelongsCategoryAccommodation() {
        super.delete(ACCOMMODATIONBELONGSCATEGORYACCOMMODATION_TABLE, null, null);
    }

    @Override
    public String getCategoryAccommodationByIdAccommodation(int idAccommodation){
        final String selectionArgs[] = {String.valueOf(idAccommodation)};
        final String selection = KEY_ACCOMMODATIONBELONGSCATEGORYACCOMMODATION_IDACCOMMODATION + " = ?";
        AccommodationBelongsCategoryAccommodation catAccommodation = new AccommodationBelongsCategoryAccommodation();
        cursor = super.query(ACCOMMODATIONBELONGSCATEGORYACCOMMODATION_TABLE, ACCOMMODATIONBELONGSCATEGORYACCOMMODATION_COLUMNS, selection, selectionArgs, KEY_ACCOMMODATIONBELONGSCATEGORYACCOMMODATION_IDACCOMMODATION);
        if(cursor != null) {
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                catAccommodation = cursorToEntity(cursor);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return catAccommodation.getNameCategoryAccommodation();
    }

    @Override
    public boolean deleteAccommodationBelongsCategoryAccommodation(int idAccommodation) {
        try {
            return super.delete(ACCOMMODATIONBELONGSCATEGORYACCOMMODATION_TABLE, "idAccommodation = ?", new String[]{String.valueOf(idAccommodation)}) >0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public boolean updateAccommodationBelongsCategoryAccommodation(int idAccommodation, String category) {
        ContentValues cv = new ContentValues();
        cv.put("nameCategoryAccommodation",category);
        try {
            return super.update(ACCOMMODATIONBELONGSCATEGORYACCOMMODATION_TABLE,cv, "idAccommodation = ?",new String[]{String.valueOf(idAccommodation)}) >0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }


    @Override
    protected AccommodationBelongsCategoryAccommodation cursorToEntity(Cursor cursor) {
        AccommodationBelongsCategoryAccommodation accommodationBelongsCategoryAccommodation = new AccommodationBelongsCategoryAccommodation();

        int idAccommodationIndex;
        int nameCategoryAccommodationIndex;


        if(cursor != null) {
            if(cursor.getColumnIndex(KEY_ACCOMMODATIONBELONGSCATEGORYACCOMMODATION_IDACCOMMODATION) != -1) {
                idAccommodationIndex = cursor.getColumnIndexOrThrow(KEY_ACCOMMODATIONBELONGSCATEGORYACCOMMODATION_IDACCOMMODATION);
                accommodationBelongsCategoryAccommodation.setIdAccommodation(cursor.getInt(idAccommodationIndex));
            }
            if(cursor.getColumnIndex(KEY_ACCOMMODATIONBELONGSCATEGORYACCOMMODATION_NAMECATEGORYACCOMMODATION) != -1) {
                nameCategoryAccommodationIndex = cursor.getColumnIndexOrThrow(KEY_ACCOMMODATIONBELONGSCATEGORYACCOMMODATION_NAMECATEGORYACCOMMODATION);
                accommodationBelongsCategoryAccommodation.setNameCategoryAccommodation(cursor.getString(nameCategoryAccommodationIndex));
            }

        }
        return accommodationBelongsCategoryAccommodation;
    }
}
