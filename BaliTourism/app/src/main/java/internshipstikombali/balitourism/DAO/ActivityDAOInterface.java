package internshipstikombali.balitourism.DAO;

import java.util.List;

import internshipstikombali.balitourism.business.Activity;

/**
 * Created by Pierre on 22/06/2016.
 */
public interface ActivityDAOInterface {
    public boolean addActivity(Activity activity);
    public boolean deleteActivity(int idActivity);
    public boolean updateActivity(int idAct, String name, String longitude, String latitude, String phone, String website, int price, String description, String url, int duration, int oh, int om, int ch, int cm);
    public void deleteAllActivities();
    public List<Activity> fetchAllActivities();
    public Activity fetchActivityById(int idActivity);
}
