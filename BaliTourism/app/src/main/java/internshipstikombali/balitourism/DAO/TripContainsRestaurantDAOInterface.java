package internshipstikombali.balitourism.DAO;

import java.util.List;

import internshipstikombali.balitourism.business.TripContainsRestaurant;

/**
 * Created by Pierre on 22/06/2016.
 */
public interface TripContainsRestaurantDAOInterface {
    public boolean addTripContainsRestaurant(TripContainsRestaurant tripContainsRestaurant);
    public void deleteAllTripContainsRestaurant();
    public List<TripContainsRestaurant> fetchAllTripContainsRestaurant();
    public boolean deleteTripContainsRestaurant(int idTrip,int idRestaurant, String day, String arrivalTime);
    public boolean deleteTripContainsRestaurantById(int idTrip);
    public boolean updateTripContainsRestaurant(int idTrip, String day, String formatted, String arrivalTime, String newArrivalTime);

}
