package internshipstikombali.balitourism.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import internshipstikombali.balitourism.business.ActivityBelongsCategoryActivity;
import internshipstikombali.balitourism.helper.DatabaseContentProvider;
import internshipstikombali.balitourism.helper.DatabaseSchema;

/**
 * Created by Pierre on 22/06/2016.
 */
public class ActivityBelongsCategoryActivityDAO extends DatabaseContentProvider implements ActivityBelongsCategoryActivityDAOInterface, DatabaseSchema {
    private Cursor cursor;
    private ContentValues initialValues;

    public ActivityBelongsCategoryActivityDAO(SQLiteDatabase db) {
        super(db);
    }

    private void setContentValue(ActivityBelongsCategoryActivity activityBelongsCategoryActivity) {
        initialValues = new ContentValues();
        initialValues.put(KEY_ACTIVITYBELONGSCATEGORYACTIVITY_IDACTIVITY, activityBelongsCategoryActivity.getIdActivity());
        initialValues.put(KEY_ACTIVITYBELONGSCATEGORYACTIVITY_NAMECATEGORYACTIVITY, activityBelongsCategoryActivity.getNameCategoryActivity());
    }

    private ContentValues getContentValue() {
        return initialValues;
    }

    @Override
    public boolean addActivityBelongsCategoryActivity(ActivityBelongsCategoryActivity activityBelongsCategoryActivity) {
        setContentValue(activityBelongsCategoryActivity);
        try {
            return super.insert(ACTIVITYBELONGSCATEGORYACTIVITY_TABLE, getContentValue()) > 0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public String getCategoryActivityByIdActivity(int idActivity){
        final String selectionArgs[] = {String.valueOf(idActivity)};
        final String selection = KEY_ACTIVITYBELONGSCATEGORYACTIVITY_IDACTIVITY + " = ?";
        ActivityBelongsCategoryActivity catActivity = new ActivityBelongsCategoryActivity();
        cursor = super.query(ACTIVITYBELONGSCATEGORYACTIVITY_TABLE, ACTIVITYBELONGSCATEGORYACTIVITY_COLUMNS, selection, selectionArgs, KEY_ACTIVITYBELONGSCATEGORYACTIVITY_IDACTIVITY);
        if(cursor != null) {
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                catActivity = cursorToEntity(cursor);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return catActivity.getNameCategoryActivity();
    }

    @Override
    public void deleteAllActivityBelongsCategoryActivity() {
        super.delete(ACTIVITYBELONGSCATEGORYACTIVITY_TABLE, null, null);
    }

    @Override
    public boolean deleteActivityBelongsCategoryActivity(int idActivity) {
        try {
            return super.delete(ACTIVITYBELONGSCATEGORYACTIVITY_TABLE, "idActivity = ?", new String[]{String.valueOf(idActivity)}) >0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public List<ActivityBelongsCategoryActivity> fetchAllActivityBelongsCategoryActivity(){
        List<ActivityBelongsCategoryActivity> resList = new ArrayList<ActivityBelongsCategoryActivity>();
        cursor = super.query(ACTIVITYBELONGSCATEGORYACTIVITY_TABLE, ACTIVITYBELONGSCATEGORYACTIVITY_COLUMNS, null, null, null);
        if(cursor != null){
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                ActivityBelongsCategoryActivity cat = cursorToEntity(cursor);
                resList.add(cat);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return resList;
    }

    @Override
    public ArrayList<String> fetchAllCategoryActivityByIdActivity(int idActivity){
        ArrayList<String> nameCategoryActivityList = new ArrayList<String>();
        final String selectionArgs[] = {String.valueOf(idActivity)};
        final String selection = KEY_ACTIVITYBELONGSCATEGORYACTIVITY_IDACTIVITY + " = ?";
        cursor = super.query(ACTIVITYBELONGSCATEGORYACTIVITY_TABLE, ACTIVITYBELONGSCATEGORYACTIVITY_COLUMNS, selection, selectionArgs, KEY_ACTIVITYBELONGSCATEGORYACTIVITY_IDACTIVITY);
        if(cursor != null){
            cursor.moveToFirst();
            while (!cursor.isAfterLast()){
               ActivityBelongsCategoryActivity activityBelongsCategoryActivity = cursorToEntity(cursor);
                nameCategoryActivityList.add(activityBelongsCategoryActivity.getNameCategoryActivity());
                cursor.moveToNext();
            }
            cursor.close();
        }
        return nameCategoryActivityList;
        }


    @Override
    protected ActivityBelongsCategoryActivity cursorToEntity(Cursor cursor) {
        ActivityBelongsCategoryActivity activityBelongsCategoryActivity = new ActivityBelongsCategoryActivity();

        int idActivityIndex;
        int nameCategoryActivityIndex;


        if(cursor != null) {
            if(cursor.getColumnIndex(KEY_ACTIVITYBELONGSCATEGORYACTIVITY_IDACTIVITY) != -1) {
                idActivityIndex = cursor.getColumnIndexOrThrow(KEY_ACTIVITYBELONGSCATEGORYACTIVITY_IDACTIVITY);
                activityBelongsCategoryActivity.setIdActivity(cursor.getInt(idActivityIndex));
            }
            if(cursor.getColumnIndex(KEY_ACTIVITYBELONGSCATEGORYACTIVITY_NAMECATEGORYACTIVITY) != -1) {
                nameCategoryActivityIndex = cursor.getColumnIndexOrThrow(KEY_ACTIVITYBELONGSCATEGORYACTIVITY_NAMECATEGORYACTIVITY);
                activityBelongsCategoryActivity.setNameCategoryActivity(cursor.getString(nameCategoryActivityIndex));
            }

        }
        return activityBelongsCategoryActivity;
    }
}
