package internshipstikombali.balitourism.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import internshipstikombali.balitourism.business.UserHasCategoryAccommodationPreferences;
import internshipstikombali.balitourism.helper.DatabaseContentProvider;
import internshipstikombali.balitourism.helper.DatabaseSchema;

/**
 * Created by Pierre on 22/06/2016.
 */
public class UserHasCategoryAccommodationPreferencesDAO extends DatabaseContentProvider implements UserHasCategoryAccommodationPreferencesDAOInterface, DatabaseSchema {
    private Cursor cursor;
    private ContentValues initialValues;

    public UserHasCategoryAccommodationPreferencesDAO(SQLiteDatabase db) {
        super(db);
    }

    private void setContentValue(UserHasCategoryAccommodationPreferences userHasCategoryAccommodationPreferences) {
        initialValues = new ContentValues();
        initialValues.put(KEY_USERHASCATEGORYACCOMMODATIONPREFERENCES_IDUSER, userHasCategoryAccommodationPreferences.getIdUser());
        initialValues.put(KEY_USERHASCATEGORYACCOMMODATIONPREFERENCES_NAMECATEGORYACCOMMODATION, userHasCategoryAccommodationPreferences.getNameCategoryAccommodation());
    }

    private ContentValues getContentValue() {
        return initialValues;
    }

    @Override
    public boolean addUserHasCategoryAccommodationPreferences(UserHasCategoryAccommodationPreferences userHasCategoryAccommodationPreferences) {
        setContentValue(userHasCategoryAccommodationPreferences);
        try {
            return super.insert(USERHASCATEGORYACCOMMODATIONPREFERENCES_TABLE, getContentValue()) > 0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public void deleteAllUserHasCategoryAccommodationPreferences() {
        super.delete(USERHASCATEGORYACCOMMODATIONPREFERENCES_TABLE, null, null);
    }

    @Override
    protected UserHasCategoryAccommodationPreferences cursorToEntity(Cursor cursor) {
        UserHasCategoryAccommodationPreferences userHasCategoryAccommodationPreferences = new UserHasCategoryAccommodationPreferences();

        int idUserIndex;
        int nameCategoryAccommodationIndex;

        if(cursor != null) {
            if(cursor.getColumnIndex(KEY_USERHASCATEGORYACCOMMODATIONPREFERENCES_IDUSER) != -1) {
                idUserIndex = cursor.getColumnIndexOrThrow(KEY_USERHASCATEGORYACCOMMODATIONPREFERENCES_IDUSER);
                userHasCategoryAccommodationPreferences.setIdUser(cursor.getInt(idUserIndex));
            }
            if(cursor.getColumnIndex(KEY_USERHASCATEGORYACCOMMODATIONPREFERENCES_NAMECATEGORYACCOMMODATION) != -1) {
                nameCategoryAccommodationIndex = cursor.getColumnIndexOrThrow(KEY_USERHASCATEGORYACCOMMODATIONPREFERENCES_NAMECATEGORYACCOMMODATION);
                userHasCategoryAccommodationPreferences.setNameCategoryAccommodation(cursor.getString(nameCategoryAccommodationIndex));
            }

        }
        return userHasCategoryAccommodationPreferences;
    }
}
