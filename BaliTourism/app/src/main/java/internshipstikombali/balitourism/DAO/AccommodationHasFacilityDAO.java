package internshipstikombali.balitourism.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import internshipstikombali.balitourism.business.AccommodationHasFacility;
import internshipstikombali.balitourism.helper.DatabaseContentProvider;
import internshipstikombali.balitourism.helper.DatabaseSchema;

/**
 * Created by Pierre on 22/06/2016.
 */
public class AccommodationHasFacilityDAO extends DatabaseContentProvider implements AccommodationHasFacilityDAOInterface, DatabaseSchema {
    private Cursor cursor;
    private ContentValues initialValues;

    public AccommodationHasFacilityDAO(SQLiteDatabase db) {
        super(db);
    }

    private void setContentValue(AccommodationHasFacility accommodationHasFacility) {
        initialValues = new ContentValues();
        initialValues.put(KEY_ACCOMMODATIONHASFACILITY_IDACCOMMODATION, accommodationHasFacility.getIdAccommodation());
        initialValues.put(KEY_ACCOMMODATIONHASFACILITY_NAMEFACILITY, accommodationHasFacility.getNameFacility());
    }

    private ContentValues getContentValue() {
        return initialValues;
    }

    @Override
    public boolean addAccommodationHasFacility(AccommodationHasFacility accommodationHasFacility) {
        setContentValue(accommodationHasFacility);
        try {
            return super.insert(ACCOMMODATIONHASFACILITY_TABLE, getContentValue()) > 0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public void deleteAllAccommodationHasFacility() {
        super.delete(ACCOMMODATIONHASFACILITY_TABLE, null, null);
    }

    @Override
    public List<AccommodationHasFacility> fetchAllAccommodationHasFacility(){
        List<AccommodationHasFacility> resList = new ArrayList<AccommodationHasFacility>();
        cursor = super.query(ACCOMMODATIONHASFACILITY_TABLE, ACCOMMODATIONHASFACILITY_COLUMNS, null, null, null);
        if(cursor != null){
            cursor.moveToFirst();
            while(!cursor.isAfterLast()){
                AccommodationHasFacility facility = cursorToEntity(cursor);
                resList.add(facility);
                cursor.moveToNext();
            }
            cursor.close();
        }
        return resList;
    }

    @Override
    public ArrayList<String> fetchAllFacilitiesByIdAccommodation(int idAccommodation) {
        ArrayList<String> nameFacilityList = new ArrayList<String>();
        final String selectionArgs[] = {String.valueOf(idAccommodation)};
        final String selection = KEY_ACCOMMODATIONHASFACILITY_IDACCOMMODATION + " = ?";
        cursor = super.query(ACCOMMODATIONHASFACILITY_TABLE, ACCOMMODATIONHASFACILITY_COLUMNS, selection, selectionArgs, KEY_ACCOMMODATIONHASFACILITY_IDACCOMMODATION);
        if (cursor != null) {
            cursor.moveToFirst();
            if (cursor.moveToFirst()) {
                while (cursor.isAfterLast()) {
                    AccommodationHasFacility accommodationHasFacility = cursorToEntity(cursor);
                    nameFacilityList.add(accommodationHasFacility.getNameFacility());
                    cursor.moveToNext();
                }
                cursor.close();
            }
            return nameFacilityList;
        } else return null;
    }



    @Override
    protected AccommodationHasFacility cursorToEntity(Cursor cursor) {
        AccommodationHasFacility accommodationHasFacility = new AccommodationHasFacility();

        int idAccommodationIndex;
        int nameFacilityIndex;


        if(cursor != null) {
            if(cursor.getColumnIndex(KEY_ACCOMMODATIONHASFACILITY_IDACCOMMODATION) != -1) {
                idAccommodationIndex = cursor.getColumnIndexOrThrow(KEY_ACCOMMODATIONHASFACILITY_IDACCOMMODATION);
                accommodationHasFacility.setIdAccommodation(cursor.getInt(idAccommodationIndex));
            }
            if(cursor.getColumnIndex(KEY_ACCOMMODATIONHASFACILITY_NAMEFACILITY) != -1) {
                nameFacilityIndex = cursor.getColumnIndexOrThrow(KEY_ACCOMMODATIONHASFACILITY_NAMEFACILITY);
                accommodationHasFacility.setNameFacility(cursor.getString(nameFacilityIndex));
            }
        }
        return accommodationHasFacility;
    }
}
