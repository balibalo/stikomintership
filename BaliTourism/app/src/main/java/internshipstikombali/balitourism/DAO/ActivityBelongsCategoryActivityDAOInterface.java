package internshipstikombali.balitourism.DAO;

import java.util.ArrayList;
import java.util.List;

import internshipstikombali.balitourism.business.ActivityBelongsCategoryActivity;

/**
 * Created by Pierre on 22/06/2016.
 */
public interface ActivityBelongsCategoryActivityDAOInterface {
    public boolean addActivityBelongsCategoryActivity(ActivityBelongsCategoryActivity activityBelongsCategoryActivity);
    public void deleteAllActivityBelongsCategoryActivity();
    public String getCategoryActivityByIdActivity(int idActivity);
    public List<ActivityBelongsCategoryActivity> fetchAllActivityBelongsCategoryActivity();
    public boolean deleteActivityBelongsCategoryActivity(int idActivity);
    public ArrayList<String> fetchAllCategoryActivityByIdActivity(int idActivity);

}
