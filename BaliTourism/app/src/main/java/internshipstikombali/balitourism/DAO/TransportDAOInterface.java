package internshipstikombali.balitourism.DAO;

import java.util.List;

import internshipstikombali.balitourism.business.Transport;

/**
 * Created by Pierre on 22/06/2016.
 */
public interface TransportDAOInterface {
    public boolean addTransport(Transport transport);
    public boolean updateTransport(int idTransport, String name, String description, String url, int places,  int License, int price);
    public boolean deleteTransport(int idTransport);
    public void deleteAllTransports();
    public List<Transport> fetchAllTransports();
    public Transport fetchTransportById(int idTransport);
}
