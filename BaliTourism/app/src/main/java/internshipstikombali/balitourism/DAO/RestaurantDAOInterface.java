package internshipstikombali.balitourism.DAO;

import java.util.List;

import internshipstikombali.balitourism.business.Restaurant;

/**
 * Created by Pierre on 22/06/2016.
 */
public interface RestaurantDAOInterface {
    public boolean addRestaurant(Restaurant restaurant);
    public boolean deleteRestaurant(int idRestaurant);
    public boolean updateRestaurant(int idRest, String name, String longitude, String latitude, String phone, String website, int price, String description, String url, int oh, int om, int ch, int cm);
    public void deleteAllRestaurants();
    public List<Restaurant> fetchAllRestaurants();
    public Restaurant fetchRestaurantById(int idRestaurant);

}
