package internshipstikombali.balitourism.DAO;

import java.util.List;

import internshipstikombali.balitourism.business.Accommodation;

/**
 * Created by Pierre on 22/06/2016.
 */
public interface AccommodationDAOInterface {
    public boolean addAccommodation(Accommodation accommodation);
    public boolean updateAccommodation(int idAcco, String name, String longitude, String latitude, String phone, String website, int price, String description, String url, int places);
    public void deleteAllAccommodations();
    public boolean deleteAccommodation(int idAccommodation);
    public List<Accommodation> fetchAllAccommodations();
    public Accommodation fetchAccommodationById(int idAccommodation);
}
