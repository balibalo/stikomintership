package internshipstikombali.balitourism.DAO;

import java.util.List;

import internshipstikombali.balitourism.business.Trip;

/**
 * Created by Pierre on 22/06/2016.
 */
public interface TripDAOInterface {
    public boolean addTrip(Trip trip);
    public void deleteAllTrips();
    public boolean deleteTripById(int idTrip);
    public List<Trip> fetchAllTrip();
    public Trip fetchTripByIdUser(int idUser);
}
