package internshipstikombali.balitourism.DAO;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;

import internshipstikombali.balitourism.business.UserHasFacilityPreferences;
import internshipstikombali.balitourism.helper.DatabaseContentProvider;
import internshipstikombali.balitourism.helper.DatabaseSchema;

/**
 * Created by Pierre on 22/06/2016.
 */
public class UserHasFacilityPreferencesDAO extends DatabaseContentProvider implements UserHasFacilityPreferencesDAOInterface, DatabaseSchema {
    private Cursor cursor;
    private ContentValues initialValues;

    public UserHasFacilityPreferencesDAO(SQLiteDatabase db) {
        super(db);
    }

    private void setContentValue(UserHasFacilityPreferences userHasFacilityPreferences) {
        initialValues = new ContentValues();
        initialValues.put(KEY_USERHASFACILITYPREFERENCES_IDUSER, userHasFacilityPreferences.getIdUser());
        initialValues.put(KEY_USERHASFACILITYPREFERENCES_NAMEFACILITY, userHasFacilityPreferences.getNameFacility());
    }

    private ContentValues getContentValue() {
        return initialValues;
    }

    @Override
    public boolean addUserHasFacilityPreferences(UserHasFacilityPreferences userHasFacilityPreferences) {
        setContentValue(userHasFacilityPreferences);
        try {
            return super.insert(USERHASFACILITYPREFERENCES_TABLE, getContentValue()) > 0;
        }
        catch(SQLiteConstraintException ex) {
            return false;
        }
    }

    @Override
    public void deleteAllUserHasFacilityPreferences() {
        super.delete(USERHASFACILITYPREFERENCES_TABLE, null, null);
    }

    @Override
    protected UserHasFacilityPreferences cursorToEntity(Cursor cursor) {
        UserHasFacilityPreferences userHasFacilityPreferences = new UserHasFacilityPreferences();

        int idUserIndex;
        int nameFacilityIndex;

        if(cursor != null) {
            if(cursor.getColumnIndex(KEY_USERHASFACILITYPREFERENCES_IDUSER) != -1) {
                idUserIndex = cursor.getColumnIndexOrThrow(KEY_USERHASFACILITYPREFERENCES_IDUSER);
                userHasFacilityPreferences.setIdUser(cursor.getInt(idUserIndex));
            }
            if(cursor.getColumnIndex(KEY_USERHASFACILITYPREFERENCES_NAMEFACILITY) != -1) {
                nameFacilityIndex = cursor.getColumnIndexOrThrow(KEY_USERHASFACILITYPREFERENCES_NAMEFACILITY);
                userHasFacilityPreferences.setNameFacility(cursor.getString(nameFacilityIndex));
            }
        }
        return userHasFacilityPreferences;
    }
}
