package internshipstikombali.balitourism.app;

/**
 * Created by Pierre on 20/06/2016.
 */
public class AppConfig {
    // Login URL on server
    public static String URL_LOGIN = "http://stikomapi-gabartigues.rhcloud.com/login.php";

    // Register URL on server
    public static String URL_REGISTER = "http://stikomapi-gabartigues.rhcloud.com/register.php";

    // Newtrip URL on server
    public static String URL_NEWTRIP = "http://stikomapi-gabartigues.rhcloud.com/newtrip.php";

    // GeneticAlgorithm URL on server
    public static String URL_GENETICALGORITHM = "http://stikomapi-gabartigues.rhcloud.com/geneticalgorithm.php";

    // GeneticAlgorithm URL on server
    public static String URL_ACCOMMODATIONS = "http://stikomapi-gabartigues.rhcloud.com/accommodations.php";

    // GeneticAlgorithm URL on server
    public static String URL_ACTIVITIES = "http://stikomapi-gabartigues.rhcloud.com/activities.php";

    // GeneticAlgorithm URL on server
    public static String URL_RESTAURANTS = "http://stikomapi-gabartigues.rhcloud.com/restaurants.php";

    // GeneticAlgorithm URL on server
    public static String URL_TRANSPORTS = "http://stikomapi-gabartigues.rhcloud.com/transports.php";

    // GeneticAlgorithm URL on server
    public static String URL_ACTIVITYBCA = "http://stikomapi-gabartigues.rhcloud.com/activitybelongscategoryactivity.php";

    // GeneticAlgorithm URL on server
    public static String URL_ACCOMMODATIONBCA = "http://stikomapi-gabartigues.rhcloud.com/accommodationbelongscategoryaccommodation.php";

    // GeneticAlgorithm URL on server
    public static String URL_ACCOMMODATIONHASFACILITY = "http://stikomapi-gabartigues.rhcloud.com/accommodationhasfacility.php";

    public static String URL_BESTTRIPACCOMMODATION = "http://stikomapi-gabartigues.rhcloud.com/besttripaccommodation.php";

    public static String URL_BESTTRIPACTIVITY = "http://stikomapi-gabartigues.rhcloud.com/besttripactivity.php";

    public static String URL_BESTTRIPRESTAURANT = "http://stikomapi-gabartigues.rhcloud.com/besttriprestaurant.php";

    public static String URL_BESTTRIPTRANSPORT = "http://stikomapi-gabartigues.rhcloud.com/besttriptransport.php";

    public static String URL_DELETEACCOMMODATION = "http://stikomapi-gabartigues.rhcloud.com/deleteaccommodation.php";

    public static String URL_UPDATEACCOMMODATION = "http://stikomapi-gabartigues.rhcloud.com/updateaccommodation.php";

    public static String URL_ADDACCOMMODATION = "http://stikomapi-gabartigues.rhcloud.com/addaccommodation.php";

    public static String URL_CHECKTRIP = "http://stikomapi-gabartigues.rhcloud.com/checktrip.php";

    public static String URL_DELETETRIP = "http://stikomapi-gabartigues.rhcloud.com/deletetrip.php";

    public static String URL_TRIPCONTAINSACCOMMODATION = "http://stikomapi-gabartigues.rhcloud.com/tripcontainsaccommodation.php";

    public static String URL_USER = "http://stikomapi-gabartigues.rhcloud.com/user.php";

    public static String URL_TRIP = "http://stikomapi-gabartigues.rhcloud.com/trip.php";

    public static String URL_TRIPBYIDUSER = "http://stikomapi-gabartigues.rhcloud.com/tripbyiduser.php";

    public static String URL_DELETETRANSPORT = "http://stikomapi-gabartigues.rhcloud.com/deletetransport.php";

    public static String URL_UPDATETRANSPORT = "http://stikomapi-gabartigues.rhcloud.com/updatetransport.php";

    public static String URL_DELETEACTIVITY = "http://stikomapi-gabartigues.rhcloud.com/deleteactivity.php";

    public static String URL_UPDATEACTIVITY = "http://stikomapi-gabartigues.rhcloud.com/updateactivity.php";

    public static String URL_DELETERESTAURANT = "http://stikomapi-gabartigues.rhcloud.com/deleterestaurant.php";

    public static String URL_UPDATERESTAURANT = "http://stikomapi-gabartigues.rhcloud.com/updaterestaurant.php";

    public static String URL_ACCOBYTRIPDATE = "http://stikomapi-gabartigues.rhcloud.com/accommodationbyidtripdate.php";

    public static String URL_TRIPCONTAINSACTIVITY = "http://stikomapi-gabartigues.rhcloud.com/tripcontainsactivity.php";

    public static String URL_TRIPCONTAINSRESTAURANT = "http://stikomapi-gabartigues.rhcloud.com/tripcontainsrestaurant.php";

    public static String URL_TRIPCONTAINSTRANSPORT = "http://stikomapi-gabartigues.rhcloud.com/tripcontainstransport.php";

    public static String URL_ADDACTIVITY = "http://stikomapi-gabartigues.rhcloud.com/addactivity.php";

    public static String URL_ADDTRANSPORT = "http://stikomapi-gabartigues.rhcloud.com/addtransport.php";

    public static String URL_ADDRESTAURANT = "http://stikomapi-gabartigues.rhcloud.com/addrestaurant.php";

    public static String URL_DELETEACCOMMODATIONADMIN = "http://stikomapi-gabartigues.rhcloud.com/deleteaccommodationadmin.php";

    public static String URL_DELETEACTIVITYADMIN = "http://stikomapi-gabartigues.rhcloud.com/deleteactivityadmin.php";

    public static String URL_DELETERESTAURANTADMIN = "http://stikomapi-gabartigues.rhcloud.com/deleterestaurantadmin.php";

    public static String URL_DELETETRANSPORTADMIN = "http://stikomapi-gabartigues.rhcloud.com/deletetransportadmin.php";

    public static String URL_ADDACCOMMODATIONADMIN = "http://stikomapi-gabartigues.rhcloud.com/addaccommodationadmin.php";

    public static String URL_ADDTRANSPORTADMIN = "http://stikomapi-gabartigues.rhcloud.com/addtransportadmin.php";

    public static String URL_ADDACTIVITYADMIN = "http://stikomapi-gabartigues.rhcloud.com/addactivityadmin.php";

    public static String URL_ADDRESTAURANTADMIN = "http://stikomapi-gabartigues.rhcloud.com/addrestaurantadmin.php";

    public static String URL_ADDACCOMMODATIONBELONGSCATEGORY = "http://stikomapi-gabartigues.rhcloud.com/addaccommodationbelongscategory.php";

    public static String URL_ADDACTIVITYBELONGSCATEGORY = "http://stikomapi-gabartigues.rhcloud.com/addactivitybelongscategory.php";

    public static String URL_UPDATEACCOMMODATIONADMIN = "http://stikomapi-gabartigues.rhcloud.com/updateaccommodationadmin.php";

    public static String URL_DELETEACCOMMODATIONBELONGSCATEGORY = "http://stikomapi-gabartigues.rhcloud.com/deleteaccommodationbelongscategory.php";

    public static String URL_UPDATETRANSPORTADMIN = "http://stikomapi-gabartigues.rhcloud.com/updatetransportadmin.php";

    public static String URL_UPDATEACTIVITYADMIN = "http://stikomapi-gabartigues.rhcloud.com/updateactivityadmin.php";

    public static String URL_DELETEACTIVITYBELONGSCATEGORY = "http://stikomapi-gabartigues.rhcloud.com/deleteactivitybelongscategory.php";

    public static String URL_UPDATERESTAURANTADMIN = "http://stikomapi-gabartigues.rhcloud.com/updaterestaurantadmin.php";

    public static String URL_ADDADMIN = "http://stikomapi-gabartigues.rhcloud.com/addadmin.php";

    public static String URL_LOGINADMIN = "http://stikomapi-gabartigues.rhcloud.com/loginadmin.php";

}
