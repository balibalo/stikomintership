package internshipstikombali.balitourism.app;

import android.app.Application;
import android.media.MediaPlayer;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.sql.SQLException;

import internshipstikombali.balitourism.R;
import internshipstikombali.balitourism.helper.Database;

/**
 * Created by Pierre on 20/06/2016.
 */
public class AppController extends Application {
    public static final String TAG = AppController.class.getSimpleName();

    private static AppController myAppController;

    private RequestQueue mRequestQueue;

    public static Database myDatabase;

    private MediaPlayer player;

    public MediaPlayer getPlayer() {
        return player;
    }

    public void setPlayer(MediaPlayer player) {
        this.player = player;
    }

    public void onStart() {
        player.start();
    }

    public void onStop() {
        player.stop();
    }

    public void onPause() {
        player.pause();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.player = MediaPlayer.create(this, R.raw.song);
        this.player.setLooping(true); // Set looping
        this.player.setVolume(100,100);
        myAppController = this;
        myDatabase = new Database(this);
        try {
            myDatabase.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTerminate() {
        myDatabase.close();
        super.onTerminate();
    }

    public static synchronized AppController getInstance() {
        return myAppController;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
