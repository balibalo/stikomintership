<?php 

/**
 * This class contains all functions used for the activity table
 * @author Gabriel ARTIGUES
 */
class DB_TripContainsAccommodationFunctions {
	
	private $conn;
	
	// Constructor
	function __construct() {
		require_once 'DB_Connect.php';
		// Creating connection to database
		$db = new DB_Connect();
		$this->conn = $db->connect();
	}
	
	// Destructor
	function __destruct() {}
	
	// ACCOMMODATIONBELONGSCATEGORYACCOMMODATION FUNCTIONS


	public function addTripContainsAccommodation($idTrip, $idAccommodation, $arrivalDate){
		$stmt = $this->conn->prepare("INSERT INTO tripContainsAccommodation(idTrip, idAccommodation,  arrivalDate) VALUES (:idTrip, :idAccommodation, :arrivalDate);");
		$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
		$stmt->bindValue(':idAccommodation', $idAccommodation, PDO::PARAM_INT);
		$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
		
		$result = $stmt->execute();
		$stmt->closeCursor();
		
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM tripContainsAccommodation WHERE idTrip = :idTrip AND idAccommodation = :idAccommodation AND arrivalDate = :arrivalDate");
			$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
			$stmt->bindValue(':idAccommodation', $idAccommodation, PDO::PARAM_INT);
			$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
			$stmt->execute();
			$trip = $stmt->fetch();
			$stmt->closeCursor();
			return $trip;
		}
		else {
			return false;
		}
	}

	/**
	 * Gets all activities in the database
	 * @return all the activities stored in the database if there are some, false otherwise
	 */
	public function fetchAllTripContainsAccommodation() {
			$stmt = $this->conn->prepare("SELECT * from tripContainsAccommodation");
			if($stmt->execute()){
				$result = $stmt->fetchAll();
				$stmt->closeCursor();
				return $result;
			}
			else{
				return null;
			}	
	}
	
	/**
	 * Checks if the table constructionSite is empty
	 * @return true if empty, false otherwise
	 */
	public function isTripContainsAccommodationEmpty(){
		$stmt = $this->conn->prepare("SELECT COUNT(idAccommodation) as NbAccommodations from tripContainsAccommodation");

		$stmt->execute();
		
		$result = $stmt->fetch();
		$stmt->closeCursor();
		
		return ($result["NbAccommodations"] <= 0);
	}

	public function deleteTripContainsAccommodation($idTrip, $idAccommodation, $arrivalDate){
		$stmt = $this->conn->prepare("DELETE FROM tripContainsAccommodation WHERE idTrip = :idTrip AND idAccommodation = :idAccommodation AND arrivalDate = :arrivalDate");
		$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
		$stmt->bindValue(':idAccommodation', $idAccommodation, PDO::PARAM_INT);
		$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
		
		$result = $stmt->execute();
		$stmt->closeCursor();
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM tripContainsAccommodation WHERE idTrip = :idTrip AND idAccommodation = :idAccommodation AND arrivalDate = :arrivalDate");
			$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
			$stmt->bindValue(':idAccommodation', $idAccommodation, PDO::PARAM_INT);
			$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
			$stmt->execute();
			$trip = $stmt->fetch();
			$stmt->closeCursor();
			return $trip;
		}
		else {
			return false;
		}
	}

	public function updateTripContainsAccommodation($idTrip, $arrivalDate, $newDate){
		$stmt = $this->conn->prepare("UPDATE `tripContainsAccommodation` SET `arrivalDate`=:newDate WHERE idTrip = :idTrip AND arrivalDate = :arrivalDate");
		$stmt->bindValue(':newDate', $newDate, PDO::PARAM_STR);
		$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
		$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);

		$result = $stmt->execute();
		$stmt->closeCursor();
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM tripContainsAccommodation WHERE idTrip = :idTrip AND arrivalDate = :newDate");
			$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
			$stmt->bindValue(':newDate', $newDate, PDO::PARAM_STR);
			$stmt->execute();
			$trip = $stmt->fetch();
			$stmt->closeCursor();
			return $trip;
		}
		else {
			return false;
		}
	}

	public function getTripContainsAccoByIdTripDate($idTrip, $arrivalDate){

        $stmt = $this->conn->prepare("SELECT idAccommodation from tripContainsAccommodation WHERE idTrip = :idTrip AND arrivalDate = :arrivalDate");
        $stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
		$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
        if($stmt->execute()){
            $result = $stmt->fetch();
            $stmt->closeCursor();
            return $result;
        }
        else{
            return false;
        }
    }

}
?>
	