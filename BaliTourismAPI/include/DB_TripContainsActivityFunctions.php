<?php 

/**
 * This class contains all functions used for the activity table
 * @author Gabriel ARTIGUES
 */
class DB_TripContainsActivityFunctions {
	
	private $conn;
	
	// Constructor
	function __construct() {
		require_once 'DB_Connect.php';
		// Creating connection to database
		$db = new DB_Connect();
		$this->conn = $db->connect();
	}
	
	// Destructor
	function __destruct() {}
	
	// ACCOMMODATIONBELONGSCATEGORYACCOMMODATION FUNCTIONS


	public function addTripContainsActivity($idTrip, $idActivity, $arrivalDate, $arrivalTime){
		$stmt = $this->conn->prepare("INSERT INTO tripContainsActivity(idTrip, idActivity, arrivalDate, arrivalTime) VALUES (:idTrip, :idActivity, :arrivalDate, :arrivalTime);");
		$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
		$stmt->bindValue(':idActivity', $idActivity, PDO::PARAM_INT);
		$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
		$stmt->bindValue(':arrivalTime', $arrivalTime, PDO::PARAM_STR);
		
		$result = $stmt->execute();
		$stmt->closeCursor();
		
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM tripContainsActivity WHERE idTrip = :idTrip AND idActivity = :idActivity AND arrivalDate = :arrivalDate AND :arrivalTime = :arrivalTime");
			$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
			$stmt->bindValue(':idActivity', $idActivity, PDO::PARAM_INT);
			$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
			$stmt->bindValue(':arrivalTime', $arrivalTime, PDO::PARAM_STR);
			$stmt->execute();
			$trip = $stmt->fetch();
			$stmt->closeCursor();
			return $trip;
		}
		else {
			return false;
		}
	}

	/**
	 * Gets all activities in the database
	 * @return all the activities stored in the database if there are some, false otherwise
	 */
	public function fetchAllTripContainsActivity() {
			$stmt = $this->conn->prepare("SELECT * from tripContainsActivity");
			if($stmt->execute()){
				$result = $stmt->fetchAll();
				$stmt->closeCursor();
				return $result;
			}
			else{
				return null;
			}	
	}
	
	/**
	 * Checks if the table constructionSite is empty
	 * @return true if empty, false otherwise
	 */
	public function isTripContainsActivityEmpty(){
		$stmt = $this->conn->prepare("SELECT COUNT(idActivity) as NbActivities from tripContainsActivity");

		$stmt->execute();
		
		$result = $stmt->fetch();
		$stmt->closeCursor();
		
		return ($result["NbActivities"] <= 0);
	}

	public function deleteTripContainsActivity($idTrip, $idActivity, $arrivalDate, $arrivalTime){
		$stmt = $this->conn->prepare("DELETE FROM tripContainsActivity WHERE idTrip = :idTrip AND idActivity = :idActivity AND arrivalDate = :arrivalDate AND arrivalTime = :arrivalTime");
		$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
		$stmt->bindValue(':idActivity', $idActivity, PDO::PARAM_INT);
		$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
		$stmt->bindValue(':arrivalTime', $arrivalTime, PDO::PARAM_STR);
		
		$result = $stmt->execute();
		$stmt->closeCursor();
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM tripContainsActivity WHERE idTrip = :idTrip AND idActivity = :idActivity AND arrivalDate = :arrivalDate AND arrivalTime = :arrivalTime");
			$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
			$stmt->bindValue(':idActivity', $idActivity, PDO::PARAM_INT);
			$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
			$stmt->bindValue(':arrivalTime', $arrivalTime, PDO::PARAM_STR);
			$stmt->execute();
			$trip = $stmt->fetch();
			$stmt->closeCursor();
			return $trip;
		}
		else {
			return false;
		}
	}

	public function updateTripContainsActivity($idTrip, $arrivalDate, $newDate, $arrivalTime, $newTime){
		$stmt = $this->conn->prepare("UPDATE tripContainsActivity SET arrivalDate =:newDate, arrivalTime = :newTime WHERE idTrip = :idTrip AND arrivalDate = :arrivalDate AND arrivalTime = :arrivalTime");
		$stmt->bindValue(':newDate', $newDate, PDO::PARAM_STR);
		$stmt->bindValue(':newTime', $newTime, PDO::PARAM_STR);
		$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
		$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
		$stmt->bindValue(':arrivalTime', $arrivalTime, PDO::PARAM_STR);
		$result = $stmt->execute();
		$stmt->closeCursor();
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM tripContainsAccommodation WHERE idTrip = :idTrip AND arrivalDate = :newDate AND arrivalTime = :newTime");
			$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
			$stmt->bindValue(':newDate', $newDate, PDO::PARAM_STR);
			$stmt->bindValue(':newTime', $newTime, PDO::PARAM_STR);
			$stmt->execute();
			$trip = $stmt->fetch();
			$stmt->closeCursor();
			return $trip;
		}
		else {
			return false;
		}
	}
}
?>