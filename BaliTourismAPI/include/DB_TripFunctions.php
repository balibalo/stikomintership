<?php 

/**
 * This class contains all functions used for the trip table
 * @author Gabriel ARTIGUES
 */
class DB_TripFunctions {
	
	private $conn;
	
	// Constructor
	function __construct() {
		require_once 'DB_Connect.php';
		// Creating connection to database
		$db = new DB_Connect();
		$this->conn = $db->connect();
	}
	
	// Destructor
	function __destruct() {}

	// TRIP FUNCTIONS
	/**
	 * Stores a trip in the database
     * @return the created trip if successful, false otherwise
	 */
	public function storeTrip($idUser, $arrivalDate, $duration, $numberOfTravellers, $budget){
		$stmt = $this->conn->prepare("INSERT INTO trip(idUser, arrivalDate, duration, numberOfTravellers, budget) VALUES (:idUser, :arrivalDate, :duration, :numberOfTravellers, :budget);");
		$stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
		$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
		$stmt->bindValue(':duration', $duration, PDO::PARAM_INT);
		$stmt->bindValue(':numberOfTravellers', $numberOfTravellers, PDO::PARAM_INT);
		$stmt->bindValue(':budget', $budget, PDO::PARAM_INT);
		
		$result = $stmt->execute();
		$stmt->closeCursor();
		
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM trip WHERE idUser = :idUser AND arrivalDate = :arrivalDate AND duration = :duration AND numberOfTravellers = :numberOfTravellers AND budget = :budget");
			$stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
			$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
			$stmt->bindValue(':duration', $duration, PDO::PARAM_INT);
			$stmt->bindValue(':numberOfTravellers', $numberOfTravellers, PDO::PARAM_INT);
			$stmt->bindValue(':budget', $budget, PDO::PARAM_INT);
			$stmt->execute();
			$trip = $stmt->fetch();
			$stmt->closeCursor();
			return $trip;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Gets a trip by its id
	 * @return the trip stored in the database if there is one, false otherwise
	 */
	public function fetchTripById($idTrip) {
		$stmt = $this->conn->prepare("SELECT * from trip WHERE idTrip = :idTrip");
		$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
		if($stmt->execute()){
			$result = $stmt->fetch();
			$stmt->closeCursor();
			return $result;
		}
		else{
			return false;
		}
	}

	/**
	 * Gets a trip by its idUser
	 * @return the trip stored in the database if there is one, false otherwise
	 */
	public function fetchTripByUserId($idUser) {
		$stmt = $this->conn->prepare("SELECT * from trip WHERE idUser = :idUser");
		$stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
		if($stmt->execute()){
			$result = $stmt->fetch();
			$stmt->closeCursor();
			return $result;
		}
		else{
			return false;
		}
	}


	public function fetchAllTrips() {
			$stmt = $this->conn->prepare("SELECT * from trip");
			if($stmt->execute()){
				$result = $stmt->fetchAll();
				$stmt->closeCursor();
				return $result;
			}
			else{
				return null;
			}	
	}


	
	/**
	 * Checks if the table constructionSite is empty
	 * @return true if empty, false otherwise
	 */
	public function isTripEmpty(){
		$stmt = $this->conn->prepare("SELECT COUNT(idTrip) as NbTrip from trip");

		$stmt->execute();
		
		$result = $stmt->fetch();
		$stmt->closeCursor();
		
		return ($result["NbTrip"] <= 0);
	}


	public function deleteTrip($idTrip){
		$stmt = $this->conn->prepare("DELETE FROM trip WHERE idTrip = :idTrip");
		$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);		
		$result = $stmt->execute();
		$stmt->closeCursor();
		if($result) {
			return false;
		}
		else {
			$stmt = $this->conn->prepare("SELECT * FROM trip WHERE idTrip = :idTrip");
			$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
			$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
			$stmt->execute();
			$trip = $stmt->fetch();
			$stmt->closeCursor();
			return $trip;
		}
	}
}
?>