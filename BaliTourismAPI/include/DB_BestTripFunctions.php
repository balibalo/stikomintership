<?php 


/**
 * This class contains all functions used for the trip table
 * @author Gabriel ARTIGUES
 */
class DB_BestTripFunctions {
	
	private $conn;
	
	// Constructor
	function __construct() {
		require_once 'DB_Connect.php';
		// Creating connection to database
		$db = new DB_Connect();
		$this->conn = $db->connect();
	}
	
	// Destructor
	function __destruct() {}

	// TRIP FUNCTIONS
	/**
	 * Stores a trip in the database
     * @return the created trip if successful, false otherwise
	 */
	public function storeTripContainsAccommodation($idUser, $idAccommodation, $arrivalDate){
		$stmt = $this->conn->prepare("INSERT INTO tripContainsAccommodation (idTrip, idAccommodation, arrivalDate) VALUES ((SELECT idTrip FROM user WHERE idUser = :idUser), :idAccommodation, :arrivalDate);");
		$stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
		$stmt->bindValue(':idAccommodation', $idAccommodation, PDO::PARAM_INT);
		$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
		
		$result = $stmt->execute();
		$stmt->closeCursor();
		
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM tripContainsAccommodation WHERE idTrip = (SELECT idTrip FROM user WHERE idUser = :idUser) AND idAccommodation = :idAccommodation AND arrivalDate = :arrivalDate");
			$stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
			$stmt->bindValue(':idAccommodation', $idAccommodation, PDO::PARAM_INT);
			$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
			$stmt->execute();
			$tripContainsAccommodation = $stmt->fetch();
			$stmt->closeCursor();
			return $tripContainsAccommodation;
		}
		else {
			return false;
		}
	}

	public function storeTripContainsActivity($idUser, $idActivity, $arrivalDate, $arrivalTime){
		$stmt = $this->conn->prepare("INSERT INTO tripContainsActivity (idTrip, idActivity, arrivalDate, arrivalTime) VALUES ((SELECT idTrip FROM user WHERE idUser = :idUser), :idActivity, :arrivalDate, :arrivalTime);");
		$stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
		$stmt->bindValue(':idActivity', $idActivity, PDO::PARAM_INT);
		$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
		$stmt->bindValue(':arrivalTime', $arrivalTime, PDO::PARAM_STR);

		
		$result = $stmt->execute();
		$stmt->closeCursor();
		
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM tripContainsActivity WHERE idTrip = (SELECT idTrip FROM user WHERE idUser = :idUser) AND idActivity = :idActivity AND arrivalDate = :arrivalDate AND arrivalTime = :arrivalTime");
			$stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
			$stmt->bindValue(':idActivity', $idActivity, PDO::PARAM_INT);
			$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
			$stmt->bindValue(':arrivalTime', $arrivalTime, PDO::PARAM_STR);

			$stmt->execute();
			$tripContainsActivity = $stmt->fetch();
			$stmt->closeCursor();
			return $tripContainsActivity;
		}
		else {
			return false;
		}
	}

	public function storeTripContainsRestaurant($idUser, $idRestaurant, $arrivalDate, $arrivalTime){
		$stmt = $this->conn->prepare("INSERT INTO tripContainsRestaurant (idTrip, idRestaurant, arrivalDate, arrivalTime) VALUES ((SELECT idTrip FROM user WHERE idUser = :idUser), :idRestaurant, :arrivalDate, :arrivalTime);");
		$stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
		$stmt->bindValue(':idRestaurant', $idRestaurant, PDO::PARAM_INT);
		$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
		$stmt->bindValue(':arrivalTime', $arrivalTime, PDO::PARAM_STR);

		$result = $stmt->execute();
		$stmt->closeCursor();
		
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM tripContainsRestaurant WHERE idTrip = (SELECT idTrip FROM user WHERE idUser = :idUser) AND idRestaurant = :idRestaurant AND arrivalDate = :arrivalDate AND arrivalTime = :arrivalTime");
			$stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
			$stmt->bindValue(':idRestaurant', $idRestaurant, PDO::PARAM_INT);
			$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
			$stmt->bindValue(':arrivalTime', $arrivalTime, PDO::PARAM_STR);

			$stmt->execute();
			$tripContainsRestaurant = $stmt->fetch();
			$stmt->closeCursor();
			return $tripContainsRestaurant;
		}
		else {
			return false;
		}
	}

	public function storeTripContainsTransport($idUser, $idTransport, $dateTransport){
		$stmt = $this->conn->prepare("INSERT INTO tripContainsTransport (idTrip, idTransport, dateTransport) VALUES ((SELECT idTrip FROM user WHERE idUser = :idUser), :idTransport, :dateTransport);");
		$stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
		$stmt->bindValue(':idTransport', $idTransport, PDO::PARAM_INT);
		$stmt->bindValue(':dateTransport', $dateTransport, PDO::PARAM_STR);

		$result = $stmt->execute();
		$stmt->closeCursor();
		
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM tripContainsTransport WHERE idTrip = (SELECT idTrip FROM user WHERE idUser = :idUser) AND idTransport = :idTransport AND dateTransport = :dateTransport");
			$stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
			$stmt->bindValue(':idTransport', $idTransport, PDO::PARAM_INT);
			$stmt->bindValue(':dateTransport', $dateTransport, PDO::PARAM_STR);

			$stmt->execute();
			$tripContainsTransport = $stmt->fetch();
			$stmt->closeCursor();
			return $tripContainsTransport;
		}
		else {
			return false;
		}
	}
}
?>