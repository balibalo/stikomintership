<?php
 
/**
 * Database config variables
 */
define("DB_HOST", getenv('OPENSHIFT_MYSQL_DB_HOST'));
define("DB_USER", getenv('OPENSHIFT_MYSQL_DB_USERNAME'));
define("DB_PASSWORD", getenv('OPENSHIFT_MYSQL_DB_PASSWORD'));
define("DB_DATABASE", getenv('OPENSHIFT_GEAR_NAME'));
?>