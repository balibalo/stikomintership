<?php 

/**
 * This class contains all functions used for the Accommodation table
 * @author Gabriel ARTIGUES
 */
class DB_AccommodationFunctions {
	
	private $conn;
	
	// Constructor
	function __construct() {
		require_once 'DB_Connect.php';
		// Creating connection to database
		$db = new DB_Connect();
		$this->conn = $db->connect();
	}
	
	// Destructor
	function __destruct() {}
	
	// ACCOMMODATION FUNCTIONS
	

	/**
	 * Gets all accommodations in the database
	 * @return all the accommodations stored in the database if there are some, false otherwise
	 */
	public function fetchAllAccommodations() {
			$stmt = $this->conn->prepare("SELECT * from accommodation");
			if($stmt->execute()){
				$result = $stmt->fetchAll();
				$stmt->closeCursor();
				return $result;
			}
			else{
				return null;
			}	
	}

	/**
	 * Checks if the table accommodation is empty
	 * @return true if empty, false otherwise
	 */
	public function isAccommodationEmpty(){
		$stmt = $this->conn->prepare("SELECT COUNT(idAccommodation) as NbAccommodations from accommodation");

		$stmt->execute();
		
		$result = $stmt->fetch();
		$stmt->closeCursor();
		
		return ($result["NbAccommodations"] <= 0);
	}

	/**
	 * Gets an accommodation by its id
	 * @return all the accommodations stored in the database if there are some, false otherwise
	 */
	public function fetchAccommodationById($idAccommodation) {
		$stmt = $this->conn->prepare("SELECT * from accommodation WHERE idAccommodation = :idAccommodation");
		$stmt->bindValue(':idAccommodation', $idAccommodation, PDO::PARAM_INT);
		if($stmt->execute()){
			$result = $stmt->fetch();
			$stmt->closeCursor();
			return $result;
		}
		else{
			return false;
		}
	}


	public function addAccommodationAdmin($name, $longitude, $latitude, $phoneNumber, $website, $price, $description, $picture, $numberOfPlaces){
		$stmt = $this->conn->prepare("INSERT INTO accommodation(name, longitude, latitude, phoneNumber, website, price, description, picture, numberOfPlaces) VALUES (:name, :longitude, :latitude, :phoneNumber, :website, :price, :description, :picture, :numberOfPlaces);");
		$stmt->bindValue(':name', $name, PDO::PARAM_STR);
		$stmt->bindValue(':longitude', $longitude, PDO::PARAM_STR);
		$stmt->bindValue(':latitude', $latitude, PDO::PARAM_STR);
		$stmt->bindValue(':phoneNumber', $phoneNumber, PDO::PARAM_STR);
		$stmt->bindValue(':website', $website, PDO::PARAM_STR);
		$stmt->bindValue(':price', $price, PDO::PARAM_INT);
		$stmt->bindValue(':description', $description, PDO::PARAM_STR);
		$stmt->bindValue(':picture', $picture, PDO::PARAM_STR);
		$stmt->bindValue(':numberOfPlaces', $numberOfPlaces, PDO::PARAM_INT);
		
		$result = $stmt->execute();
		$stmt->closeCursor();
		
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM accommodation WHERE name = :name AND longitude = :longitude AND latitude = :latitude AND price = :price");
			$stmt->bindValue(':name', $name, PDO::PARAM_STR);
			$stmt->bindValue(':longitude', $longitude, PDO::PARAM_STR);
			$stmt->bindValue(':latitude', $latitude, PDO::PARAM_STR);
			$stmt->bindValue(':price', $price, PDO::PARAM_INT);
			$stmt->execute();
			$accommodation = $stmt->fetch();
			$stmt->closeCursor();
			return $accommodation;
		}
		else {
			return false;
		}
	}

	public function deleteAccommodationAdmin($idAccommodation){
		$stmt = $this->conn->prepare("DELETE FROM accommodation WHERE idAccommodation = :idAccommodation");
		$stmt->bindValue(':idAccommodation', $idAccommodation, PDO::PARAM_INT);
		
		$result = $stmt->execute();
		$stmt->closeCursor();
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM accommodation WHERE idAccommodation = :idAccommodation");
			$stmt->bindValue(':idAccommodation', $idAccommodation, PDO::PARAM_INT);
			$stmt->execute();
			$accommodation = $stmt->fetch();
			$stmt->closeCursor();
			return $accommodation;
		}
		else {
			return false;
		}
	}


	public function updateAccommodationAdmin($idAccommodation, $name, $longitude, $latitude, $phoneNumber, $website, $price, $description, $picture, $numberOfPlaces){
		$stmt = $this->conn->prepare("UPDATE accommodation SET name = :name, longitude = :longitude, latitude = :latitude, phoneNumber = :phoneNumber, website = :website, price = :price, description = :description, picture = :picture, numberOfPlaces = :numberOfPlaces WHERE idAccommodation = :idAccommodation");
		$stmt->bindValue(':name', $name, PDO::PARAM_STR);
		$stmt->bindValue(':longitude', $longitude, PDO::PARAM_STR);
		$stmt->bindValue(':latitude', $latitude, PDO::PARAM_STR);
		$stmt->bindValue(':phoneNumber', $phoneNumber, PDO::PARAM_STR);
		$stmt->bindValue(':website', $website, PDO::PARAM_STR);
		$stmt->bindValue(':price', $price, PDO::PARAM_INT);
		$stmt->bindValue(':description', $description, PDO::PARAM_STR);
		$stmt->bindValue(':picture', $picture, PDO::PARAM_STR);
		$stmt->bindValue(':numberOfPlaces', $numberOfPlaces, PDO::PARAM_INT);
		$stmt->bindValue(':idAccommodation', $idAccommodation, PDO::PARAM_INT);

		$result = $stmt->execute();
		$stmt->closeCursor();
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM accommodation WHERE idAccommodation = :idAccommodation");
			$stmt->bindValue(':idAccommodation', $idAccommodation, PDO::PARAM_INT);
			
			$stmt->execute();
			$accommodation = $stmt->fetch();
			$stmt->closeCursor();
			return $accommodation;
		}
		else {
			return false;
		}
	}
}
?>
	