<?php
 
 
/**
 * This class contains all functions used for the preferences tables
 * @author Gabriel Artigues
 *
 */
class DB_PreferencesFunctions {
 
    private $conn;
 
    // constructor
    function __construct() {
        require_once 'DB_Connect.php';
        // Creating connection to database
        $db = new DB_Connect();
        $this->conn = $db->connect();
    }
 
    // destructor
    function __destruct() {}
 
    // PREFERENCES FUNCTIONS
    //insert into userHasCategoryAccommodationPreferences table
     public function storeAccommodationPreferences($idUser, $accommodationArray) {

        $nameCategoryAccommodation = explode(",", $accommodationArray);

        for ($i=0; $i < sizeof($nameCategoryAccommodation); $i++) { 
             $stmt = $this->conn->prepare("INSERT INTO userHasCategoryAccommodationPreferences(idUser, nameCategoryAccommodation) VALUES(:idUser, :nameCategoryAccommodation)");
            
            $stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
            $stmt->bindValue(':nameCategoryAccommodation', $nameCategoryAccommodation[$i], PDO::PARAM_STR);
            $result = $stmt->execute();
            
            $stmt->closeCursor();

        }
    }

    //insert into userHasFacilityPreferences table
     public function storeFacilityPreferences($idUser, $facilityArray) {

        $nameFacility = explode(",", $facilityArray);

        for ($i=0; $i < sizeof($nameFacility); $i++) { 

             $stmt = $this->conn->prepare("INSERT INTO userHasFacilityPreferences(idUser, nameFacility) VALUES(:idUser, :nameFacility)");
            
            $stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
            $stmt->bindValue(':nameFacility', $nameFacility[$i], PDO::PARAM_STR);
            $result = $stmt->execute();
            
            $stmt->closeCursor();

        }
    }

     //insert into userHasCategoryActivityPreferences table

     public function storeActivityPreferences($idUser, $activityArray) {

        $nameCategoryActivity = explode(",", $activityArray);

        for ($i=0; $i < sizeof($nameCategoryActivity); $i++) { 
            
             $stmt = $this->conn->prepare("INSERT INTO userHasCategoryActivityPreferences(idUser, nameCategoryActivity) VALUES(:idUser, :nameCategoryActivity)");
            
            $stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
            $stmt->bindValue(':nameCategoryActivity', $nameCategoryActivity[$i], PDO::PARAM_STR);
            $result = $stmt->execute();
            
            $stmt->closeCursor();

        }
    }


    public function fetchAllActivityPreferences($idUser){
        $stmt = $this->conn->prepare("SELECT * from userHasCategoryActivityPreferences WHERE idUser = :idUser");
        $stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
        if($stmt->execute()){
            $result = $stmt->fetch();
            $stmt->closeCursor();
            return $result;
        }
        else{
            return false;
        }
    }
    
    public function isPrefActivityEmpty(){
        $stmt = $this->conn->prepare("SELECT COUNT(idUser) as NbActivities from userHasCategoryActivityPreferences");

        $stmt->execute();
        
        $result = $stmt->fetch();
        $stmt->closeCursor();
        
        return ($result["NbActivities"] <= 0);
    }


    public function fetchAllAccommodationPreferences($idUser){
        $stmt = $this->conn->prepare("SELECT * from userHasAccommodationPreferences WHERE idUser = :idUser");
        $stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
        if($stmt->execute()){
            $result = $stmt->fetch();
            $stmt->closeCursor();
            return $result;
        }
        else{
            return false;
        }
    }
    
    public function isPrefAccommodationEmpty(){
        $stmt = $this->conn->prepare("SELECT COUNT(idUser) as NbActivities from userHasCategoryAccommodationPreferences");

        $stmt->execute();
        
        $result = $stmt->fetch();
        $stmt->closeCursor();
        
        return ($result["NbActivities"] <= 0);
    }


        public function fetchAllFacilityPreferences($idUser){
        $stmt = $this->conn->prepare("SELECT * from userHasFacilityPreferences WHERE idUser = :idUser");
        $stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
        if($stmt->execute()){
            $result = $stmt->fetch();
            $stmt->closeCursor();
            return $result;
        }
        else{
            return false;
        }
    }
    
    public function isPrefFacilityEmpty(){
        $stmt = $this->conn->prepare("SELECT COUNT(idUser) as NbActivities from userHasFacilityPreferences");

        $stmt->execute();
        
        $result = $stmt->fetch();
        $stmt->closeCursor();
        
        return ($result["NbActivities"] <= 0);
    }

}
?>