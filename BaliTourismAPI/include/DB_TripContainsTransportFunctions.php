<?php 

/**
 * This class contains all functions used for the activity table
 * @author Gabriel ARTIGUES
 */
class DB_TripContainsTransportFunctions {
	
	private $conn;
	
	// Constructor
	function __construct() {
		require_once 'DB_Connect.php';
		// Creating connection to database
		$db = new DB_Connect();
		$this->conn = $db->connect();
	}
	
	// Destructor
	function __destruct() {}
	
	// ACCOMMODATIONBELONGSCATEGORYACCOMMODATION FUNCTIONS


	public function addTripContainsTransport($idTrip, $idTransport, $dateTransport){
		$stmt = $this->conn->prepare("INSERT INTO tripContainsTransport(idTrip, idTransport, dateTransport) VALUES (:idTrip, :idTransport, :dateTransport);");
		$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
		$stmt->bindValue(':idTransport', $idTransport, PDO::PARAM_INT);
		$stmt->bindValue(':dateTransport', $dateTransport, PDO::PARAM_STR);
		
		$result = $stmt->execute();
		$stmt->closeCursor();
		
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM tripContainsTransport WHERE idTrip = :idTrip AND idTransport = :idTransport AND dateTransport = :dateTransport");
			$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
			$stmt->bindValue(':idTransport', $idTransport, PDO::PARAM_INT);
			$stmt->bindValue(':dateTransport', $dateTransport, PDO::PARAM_STR);
			$stmt->execute();
			$trip = $stmt->fetch();
			$stmt->closeCursor();
			return $trip;
		}
		else {
			return false;
		}
	}

	/**
	 * Gets all activities in the database
	 * @return all the activities stored in the database if there are some, false otherwise
	 */
	public function fetchAllTripContainsTransport() {
			$stmt = $this->conn->prepare("SELECT * from tripContainsTransport");
			if($stmt->execute()){
				$result = $stmt->fetchAll();
				$stmt->closeCursor();
				return $result;
			}
			else{
				return null;
			}	
	}
	
	/**
	 * Checks if the table constructionSite is empty
	 * @return true if empty, false otherwise
	 */
	public function isTripContainsTransportEmpty(){
		$stmt = $this->conn->prepare("SELECT COUNT(idTransport) as NbTransports from tripContainsTransport");

		$stmt->execute();
		
		$result = $stmt->fetch();
		$stmt->closeCursor();
		
		return ($result["NbTransports"] <= 0);
	}

	public function deleteTripContainsTransport($idTrip, $idTransport, $dateTransport){
		$stmt = $this->conn->prepare("DELETE FROM tripContainsTransport WHERE idTrip = :idTrip AND idTransport = :idTransport AND dateTransport = :dateTransport");
		$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
		$stmt->bindValue(':idTransport', $idTransport, PDO::PARAM_INT);
		$stmt->bindValue(':dateTransport', $dateTransport, PDO::PARAM_STR);
		
		$result = $stmt->execute();
		$stmt->closeCursor();
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM tripContainsTransport WHERE idTrip = :idTrip AND idTransport = :idTransport AND dateTransport = :dateTransport");
			$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
			$stmt->bindValue(':idTransport', $idTransport, PDO::PARAM_INT);
			$stmt->bindValue(':dateTransport', $dateTransport, PDO::PARAM_STR);
			$stmt->execute();
			$trip = $stmt->fetch();
			$stmt->closeCursor();
			return $trip;
		}
		else {
			return false;
		}
	}

	public function updateTripContainsTransport($idTrip, $dateTransport, $newDate){
		$stmt = $this->conn->prepare("UPDATE tripContainsTransport SET dateTransport=:newDate WHERE idTrip = :idTrip AND dateTransport = :dateTransport");
		$stmt->bindValue(':newDate', $newDate, PDO::PARAM_STR);
		$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
		$stmt->bindValue(':dateTransport', $dateTransport, PDO::PARAM_STR);

		$result = $stmt->execute();
		$stmt->closeCursor();
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM tripContainsTransport WHERE idTrip = :idTrip AND dateTransport = :newDate");
			$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
			$stmt->bindValue(':newDate', $newDate, PDO::PARAM_STR);
			$stmt->execute();
			$trip = $stmt->fetch();
			$stmt->closeCursor();
			return $trip;
		}
		else {
			return false;
		}
	}

}
?>