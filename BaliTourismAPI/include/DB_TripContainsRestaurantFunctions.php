<?php 

/**
 * This class contains all functions used for the activity table
 * @author Gabriel ARTIGUES
 */
class DB_TripContainsRestaurantFunctions {
	
	private $conn;
	
	// Constructor
	function __construct() {
		require_once 'DB_Connect.php';
		// Creating connection to database
		$db = new DB_Connect();
		$this->conn = $db->connect();
	}
	
	// Destructor
	function __destruct() {}
	
	// ACCOMMODATIONBELONGSCATEGORYACCOMMODATION FUNCTIONS


	public function addTripContainsRestaurant($idTrip, $idRestaurant, $arrivalDate, $arrivalTime){
		$stmt = $this->conn->prepare("INSERT INTO tripContainsRestaurant(idTrip, idRestaurant, arrivalDate, arrivalTime) VALUES (:idTrip, :idRestaurant, :arrivalDate, :arrivalTime);");
		$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
		$stmt->bindValue(':idRestaurant', $idRestaurant, PDO::PARAM_INT);
		$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
		$stmt->bindValue(':arrivalTime', $arrivalTime, PDO::PARAM_STR);
		
		$result = $stmt->execute();
		$stmt->closeCursor();
		
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM tripContainsRestaurant WHERE idTrip = :idTrip AND idRestaurant = :idRestaurant AND arrivalDate = :arrivalDate AND arrivalTime = :arrivalTime");
			$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
			$stmt->bindValue(':idRestaurant', $idRestaurant, PDO::PARAM_INT);
			$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
			$stmt->bindValue(':arrivalTime', $arrivalTime, PDO::PARAM_STR);
			$stmt->execute();
			$trip = $stmt->fetch();
			$stmt->closeCursor();
			return $trip;
		}
		else {
			return false;
		}
	}

	/**
	 * Gets all activities in the database
	 * @return all the activities stored in the database if there are some, false otherwise
	 */
	public function fetchAllTripContainsRestaurant() {
			$stmt = $this->conn->prepare("SELECT * from tripContainsRestaurant");
			if($stmt->execute()){
				$result = $stmt->fetchAll();
				$stmt->closeCursor();
				return $result;
			}
			else{
				return null;
			}	
	}
	
	/**
	 * Checks if the table constructionSite is empty
	 * @return true if empty, false otherwise
	 */
	public function isTripContainsRestaurantEmpty(){
		$stmt = $this->conn->prepare("SELECT COUNT(idRestaurant) as NbRestaurants from tripContainsRestaurant");

		$stmt->execute();
		
		$result = $stmt->fetch();
		$stmt->closeCursor();
		
		return ($result["NbRestaurants"] <= 0);
	}

	public function deleteTripContainsRestaurant($idTrip, $idRestaurant, $arrivalDate, $arrivalTime){
		$stmt = $this->conn->prepare("DELETE FROM tripContainsRestaurant WHERE idTrip = :idTrip AND idRestaurant = :idRestaurant AND arrivalDate = :arrivalDate AND arrivalTime = :arrivalTime");
		$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
		$stmt->bindValue(':idRestaurant', $idRestaurant, PDO::PARAM_INT);
		$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
		$stmt->bindValue(':arrivalTime', $arrivalTime, PDO::PARAM_STR);
		
		$result = $stmt->execute();
		$stmt->closeCursor();
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM tripContainsRestaurant WHERE idTrip = :idTrip AND idRestaurant = :idRestaurant AND arrivalDate = :arrivalDate AND arrivalTime = :arrivalTime");
			$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
			$stmt->bindValue(':idRestaurant', $idRestaurant, PDO::PARAM_INT);
			$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
			$stmt->bindValue(':arrivalTime', $arrivalTime, PDO::PARAM_STR);
			$stmt->execute();
			$trip = $stmt->fetch();
			$stmt->closeCursor();
			return $trip;
		}
		else {
			return false;
		}
	}

	public function updateTripContainsRestaurant($idTrip, $arrivalDate, $newDate, $arrivalTime, $newTime){
		$stmt = $this->conn->prepare("UPDATE tripContainsRestaurant SET arrivalDate =:newDate, arrivalTime = :newTime WHERE idTrip = :idTrip AND arrivalDate = :arrivalDate AND arrivalTime = :arrivalTime");
		$stmt->bindValue(':newDate', $newDate, PDO::PARAM_STR);
		$stmt->bindValue(':newTime', $newTime, PDO::PARAM_STR);
		$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
		$stmt->bindValue(':arrivalDate', $arrivalDate, PDO::PARAM_STR);
		$stmt->bindValue(':arrivalTime', $arrivalTime, PDO::PARAM_STR);
		$result = $stmt->execute();
		$stmt->closeCursor();
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM tripContainsRestaurant WHERE idTrip = :idTrip AND arrivalDate = :newDate AND arrivalTime = :newTime");
			$stmt->bindValue(':idTrip', $idTrip, PDO::PARAM_INT);
			$stmt->bindValue(':newDate', $newDate, PDO::PARAM_STR);
			$stmt->bindValue(':newTime', $newTime, PDO::PARAM_STR);
			$stmt->execute();
			$trip = $stmt->fetch();
			$stmt->closeCursor();
			return $trip;
		}
		else {
			return false;
		}
	}
}
?>