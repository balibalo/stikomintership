<?php 

/**
 * This class contains all functions used for the restaurant table
 * @author Gabriel ARTIGUES
 */
class DB_RestaurantFunctions {
	
	private $conn;
	
	// Constructor
	function __construct() {
		require_once 'DB_Connect.php';
		// Creating connection to database
		$db = new DB_Connect();
		$this->conn = $db->connect();
	}
	
	// Destructor
	function __destruct() {}
	
	// RESTAURANT FUNCTIONS
	

	/**
	 * Gets all restaurants in the database
	 * @return all the restaurants stored in the database if there are some, false otherwise
	 */
	public function fetchAllRestaurants() {
			$stmt = $this->conn->prepare("SELECT * from restaurant");
			if($stmt->execute()){
				$result = $stmt->fetchAll();
				$stmt->closeCursor();
				return $result;
			}
			else{
				return null;
			}	
	}

	/**
	 * Checks if the table restaurant is empty
	 * @return true if empty, false otherwise
	 */
	public function isRestaurantEmpty(){
		$stmt = $this->conn->prepare("SELECT COUNT(idRestaurant) as NbRestaurants from restaurant");

		$stmt->execute();
		
		$result = $stmt->fetch();
		$stmt->closeCursor();
		
		return ($result["NbRestaurants"] <= 0);
	}

	/**
	 * Gets a restaurant by its id
	 * @return all the restaurants stored in the database if there are some, false otherwise
	 */
	public function fetchRestaurantById($idRestaurant) {
		$stmt = $this->conn->prepare("SELECT * from restaurant WHERE idRestaurant = :idRestaurant");
		$stmt->bindValue(':idRestaurant', $idRestaurant, PDO::PARAM_INT);
		if($stmt->execute()){
			$result = $stmt->fetch();
			$stmt->closeCursor();
			return $result;
		}
		else{
			return false;
		}
	}

	public function addRestaurantAdmin($name, $longitude, $latitude, $phoneNumber, $website, $price, $description, $picture, $openingHour, $openingMinute, $closingHour, $closingMinute){
		$stmt = $this->conn->prepare("INSERT INTO restaurant(name, longitude, latitude, phoneNumber, website, price, description, picture, openingHour, openingMinute, closingHour, closingMinute) VALUES (:name, :longitude, :latitude, :phoneNumber, :website, :price, :description, :picture, :openingHour, :openingMinute, :closingHour, :closingMinute);");
		$stmt->bindValue(':name', $name, PDO::PARAM_STR);
		$stmt->bindValue(':longitude', $longitude, PDO::PARAM_STR);
		$stmt->bindValue(':latitude', $latitude, PDO::PARAM_STR);
		$stmt->bindValue(':phoneNumber', $phoneNumber, PDO::PARAM_STR);
		$stmt->bindValue(':website', $website, PDO::PARAM_STR);
		$stmt->bindValue(':price', $price, PDO::PARAM_INT);
		$stmt->bindValue(':description', $description, PDO::PARAM_STR);
		$stmt->bindValue(':picture', $picture, PDO::PARAM_STR);
		$stmt->bindValue(':openingHour', $openingHour, PDO::PARAM_INT);
		$stmt->bindValue(':openingMinute', $openingMinute, PDO::PARAM_INT);
		$stmt->bindValue(':closingHour', $closingHour, PDO::PARAM_INT);
		$stmt->bindValue(':closingMinute', $closingMinute, PDO::PARAM_INT);
		
		$result = $stmt->execute();
		$stmt->closeCursor();
		
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM restaurant WHERE name = :name AND longitude = :longitude AND latitude = :latitude AND price = :price");
			$stmt->bindValue(':name', $name, PDO::PARAM_STR);
			$stmt->bindValue(':longitude', $longitude, PDO::PARAM_STR);
			$stmt->bindValue(':latitude', $latitude, PDO::PARAM_STR);
			$stmt->bindValue(':price', $price, PDO::PARAM_INT);
			$stmt->execute();
			$restaurant = $stmt->fetch();
			$stmt->closeCursor();
			return $restaurant;
		}
		else {
			return false;
		}
	}

	public function deleteRestaurantAdmin($idRestaurant){
		$stmt = $this->conn->prepare("DELETE FROM restaurant WHERE idRestaurant = :idRestaurant");
		$stmt->bindValue(':idRestaurant', $idRestaurant, PDO::PARAM_INT);
		
		$result = $stmt->execute();
		$stmt->closeCursor();
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM restaurant WHERE idRestaurant = :idRestaurant");
			$stmt->bindValue(':idRestaurant', $idRestaurant, PDO::PARAM_INT);
			$stmt->execute();
			$restaurant = $stmt->fetch();
			$stmt->closeCursor();
			return $restaurant;
		}
		else {
			return false;
		}
	}

	public function updateRestaurantAdmin($idRestaurant, $name, $longitude, $latitude, $phoneNumber, $website, $price, $description, $picture, $openingHour, $openingMinute, $closingHour, $closingMinute){
		$stmt = $this->conn->prepare("UPDATE restaurant SET name = :name, longitude = :longitude, latitude = :latitude, phoneNumber = :phoneNumber, website = :website, price = :price, description = :description, picture = :picture, openingHour = :openingHour, openingMinute = :openingMinute, closingHour = :closingHour, closingMinute = :closingMinute WHERE idRestaurant = :idRestaurant");
		$stmt->bindValue(':name', $name, PDO::PARAM_STR);
		$stmt->bindValue(':longitude', $longitude, PDO::PARAM_STR);
		$stmt->bindValue(':latitude', $latitude, PDO::PARAM_STR);
		$stmt->bindValue(':phoneNumber', $phoneNumber, PDO::PARAM_STR);
		$stmt->bindValue(':website', $website, PDO::PARAM_STR);
		$stmt->bindValue(':price', $price, PDO::PARAM_INT);
		$stmt->bindValue(':description', $description, PDO::PARAM_STR);
		$stmt->bindValue(':picture', $picture, PDO::PARAM_STR);
		$stmt->bindValue(':openingHour', $openingHour, PDO::PARAM_INT);
		$stmt->bindValue(':openingMinute', $openingMinute, PDO::PARAM_INT);
		$stmt->bindValue(':closingHour', $closingHour, PDO::PARAM_INT);
		$stmt->bindValue(':closingMinute', $closingMinute, PDO::PARAM_INT);
		$stmt->bindValue(':idRestaurant', $idRestaurant, PDO::PARAM_INT);

		$result = $stmt->execute();
		$stmt->closeCursor();
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM restaurant WHERE idRestaurant = :idRestaurant");
			$stmt->bindValue(':idRestaurant', $idRestaurant, PDO::PARAM_INT);
			
			$stmt->execute();
			$restaurant = $stmt->fetch();
			$stmt->closeCursor();
			return $restaurant;
		}
		else {
			return false;
		}
	}
}
?>
	