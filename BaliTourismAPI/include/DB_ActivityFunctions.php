<?php 

/**
 * This class contains all functions used for the activity table
 * @author Gabriel ARTIGUES
 */
class DB_ActivityFunctions {
	
	private $conn;
	
	// Constructor
	function __construct() {
		require_once 'DB_Connect.php';
		// Creating connection to database
		$db = new DB_Connect();
		$this->conn = $db->connect();
	}
	
	// Destructor
	function __destruct() {}
	
	// ACTIVITY FUNCTIONS
	

	/**
	 * Gets all activities in the database
	 * @return all the activities stored in the database if there are some, false otherwise
	 */
	public function fetchAllActivities() {
			$stmt = $this->conn->prepare("SELECT * from activity");
			if($stmt->execute()){
				$result = $stmt->fetchAll();
				$stmt->closeCursor();
				return $result;
			}
			else{
				return null;
			}	
	}

	/**
	 * Checks if the table activity is empty
	 * @return true if empty, false otherwise
	 */
	public function isActivityEmpty(){
		$stmt = $this->conn->prepare("SELECT COUNT(idActivity) as NbActivities from activity");

		$stmt->execute();
		
		$result = $stmt->fetch();
		$stmt->closeCursor();
		
		return ($result["NbActivities"] <= 0);
	}

	/**
	 * Gets an activity by its id
	 * @return all the activities stored in the database if there are some, false otherwise
	 */
	public function fetchActivityById($idActivity) {
		$stmt = $this->conn->prepare("SELECT * from activity WHERE idActivity = :idActivity");
		$stmt->bindValue(':idActivity', $idActivity, PDO::PARAM_INT);
		if($stmt->execute()){
			$result = $stmt->fetch();
			$stmt->closeCursor();
			return $result;
		}
		else{
			return false;
		}
	}


	public function addActivityAdmin($name, $longitude, $latitude, $phoneNumber, $website, $price, $description, $picture, $duration, $openingHour, $openingMinute, $closingHour, $closingMinute){
		$stmt = $this->conn->prepare("INSERT INTO activity(name, longitude, latitude, phoneNumber, website, price, description, picture, duration, openingHour, openingMinute, closingHour, closingMinute) VALUES (:name, :longitude, :latitude, :phoneNumber, :website, :price, :description, :picture, :duration, :openingHour, :openingMinute, :closingHour, :closingMinute);");
		$stmt->bindValue(':name', $name, PDO::PARAM_STR);
		$stmt->bindValue(':longitude', $longitude, PDO::PARAM_STR);
		$stmt->bindValue(':latitude', $latitude, PDO::PARAM_STR);
		$stmt->bindValue(':phoneNumber', $phoneNumber, PDO::PARAM_STR);
		$stmt->bindValue(':website', $website, PDO::PARAM_STR);
		$stmt->bindValue(':price', $price, PDO::PARAM_INT);
		$stmt->bindValue(':description', $description, PDO::PARAM_STR);
		$stmt->bindValue(':picture', $picture, PDO::PARAM_STR);
		$stmt->bindValue(':duration', $duration, PDO::PARAM_INT);
		$stmt->bindValue(':openingHour', $openingHour, PDO::PARAM_INT);
		$stmt->bindValue(':openingMinute', $openingMinute, PDO::PARAM_INT);
		$stmt->bindValue(':closingHour', $closingHour, PDO::PARAM_INT);
		$stmt->bindValue(':closingMinute', $closingMinute, PDO::PARAM_INT);
		
		$result = $stmt->execute();
		$stmt->closeCursor();
		
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM activity WHERE name = :name AND longitude = :longitude AND latitude = :latitude AND price = :price");
			$stmt->bindValue(':name', $name, PDO::PARAM_STR);
			$stmt->bindValue(':longitude', $longitude, PDO::PARAM_STR);
			$stmt->bindValue(':latitude', $latitude, PDO::PARAM_STR);
			$stmt->bindValue(':price', $price, PDO::PARAM_INT);
			$stmt->execute();
			$activity = $stmt->fetch();
			$stmt->closeCursor();
			return $activity;
		}
		else {
			return false;
		}
	}

	public function deleteActivityAdmin($idActivity){
		$stmt = $this->conn->prepare("DELETE FROM activity WHERE idActivity = :idActivity");
		$stmt->bindValue(':idActivity', $idActivity, PDO::PARAM_INT);
		
		$result = $stmt->execute();
		$stmt->closeCursor();
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM activity WHERE idActivity = :idActivity");
			$stmt->bindValue(':idActivity', $idActivity, PDO::PARAM_INT);
			$stmt->execute();
			$activity = $stmt->fetch();
			$stmt->closeCursor();
			return $activity;
		}
		else {
			return false;
		}
	}

	public function updateActivityAdmin($idActivity, $name, $longitude, $latitude, $phoneNumber, $website, $price, $description, $picture, $duration, $openingHour, $openingMinute, $closingHour, $closingMinute){
		$stmt = $this->conn->prepare("UPDATE activity SET name = :name, longitude = :longitude, latitude = :latitude, phoneNumber = :phoneNumber, website = :website, price = :price, description = :description, picture = :picture, duration = :duration, openingHour = :openingHour, openingMinute = :openingMinute, closingHour = :closingHour, closingMinute = :closingMinute WHERE idActivity = :idActivity");
		$stmt->bindValue(':name', $name, PDO::PARAM_STR);
		$stmt->bindValue(':longitude', $longitude, PDO::PARAM_STR);
		$stmt->bindValue(':latitude', $latitude, PDO::PARAM_STR);
		$stmt->bindValue(':phoneNumber', $phoneNumber, PDO::PARAM_STR);
		$stmt->bindValue(':website', $website, PDO::PARAM_STR);
		$stmt->bindValue(':price', $price, PDO::PARAM_INT);
		$stmt->bindValue(':description', $description, PDO::PARAM_STR);
		$stmt->bindValue(':picture', $picture, PDO::PARAM_STR);
		$stmt->bindValue(':duration', $duration, PDO::PARAM_INT);
		$stmt->bindValue(':openingHour', $openingHour, PDO::PARAM_INT);
		$stmt->bindValue(':openingMinute', $openingMinute, PDO::PARAM_INT);
		$stmt->bindValue(':closingHour', $closingHour, PDO::PARAM_INT);
		$stmt->bindValue(':closingMinute', $closingMinute, PDO::PARAM_INT);
		$stmt->bindValue(':idActivity', $idActivity, PDO::PARAM_INT);

		$result = $stmt->execute();
		$stmt->closeCursor();
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM activity WHERE idActivity = :idActivity");
			$stmt->bindValue(':idActivity', $idActivity, PDO::PARAM_INT);
			
			$stmt->execute();
			$activity = $stmt->fetch();
			$stmt->closeCursor();
			return $activity;
		}
		else {
			return false;
		}
	}
}
?>
	