<?php 

/**
 * This class contains all functions used for the activity table
 * @author Gabriel ARTIGUES
 */
class DB_AccommodationBelongsCategoryAccommodationFunctions {
	
	private $conn;
	
	// Constructor
	function __construct() {
		require_once 'DB_Connect.php';
		// Creating connection to database
		$db = new DB_Connect();
		$this->conn = $db->connect();
	}
	
	// Destructor
	function __destruct() {}
	
	// ACCOMMODATIONBELONGSCATEGORYACCOMMODATION FUNCTIONS

	/**
	 * Gets all activities in the database
	 * @return all the activities stored in the database if there are some, false otherwise
	 */
	public function fetchAllAccommodationBelongsCategoryAccommodation() {
			$stmt = $this->conn->prepare("SELECT * from accommodationBelongsCategoryAccommodation");
			if($stmt->execute()){
				$result = $stmt->fetchAll();
				$stmt->closeCursor();
				return $result;
			}
			else{
				return null;
			}	
	}
	
	/**
	 * Checks if the table constructionSite is empty
	 * @return true if empty, false otherwise
	 */
	public function isAccommodationBelongsCategoryAccommodationEmpty(){
		$stmt = $this->conn->prepare("SELECT COUNT(idAccommodation) as NbAccommodations from accommodationBelongsCategoryAccommodation");

		$stmt->execute();
		
		$result = $stmt->fetch();
		$stmt->closeCursor();
		
		return ($result["NbAccommodations"] <= 0);
	}

	public function addAccommodationBelongsCategory($idAccommodation, $nameCategoryAccommodation){
		$stmt = $this->conn->prepare("INSERT INTO accommodationBelongsCategoryAccommodation(idAccommodation, nameCategoryAccommodation) VALUES (:idAccommodation, :nameCategoryAccommodation);");
		$stmt->bindValue(':idAccommodation', $idAccommodation, PDO::PARAM_INT);
		$stmt->bindValue(':nameCategoryAccommodation', $nameCategoryAccommodation, PDO::PARAM_STR);
		$result = $stmt->execute();
		$stmt->closeCursor();
		
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM accommodationBelongsCategoryAccommodation WHERE idAccommodation = :idAccommodation AND nameCategoryAccommodation = :nameCategoryAccommodation");
			$stmt->bindValue(':idAccommodation', $idAccommodation, PDO::PARAM_INT);
			$stmt->bindValue(':nameCategoryAccommodation', $nameCategoryAccommodation, PDO::PARAM_STR);
			$stmt->execute();
			$accommodation = $stmt->fetch();
			$stmt->closeCursor();
			return $accommodation;
		}
		else {
			return false;
		}
	}

	public function deleteAccommodationBelongsCategory($idAccommodation){
		$stmt = $this->conn->prepare("DELETE FROM accommodationBelongsCategoryAccommodation WHERE idAccommodation = :idAccommodation");
		$stmt->bindValue(':idAccommodation', $idAccommodation, PDO::PARAM_INT);
		
		$result = $stmt->execute();
		$stmt->closeCursor();
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM accommodation WHERE idAccommodation = :idAccommodation");
			$stmt->bindValue(':idAccommodation', $idAccommodation, PDO::PARAM_INT);
			$stmt->execute();
			$accommodation = $stmt->fetch();
			$stmt->closeCursor();
			return $accommodation;
		}
		else {
			return false;
		}
	}

}
?>
	