<?php
 
 
/**
 * This class contains all functions used for the user table
 * @author Gabriel Artigues
 *
 */
class DB_AdminFunctions{
 
    private $conn;
 
    // constructor
    function __construct() {
        require_once 'DB_Connect.php';
        // Creating connection to database
        $db = new DB_Connect();
        $this->conn = $db->connect();
    }
 
    // destructor
    function __destruct() {}
 
    // USER FUNCTIONS
    /**
     * Stores a user in the database
     * @param username the user's name
     * @param password the user's password
     * @return the created user if successful, false otherwise
     */
    public function storeAdmin($login, $password) {
        $encryptedPassword = sha1($password);
        
        $stmt = $this->conn->prepare("INSERT INTO administrator(login, encryptedPassword) VALUES(:login, :encryptedPassword)");
        
        $stmt->bindValue(':login', $login, PDO::PARAM_STR);
        $stmt->bindValue(':encryptedPassword', $encryptedPassword, PDO::PARAM_STR);
        $result = $stmt->execute();
        
        $stmt->closeCursor();
 
        // check for successful store
        if ($result) {
            $stmt = $this->conn->prepare("SELECT * FROM administrator WHERE login = :login");
            $stmt->bindValue(':login', $login, PDO::PARAM_STR);
            $stmt->execute();
            $admin = $stmt->fetch();
            $stmt->closeCursor();
 
            return $admin;
        }
        else {
            return false;
        }
    }

    public function getAdminByLogin($login, $password) {
 
        $stmt = $this->conn->prepare("SELECT * FROM administrator WHERE login = :login");
 
        $stmt->bindValue(':login', $login, PDO::PARAM_STR);
 
        if ($stmt->execute()) {
            $admin = $stmt->fetch();
            $stmt->closeCursor();
            
            // check for password equality
            if (sha1($password) == $admin['encryptedPassword']) {
                // user authentication details are correct
                return $admin;
            }
        }
        else {
            return NULL;
        }
    }
 
    /**
     * Checks if a user already exists base on its username
     * @param username the checked user's name
     * @return true if the user exists, false otherwise
     */
    public function isAdminExisting($login) {
        $stmt = $this->conn->prepare("SELECT login from administrator WHERE login = :login");
 
        $stmt->bindValue(':login', $login, PDO::PARAM_STR);
 
        $stmt->execute();
 
        if ($stmt->rowCount() > 0) {
            // user exists 
            $stmt->closeCursor();
            return true;
        }
        else {
            // user does not exist
            $stmt->closeCursor();
            return false;
        }
    }

    

    /**
     * Gets all activities in the database
     * @return all the activities stored in the database if there are some, false otherwise
     */
    public function fetchAllAdmins() {
            $stmt = $this->conn->prepare("SELECT * from administrator");
            if($stmt->execute()){
                $result = $stmt->fetchAll();
                $stmt->closeCursor();
                return $result;
            }
            else{
                return null;
            }   
    }
    
    /**
     * Checks if the table constructionSite is empty
     * @return true if empty, false otherwise
     */
    public function isAdminEmpty(){
        $stmt = $this->conn->prepare("SELECT COUNT(idAdmin) as NbAdmin from administrator");

        $stmt->execute();
        
        $result = $stmt->fetch();
        $stmt->closeCursor();
        
        return ($result["NbAdmin"] <= 0);
    }
}
?>