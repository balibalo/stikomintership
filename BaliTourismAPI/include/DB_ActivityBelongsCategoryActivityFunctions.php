<?php 

/**
 * This class contains all functions used for the activity table
 * @author Gabriel ARTIGUES
 */
class DB_ActivityBelongsCategoryActivityFunctions {
	
	private $conn;
	
	// Constructor
	function __construct() {
		require_once 'DB_Connect.php';
		// Creating connection to database
		$db = new DB_Connect();
		$this->conn = $db->connect();
	}
	
	// Destructor
	function __destruct() {}
	
	// ACTIVITYBELONGSCATEGORYACTIVITY FUNCTIONS

	/**
	 * Gets all activities in the database
	 * @return all the activities stored in the database if there are some, false otherwise
	 */
	public function fetchAllActivityBelongsCategoryActivity() {
			$stmt = $this->conn->prepare("SELECT * from activityBelongsCategoryActivity");
			if($stmt->execute()){
				$result = $stmt->fetchAll();
				$stmt->closeCursor();
				return $result;
			}
			else{
				return null;
			}	
	}
	
	/**
	 * Checks if the table constructionSite is empty
	 * @return true if empty, false otherwise
	 */
	public function isActivityBelongsCategoryActivityEmpty(){
		$stmt = $this->conn->prepare("SELECT COUNT(idActivity) as NbActivities from activityBelongsCategoryActivity");

		$stmt->execute();
		
		$result = $stmt->fetch();
		$stmt->closeCursor();
		
		return ($result["NbActivities"] <= 0);
	}

	/**
	 * Gets an activity by its id
	 * @return all the activities stored in the database if there are some, false otherwise
	 */
	public function fetchAllCategoryActivityByIdAcitivity($idActivity) {
		$stmt = $this->conn->prepare("SELECT nameCategoryActivity from activityBelongsCategoryActivity WHERE idActivity = :idActivity");
		$stmt->bindValue(':idActivity', $idActivity, PDO::PARAM_INT);
		if($stmt->execute()){
			$result = $stmt->fetch();
			$stmt->closeCursor();
			return $result;
		}
		else{
			return false;
		}
	}

	public function addActivityBelongsCategory($idActivity, $nameCategoryActivity){
		$stmt = $this->conn->prepare("INSERT INTO activityBelongsCategoryActivity(idActivity, nameCategoryActivity) VALUES (:idActivity, :nameCategoryActivity);");
		$stmt->bindValue(':idActivity', $idActivity, PDO::PARAM_INT);
		$stmt->bindValue(':nameCategoryActivity', $nameCategoryActivity, PDO::PARAM_STR);
		$result = $stmt->execute();
		$stmt->closeCursor();
		
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM activityBelongsCategoryActivity WHERE idActivity = :idActivity AND nameCategoryActivity = :nameCategoryActivity");
			$stmt->bindValue(':idActivity', $idActivity, PDO::PARAM_INT);
			$stmt->bindValue(':nameCategoryActivity', $nameCategoryActivity, PDO::PARAM_STR);
			$stmt->execute();
			$activity = $stmt->fetch();
			$stmt->closeCursor();
			return $activity;
		}
		else {
			return false;
		}
	}

	public function deleteActivityBelongsCategory($idActivity){
		$stmt = $this->conn->prepare("DELETE FROM activityBelongsCategoryActivity WHERE idActivity = :idActivity");
		$stmt->bindValue(':idActivity', $idActivity, PDO::PARAM_INT);
		
		$result = $stmt->execute();
		$stmt->closeCursor();
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM activityBelongsCategoryActivity WHERE idActivity = :idActivity");
			$stmt->bindValue(':idActivity', $idActivity, PDO::PARAM_INT);
			$stmt->execute();
			$activity = $stmt->fetch();
			$stmt->closeCursor();
			return $activity;
		}
		else {
			return false;
		}
	}

}
?>
	