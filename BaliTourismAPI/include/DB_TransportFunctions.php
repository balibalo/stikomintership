<?php 

/**
 * This class contains all functions used for the transport table
 * @author Gabriel ARTIGUES
 */
class DB_TransportFunctions {
	
	private $conn;
	
	// Constructor
	function __construct() {
		require_once 'DB_Connect.php';
		// Creating connection to database
		$db = new DB_Connect();
		$this->conn = $db->connect();
	}
	
	// Destructor
	function __destruct() {}
	
	// TRANSPORT FUNCTIONS
	

	/**
	 * Gets all transports in the database
	 * @return all the transports stored in the database if there are some, false otherwise
	 */
	public function fetchAllTransports() {
			$stmt = $this->conn->prepare("SELECT * from transport");
			if($stmt->execute()){
				$result = $stmt->fetchAll();
				$stmt->closeCursor();
				return $result;
			}
			else{
				return null;
			}	
	}

	/**
	 * Checks if the table transport is empty
	 * @return true if empty, false otherwise
	 */
	public function isTransportEmpty(){
		$stmt = $this->conn->prepare("SELECT COUNT(idTransport) as NbTransports from transport");

		$stmt->execute();
		
		$result = $stmt->fetch();
		$stmt->closeCursor();
		
		return ($result["NbTransports"] <= 0);
	}

	/**
	 * Gets a transport by its id
	 * @return all the transports stored in the database if there are some, false otherwise
	 */
	public function fetchTransportById($idTransport) {
		$stmt = $this->conn->prepare("SELECT * from transport WHERE idTransport = :idTransport");
		$stmt->bindValue(':idTransport', $idTransport, PDO::PARAM_INT);
		if($stmt->execute()){
			$result = $stmt->fetch();
			$stmt->closeCursor();
			return $result;
		}
		else{
			return false;
		}
	}

	public function addTransportAdmin($name, $description, $picture, $numberOfPlaces, $needDrivingLicense, $price){
		$stmt = $this->conn->prepare("INSERT INTO transport(name, description, picture, numberOfPlaces, needDrivingLicense, price) VALUES (:name, :description, :picture, :numberOfPlaces, :needDrivingLicense, :price);");
		$stmt->bindValue(':name', $name, PDO::PARAM_STR);
		$stmt->bindValue(':description', $description, PDO::PARAM_STR);
		$stmt->bindValue(':picture', $picture, PDO::PARAM_STR);
		$stmt->bindValue(':numberOfPlaces', $numberOfPlaces, PDO::PARAM_INT);
		$stmt->bindValue(':needDrivingLicense', $needDrivingLicense, PDO::PARAM_INT);
		$stmt->bindValue(':price', $price, PDO::PARAM_INT);

		$result = $stmt->execute();
		$stmt->closeCursor();
		
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM transport WHERE name = :name AND price = :price");
			$stmt->bindValue(':name', $name, PDO::PARAM_STR);
			$stmt->bindValue(':price', $price, PDO::PARAM_INT);
			$stmt->execute();
			$transport = $stmt->fetch();
			$stmt->closeCursor();
			return $transport;
		}
		else {
			return false;
		}
	}

	public function deleteTransportAdmin($idTransport){
		$stmt = $this->conn->prepare("DELETE FROM transport WHERE idTransport = :idTransport");
		$stmt->bindValue(':idTransport', $idTransport, PDO::PARAM_INT);
		
		$result = $stmt->execute();
		$stmt->closeCursor();
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM transport WHERE idTransport = :idTransport");
			$stmt->bindValue(':idTransport', $idTransport, PDO::PARAM_INT);
			$stmt->execute();
			$transport = $stmt->fetch();
			$stmt->closeCursor();
			return $transport;
		}
		else {
			return false;
		}
	}


	public function updateTransportAdmin($idTransport, $name, $description, $picture, $numberOfPlaces, $needDrivingLicense, $price){
		$stmt = $this->conn->prepare("UPDATE transport SET name = :name, description = :description, picture = :picture, numberOfPlaces = :numberOfPlaces, needDrivingLicense = :needDrivingLicense, price = :price WHERE idTransport = :idTransport");
		$stmt->bindValue(':name', $name, PDO::PARAM_STR);
		$stmt->bindValue(':description', $description, PDO::PARAM_STR);
		$stmt->bindValue(':picture', $picture, PDO::PARAM_STR);
		$stmt->bindValue(':numberOfPlaces', $numberOfPlaces, PDO::PARAM_INT);
		$stmt->bindValue(':needDrivingLicense', $needDrivingLicense, PDO::PARAM_INT);
		$stmt->bindValue(':price', $price, PDO::PARAM_INT);
		$stmt->bindValue(':idTransport', $idTransport, PDO::PARAM_INT);

		$result = $stmt->execute();
		$stmt->closeCursor();
		if($result) {
			$stmt = $this->conn->prepare("SELECT * FROM transport WHERE idTransport = :idTransport");
			$stmt->bindValue(':idTransport', $idTransport, PDO::PARAM_INT);
			
			$stmt->execute();
			$transport = $stmt->fetch();
			$stmt->closeCursor();
			return $transport;
		}
		else {
			return false;
		}
	}
}
?>
	