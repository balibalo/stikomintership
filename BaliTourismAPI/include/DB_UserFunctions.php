<?php
 
 
/**
 * This class contains all functions used for the user table
 * @author Gabriel Artigues
 *
 */
class DB_UserFunctions{
 
    private $conn;
 
    // constructor
    function __construct() {
        require_once 'DB_Connect.php';
        // Creating connection to database
        $db = new DB_Connect();
        $this->conn = $db->connect();
    }
 
    // destructor
    function __destruct() {}
 
    // USER FUNCTIONS
    /**
     * Stores a user in the database
     * @param username the user's name
     * @param password the user's password
     * @return the created user if successful, false otherwise
     */
    public function storeUser($username, $password) {
        $encryptedPassword = sha1($password);
    	
        $stmt = $this->conn->prepare("INSERT INTO user(username, encryptedPassword) VALUES(:username, :encryptedPassword)");
        
        $stmt->bindValue(':username', $username, PDO::PARAM_STR);
        $stmt->bindValue(':encryptedPassword', $encryptedPassword, PDO::PARAM_STR);
        $result = $stmt->execute();
        
        $stmt->closeCursor();
 
        // check for successful store
        if ($result) {
            $stmt = $this->conn->prepare("SELECT * FROM user WHERE username = :username");
            $stmt->bindValue(':username', $username, PDO::PARAM_STR);
            $stmt->execute();
            $user = $stmt->fetch();
            $stmt->closeCursor();
 
            return $user;
        }
        else {
            return false;
        }
    }
 
    /**
     * Gets a user by its username and password
     * @param username the user's name
     * @param password the user's password
     * @return the user if successful, NULL otherwise
     */
    public function getUserByLogin($username, $password) {
 
        $stmt = $this->conn->prepare("SELECT * FROM user WHERE username = :username");
 
        $stmt->bindValue(':username', $username, PDO::PARAM_STR);
 
        if ($stmt->execute()) {
            $user = $stmt->fetch();
            $stmt->closeCursor();
            
            // check for password equality
            if (sha1($password) == $user['encryptedPassword']) {
                // user authentication details are correct
                return $user;
            }
        }
        else {
            return NULL;
        }
    }
 
    /**
     * Checks if a user already exists base on its username
     * @param username the checked user's name
     * @return true if the user exists, false otherwise
     */
    public function isUserExisting($username) {
        $stmt = $this->conn->prepare("SELECT username from user WHERE username = :username");
 
        $stmt->bindValue(':username', $username, PDO::PARAM_STR);
 
        $stmt->execute();
 
        if ($stmt->rowCount() > 0) {
            // user exists 
            $stmt->closeCursor();
            return true;
        }
        else {
            // user does not exist
            $stmt->closeCursor();
            return false;
        }
    }


    public function getIdTripByUser($idUser){

        $stmt = $this->conn->prepare("SELECT idTrip from trip WHERE idUser = :idUser");
        $stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
        if($stmt->execute()){
            $result = $stmt->fetch();
            $stmt->closeCursor();
            return $result;
        }
        else{
            return false;
        }
    }


    public function updateUser($idUser, $timeSpentOutside, $drivingLicense){
        $stmt = $this->conn->prepare("UPDATE user SET drivingLicense = :drivingLicense, timeSpentOutside = :timeSpentOutside WHERE idUser = :idUser;");
         $stmt->bindValue('drivingLicense', $drivingLicense, PDO::PARAM_INT);
         $stmt->bindValue('timeSpentOutside', $timeSpentOutside, PDO::PARAM_INT);
         $stmt->bindValue('idUser', $idUser, PDO::PARAM_INT);
         $stmt->execute();
         $stmt->closeCursor();
    }

    public function updateUserIdTrip($idUser){


        /*$stmt1 = $this ->conn->prepare("SELECT idTrip FROM trip WHERE idUser = :idUser;");
        $stmt1->bindValue('idUser', $idUser, PDO::PARAM_INT);
        $stmt1->execute();
        $trip = $stmt1->fetch();
        $stmt1->closeCursor();*/

        $stmt = $this->conn->prepare("UPDATE user SET idTrip = (SELECT idTrip from trip where idUser = :idUser) WHERE idUser = :idUser;");
         $stmt->bindValue('idUser', $idUser, PDO::PARAM_INT);
         $stmt->execute();
         $stmt->closeCursor();
        }


    /**
     * Gets all activities in the database
     * @return all the activities stored in the database if there are some, false otherwise
     */
    public function fetchAllUsers() {
            $stmt = $this->conn->prepare("SELECT * from user");
            if($stmt->execute()){
                $result = $stmt->fetchAll();
                $stmt->closeCursor();
                return $result;
            }
            else{
                return null;
            }   
    }
    
    /**
     * Checks if the table constructionSite is empty
     * @return true if empty, false otherwise
     */
    public function isUserEmpty(){
        $stmt = $this->conn->prepare("SELECT COUNT(idUser) as NbUser from user");

        $stmt->execute();
        
        $result = $stmt->fetch();
        $stmt->closeCursor();
        
        return ($result["NbUser"] <= 0);
    }


    public function fetchUserById($idUser) {
        $stmt = $this->conn->prepare("SELECT * from user WHERE idUser = :idUser");
        $stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
        if($stmt->execute()){
            $result = $stmt->fetch();
            $stmt->closeCursor();
            return $result;
        }
        else{
            return false;
        }
    }
}
?>