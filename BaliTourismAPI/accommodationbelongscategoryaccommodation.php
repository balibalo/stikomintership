<?php 

require_once 'include/DB_AccommodationBelongsCategoryAccommodationFunctions.php';
$db = new DB_AccommodationBelongsCategoryAccommodationFunctions();

// Json response array
$response = array("error"=>false);

if(isset($_POST["id"])){
	if(!$db->isAccommodationBelongsCategoryAccommodationEmpty()){
		// Getting all activities
		if($_POST["id"]=="all"){
			$response["error"] = false;
			$response["accommodationsBelongsCategoryAccommodation"] = array();
		
			foreach($db->fetchAllAccommodationBelongsCategoryAccommodation() as $accommodationBelongsCategoryAccommodation){
				$myResponse = array();
				$myResponse["accommodationBelongsCategoryAccommodation"] = array();
				$myResponse["accommodationBelongsCategoryAccommodation"]["idAccommodation"] = $accommodationBelongsCategoryAccommodation["idAccommodation"];
				$myResponse["accommodationBelongsCategoryAccommodation"]["nameCategoryAccommodation"] = $accommodationBelongsCategoryAccommodation["nameCategoryAccommodation"];


				array_push($response["accommodationsBelongsCategoryAccommodation"], $myResponse);
			}
			echo json_encode($response);
		}
	}
	else{
		$response["error"] = TRUE;
		$response["error_msg"] = "No activity stored";
		echo json_encode($response);
	}
}
else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Error with URL !";
    echo json_encode($response);
}

?>