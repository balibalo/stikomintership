<?php 

require_once 'include/DB_TripContainsRestaurantFunctions.php';

$db = new DB_TripContainsRestaurantFunctions();

// Json response array
$response = array("error"=>false);

if (isset($_POST['idTrip']) & isset($_POST['idRestaurant']) & isset($_POST['arrivalDate']) & isset($_POST['arrivalTime'])) {

	//Receiving post params
	$idTrip = $_POST['idTrip'];
	$idRestaurant = $_POST['idRestaurant'];
	$arrivalDate = $_POST['arrivalDate'];
	$arrivalTime = $_POST['arrivalTime'];

	// Create a new trip
		$tripContainsRestaurant = $db->deleteTripContainsRestaurant($idTrip, $idRestaurant, $arrivalDate, $arrivalTime);
		if($tripContainsRestaurant){
			// trip stored succesfully
			$response["error"] = false;
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
	}
else{
	$response["error"] = true;
	$response["error_msg"] = "Missing fields";
	echo json_encode($response);
}


?>