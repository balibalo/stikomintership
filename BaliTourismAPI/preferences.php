<?php 

require_once 'include/DB_PreferencesFunctions.php';
$db = new DB_PreferencesFunctions();

// Json response array
$response = array("error"=>false);

if(isset($_POST["idUser"])){
	if(!$db->isPrefActivityEmpty()){
			$response["error"] = false;
			$response["prefActivities"] = array();
		
			foreach($db->fetchAllActivityPreferences($_POST["idUser"]) as $userHasActivityPreferences){
				$myResponse = array();
				$myResponse["userHasActivityPreferences"] = array();
				$myResponse["userHasActivityPreferences"]["idUser"] = $userHasActivityPreferences["idUser"];
				$myResponse["userHasActivityPreferences"]["nameCategoryActivity"] = $userHasActivityPreferences["nameCategoryActivity"];


				array_push($response["prefActivities"], $myResponse);
			}
			echo json_encode($response);
		}
	else{
		$response["error"] = TRUE;
		$response["error_msg"] = "No activity stored";
		echo json_encode($response);
	}
	if(!$db->isPrefAccommodationEmpty()){
			$response["error"] = false;
			$response["prefAccommodations"] = array();
		
			foreach($db->fetchAllAccommodationPreferences($_POST["idUser"]) as $userHasAccommodationPreferences){
				$myResponse = array();
				$myResponse["userHasAccommodationPreferences"] = array();
				$myResponse["userHasAccommodationPreferences"]["idUser"] = $userHasAccommodationPreferences["idUser"];
				$myResponse["userHasAccommodationPreferences"]["nameCategoryAccommodation"] = $userHasAccommodationPreferences["nameCategoryAccommodation"];


				array_push($response["prefAccommodations"], $myResponse);
			}
			echo json_encode($response);
		}
	else{
		$response["error"] = TRUE;
		$response["error_msg"] = "No activity stored";
		echo json_encode($response);
	}
	if(!$db->isPrefFacilityEmpty()){
			$response["error"] = false;
			$response["prefFacilities"] = array();
		
			foreach($db->fetchAllFacilityPreferences($_POST["idUser"]) as $userHasFacilityPreferences){
				$myResponse = array();
				$myResponse["userHasFacilityPreferences"] = array();
				$myResponse["userHasFacilityPreferences"]["idUser"] = $userHasFacilityPreferences["idUser"];
				$myResponse["userHasFacilityPreferences"]["nameFacility"] = $userHasFacilityPreferences["nameFacility"];


				array_push($response["prefFacilities"], $myResponse);
			}
			echo json_encode($response);
		}
	else{
		$response["error"] = TRUE;
		$response["error_msg"] = "No activity stored";
		echo json_encode($response);
	}
}

else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Error with URL !";
    echo json_encode($response);
}

?>