<?php 

require_once 'include/DB_BestTripFunctions.php';

$db = new DB_BestTripFunctions();

// Json response array
$response = array("error"=>false);

if (isset($_POST['idUser']) & isset($_POST['idAccommodation']) & isset($_POST['arrivalDateAccommodation'])) {

	//Receiving post params
	$idUser = $_POST['idUser'];
	$idAccommodation = $_POST['idAccommodation'];
	$arrivalDateAccommodation = $_POST['arrivalDateAccommodation'];

	// Create a new trip
		$tripContainsAccommodation = $db->storeTripContainsAccommodation($idUser, $idAccommodation, $arrivalDateAccommodation);
		if($tripContainsAccommodation){
			// trip stored succesfully
			$response["error"] = false;
			$response["tripContainsAccommodation"] = array();
			$response["tripContainsAccommodation"]["idTrip"] = $tripContainsAccommodation["idTrip"];
			$response["tripContainsAccommodation"]["idAccommodation"] = $tripContainsAccommodation["idAccommodation"];
			$response["tripContainsAccommodation"]["arrivalDate"] = $tripContainsAccommodation["arrivalDate"];
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
}
	else{
		$response["error"] = true;
		$response["error_msg"] = "Missing fields";
		echo json_encode($response);
	}

?>