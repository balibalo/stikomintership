<?php 

require_once 'include/DB_TripFunctions.php';
$db = new DB_TripFunctions();

// Json response array
$response = array("error"=>false);

if(isset($_POST["idUser"])){
	if(!$db->isTripEmpty()){
		// Getting all activities
		$trip = $db->fetchTripByUserId($_POST["idUser"]);
		if ($trip != false) {
			$response["error"] = false;
			$response["trip"] = array();
			$response["trip"]["idTrip"] = $trip["idTrip"];
			$response["trip"]["idUser"] = $trip["idUser"];
			$response["trip"]["arrivalDate"] = $trip["arrivalDate"];
			$response["trip"]["duration"] = $trip["duration"];
			$response["trip"]["numberOfTravellers"] = $trip["numberOfTravellers"];
			$response["trip"]["budget"] = $trip["budget"];
			echo json_encode($response);
		}
		else{
			$response["error"] = TRUE;
				$response["error_msg"] = "No trip for this idUser";
				echo json_encode($response);
		}
	}
	else{
		$response["error"] = TRUE;
		$response["error_msg"] = "No trip stored";
		echo json_encode($response);
	}
}
else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Error with URL !";
    echo json_encode($response);
}

?>