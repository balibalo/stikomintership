<?php 

require_once 'include/DB_TransportFunctions.php';

$db = new DB_TransportFunctions();

// Json response array
$response = array("error"=>false);

if (isset($_POST['idTransport'])) {

	//Receiving post params
	$idTransport = $_POST['idTransport'];

	// Create a new trip
		$transport = $db->deleteTransportAdmin($idTransport);
		if($transport){
			// trip stored succesfully
			$response["error"] = false;
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
	}
else{
	$response["error"] = true;
	$response["error_msg"] = "Missing fields";
	echo json_encode($response);
}


?>