<?php 

require_once 'include/DB_TripContainsAccommodationFunctions.php';
$db = new DB_TripContainsAccommodationFunctions();

// Json response array
$response = array("error"=>false);

if(isset($_POST["idTrip"]) & isset($_POST["arrivalDate"])){
	if(!$db->isTripContainsAccommodationEmpty()){
		// Getting all activities
		$trip = $db->getTripContainsAccoByIdTripDate($_POST["idTrip"], $_POST["arrivalDate"]);
		if ($trip != false) {
			$response["error"] = false;
			$response["trip"] = array();
			$response["trip"]["idTrip"] = $trip["idTrip"];
			$response["trip"]["idAccommodation"] = $trip["idAccommodation"];
			$response["trip"]["arrivalDate"] = $trip["arrivalDate"];
			echo json_encode($response);
		}
		else{
			$response["error"] = TRUE;
				$response["error_msg"] = "No trip for this idUser";
				echo json_encode($response);
		}
	}
	else{
		$response["error"] = TRUE;
		$response["error_msg"] = "No trip stored";
		echo json_encode($response);
	}
}
else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Error with URL !";
    echo json_encode($response);
}

?>