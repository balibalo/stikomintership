<?php 

require_once 'include/DB_TripContainsTransportFunctions.php';
$db = new DB_TripContainsTransportFunctions();

// Json response array
$response = array("error"=>false);

if(isset($_POST["id"])){
	if(!$db->isTripContainsTransportEmpty()){
		// Getting all activities
		if($_POST["id"]=="all"){
			$response["error"] = false;
			$response["tripContainsTransports"] = array();
		
			foreach($db->fetchAllTripContainsTransport() as $tripContainsTransport){
				$myResponse = array();
				$myResponse["tripContainsTransport"] = array();
				$myResponse["tripContainsTransport"]["idTrip"] = $tripContainsTransport["idTrip"];
				$myResponse["tripContainsTransport"]["idTransport"] = $tripContainsTransport["idTransport"];
				$myResponse["tripContainsTransport"]["dateTransport"] = $tripContainsTransport["dateTransport"];



				array_push($response["tripContainsTransports"], $myResponse);
			}
			echo json_encode($response);
		}
	}
	else{
		$response["error"] = TRUE;
		$response["error_msg"] = "No activity stored";
		echo json_encode($response);
	}
}
else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Error with URL !";
    echo json_encode($response);
}

?>