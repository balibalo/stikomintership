<?php

require_once 'include/DB_UserFunctions.php';
$db = new DB_UserFunctions();
 
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['username']) && isset($_POST['password'])) {
 
    // receiving the post params
   	$username = $_POST['username'];
    $password = $_POST['password'];
 
    // get the user by login
    $user = $db->getUserByLogin($username, $password);
 
    if ($user != false) {
        // user is found
        $response["error"] = FALSE;
        $response["user"] = array();
        $response["user"]["idUser"] = $user["idUser"];
        $response["user"]["username"] = $user["username"];
        echo json_encode($response);
    } else {
        // user is not found with the credentials
        $response["error"] = TRUE;
        $response["error_msg"] = "Incorrect username or password !";
        echo json_encode($response);
    }
}
else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "Missing field";
    echo json_encode($response);
}
?>