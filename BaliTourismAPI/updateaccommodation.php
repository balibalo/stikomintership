<?php 

require_once 'include/DB_TripContainsAccommodationFunctions.php';

$db = new DB_TripContainsAccommodationFunctions();

// Json response array
$response = array("error"=>false);

if (isset($_POST['idTrip']) & isset($_POST['arrivalDate']) & isset($_POST['newDate'])) {

	//Receiving post params
	$idTrip = $_POST['idTrip'];
	$newDate = $_POST['newDate'];
	$arrivalDate = $_POST['arrivalDate'];

	// Create a new trip
		$tripContainsAccommodation = $db->updateTripContainsAccommodation($idTrip, $arrivalDate, $newDate);
		if($tripContainsAccommodation){
			// trip stored succesfully
			$response["error"] = false;
			$response["trip"] = array();
			$response["tripContainsAccommodation"]["idTrip"] = $tripContainsAccommodation["idTrip"];
			$response["tripContainsAccommodation"]["idAccommodation"] = $tripContainsAccommodation["idAccommodation"];
			$response["tripContainsAccommodation"]["arrivalDate"] = $tripContainsAccommodation["arrivalDate"];
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
	}
else{
	$response["error"] = true;
	$response["error_msg"] = "Missing fields";
	echo json_encode($response);
}


?>
