<?php 

require_once 'include/DB_TransportFunctions.php';

$db = new DB_TransportFunctions();

// Json response array
$response = array("error"=>false);

if (isset($_POST['name']) & isset($_POST['price']) & isset($_POST['description']) & isset($_POST['picture']) & isset($_POST['numberOfPlaces']) & isset($_POST['needDrivingLicense'])) {

	//Receiving post params
	$name = $_POST['name'];
	$price = $_POST['price'];
	$description = $_POST['description'];
	$picture = $_POST['picture'];
	$numberOfPlaces = $_POST['numberOfPlaces'];
	$needDrivingLicense = $_POST['needDrivingLicense'];


	// Create a new trip
		$transport = $db->addTransportAdmin($name, $description, $picture, $numberOfPlaces, $needDrivingLicense, $price);
		if($transport){
			// trip stored succesfully
			$response["error"] = false;
			$response["addTransportAdmin"] = array();
			$response["transport"]["idTransport"] = $transport["idTransport"];
			$response["transport"]["name"] = $transport["name"];
			$response["transport"]["description"] = $transport["description"];
			$response["transport"]["picture"] = $transport["picture"];
			$response["transport"]["numberOfPlaces"] = $transport["numberOfPlaces"];
			$response["transport"]["needDrivingLicense"] = $transport["needDrivingLicense"];
			$response["transport"]["price"] = $transport["price"];
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
	}
else{
	$response["error"] = true;
	$response["error_msg"] = "Missing fields";
	echo json_encode($response);
}


?>
