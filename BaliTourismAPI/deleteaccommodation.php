<?php 

require_once 'include/DB_TripContainsAccommodationFunctions.php';

$db = new DB_TripContainsAccommodationFunctions();

// Json response array
$response = array("error"=>false);

if (isset($_POST['idTrip']) & isset($_POST['idAccommodation']) & isset($_POST['arrivalDate'])) {

	//Receiving post params
	$idTrip = $_POST['idTrip'];
	$arrivalDate = $_POST['arrivalDate'];
	$idAccommodation = $_POST['idAccommodation'];

	// Create a new trip
		$tripContainsAccommodation = $db->deleteTripContainsAccommodation($idTrip, $idAccommodation, $arrivalDate);
		if($tripContainsAccommodation){
			// trip stored succesfully
			$response["error"] = false;
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
	}
else{
	$response["error"] = true;
	$response["error_msg"] = "Missing fields";
	echo json_encode($response);
}


?>