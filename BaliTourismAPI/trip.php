<?php 

require_once 'include/DB_TripFunctions.php';
$db = new DB_TripFunctions();

// Json response array
$response = array("error"=>false);

if(isset($_POST["id"])){
	if(!$db->isTripEmpty()){
		// Getting all activities
		if($_POST["id"]=="all"){
			$response["error"] = false;
			$response["trips"] = array();
		
			foreach($db->fetchAllTrips() as $trip){
				$myResponse = array();
				$myResponse["trip"] = array();
				$myResponse["trip"]["idTrip"] = $trip["idTrip"];
				$myResponse["trip"]["idUser"] = $trip["idUser"];
				$myResponse["trip"]["arrivalDate"] = $trip["arrivalDate"];
				$myResponse["trip"]["duration"] = $trip["duration"];
				$myResponse["trip"]["numberOfTravellers"] = $trip["numberOfTravellers"];
				$myResponse["trip"]["budget"] = $trip["budget"];


				array_push($response["trips"], $myResponse);
			}
			echo json_encode($response);
		}
	}
	else{
		$response["error"] = TRUE;
		$response["error_msg"] = "No activity stored";
		echo json_encode($response);
	}
}
else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Error with URL !";
    echo json_encode($response);
}

?>