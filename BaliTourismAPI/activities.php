<?php 

require_once 'include/DB_ActivityFunctions.php';
$db = new DB_ActivityFunctions();

// Json response array
$response = array("error"=>false);

if(isset($_POST["id"])){
	if(!$db->isActivityEmpty()){
		// Getting all activities
		if($_POST["id"]=="all"){
			$response["error"] = false;
			$response["activities"] = array();
		
			foreach($db->fetchAllActivities() as $activity){
				$myResponse = array();
				$myResponse["activity"] = array();
				$myResponse["activity"]["idActivity"] = $activity["idActivity"];
				$myResponse["activity"]["name"] = $activity["name"];
				$myResponse["activity"]["longitude"] = $activity["longitude"];
				$myResponse["activity"]["latitude"] = $activity["latitude"];
				$myResponse["activity"]["phoneNumber"] = $activity["phoneNumber"];
				$myResponse["activity"]["website"] = $activity["website"];
				$myResponse["activity"]["price"] = $activity["price"];
				$myResponse["activity"]["description"] = $activity["description"];
				$myResponse["activity"]["picture"] = $activity["picture"];
				$myResponse["activity"]["duration"] = $activity["duration"];
				$myResponse["activity"]["openingHour"] = $activity["openingHour"];
				$myResponse["activity"]["openingMinute"] = $activity["openingMinute"];
				$myResponse["activity"]["closingHour"] = $activity["closingHour"];
				$myResponse["activity"]["closingMinute"] = $activity["closingMinute"];


				array_push($response["activities"], $myResponse);
			}
			echo json_encode($response);
		}
		// Getting activity which id is specified
		else{
			$activity = $db->fetchActivityById($_POST["id"]);
			if($activity != false){
				$response["error"] = false;
				$response["activity"] = array();
				$myResponse["activity"]["idActivity"] = $activity["idActivity"];
				$myResponse["activity"]["name"] = $activity["name"];
				$myResponse["activity"]["longitude"] = $activity["longitude"];
				$myResponse["activity"]["latitude"] = $activity["latitude"];
				$myResponse["activity"]["phoneNumber"] = $activity["phoneNumber"];
				$myResponse["activity"]["website"] = $activity["website"];
				$myResponse["activity"]["price"] = $activity["price"];
				$myResponse["activity"]["description"] = $activity["description"];
				$myResponse["activity"]["picture"] = $activity["picture"];
				$myResponse["activity"]["duration"] = $activity["duration"];
				$myResponse["activity"]["openingHour"] = $activity["openingHour"];
				$myResponse["activity"]["openingMinute"] = $activity["openingMinute"];
				$myResponse["activity"]["closingHour"] = $activity["closingHour"];
				$myResponse["activity"]["closingMinute"] = $activity["closingMinute"];

				echo json_encode($response);
			}
			else {
				// activity with this id not found
				$response["error"] = TRUE;
				$response["error_msg"] = "No activity for this id";
				echo json_encode($response);
			}
			
		}
	}
	else{
		$response["error"] = TRUE;
		$response["error_msg"] = "No activity stored";
		echo json_encode($response);
	}
}
else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Error with URL !";
    echo json_encode($response);
}

?>