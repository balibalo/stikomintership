<?php
 
require_once 'include/DB_AdminFunctions.php';
$db = new DB_AdminFunctions();
 
// json response array
$response = array("error" => FALSE);

if (isset($_POST["username"]) && isset($_POST["password"])) {
 
    // receiving the post params
    $username = $_POST['username'];
    $password = $_POST['password'];
 
    // check if a user with the same username already exists
    if ($db->isAdminExisting($username)) {
        // user already exists
        $response["error"] = TRUE;
        $response["error_msg"] = "User " . $username . " already exists.";
        echo json_encode($response);
    }
    else {
        // create a new user
        $admin = $db->storeAdmin($username, $password);
        if ($user) {
            // user stored successfully
            $response["error"] = FALSE;
            $response["admin"] = array();
            $response["admin"]["idAdmin"] = $admin["idAdmin"];
            $response["admin"]["login"] = $admin["login"];
            echo json_encode($response);
        }
        else {
            // user failed to store
            $response["error"] = TRUE;
            $response["error_msg"] = "Error while saving";
            echo json_encode($response);
        }
    }
}
else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Missing field";
    echo json_encode($response);
}
?>