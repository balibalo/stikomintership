<?php 

require_once 'include/DB_AccommodationFunctions.php';
$db = new DB_AccommodationFunctions();

// Json response array
$response = array("error"=>false);

if(isset($_POST["id"])){
	if(!$db->isAccommodationEmpty()){
		// Getting all construction sites
		if($_POST["id"]=="all"){
			$response["error"] = false;
			$response["accommodations"] = array();
		
			foreach($db->fetchAllAccommodations() as $accommodation){
				$myResponse = array();
				$myResponse["accommodation"] = array();
				$myResponse["accommodation"]["idAccommodation"] = $accommodation["idAccommodation"];
				$myResponse["accommodation"]["name"] = $accommodation["name"];
				$myResponse["accommodation"]["longitude"] = $accommodation["longitude"];
				$myResponse["accommodation"]["latitude"] = $accommodation["latitude"];
				$myResponse["accommodation"]["phoneNumber"] = $accommodation["phoneNumber"];
				$myResponse["accommodation"]["website"] = $accommodation["website"];
				$myResponse["accommodation"]["price"] = $accommodation["price"];
				$myResponse["accommodation"]["description"] = $accommodation["description"];
				$myResponse["accommodation"]["picture"] = $accommodation["picture"];
				$myResponse["accommodation"]["numberOfPlaces"] = $accommodation["numberOfPlaces"];

				array_push($response["accommodations"], $myResponse);
			}
			echo json_encode($response);
		}
		// Getting accommodation which id is specified
		else{
			$accommodation = $db->fetchAccommodationById($_POST["id"]);
			if($accommodation != false){
				$response["error"] = false;
				$response["accommodation"] = array();
				$response["accommodation"]["idAccommodation"] = $accommodation["idAccommodation"];
				$myResponse["accommodation"]["name"] = $accommodation["name"];
				$myResponse["accommodation"]["longitude"] = $accommodation["longitude"];
				$myResponse["accommodation"]["latitude"] = $accommodation["latitude"];
				$myResponse["accommodation"]["phoneNumber"] = $accommodation["phoneNumber"];
				$myResponse["accommodation"]["website"] = $accommodation["website"];
				$myResponse["accommodation"]["price"] = $accommodation["price"];
				$myResponse["accommodation"]["description"] = $accommodation["description"];
				$myResponse["accommodation"]["picture"] = $accommodation["picture"];
				$myResponse["accommodation"]["numberOfPlaces"] = $accommodation["numberOfPlaces"];
				echo json_encode($response);
			}
			else {
				// accommodation with this id not found
				$response["error"] = TRUE;
				$response["error_msg"] = "No accommodation for this id";
				echo json_encode($response);
			}
			
		}
	}
	else{
		$response["error"] = TRUE;
		$response["error_msg"] = "No accommodation stored";
		echo json_encode($response);
	}
}
else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Error with URL !";
    echo json_encode($response);
}

?>