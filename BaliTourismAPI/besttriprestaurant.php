<?php 

require_once 'include/DB_BestTripFunctions.php';

$db = new DB_BestTripFunctions();

// Json response array
$response = array("error"=>false);

if (isset($_POST['idUser']) & isset($_POST['idRestaurant']) & isset($_POST['arrivalDateRestaurant']) & isset($_POST['arrivalTimeRestaurant'])) {

	//Receiving post params
	$idUser = $_POST['idUser'];
	$idRestaurant = $_POST['idRestaurant'];
	$arrivalDateRestaurant = $_POST['arrivalDateRestaurant'];
	$arrivalTimeRestaurant = $_POST['arrivalTimeRestaurant'];


	// Create a new trip
		$tripContainsRestaurant = $db->storeTripContainsRestaurant($idUser, $idRestaurant, $arrivalDateRestaurant, $arrivalTimeRestaurant);
		if($tripContainsRestaurant){
			// trip stored succesfully
			$response["error"] = false;
			$response["tripContainsRestaurant"] = array();
			$response["tripContainsRestaurant"]["idTrip"] = $tripContainsRestaurant["idTrip"];
			$response["tripContainsRestaurant"]["idRestaurant"] = $tripContainsRestaurant["idRestaurant"];
			$response["tripContainsRestaurant"]["arrivalDate"] = $tripContainsRestaurant["arrivalDate"];
			$response["tripContainsRestaurant"]["arrivalTime"] = $tripContainsRestaurant["arrivalTime"];

			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
}
	else{
		$response["error"] = true;
		$response["error_msg"] = "Missing fields";
		echo json_encode($response);
	}

?>