<?php 

require_once 'include/DB_UserFunctions.php';
$db = new DB_UserFunctions();

// Json response array
$response = array("error"=>false);

if(isset($_POST["id"])){
	if(!$db->isUserEmpty()){
		// Getting all activities
		if($_POST["id"]=="all"){
			$response["error"] = false;
			$response["users"] = array();
		
			foreach($db->fetchAllUsers() as $user){
				$myResponse = array();
				$myResponse["user"] = array();
				$myResponse["user"]["idUser"] = $user["idUser"];
				$myResponse["user"]["idTrip"] = $user["idTrip"];
				$myResponse["user"]["username"] = $user["username"];
				$myResponse["user"]["encryptedPassword"] = $user["encryptedPassword"];
				$myResponse["user"]["emailAddress"] = $user["emailAddress"];
				$myResponse["user"]["drivingLicense"] = $user["drivingLicense"];
				$myResponse["user"]["timeSpentOutside"] = $user["timeSpentOutside"];



				array_push($response["users"], $myResponse);
			}
			echo json_encode($response);
		}
	}
	else{
		$response["error"] = TRUE;
		$response["error_msg"] = "No activity stored";
		echo json_encode($response);
	}
}
else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Error with URL !";
    echo json_encode($response);
}

?>