<?php

require_once 'include/DB_AdminFunctions.php';
$db = new DB_AdminFunctions();
 
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['login']) && isset($_POST['password'])) {
 
    // receiving the post params
   	$login = $_POST['login'];
    $password = $_POST['password'];
 
    // get the user by login
    $admin = $db->getAdminByLogin($login, $password);
 
    if ($admin != false) {
        // user is found
        $response["error"] = FALSE;
        $response["admin"] = array();
        $response["admin"]["idAdmin"] = $admin["idAdmin"];
        $response["admin"]["login"] = $admin["login"];
        echo json_encode($response);
    } else {
        // user is not found with the credentials
        $response["error"] = TRUE;
        $response["error_msg"] = "Incorrect username or password !";
        echo json_encode($response);
    }
}
else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "Missing field";
    echo json_encode($response);
}
?>