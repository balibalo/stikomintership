<?php 

require_once 'include/DB_RestaurantFunctions.php';
$db = new DB_RestaurantFunctions();

// Json response array
$response = array("error"=>false);

if(isset($_POST["id"])){
	if(!$db->isRestaurantEmpty()){
		// Getting all restaurants
		if($_POST["id"]=="all"){
			$response["error"] = false;
			$response["restaurants"] = array();
		
			foreach($db->fetchAllRestaurants() as $restaurant){
				$myResponse = array();
				$myResponse["restaurant"] = array();
				$myResponse["restaurant"]["idRestaurant"] = $restaurant["idRestaurant"];
				$myResponse["restaurant"]["name"] = $restaurant["name"];
				$myResponse["restaurant"]["longitude"] = $restaurant["longitude"];
				$myResponse["restaurant"]["latitude"] = $restaurant["latitude"];
				$myResponse["restaurant"]["phoneNumber"] = $restaurant["phoneNumber"];
				$myResponse["restaurant"]["website"] = $restaurant["website"];
				$myResponse["restaurant"]["price"] = $restaurant["price"];
				$myResponse["restaurant"]["description"] = $restaurant["description"];
				$myResponse["restaurant"]["picture"] = $restaurant["picture"];
				$myResponse["restaurant"]["openingHour"] = $restaurant["openingHour"];
				$myResponse["restaurant"]["openingMinute"] = $restaurant["openingMinute"];
				$myResponse["restaurant"]["closingHour"] = $restaurant["closingHour"];
				$myResponse["restaurant"]["closingMinute"] = $restaurant["closingMinute"];


				array_push($response["restaurants"], $myResponse);
			}
			echo json_encode($response);
		}
		// Getting restaurant which id is specified
		else{
			$restaurant = $db->fetchRestaurantById($_POST["id"]);
			if($restaurant != false){
				$response["error"] = false;
				$response["restaurant"] = array();
				$myResponse["restaurant"]["idRestaurant"] = $restaurant["idRestaurant"];
				$myResponse["restaurant"]["name"] = $restaurant["name"];
				$myResponse["restaurant"]["longitude"] = $restaurant["longitude"];
				$myResponse["restaurant"]["latitude"] = $restaurant["latitude"];
				$myResponse["restaurant"]["phoneNumber"] = $restaurant["phoneNumber"];
				$myResponse["restaurant"]["website"] = $restaurant["website"];
				$myResponse["restaurant"]["price"] = $restaurant["price"];
				$myResponse["restaurant"]["description"] = $restaurant["description"];
				$myResponse["restaurant"]["picture"] = $restaurant["picture"];
				$myResponse["restaurant"]["openingHour"] = $restaurant["openingHour"];
				$myResponse["restaurant"]["openingMinute"] = $restaurant["openingMinute"];
				$myResponse["restaurant"]["closingHour"] = $restaurant["closingHour"];
				$myResponse["restaurant"]["closingMinute"] = $restaurant["closingMinute"];

				echo json_encode($response);
			}
			else {
				// restaurant with this id not found
				$response["error"] = TRUE;
				$response["error_msg"] = "No restaurant for this id";
				echo json_encode($response);
			}
			
		}
	}
	else{
		$response["error"] = TRUE;
		$response["error_msg"] = "No restaurant stored";
		echo json_encode($response);
	}
}
else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Error with URL !";
    echo json_encode($response);
}

?>