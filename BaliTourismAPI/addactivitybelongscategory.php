<?php 

require_once 'include/DB_ActivityBelongsCategoryActivityFunctions.php';

$db = new DB_ActivityBelongsCategoryActivityFunctions();

// Json response array
$response = array("error"=>false);

if (isset($_POST['idActivity']) & isset($_POST['nameCategoryActivity'])) {

	//Receiving post params
	$idActivity = $_POST['idActivity'];
	$nameCategoryActivity = $_POST['nameCategoryActivity'];

	// Create a new trip
		$activity = $db->addActivityBelongsCategory($idActivity, $nameCategoryActivity);
		if($activity){
			// trip stored succesfully
			$response["error"] = false;
			$response["addActivityBelongsCategory"] = array();
			$response["activity"]["idActivity"] = $activity["idActivity"];
			$response["activity"]["nameCategoryActivity"] = $activity["nameCategoryActivity"];
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
	}
else{
	$response["error"] = true;
	$response["error_msg"] = "Missing fields";
	echo json_encode($response);
}


?>