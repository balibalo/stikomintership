<?php 

require_once 'include/DB_TripContainsActivityFunctions.php';
$db = new DB_TripContainsActivityFunctions();

// Json response array
$response = array("error"=>false);

if(isset($_POST["id"])){
	if(!$db->isTripContainsActivityEmpty()){
		// Getting all activities
		if($_POST["id"]=="all"){
			$response["error"] = false;
			$response["tripContainsActivities"] = array();
		
			foreach($db->fetchAllTripContainsActivity() as $tripContainsActivity){
				$myResponse = array();
				$myResponse["tripContainsActivity"] = array();
				$myResponse["tripContainsActivity"]["idTrip"] = $tripContainsActivity["idTrip"];
				$myResponse["tripContainsActivity"]["idActivity"] = $tripContainsActivity["idActivity"];
				$myResponse["tripContainsActivity"]["arrivalDate"] = $tripContainsActivity["arrivalDate"];
				$myResponse["tripContainsActivity"]["arrivalTime"] = $tripContainsActivity["arrivalTime"];



				array_push($response["tripContainsActivities"], $myResponse);
			}
			echo json_encode($response);
		}
	}
	else{
		$response["error"] = TRUE;
		$response["error_msg"] = "No activity stored";
		echo json_encode($response);
	}
}
else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Error with URL !";
    echo json_encode($response);
}

?>