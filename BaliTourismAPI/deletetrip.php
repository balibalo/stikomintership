<?php 

require_once 'include/DB_TripFunctions.php';

$db = new DB_TripFunctions();

// Json response array
$response = array("error"=>false);

if (isset($_POST['idTrip'])) {

	//Receiving post params
	$idTrip = $_POST['idTrip'];

	// Create a new trip
		$trip = $db->deleteTrip($idTrip);
		if(!$trip){
			// trip stored succesfully
			$response["error"] = false;
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
	}
else{
	$response["error"] = true;
	$response["error_msg"] = "Missing fields";
	echo json_encode($response);
}


?>