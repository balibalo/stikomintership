<?php 

require_once 'include/DB_TripFunctions.php';
require_once 'include/DB_UserFunctions.php';
require_once 'include/DB_PreferencesFunctions.php';

$db = new DB_TripFunctions();
$db_user_functions = new DB_UserFunctions();
$db_pref_functions = new DB_PreferencesFunctions();

// Json response array
$response = array("error"=>false);

if (isset($_POST['idUser']) & isset($_POST['arrivalDate']) & isset($_POST['duration']) & isset($_POST['numberOfTravellers']) & isset($_POST['budget']) & isset($_POST['timeSpentOutside']) & isset($_POST['drivingLicense'])) {

	//Receiving post params
	$idUser = $_POST['idUser'];
	$arrivalDate = $_POST['arrivalDate'];
	$duration = $_POST['duration'];
	$numberOfTravellers = $_POST['numberOfTravellers'];
	$budget = $_POST['budget'];
	$timeSpentOutside = $_POST['timeSpentOutside'];
	$drivingLicense = $_POST['drivingLicense'];
	$accommodationArray = $_POST['accommodationArray'];
	$facilityArray = $_POST['facilityArray'];
	$activityArray = $_POST['activityArray'];

	// Create a new trip
		$trip = $db->storeTrip($idUser, $arrivalDate, $duration, $numberOfTravellers, $budget);
		if($trip){
			// trip stored succesfully
			$response["error"] = false;
			$response["trip"] = array();
			$response["trip"]["idTrip"] = $trip["idTrip"];
			$response["trip"]["idUser"] = $trip["idUser"];
			$response["trip"]["arrivalDate"] = $trip["arrivalDate"];
			$response["trip"]["duration"] = $trip["duration"];			
			$response["trip"]["numberOfTravellers"] = $trip["numberOfTravellers"];
			$response["trip"]["budget"] = $trip["budget"];
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
	//update user table
		$updateUser = $db_user_functions->updateUser($idUser, $timeSpentOutside, $drivingLicense);
		if($updateUser){
			//update successful
			$response["error"] = false;
			$response["user"] = array();
			$response["user"]["timeSpentOutside"] = $updateUser["timeSpentOutside"];
			$response["user"]["drivingLicense"] = $updateUser["drivingLicense"];
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
		//insert into userHasCategoryAccommodationPreferences table
		$storeAccPref = $db_pref_functions->storeAccommodationPreferences($idUser, $accommodationArray);
		if($storeAccPref){
			//insertion successful
			$response["error"] = false;
			$response["userHasCategoryAccommodationPreferences"] = array();
			$response["userHasCategoryAccommodationPreferences"]["idUser"] = $storeAccPref["idUser"];
			$response["userHasCategoryAccommodationPreferences"]["accommodationArray"] = $storeAccPref["accommodationArray"];
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
		//insert into userHasFacilityPreferences table
		$storeFacPref = $db_pref_functions->storeFacilityPreferences($idUser, $facilityArray);
		if($storeFacPref){
			//insertion successful
			$response["error"] = false;
			$response["userHasFacilityPreferences"] = array();
			$response["userHasFacilityPreferences"]["idUser"] = $storeFacPref["idUser"];
			$response["userHasFacilityPreferences"]["facilityArray"] = $storeFacPref["facilityArray"];
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
		//insert into userHasCategoryActivityPreferences table
		$storeActPref = $db_pref_functions->storeActivityPreferences($idUser, $activityArray);
		if($storeActPref){
			//insertion successful
			$response["error"] = false;
			$response["userHasCategoryActivityPreferences"] = array();
			$response["userHasCategoryActivityPreferences"]["idUser"] = $storeActPref["idUser"];
			$response["userHasCategoryActivityPreferences"]["activityArray"] = $storeActPref["activityArray"];
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
		/*
		//update idTrip in user table
		$idTrip = $db_user_functions->getIdTripByUser($idUser);
		if($idTrip != false){
			$updateIdTrip = $db_user_functions->updateUserIdTrip($idUser, $idTrip);
		if($updateIdTrip){
			//update successful
			$response["error"] = false;
			$response["user"] = array();
			$response["user"]["idTrip"] = $updateIdTrip["idTrip"];
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
		*/
		
}
	else{
		$response["error"] = true;
		$response["error_msg"] = "Missing fields";
		echo json_encode($response);
	}

?>