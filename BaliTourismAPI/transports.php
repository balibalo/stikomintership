<?php 

require_once 'include/DB_TransportFunctions.php';
$db = new DB_TransportFunctions();

// Json response array
$response = array("error"=>false);

if(isset($_POST["id"])){
	if(!$db->isTransportEmpty()){
		// Getting all transports
		if($_POST["id"]=="all"){
			$response["error"] = false;
			$response["transports"] = array();
		
			foreach($db->fetchAllTransports() as $transport){
				$myResponse = array();
				$myResponse["transport"] = array();
				$myResponse["transport"]["idTransport"] = $transport["idTransport"];
				$myResponse["transport"]["name"] = $transport["name"];
				$myResponse["transport"]["description"] = $transport["description"];
				$myResponse["transport"]["picture"] = $transport["picture"];
				$myResponse["transport"]["numberOfPlaces"] = $transport["numberOfPlaces"];
				$myResponse["transport"]["needDrivingLicense"] = $transport["needDrivingLicense"];
				$myResponse["transport"]["price"] = $transport["price"];

				array_push($response["transports"], $myResponse);
			}
			echo json_encode($response);
		}
		// Getting transport which id is specified
		else{
			$transport = $db->fetchTransportById($_POST["id"]);
			if($transport != false){
				$response["error"] = false;
				$response["transport"] = array();
				$myResponse["transport"]["idTransport"] = $transport["idTransport"];
				$myResponse["transport"]["name"] = $transport["name"];
				$myResponse["transport"]["description"] = $transport["description"];
				$myResponse["transport"]["picture"] = $transport["picture"];
				$myResponse["transport"]["numberOfPlaces"] = $transport["numberOfPlaces"];
				$myResponse["transport"]["needDrivingLicense"] = $transport["needDrivingLicense"];
				$myResponse["transport"]["price"] = $transport["price"];

				echo json_encode($response);
			}
			else {
				// transport with this id not found
				$response["error"] = TRUE;
				$response["error_msg"] = "No transport for this id";
				echo json_encode($response);
			}
			
		}
	}
	else{
		$response["error"] = TRUE;
		$response["error_msg"] = "No transport stored";
		echo json_encode($response);
	}
}
else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Error with URL !";
    echo json_encode($response);
}

?>