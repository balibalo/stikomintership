<?php 

require_once 'include/DB_AccommodationBelongsCategoryAccommodationFunctions.php';

$db = new DB_AccommodationBelongsCategoryAccommodationFunctions();

// Json response array
$response = array("error"=>false);

if (isset($_POST['idAccommodation']) & isset($_POST['nameCategoryAccommodation'])) {

	//Receiving post params
	$idAccommodation = $_POST['idAccommodation'];
	$nameCategoryAccommodation = $_POST['nameCategoryAccommodation'];

	// Create a new trip
		$accommodation = $db->addAccommodationBelongsCategory($idAccommodation, $nameCategoryAccommodation);
		if($accommodation){
			// trip stored succesfully
			$response["error"] = false;
			$response["addAccommodationBelongsCategory"] = array();
			$response["accommodation"]["idAccommodation"] = $accommodation["idAccommodation"];
			$response["accommodation"]["nameCategoryAccommodation"] = $accommodation["nameCategoryAccommodation"];
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
	}
else{
	$response["error"] = true;
	$response["error_msg"] = "Missing fields";
	echo json_encode($response);
}


?>
