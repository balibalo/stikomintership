<?php 

require_once 'include/DB_UserFunctions.php';
$db = new DB_UserFunctions();

// Json response array
$response = array("error"=>false);

if(isset($_POST["idUser"])){
	$user = $db->fetchUserById($_POST["idUser"]);
			if($user != false){
				$response["error"] = false;
				$response["user"] = array();
				$response["user"]["idUser"] = $user["idUser"];
				$response["user"]["idTrip"] = $user["idTrip"];

				echo json_encode($response);
			}
			else {
				// transport with this id not found
				$response["error"] = TRUE;
				$response["error_msg"] = "No user for this id";
				echo json_encode($response);
			}
}
else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Error with URL !";
    echo json_encode($response);
}

?>