<?php 

require_once 'include/DB_ActivityBelongsCategoryActivityFunctions.php';

$db = new DB_ActivityBelongsCategoryActivityFunctions();

// Json response array
$response = array("error"=>false);

if (isset($_POST['idActivity'])) {

	//Receiving post params
	$idActivity = $_POST['idActivity'];

	// Create a new trip
		$activity = $db->deleteActivityBelongsCategory($idActivity);
		if($activity){
			// trip stored succesfully
			$response["error"] = false;
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
	}
else{
	$response["error"] = true;
	$response["error_msg"] = "Missing fields";
	echo json_encode($response);
}


?>