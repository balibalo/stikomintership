<?php 

require_once 'include/DB_AccommodationFunctions.php';

$db = new DB_AccommodationFunctions();

// Json response array
$response = array("error"=>false);

if (isset($_POST['idAccommodation'])) {

	//Receiving post params
	$idAccommodation = $_POST['idAccommodation'];

	// Create a new trip
		$accommodation = $db->deleteAccommodationAdmin($idAccommodation);
		if($accommodation){
			// trip stored succesfully
			$response["error"] = false;
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
	}
else{
	$response["error"] = true;
	$response["error_msg"] = "Missing fields";
	echo json_encode($response);
}


?>