<?php 

require_once 'include/DB_ActivityBelongsCategoryActivityFunctions.php';
$db = new DB_ActivityBelongsCategoryActivityFunctions();

// Json response array
$response = array("error"=>false);

if(isset($_POST["id"])){
	if(!$db->isActivityBelongsCategoryActivityEmpty()){
		// Getting all activities
		if($_POST["id"]=="all"){
			$response["error"] = false;
			$response["activitiesBelongsCategoryActivity"] = array();
		
			foreach($db->fetchAllActivityBelongsCategoryActivity() as $activityBelongsCategoryActivity){
				$myResponse = array();
				$myResponse["activityBelongsCategoryActivity"] = array();
				$myResponse["activityBelongsCategoryActivity"]["idActivity"] = $activityBelongsCategoryActivity["idActivity"];
				$myResponse["activityBelongsCategoryActivity"]["nameCategoryActivity"] = $activityBelongsCategoryActivity["nameCategoryActivity"];


				array_push($response["activitiesBelongsCategoryActivity"], $myResponse);
			}
			echo json_encode($response);
		}
	}
	else{
		$response["error"] = TRUE;
		$response["error_msg"] = "No activity stored";
		echo json_encode($response);
	}
}
else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Error with URL !";
    echo json_encode($response);
}




/*
// Json response array
$response = array("error"=>false);

if(isset($_POST["idActivity"])){
	if(!$db->isActivityBelongsCategoryActivityEmpty()){
		// Getting all construction sites
			$response["error"] = false;
			$response["activitiesBelongsCategoryActivity"] = array();
		
			foreach($db->fetchAllCategoryActivityByIdAcitivity($_POST["idActivity"]) as $activityBelongsCategoryActivity){
				$myResponse = array();
				$myResponse["activityBelongsCategoryActivity"] = array();
				$myResponse["activityBelongsCategoryActivity"]["nameCategoryActivity"] = $activityBelongsCategoryActivity["nameCategoryActivity"];

				array_push($response["activitiesBelongsCategoryActivity"], $myResponse);
			}
			echo json_encode($response);
		}
		else{
		$response["error"] = TRUE;
		$response["error_msg"] = "No activityBelongsCategoryActivity stored";
		echo json_encode($response);
		}
	}
	else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Error with URL !";
    echo json_encode($response);
}
*/
?>