<?php 

require_once 'include/DB_UserFunctions.php';

$db = new DB_UserFunctions();

// Json response array
$response = array("error"=>false);

//Receiving post params
	$idUser = $_POST['idUser'];

//update idTrip in user table
		$updateIdTrip = $db->updateUserIdTrip($idUser);
		if($updateIdTrip){
			//update successful
			$response["error"] = false;
			$response["user"] = array();
			$response["user"]["idTrip"] = $updateIdTrip["idTrip"];
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
?>