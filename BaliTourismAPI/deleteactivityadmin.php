<?php 

require_once 'include/DB_ActivityFunctions.php';

$db = new DB_ActivityFunctions();

// Json response array
$response = array("error"=>false);

if (isset($_POST['idActivity'])) {

	//Receiving post params
	$idActivity = $_POST['idActivity'];

	// Create a new trip
		$activity = $db->deleteActivityAdmin($idActivity);
		if($activity){
			// trip stored succesfully
			$response["error"] = false;
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
	}
else{
	$response["error"] = true;
	$response["error_msg"] = "Missing fields";
	echo json_encode($response);
}


?>