<?php 

require_once 'include/DB_RestaurantFunctions.php';

$db = new DB_RestaurantFunctions();

// Json response array
$response = array("error"=>false);

if (isset($_POST['idRestaurant'])) {

	//Receiving post params
	$idRestaurant = $_POST['idRestaurant'];

	// Create a new trip
		$restaurant = $db->deleteRestaurantAdmin($idRestaurant);
		if($restaurant){
			// trip stored succesfully
			$response["error"] = false;
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
	}
else{
	$response["error"] = true;
	$response["error_msg"] = "Missing fields";
	echo json_encode($response);
}


?>