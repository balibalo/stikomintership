<?php 

require_once 'include/DB_AccommodationFunctions.php';

$db = new DB_AccommodationFunctions();

// Json response array
$response = array("error"=>false);

if (isset($_POST['name']) & isset($_POST['longitude']) & isset($_POST['latitude']) & isset($_POST['phoneNumber']) & isset($_POST['website']) & isset($_POST['price']) & isset($_POST['description']) & isset($_POST['picture']) & isset($_POST['numberOfPlaces'])) {

	//Receiving post params
	$name = $_POST['name'];
	$longitude = $_POST['longitude'];
	$latitude = $_POST['latitude'];
	$phoneNumber = $_POST['phoneNumber'];
	$website = $_POST['website'];
	$price = $_POST['price'];
	$description = $_POST['description'];
	$picture = $_POST['picture'];
	$numberOfPlaces = $_POST['numberOfPlaces'];

	// Create a new trip
		$accommodation = $db->addAccommodationAdmin($name, $longitude, $latitude, $phoneNumber, $website, $price, $description, $picture, $numberOfPlaces);
		if($accommodation){
			// trip stored succesfully
			$response["error"] = false;
			$response["addAccommodationAdmin"] = array();
			$response["accommodation"]["idAccommodation"] = $accommodation["idAccommodation"];
			$response["accommodation"]["name"] = $accommodation["name"];
			$response["accommodation"]["longitude"] = $accommodation["longitude"];
			$response["accommodation"]["latitude"] = $accommodation["latitude"];
			$response["accommodation"]["phoneNumber"] = $accommodation["phoneNumber"];
			$response["accommodation"]["website"] = $accommodation["website"];
			$response["accommodation"]["price"] = $accommodation["price"];
			$response["accommodation"]["description"] = $accommodation["description"];
			$response["accommodation"]["picture"] = $accommodation["picture"];
			$response["accommodation"]["numberOfPlaces"] = $accommodation["numberOfPlaces"];
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
	}
else{
	$response["error"] = true;
	$response["error_msg"] = "Missing fields";
	echo json_encode($response);
}


?>
