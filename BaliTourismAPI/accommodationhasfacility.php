<?php 

require_once 'include/DB_AccommodationHasFacilityFunctions.php';
$db = new DB_AccommodationHasFacilityFunctions();

// Json response array
$response = array("error"=>false);

if(isset($_POST["id"])){
	if(!$db->isAccommodationHasFacilityEmpty()){
		// Getting all activities
		if($_POST["id"]=="all"){
			$response["error"] = false;
			$response["accommodationsHasFacility"] = array();
		
			foreach($db->fetchAllAccommodationHasFacility() as $accommodationHasFacility){
				$myResponse = array();
				$myResponse["accommodationHasFacility"] = array();
				$myResponse["accommodationHasFacility"]["idAccommodation"] = $accommodationHasFacility["idAccommodation"];
				$myResponse["accommodationHasFacility"]["nameFacility"] = $accommodationHasFacility["nameFacility"];


				array_push($response["accommodationsHasFacility"], $myResponse);
			}
			echo json_encode($response);
		}
	}
	else{
		$response["error"] = TRUE;
		$response["error_msg"] = "No activity stored";
		echo json_encode($response);
	}
}
else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Error with URL !";
    echo json_encode($response);
}

?>