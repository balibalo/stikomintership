<?php 

require_once 'include/DB_TripContainsRestaurantFunctions.php';
$db = new DB_TripContainsRestaurantFunctions();

// Json response array
$response = array("error"=>false);

if(isset($_POST["id"])){
	if(!$db->isTripContainsRestaurantEmpty()){
		// Getting all activities
		if($_POST["id"]=="all"){
			$response["error"] = false;
			$response["tripContainsRestaurants"] = array();
		
			foreach($db->fetchAllTripContainsRestaurant() as $tripContainsRestaurant){
				$myResponse = array();
				$myResponse["tripContainsRestaurant"] = array();
				$myResponse["tripContainsRestaurant"]["idTrip"] = $tripContainsRestaurant["idTrip"];
				$myResponse["tripContainsRestaurant"]["idRestaurant"] = $tripContainsRestaurant["idRestaurant"];
				$myResponse["tripContainsRestaurant"]["arrivalDate"] = $tripContainsRestaurant["arrivalDate"];
				$myResponse["tripContainsRestaurant"]["arrivalTime"] = $tripContainsRestaurant["arrivalTime"];



				array_push($response["tripContainsRestaurants"], $myResponse);
			}
			echo json_encode($response);
		}
	}
	else{
		$response["error"] = TRUE;
		$response["error_msg"] = "No activity stored";
		echo json_encode($response);
	}
}
else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Error with URL !";
    echo json_encode($response);
}

?>