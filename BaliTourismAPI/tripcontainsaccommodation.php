<?php 

require_once 'include/DB_TripContainsAccommodationFunctions.php';
$db = new DB_TripContainsAccommodationFunctions();

// Json response array
$response = array("error"=>false);

if(isset($_POST["id"])){
	if(!$db->isTripContainsAccommodationEmpty()){
		// Getting all activities
		if($_POST["id"]=="all"){
			$response["error"] = false;
			$response["tripContainsAccommodations"] = array();
		
			foreach($db->fetchAllTripContainsAccommodation() as $tripContainsAccommodation){
				$myResponse = array();
				$myResponse["tripContainsAccommodation"] = array();
				$myResponse["tripContainsAccommodation"]["idTrip"] = $tripContainsAccommodation["idTrip"];
				$myResponse["tripContainsAccommodation"]["idAccommodation"] = $tripContainsAccommodation["idAccommodation"];
				$myResponse["tripContainsAccommodation"]["arrivalDate"] = $tripContainsAccommodation["arrivalDate"];



				array_push($response["tripContainsAccommodations"], $myResponse);
			}
			echo json_encode($response);
		}
	}
	else{
		$response["error"] = TRUE;
		$response["error_msg"] = "No activity stored";
		echo json_encode($response);
	}
}
else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Error with URL !";
    echo json_encode($response);
}

?>