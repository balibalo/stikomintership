<?php 

require_once 'include/DB_BestTripFunctions.php';

$db = new DB_BestTripFunctions();

// Json response array
$response = array("error"=>false);

if (isset($_POST['idUser']) & isset($_POST['idActivity']) & isset($_POST['arrivalDateActivity']) & isset($_POST['arrivalTimeActivity'])) {

	//Receiving post params
	$idUser = $_POST['idUser'];
	$idActivity = $_POST['idActivity'];
	$arrivalDateActivity = $_POST['arrivalDateActivity'];
	$arrivalTimeActivity = $_POST['arrivalTimeActivity'];


	// Create a new trip
		$tripContainsActivity = $db->storeTripContainsActivity($idUser, $idActivity, $arrivalDateActivity, $arrivalTimeActivity);
		if($tripContainsActivity){
			// trip stored succesfully
			$response["error"] = false;
			$response["tripContainsActivity"] = array();
			$response["tripContainsActivity"]["idTrip"] = $tripContainsActivity["idTrip"];
			$response["tripContainsActivity"]["idActivity"] = $tripContainsActivity["idActivity"];
			$response["tripContainsActivity"]["arrivalDate"] = $tripContainsActivity["arrivalDate"];
			$response["tripContainsActivity"]["arrivalTime"] = $tripContainsActivity["arrivalTime"];

			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
}
	else{
		$response["error"] = true;
		$response["error_msg"] = "Missing fields";
		echo json_encode($response);
	}

?>