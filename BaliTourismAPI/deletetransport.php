<?php 

require_once 'include/DB_TripContainsTransportFunctions.php';

$db = new DB_TripContainsTransportFunctions();

// Json response array
$response = array("error"=>false);

if (isset($_POST['idTrip']) & isset($_POST['idTransport']) & isset($_POST['dateTransport'])) {

	//Receiving post params
	$idTrip = $_POST['idTrip'];
	$idTransport = $_POST['idTransport'];
	$dateTransport = $_POST['dateTransport'];

	// Create a new trip
		$tripContainsTransport = $db->deleteTripContainsTransport($idTrip, $idTransport, $dateTransport);
		if($tripContainsTransport){
			// trip stored succesfully
			$response["error"] = false;
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
	}
else{
	$response["error"] = true;
	$response["error_msg"] = "Missing fields";
	echo json_encode($response);
}


?>