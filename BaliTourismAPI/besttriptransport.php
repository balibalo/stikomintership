<?php 

require_once 'include/DB_BestTripFunctions.php';

$db = new DB_BestTripFunctions();

// Json response array
$response = array("error"=>false);

if (isset($_POST['idUser']) & isset($_POST['idTransport']) & isset($_POST['dateTransport'])) {

	//Receiving post params
	$idUser = $_POST['idUser'];
	$idTransport = $_POST['idTransport'];
	$dateTransport = $_POST['dateTransport'];

	// Create a new trip
		$tripContainsTransport = $db->storeTripContainsTransport($idUser, $idTransport, $dateTransport);
		if($tripContainsTransport){
			// trip stored succesfully
			$response["error"] = false;
			$response["tripContainsTransport"] = array();
			$response["tripContainsTransport"]["idTrip"] = $tripContainsTransport["idTrip"];
			$response["tripContainsTransport"]["idTransport"] = $tripContainsTransport["idTransport"];
			$response["tripContainsTransport"]["dateTransport"] = $tripContainsTransport["dateTransport"];

			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
}
	else{
		$response["error"] = true;
		$response["error_msg"] = "Missing fields";
		echo json_encode($response);
	}

?>