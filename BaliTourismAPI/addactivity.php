<?php 

require_once 'include/DB_TripContainsActivityFunctions.php';

$db = new DB_TripContainsActivityFunctions();

// Json response array
$response = array("error"=>false);

if (isset($_POST['idTrip']) & isset($_POST['idActivity']) & isset($_POST['arrivalDate']) & isset($_POST['arrivalTime'])) {

	//Receiving post params
	$idTrip = $_POST['idTrip'];
	$idActivity = $_POST['idActivity'];
	$arrivalDate = $_POST['arrivalDate'];
	$arrivalTime = $_POST['arrivalTime'];

	// Create a new trip
		$tripContainsActivity = $db->addTripContainsActivity($idTrip, $idActivity, $arrivalDate, $arrivalTime);
		if($tripContainsActivity){
			// trip stored succesfully
			$response["error"] = false;
			$response["trip"] = array();
			$response["tripContainsActivity"]["idTrip"] = $tripContainsActivity["idTrip"];
			$response["tripContainsActivity"]["idActivity"] = $tripContainsActivity["idActivity"];
			$response["tripContainsActivity"]["arrivalDate"] = $tripContainsActivity["arrivalDate"];
			$response["tripContainsActivity"]["arrivalTime"] = $tripContainsActivity["arrivalTime"];
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
	}
else{
	$response["error"] = true;
	$response["error_msg"] = "Missing fields";
	echo json_encode($response);
}


?>
