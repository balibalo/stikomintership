<?php 

require_once 'include/DB_ActivityFunctions.php';

$db = new DB_ActivityFunctions();

// Json response array
$response = array("error"=>false);

if (isset($_POST['name']) & isset($_POST['longitude']) & isset($_POST['latitude']) & isset($_POST['phoneNumber']) & isset($_POST['website']) & isset($_POST['price']) & isset($_POST['description']) & isset($_POST['picture']) & isset($_POST['duration']) & isset($_POST['openingHour']) & isset($_POST['openingMinute']) & isset($_POST['closingHour']) & isset($_POST['closingMinute'])) {

	//Receiving post params
	$name = $_POST['name'];
	$longitude = $_POST['longitude'];
	$latitude = $_POST['latitude'];
	$phoneNumber = $_POST['phoneNumber'];
	$website = $_POST['website'];
	$price = $_POST['price'];
	$description = $_POST['description'];
	$picture = $_POST['picture'];
	$duration = $_POST['duration'];
	$openingHour = $_POST['openingHour'];
	$openingMinute = $_POST['openingMinute'];
	$closingHour = $_POST['closingHour'];
	$closingMinute = $_POST['closingMinute'];

	// Create a new trip
		$activity = $db->addActivityAdmin($name, $longitude, $latitude, $phoneNumber, $website, $price, $description, $picture, $duration, $openingHour, $openingMinute, $closingHour, $closingMinute);
		if($activity){
			// trip stored succesfully
			$response["error"] = false;
			$response["addActivityAdmin"] = array();
			$response["activity"]["idActivity"] = $activity["idActivity"];
			$response["activity"]["name"] = $activity["name"];
			$response["activity"]["longitude"] = $activity["longitude"];
			$response["activity"]["latitude"] = $activity["latitude"];
			$response["activity"]["phoneNumber"] = $activity["phoneNumber"];
			$response["activity"]["website"] = $activity["website"];
			$response["activity"]["price"] = $activity["price"];
			$response["activity"]["description"] = $activity["description"];
			$response["activity"]["picture"] = $activity["picture"];
			$response["activity"]["duration"] = $activity["duration"];
			$response["activity"]["openingHour"] = $activity["openingHour"];
			$response["activity"]["openingMinute"] = $activity["openingMinute"];
			$response["activity"]["closingHour"] = $activity["closingHour"];
			$response["activity"]["closingMinute"] = $activity["closingMinute"];
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
	}
else{
	$response["error"] = true;
	$response["error_msg"] = "Missing fields";
	echo json_encode($response);
}


?>
