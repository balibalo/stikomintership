<?php 

require_once 'include/DB_RestaurantFunctions.php';

$db = new DB_RestaurantFunctions();

// Json response array
$response = array("error"=>false);

if (isset($_POST['idRestaurant']) & isset($_POST['name']) & isset($_POST['longitude']) & isset($_POST['latitude']) & isset($_POST['phoneNumber']) & isset($_POST['website']) & isset($_POST['price']) & isset($_POST['description']) & isset($_POST['picture']) & isset($_POST['openingHour']) & isset($_POST['openingMinute']) & isset($_POST['closingHour']) & isset($_POST['closingMinute'])) {

	//Receiving post params
	$idRestaurant = $_POST['idRestaurant'];
	$name = $_POST['name'];
	$longitude = $_POST['longitude'];
	$latitude = $_POST['latitude'];
	$phoneNumber = $_POST['phoneNumber'];
	$website = $_POST['website'];
	$price = $_POST['price'];
	$description = $_POST['description'];
	$picture = $_POST['picture'];
	$openingHour = $_POST['openingHour'];
	$openingMinute = $_POST['openingMinute'];
	$closingHour = $_POST['closingHour'];
	$closingMinute = $_POST['closingMinute'];

	// Create a new trip
		$restaurant = $db->updateRestaurantAdmin($idRestaurant, $name, $longitude, $latitude, $phoneNumber, $website, $price, $description, $picture, $openingHour, $openingMinute, $closingHour, $closingMinute);
		if($restaurant){
			// trip stored succesfully
			$response["error"] = false;
			$response["updateRestaurantAdmin"] = array();
			$response["restaurant"]["idRestaurant"] = $restaurant["idRestaurant"];
			$response["restaurant"]["name"] = $restaurant["name"];
			$response["restaurant"]["longitude"] = $restaurant["longitude"];
			$response["restaurant"]["latitude"] = $restaurant["latitude"];
			$response["restaurant"]["phoneNumber"] = $restaurant["phoneNumber"];
			$response["restaurant"]["website"] = $restaurant["website"];
			$response["restaurant"]["price"] = $restaurant["price"];
			$response["restaurant"]["description"] = $restaurant["description"];
			$response["restaurant"]["picture"] = $restaurant["picture"];
			$response["restaurant"]["openingHour"] = $restaurant["openingHour"];
			$response["restaurant"]["openingMinute"] = $restaurant["openingMinute"];
			$response["restaurant"]["closingHour"] = $restaurant["closingHour"];
			$response["restaurant"]["closingMinute"] = $restaurant["closingMinute"];
			echo json_encode($response);
		}
		else{
			$response["error"] = true;
			$response["error_msg"] = "Thank you !";
			echo json_encode($response);
		}
	}
else{
	$response["error"] = true;
	$response["error_msg"] = "Missing fields";
	echo json_encode($response);
}


?>
